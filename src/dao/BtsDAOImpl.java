package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ConexionBD;
import vista.BTS1;

/**
 *
 * @author PRR1TL
 */
public class BtsDAOImpl extends ConexionBD{
    
    //CONEXION
    private PreparedStatement ps;
    private ResultSet rs;
    
    public static String linea = BTS1.linea;
    
    String INSERTAR_BTS = "INSERT INTO bts "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    String CONSULTA_BTS = "SELECT * FROM bts WHERE linea = ? AND fecha = ? ";
    String DELETE_BTS = "DELETE FROM bts WHERE linea = ? and fecha = ?";
    String UPDATE_BTS = "UPDATE FROM "; //AQUI SE PODRIA PRIMERO ELIMINAR Y LUEGO HACER INSERT, SOLO COMO OPINION   
    
    public void InsertBTS(String linea, String fecha, String dia, String mes, String anio, String familia, String planeador, String supervisor, String semana,
            Object noParte, Object volumen, int secuencia, 
            Object noParteCambio, Object volCambio, int secCambio, Object descCambio,
            Object noParteCambioNP, Object volCambioNP, int secCambioNp, Object descCambioNP) throws Exception{
        
        try {
            this.conectar();  
            
            ps = this.conexion.prepareStatement(INSERTAR_BTS);
            ps.setString(1, linea);//linea)
            ps.setString(2, fecha);//anio
            ps.setString(3, dia);//dia
            ps.setString(4, mes);//mes
            ps.setString(5, anio);//anio
            ps.setString(6, familia);//familia
            ps.setString(7, planeador); //planeador
            ps.setString(8, supervisor); //supervisor
            ps.setString(9, semana); // semana
            ps.setObject(10, noParte);//noparte
            ps.setObject(11, volumen); //volumen
            ps.setInt(12, secCambio);//secuencia
            ps.setObject(13, noParteCambio);//noParteCambio
            ps.setObject(14, volCambio);//volCambio
            ps.setInt(15, secCambio);//secCambio
            ps.setObject(16, descCambio);//descCambio
            //secProduccionReal
            //volProducido
            //volCalificacion
            //secCalificacion
            //evalLote
            ps.setObject(22, noParteCambioNP);//noParteCambioNP
            ps.setObject(23, volCambioNP);//volCambioNP
            ps.setInt(24, secCambioNp);//secCambioNP
            ps.setObject(25, descCambioNP);//descCambioNP
            
            //System.out.println(linea+anio+mes+dia+trek+programado+tecnicas+organizacionales+calidad+scrap+"\n");

            ps.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error en INSERT " + e);
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void BorrarBTS(){
        
    }
    
    
}
