package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import modelo.BitacoraDAO;
import modelo.ConexionBD;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class BitacoraDAOImpl extends ConexionBD implements BitacoraDAO {

    int cDTC = 0;
    
    private ArrayList listaRegistros;
    private PreparedStatement ps, ps2, ps3, ps4;
    private ResultSet rs, rs2, rs3, rs4;
    
    private final String INSERTA_REGISTRO_HOURLY = "INSERT INTO HourlyCount (Fecha, NombreLinea, Hora, CantProducida, NoParteTC, Scrap, CambioDuracion, TecnicaDuracion, OrganizacionalDuracion, TiempoPDuracion, OperacionX, ProblemaX, OperacionY, ProblemaY) "
            + "VALUES(CAST(CONVERT(DATETIME, ?, 103) as DATETIME), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    //private final String BORRA_REGISTRO_HOURLY = "DELETE FROM HourlyCount WHERE NombreLinea LIKE ?";
    private final String BORRA_REGISTRO_HOURLY = "DELETE FROM OEEPercentHour WHERE hora BETWEEN ? AND ? AND linea LIKE ? AND fecha = CONVERT(DATETIME, ?, 103)";
    private final String INSERTA_REGISTRO = "INSERT INTO Bitacora (dia, mes, anio, linea, fecha, hora, tiempoIni, tiempoFin, duracion, tema, operacion, area, problema, cliente, noParte, cantPzas, noParteCambio, scrap, detalleMaterial, tiempoCiclo, iReg) "
            + "VALUES(DATEPART(DAY, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)), DATEPART(MONTH, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)), DATEPART(YEAR, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)), ?, CAST(CONVERT(DATETIME, ?, 103) as DATETIME), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";   
    private final String BORRA_REGISTRO_TIEMPO = "DELETE FROM Bitacora WHERE hora BETWEEN ? AND ? AND linea LIKE ? AND fecha = CONVERT(DATETIME, ?, 103)";
    private final String BORRA_REGISTRO_FILA = "DELETE FROM Bitacora WHERE linea LIKE ? AND fecha = CONVERT(DATETIME, ?, 103) AND hora = ? AND tiempoIni = ? AND tiempoFin = ?";
    private final String BORRA_REGISTRO_FECHA = "DELETE FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea LIKE ?";
    private final String CONSULTA_FECHA = "SELECT hora, tiempoIni, tiempoFin FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND hora = ? AND tiempoIni = ? AND tiempoFin = ? ORDER BY hora";
    private final String LISTAR_REGISTROS_FECHA = "SELECT linea, format(fecha, 'dd/MM/yyyy'), hora, tiempoIni, tiempoFin, duracion, tema, operacion, area, problema, cliente, noParte, cantPzas, noParteCambio, scrap, detalleMaterial, tiempoCiclo, iReg"
            +" FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea like ? ORDER BY hora";
    private final String LISTAR_REGISTROS_TURNO = "SELECT linea, format(fecha, 'dd/MM/yyyy'), hora, tiempoIni, tiempoFin, duracion, tema, operacion, area, problema, cliente, noParte, cantPzas, noParteCambio, scrap, detalleMaterial, tiempoCiclo, iReg"
            +" FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea like ? AND hora >= ? AND hora < ? ORDER BY hora,tiempoIni ASC";
    private final String LISTAR_TURNO_NOCTURNO = "SELECT linea, format(fecha, 'dd/MM/yyyy'), hora, tiempoIni, tiempoFin, duracion, tema, operacion, area, problema, cliente, noParte, cantPzas, noParteCambio, scrap, detalleMaterial, tiempoCiclo, iReg"
            +" FROM Bitacora WHERE (fecha = CONVERT(DATETIME, ?, 103) AND linea like ? AND hora >= ?) OR (fecha = CONVERT(DATETIME, ?, 103) AND linea like ? AND hora < ?) ORDER BY hora,tiempoIni ASC";   
    private final String EXISTEN_TURNOS_DIA = "SELECT COUNT(turno) FROM TiempoTurno WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea like ?";
    private final String LISTAR_REGISTROS_OEE_DIARIO = "SELECT * FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea like ?"; 
    //private final String     
    
    //ARELI HOURLY COUNT
    //HOURLY POR HORA
    private final String LINETACK_LINEA = "SELECT sec FROM lineTakt WHERE linea = ? ";    
    
    private final String CANT_TCDISTINTOS_PRODUCCION = "SELECT tiempoCiclo FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? AND tema = 'Piezas Producidas' GROUP BY tiempoCiclo ";
    
    //private final String TC_PONDERADO_HORA = "SELECT SUM(cantPzas*tiempoCiclo)/SUM(cantPzas) FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? AND tema = 'Piezas Producidas' ";  //ACTUALMENTE NO SE USA
    private final String TC_PONDERADO_HORA = "SELECT cantPzas, tiempoCiclo FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? AND tema = 'Piezas Producidas' ORDER BY tiempoCiclo ";  //ACTUALMENTE NO SE USA
    
    private final String TC_PONDERADO_HORA_NOPRODUCCION = "SELECT SUM(1*tiempoCiclo)/COUNT(tiempoCiclo) FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? ";  //ACTUALMENTE NO SE USA
    //private final String TC_HORA_PROD = "SELECT tiempoCiclo AS tc FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? ";  //ACTUALMENTE NO SE USA
    private final String MIN_REP_HORA = "SELECT SUM(duracion) AS pzas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ?  AND hora = ? "; 
    private final String TC_DIA_PROD = "SELECT AVG(tiempoCiclo) AS pzas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Piezas Producidas'";  //ACTUALMENTE NO SE USA
    
    private final String PZAS_ESP_DIA = "SELECT sum(pzasEsp) FROM OEEPercentHour WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? ";  //Para sacar el porcentaje de dia
    
    private final String PZAS_HORA_PROD = "SELECT SUM(cantPzas) AS pzas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Piezas Producidas'";
    private final String MIN_HORA_PROD = "SELECT SUM(duracion) AS tProd FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Piezas Producidas'";
    
    //private final String MIN_HORA_TEC = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Tecnicas' ";
    private final String MIN_HORA_TEC = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Tecnicas' AND iReg <> 'X' ";
    //private final String MIN_HORA_ORG = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Organizacionales' ";
    private final String MIN_HORA_ORG = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Organizacionales' AND iReg <> 'X' ";
    //private final String MIN_HORA_CAM = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema LIKE '%Cambio de Modelo%' ";
    private final String MIN_HORA_CAM = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Cambio de modelo' AND iReg <> 'X' ";
    //private final String PZAS_HORA_CALI = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Calidad' ";
    
    //private final String PZAS_HORA_CALI = "SELECT SUM(CAST (scrap as int )) as scrap FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Calidad' AND iReg <> 'X' ";
    //private final String PZAS_HORA_CALI = "SELECT SUM(CAST(b.scrap as int)) as scrap, CAST(b.tiempoCiclo as float), CAST(t.tcPromedio AS float) FROM Bitacora b, tcNumParte t WHERE b.fecha = CONVERT(DATETIME, ?, 103) AND b.linea = ? AND b.hora = ? AND b.tema = 'Calidad' AND b.iReg <> 'X' AND b.noParte = t.numParte AND b.linea = t.linea GROUP BY b.scrap, b.tiempoCiclo, t.tcPromedio";
    private final String PZAS_HORA_CALI = "SELECT SUM(CAST(b.scrap as int)) as scrap, CAST(b.tiempoCiclo as float), CAST(t.tcPromedio AS float) FROM Bitacora b, tcNumParte t WHERE b.fecha = CONVERT(DATETIME, ?, 103) AND b.linea = ? AND b.hora = ? AND b.scrap <> '0' AND b.tema in ('Calidad', 'Tecnicas', 'Organizacionales', 'Cambio de modelo') AND b.iReg <> 'X' AND b.noParte = t.numParte AND b.linea = t.linea GROUP BY b.scrap, b.tiempoCiclo, t.tcPromedio";
    
    private final String MIN_HORA_PLAN = "SELECT SUM(duracion) as tPlan FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Paros Planeados' AND area <> 'Comedor' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND area <> 'Arranque de linea' ";
    private final String MIN_HORA_TFALT = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Tiempo Faltante' ";
    //private final String MIN_HORA_TDES = "SELECT SUM(duracion) as tDesc FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Paros Planeados' AND area = 'Comedor' OR area = 'Arranque de linea' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? OR area = 'Junta' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? OR area = 'Junta Informativa' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? OR area = 'Descanso' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ?";
    private final String MIN_HORA_TDES = "SELECT SUM(duracion) as tDesc FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? AND tema = 'Paros Planeados' ";
    
    private final String REGISTRO_PERCENT_HORA = "SELECT id FROM OEEPercentHour WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND hora = ? ";    
    private final String INSERTA_PERCENT_HOURLY = "INSERT INTO OEEPercentHour (linea, fecha, dia, mes, anio, hora, prod, tec, org, cal, cam, tDes, pzasEsp ) VALUES (?, CAST(CONVERT(DATETIME, ?, 103) as DATETIME), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
   
    //HOURLY POR DIA
    private final String REGISTRO_PERCENT_DIA = "SELECT id FROM OEEPercentDay WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? ";
    private final String PZAS_DIA_PROD = "SELECT SUM(cantPzas) AS pzas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Piezas Producidas'";
    private final String MIN_DIA_TRABAJADOS = "SELECT SUM(duracion) AS tProd FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? ";
    
    //private final String MIN_DIA_TEC = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Tecnicas' ";
    private final String MIN_DIA_TEC = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Tecnicas' AND iReg <> 'X' ";
    //private final String MIN_DIA_ORG = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Organizacionales' ";
    private final String MIN_DIA_ORG = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Organizacionales' AND iReg <> 'X' ";
    //private final String MIN_DIA_CAM = "SELECT SUM(duracion) as tCam FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Cambio de Modelo' ";
    private final String MIN_DIA_CAM = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Cambio de modelo' AND iReg <> 'X' ";
    //private final String PZAS_DIA_CALI = "SELECT SUM(duracion) as tTecnicas FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Calidad' ";
    
    //private final String PZAS_DIA_CALI = "SELECT SUM(CAST(b.scrap as int)) as scrap, CAST(b.tiempoCiclo as float), CAST(t.tcPromedio AS float) FROM Bitacora b, tcNumParte t WHERE b.fecha = CONVERT(DATETIME, ?, 103) AND b.linea = ? AND b.tema = 'Calidad' AND b.iReg <> 'X' AND b.noParte = t.numParte AND b.linea = t.linea GROUP BY b.scrap, b.tiempoCiclo, t.tcPromedio";
    private final String PZAS_DIA_CALI = "SELECT SUM(CAST(b.scrap as int)) as scrap, CAST(b.tiempoCiclo as float), CAST(t.tcPromedio AS float) FROM Bitacora b, tcNumParte t WHERE b.fecha = CONVERT(DATETIME, ?, 103) AND b.linea = ? AND b.scrap <> '0' AND b.tema in ('Calidad', 'Tecnicas', 'Organizacionales', 'Cambio de modelo') AND b.iReg <> 'X' AND b.noParte = t.numParte AND b.linea = t.linea GROUP BY b.scrap, b.tiempoCiclo, t.tcPromedio";
    
    
    private final String MIN_DIA_PLAN = "SELECT SUM(duracion) as tPlan FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Paros Planeados' AND area <> 'Comedor' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND area <> 'Arranque de linea' ";
    private final String MIN_DIA_TFALT = "SELECT SUM(duracion) as tOrg FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Tiempo Faltante' ";
    //private final String MIN_DIA_TDES = "SELECT SUM(duracion) as tDescD FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Paros Planeados' AND area = 'Comedor' OR area = 'Arranque de linea' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? OR area = 'Junta' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? OR area = 'Junta Informativa' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? OR area = 'Descanso' AND fecha = CONVERT(DATETIME, ?, 103) AND linea = ? ";
    private final String MIN_DIA_TDES = "SELECT SUM(duracion) as tDescD FROM Bitacora WHERE fecha = CONVERT(DATETIME, ?, 103) AND linea = ? AND tema = 'Paros Planeados'";
    
    private final String INSERTA_PERCENT_DIA = "INSERT INTO OEEPercentDay (linea, fecha, dia, mes, anio, prod, tec, org, cal, cam, tDes) VALUES (?, CAST(CONVERT(DATETIME, ?, 103) as DATETIME), ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    
    
    @Override
    public void insertarRegistroAccess(ArrayList registro) throws Exception {
        Object[] reg = new Object[registro.size()];
        try {
            this.conectar();
            for (int i = 0; i < registro.size(); i++) {
                reg[i] = (Object) registro.get(i);
            }
            ps = this.conexion.prepareStatement(INSERTA_REGISTRO);
            ps.setString(1, reg[1].toString());//dia
            ps.setString(2, reg[1].toString());//mes
            ps.setString(3, reg[1].toString());//anio
            ps.setString(4, reg[0].toString());
            ps.setString(5, reg[1].toString());
            ps.setInt(6, Integer.parseInt(reg[2].toString()));
            ps.setInt(7, Integer.parseInt(reg[3].toString()));
            ps.setInt(8, Integer.parseInt(reg[4].toString()));
            ps.setInt(9, Integer.parseInt(reg[5].toString()));
            ps.setString(10, reg[6].toString());
            ps.setString(11, reg[7].toString());
            ps.setString(12, reg[8].toString());
            ps.setString(13, reg[9].toString());
            ps.setString(14, reg[10].toString());
            ps.setString(15, reg[11].toString());
            ps.setInt(16, Integer.parseInt(reg[12].toString()));
            ps.setString(17, reg[13].toString());
            ps.setString(18, reg[14].toString());
            ps.setString(19, reg[15].toString());
            ps.setString(20, reg[16].toString());
            ps.setString(21, reg[17].toString());
            
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }

    @Override
    public ArrayList existeFechaBitacora(String fecha, DefaultTableModel bitacora) throws Exception {
        ArrayList horas = new ArrayList();
        Object[] dat = new Object[3];
        try {
            this.conectar();
            for (int i = 0; i < bitacora.getRowCount(); i++) {
                ps = this.conexion.prepareStatement(CONSULTA_FECHA);
                ps.setString(1, fecha);
                ps.setInt(2, Integer.parseInt(bitacora.getValueAt(i, 2).toString()));
                ps.setInt(3, Integer.parseInt(bitacora.getValueAt(i, 3).toString()));
                ps.setInt(4, Integer.parseInt(bitacora.getValueAt(i, 4).toString()));
                rs = ps.executeQuery();
                
                if (rs.next()) {
                    dat[0] = bitacora.getValueAt(i, 2);
                    dat[1] = bitacora.getValueAt(i, 3);
                    dat[2] = bitacora.getValueAt(i, 4);
                    horas.add(dat);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en BUSCAR_FECHA " + e);
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return horas;
    }
    
    @Override
    public ArrayList listarBitacorasTurno(String fecha, String linea, int horaInicia, int horaFin) throws Exception {
        Object[] bitacoraObj;
        listaRegistros = new ArrayList();
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(LISTAR_REGISTROS_TURNO);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, horaInicia);
            ps.setInt(4, horaFin);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                bitacoraObj = new Object[18];
                bitacoraObj[0] = rs.getString(1);
                bitacoraObj[1] = rs.getString(2);
                bitacoraObj[2] = rs.getInt(3);
                bitacoraObj[3] = rs.getInt(4);
                bitacoraObj[4] = rs.getInt(5);
                bitacoraObj[5] = rs.getInt(6);
                bitacoraObj[6] = rs.getString(7);
                bitacoraObj[7] = rs.getString(8);
                bitacoraObj[8] = rs.getString(9);
                bitacoraObj[9] = rs.getString(10);
                bitacoraObj[10] = rs.getString(11);
                bitacoraObj[11] = rs.getString(12);
                bitacoraObj[12] = rs.getInt(13);
                bitacoraObj[13] = rs.getString(14);
                bitacoraObj[14] = rs.getString(15);
                bitacoraObj[15] = rs.getString(16);
                bitacoraObj[16] = rs.getString(17);
                bitacoraObj[17] = rs.getString(18);
                
                listaRegistros.add(bitacoraObj);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return listaRegistros;
    }
    
    public ArrayList listarBitacorasTurnoNocturno(String fecha, String linea, int horaInicia, String fecha2, String linea2, int horaFin) throws Exception {
        Object[] bitacoraObj;
        listaRegistros = new ArrayList();
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(LISTAR_TURNO_NOCTURNO);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, horaInicia);
            ps.setString(4, fecha2);
            ps.setString(5, linea2);
            ps.setInt(6, horaFin);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                bitacoraObj = new Object[18];
                bitacoraObj[0] = rs.getString(1);
                bitacoraObj[1] = rs.getString(2);
                bitacoraObj[2] = rs.getInt(3);
                bitacoraObj[3] = rs.getInt(4);
                bitacoraObj[4] = rs.getInt(5);
                bitacoraObj[5] = rs.getInt(6);
                bitacoraObj[6] = rs.getString(7);
                bitacoraObj[7] = rs.getString(8);
                bitacoraObj[8] = rs.getString(9);
                bitacoraObj[9] = rs.getString(10);
                bitacoraObj[10] = rs.getString(11);
                bitacoraObj[11] = rs.getString(12);
                bitacoraObj[12] = rs.getInt(13);
                bitacoraObj[13] = rs.getString(14);
                bitacoraObj[14] = rs.getString(15);
                bitacoraObj[15] = rs.getString(16);
                bitacoraObj[16] = rs.getString(17);
                bitacoraObj[17] = rs.getString(18);
                
                listaRegistros.add(bitacoraObj);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return listaRegistros;
    }


    @Override
    public void insertarFilaRegistro(ArrayList registroFila) throws Exception {

        Object[] reg = new Object[registroFila.size()];
        try {
            this.conectar();
            System.err.println("\n");
            
            for (int i = 0; i < registroFila.size(); i++) {
                reg[i] = (Object) registroFila.get(i);
                if (reg[i] == null ){
                    reg[i] = "0";
                }
            }       
            
            ps = this.conexion.prepareStatement(INSERTA_REGISTRO);
            ps.setString(1, reg[1].toString());//dia
            ps.setString(2, reg[1].toString());//mes
            ps.setString(3, reg[1].toString());//anio
            ps.setString(4, reg[0].toString());//LINEA
            ps.setString(5, reg[1].toString());//FECHA
            ps.setInt(6, Integer.parseInt(reg[2].toString()));//HORA
            ps.setInt(7, Integer.parseInt(reg[3].toString()));//MININICIO
            ps.setInt(8, Integer.parseInt(reg[4].toString()));//MINFIN
            ps.setInt(9, Integer.parseInt(reg[5].toString()));//DURACION
            ps.setString(10, reg[6].toString());//TEMA
            ps.setString(11, reg[7].toString());//OPERACION
            ps.setString(12, reg[8].toString());//AREA
            ps.setString(13, reg[9].toString());//PROBLEMA
            ps.setString(14, reg[10].toString());//CLIENTE
            ps.setString(15, reg[11].toString());//NOPARTE
            ps.setInt(16, Integer.parseInt(reg[12].toString()));//PIEZAS PRODUCIDAS
            ps.setString(17, reg[13].toString());//NUEVO NO PARTE
            ps.setString(18, reg[14].toString());//SCRAP
            ps.setString(19, reg[15].toString());//DETALLE MATERIAL
            ps.setString(20, reg[16].toString());//TIEMPO CICLO
            ps.setString(21, reg[17].toString());//iReg

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }

    @Override
    public void borrarFilaRegistro(ArrayList bRegistroFila) throws Exception {
        Object[] reg = new Object[bRegistroFila.size()];
        try {
            this.conectar();
            for (int i = 0; i < bRegistroFila.size(); i++) {
                reg[i] = (Object) bRegistroFila.get(i);
            }
            ps = this.conexion.prepareStatement(BORRA_REGISTRO_FILA);
            ps.setString(1, reg[0].toString());
            ps.setString(2, reg[1].toString());
            ps.setInt(3, Integer.parseInt(reg[2].toString()));
            ps.setInt(4, Integer.parseInt(reg[3].toString()));
            ps.setInt(5, Integer.parseInt(reg[4].toString()));
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
            ps.close();
        }
    }

    @Override
    public void borrarRegistroTiempo(int horaInicio, int horaFin, String linea, String fecha) throws Exception {
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(BORRA_REGISTRO_TIEMPO);
            ps.setInt(1, horaInicio);
            ps.setInt(2, horaFin);
            ps.setString(3, linea);
            ps.setString(4, fecha);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
            ps.close();
        }
    }

    @Override
    public ArrayList listarBitacorasPorFecha(String fecha, String linea) throws Exception {
        Object[] bitacoraObj;
        listaRegistros = new ArrayList();
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(LISTAR_REGISTROS_FECHA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                bitacoraObj = new Object[18];
                bitacoraObj[0] = rs.getString(1);
                bitacoraObj[1] = rs.getString(2);
                bitacoraObj[2] = rs.getInt(3);
                bitacoraObj[3] = rs.getInt(4);
                bitacoraObj[4] = rs.getInt(5);
                bitacoraObj[5] = rs.getInt(6);
                bitacoraObj[6] = rs.getString(7);
                bitacoraObj[7] = rs.getString(8);
                bitacoraObj[8] = rs.getString(9);
                bitacoraObj[9] = rs.getString(10);
                bitacoraObj[10] = rs.getString(11);
                bitacoraObj[11] = rs.getString(12);
                bitacoraObj[12] = rs.getInt(13);
                bitacoraObj[13] = rs.getString(14);
                bitacoraObj[14] = rs.getString(15);
                bitacoraObj[15] = rs.getString(16);
                bitacoraObj[16] = rs.getString(17);
                bitacoraObj[17] = rs.getString(18);

                listaRegistros.add(bitacoraObj);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return listaRegistros;
    }

    //ARELI    
    public double lineTackLinea (String linea) throws Exception {
        double lineTack = 0.0;    
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(LINETACK_LINEA);
            ps.setString(1, linea);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                lineTack = rs.getInt(1);
            }else{
                lineTack = (float) 0.0;
            }                 
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }       
        return lineTack;
    }

    public int contTCDisitintosProd (String fecha, String linea, int hora) throws Exception {
//        double [] tc = new double[10];
//        int [] pzas = new int[10];
//        double [] percent = new double[10];
//        
//        int pzasTotal = 0;        
//        double tcPonderado = 0.0;
        
        int cont = 0;
        
        //listaRegistros = new ArrayList();
        //double tc = 0.0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CANT_TCDISTINTOS_PRODUCCION);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();   
            
            while (rs.next()) {
//                pzasTotal += rs.getInt(1);                
//                pzas[cont] = rs.getInt(1);
//                tc[cont] = rs.getDouble(2);               
                
                cont++;
            }
            
//            for (int i = 1; i < cont; i++){
//                percent[i] = ((float) (pzas[i]*100)/pzasTotal)*.01;
//                tcPonderado += Math.round((float) (percent[i]*tc[i]));
//            }
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return cont;
    }
    
    public double tcPonderadoDia (String fecha, String linea, int hora) throws Exception {
        double [] tc = new double[10];
        double [] dTc = new double[10];
        int [] pzas = new int[10];
        double [] percent = new double[10];
        
        int pzasTotal = 0;        
        double tcPonderado = 0.0;
        
        int cont = 1;        
        double aux1 = 0.0;
        
        DecimalFormat decimales = new DecimalFormat("##.#");
        listaRegistros = new ArrayList();
        //double tc = 0.0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(TC_PONDERADO_HORA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();   
            
            while (rs.next()) {
                pzasTotal += rs.getInt(1); 
                pzas[cont] = rs.getInt(1); 
                tc[cont] = rs.getDouble(2); 
                
                //System.out.println("i: "+cont+" tci: "+tc[cont]);
                
                if (cont >= 2){  
                    //System.out.println("dao. cont >= 2: "+tc[cont-1]+" == "+tc[cont]);
                    if(tc[cont-1] == tc[cont]){
                        dTc[cDTC] = tc[cont]; 
                    } else { 
                        cDTC++; 
                        dTc[cDTC] = tc[cont]; 
                    } 
                    //System.out.println("dao.BitacoraDAOImpl.tcPonderadoDia(): cDTC = "+cDTC);
                } else { 
                    //System.out.println("!dao. cont < 2: "+tc[cont]); 
                    dTc[cDTC] = tc[cont]; 
                } 
                cont++; 
            }
            
            if (cDTC > 0) { 
                for (int i = 1; i < cont; i++) { 
                    percent[i] = ((float) (pzas[i]*100)/pzasTotal)*.01; 
                    //tcPonderado += Math.round((float) (percent[i]*tc[i]));
                    aux1 += (double) (percent[i]*tc[i]);
                    
                    tcPonderado = Double.parseDouble(decimales.format(aux1));
                    //tcPonderado += (double) (percent[i]*tc[i]);
                    
                    //System.out.println(i+" dao.tcPonderadoDia():"+pzasTotal+" pzasTc: "+pzas[i]+" tc: "+tc[i]+"percent "+percent[i]+" tcPonderado: "+tcPonderado);
                } 
            } else {
                tcPonderado = dTc[cDTC];
            }            
//            System.out.println("dao.BitacoraDAOImpl.tcPonderadoDia(): "+ tcPonderado );
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return tcPonderado;
    }
    
//    public double tcPonderadoDia (String fecha, String linea, int hora) throws Exception {
//        //listaRegistros = new ArrayList();
//        double tc = 0.0;
//        try {
//            this.conectar();
//            ps = this.conexion.prepareStatement(TC_PONDERADO_HORA);
//            ps.setString(1, fecha);
//            ps.setString(2, linea);
//            ps.setInt(3, hora);
//            rs = ps.executeQuery();            
//            if (rs.next()) {
//                tc = rs.getFloat(1);
//            } else {
//                tc = 0.0;
//            }              
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            ps.close();
//            rs.close();
//            this.cerrar();
//        }       
//        return tc;
//    }
    
    public double tcPonderadoDiaNoProduccion (String fecha, String linea, int hora) throws Exception {
        //System.out.println("dao.BitacoraDAOImpl.tcPonderadoDiaNoProduccion(): "+fecha+", "+linea+", "+hora);
        //listaRegistros = new ArrayList();
        double tc = 0.0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(TC_PONDERADO_HORA_NOPRODUCCION);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();            
            if (rs.next()) {
                tc = rs.getFloat(1);
            } else {
                tc = 0.0;
            }              
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return tc;
    }
    
    public int minRepHora (String fecha, String linea, int hora) throws Exception {
        int minHora = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_REP_HORA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minHora = rs.getInt(1);
            }else{
                minHora = 0;
            }                 
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }       
        return minHora;
    }
    
    
    public double tcDia (String fecha, String linea) throws Exception {
        //System.out.println("fTCdI: "+ fecha);
        double tcDia = 0.0;    
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(TC_DIA_PROD);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                tcDia = rs.getInt(1);
            }else{
                tcDia = (float) 0.0;
            }                 
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }       
        return tcDia;
    }
    
    public int pzasEspDia (String fecha, String linea) throws Exception {
        //System.out.println("fTCdI: "+ fecha);
        int pzasDia = 0;    
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(PZAS_ESP_DIA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                pzasDia = rs.getInt(1);
            }else{
                pzasDia = 0;
            }                 
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }       
        return pzasDia;
    }    
    
    //HOURLY POR HORA
    public int pzasProdHora(String fecha, String linea, int hora) throws Exception {
        int pzasProd = 0;      
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(PZAS_HORA_PROD);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                pzasProd = rs.getInt(1);
            } else {
                pzasProd = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return pzasProd;
    }
    
    public int minProdHora(String fecha, String linea, int hora) throws Exception {
        int pzasProd = 0;     
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_PROD);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                pzasProd = rs.getInt(1);
            } else {
                pzasProd = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return pzasProd;
    } 
    
    public int minDesHora(String fecha, String linea, int hora) throws Exception {
        int minDes = 0;       
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_TDES);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
//            ps.setString(4, fecha);
//            ps.setString(5, linea);
//            ps.setInt(6, hora);
//            ps.setString(7, fecha);
//            ps.setString(8, linea);
//            ps.setInt(9, hora);
//            ps.setString(10, fecha);
//            ps.setString(11, linea);
//            ps.setInt(12, hora);
//            ps.setString(13, fecha);
//            ps.setString(14, linea);
//            ps.setInt(15, hora);            
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
               minDes = rs.getInt(1);
            } else {
                minDes = 0;
            }            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }        
        return minDes;
    }
    
    public int minTecHora(String fecha, String linea, int hora) throws Exception {
        int minTec = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_TEC);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minTec = rs.getInt(1);
            } else {
                minTec = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minTec;
    }
    
    public int minOrgHora(String fecha, String linea, int hora) throws Exception {
        int minOrg = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_ORG);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minOrg = rs.getInt(1);
            } else {
                minOrg = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minOrg;
    }
    
    public int minPlanHora(String fecha, String linea, int hora) throws Exception {
        int minPlan = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_PLAN);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            ps.setString(4, fecha);
            ps.setString(5, linea);
            ps.setInt(6, hora);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minPlan = rs.getInt(1);
            } else {
                minPlan = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            
            this.cerrar();
        }
        rs.close();
        return minPlan;
    }
    
    public int minCamHora(String fecha, String linea, int hora) throws Exception {
        int minCam = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_CAM);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minCam = rs.getInt(1);
            } else {
                minCam = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minCam;
    }
    
    public double minCaliHora(String fecha, String linea, int hora) throws Exception {
        double minCali = 0; 
        
        double cPzas = 0; 
        double tcBit = 0.0; 
        double tcBal = 0.0; 
        double tcH = 0.0; 
        double minCumm = 0.0; 
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(PZAS_HORA_CALI);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            while (rs.next()) { 
                cPzas = rs.getInt(1); 
                tcBit = rs.getDouble(2); 
                tcBal = rs.getDouble(3); 
                
                tcH = tcBal; 
                if (tcBal == 0.0){ 
                    tcH = tcBit; 
                } 
                
                minCali += (cPzas*tcH)/60; 
                
                //System.out.println("dao.BitacoraDAOImpl.minCaliHora(). cal: "+cPzas+" tcBit: "+tcBit+" tcBal: "+tcBal+" -> "+minCali );
            } 
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minCali;
    }
    
    public int minTFalHora(String fecha, String linea, int hora) throws Exception {
        int minTFal = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_HORA_TFALT);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minTFal = rs.getInt(1);
            } else {
                minTFal = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minTFal;
    }
        
    public int regPercentsHora(String fecha, String linea, int hora) throws Exception {
        int exisReg = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(REGISTRO_PERCENT_HORA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            ps.setInt(3, hora);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                exisReg = rs.getInt(1);
            } else {
                exisReg = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            
            this.cerrar();
        }
        rs.close();
        return exisReg;
    }
    
    public int regPercentsDia(String fecha, String linea) throws Exception {
        int exisReg = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(REGISTRO_PERCENT_DIA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                exisReg = rs.getInt(1);
            } else {
                exisReg = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            
            this.cerrar();
        }
        rs.close();
        return exisReg;
    }
    
    // APARTEADO PARA CONSULTAS DE TIEMPO POR 
    public int pzasProdDia(String fecha, String linea) throws Exception {
        int pzasProd = 0;      
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(PZAS_DIA_PROD);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                pzasProd = rs.getInt(1);
            } else {
                pzasProd = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        
        return pzasProd;
    }
    
    public int minTrabajadosDia(String fecha, String linea) throws Exception {
        int pzasProd = 0;      
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_TRABAJADOS);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                pzasProd = rs.getInt(1);
            } else {
                pzasProd = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return pzasProd;
    } 
    
    public int minDesDia(String fecha, String linea) throws Exception {
        int minDes = 0;       
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_TDES);
            ps.setString(1, fecha);
            ps.setString(2, linea);     
//            ps.setString(3, fecha);
//            ps.setString(4, linea);
//            ps.setString(5, fecha);
//            ps.setString(6, linea);
//            ps.setString(7, fecha);
//            ps.setString(8, linea);
//            ps.setString(9, fecha);
//            ps.setString(10, linea);
            
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
               minDes = rs.getInt(1);
            } else {
                minDes = 0;
            }
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        
        return minDes;
    }
    
    public int minTecDia(String fecha, String linea) throws Exception {
        int minTec = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_TEC);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minTec = rs.getInt(1);
            } else {
                minTec = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minTec;
    }
    
    public int minOrgDia(String fecha, String linea) throws Exception {
        int minOrg = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_ORG);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minOrg = rs.getInt(1);
            } else {
                minOrg = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minOrg;
    }
    
    public int minPlanDia(String fecha, String linea) throws Exception {
        int minPlan = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_PLAN);
            ps.setString(1, fecha);
            ps.setString(2, linea);            
            ps.setString(3, fecha);
            ps.setString(4, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minPlan = rs.getInt(1);
            } else {
                minPlan = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            
            this.cerrar();
        }
        rs.close();
        return minPlan;
    }
    
    public int minCamDia(String fecha, String linea) throws Exception {
        int minCam = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_CAM);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minCam = rs.getInt(1);
            } else {
                minCam = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minCam;
    }
    
    public double minCaliDia(String fecha, String linea) throws Exception {
        double minCali = 0; 
        int pzasD = 0; 
        double tc = 0.0; //TIEMPO DEFINITIVO 
        double tcBit = 0.0; // TIEMPO CICLO BITACORA 
        double tcBal = 0.0; //TIEMPO CICLO BALANCEO 
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(PZAS_DIA_CALI);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();             
            while (rs.next()) {                 
                pzasD = rs.getInt(1); 
                tcBit = rs.getDouble(2); 
                tcBal = rs.getDouble(3); 
                
                tc = tcBal;
                if (tcBal == 0.0){
                    tc = tcBit;
                } 
                
                minCali += (pzasD*tc)/60;
                //System.out.println("dao.BitacoraDAOImpl.minCaliHora(). cal: "+pzasD+" tcBit: "+tcBit+" tcBal: "+tcBal+" -> "+minCali );
            } 
        } catch (Exception e) { 
            throw e; 
        } finally { 
            ps.close(); 
            rs.close(); 
            this.cerrar(); 
        } 
        return minCali; 
    } 
    
    public int minTFalDia(String fecha, String linea) throws Exception {
        int minTFal = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(MIN_DIA_TFALT);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            
            rs = ps.executeQuery();
            
            if (rs.next()) {
                minTFal = rs.getInt(1);
            } else {
                minTFal = 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return minTFal;
    } 
     
    //APARTADOS PARA INSERTAR PORCENTAJES
    //HORAS
    public void insertarPercentHoraDia(String linea, String fecha, String dia, String mes, String anio, int hora, double prod, double tec, double org, double cal, double cam, double des, int pzasMetaH) throws Exception {
        try {
            this.conectar();
            
            ps = this.conexion.prepareStatement(INSERTA_PERCENT_HOURLY);
            ps.setString(1, linea);//Linea
            ps.setString(2, fecha);//linea
            ps.setString(3, dia);//dia
            ps.setString(4, mes);//mes
            ps.setString(5, anio);//anio
            ps.setInt(6, hora);//hora
            ps.setDouble(7, prod);//produccion
            ps.setDouble(8, tec);//tecnicas
            ps.setDouble(9, org);//organizacionales
            ps.setDouble(10, cal);//calidad
            ps.setDouble(11, cam);//cambio de modelo 
            ps.setDouble(12, des);//tiempo desempenio
            ps.setDouble(13, pzasMetaH);//Pzas Esperadas en la hora

            ps.executeUpdate();
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void modificarPercentHoraDia(double prod, double tec, double org, double cal, double cam, double des, int pzasEsp, int id) throws Exception {
        //System.err.println("prod: "+prod+" tec: "+tec+" org: "+org+" cal: "+cal+" cam: "+cam+" tFal: "+des+" pzas: "+pzasEsp+" id: "+id);
        
        try {
            this.conectar();            
            ps = this.conexion.prepareStatement("UPDATE OEEPercentHour SET prod ='"+prod+"', tec ='"+tec+"', org ='"+org+"', cal = '"+cal+"', cam ='"+cam+"', tDes ='"+des+"', pzasEsp ='"+pzasEsp+"' WHERE id ='"+id+"'");           
                                                 
            ps.executeUpdate();            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    } 
    //DIA
    public void insertarPercentDia(String linea, String fecha, String dia, String mes, String anio, double prod, double tec, double org, double cal, double cam, double des) throws Exception {
        try {
            this.conectar();
            
            ps = this.conexion.prepareStatement(INSERTA_PERCENT_DIA);
            ps.setString(1, linea);//Linea
            ps.setString(2, fecha);//linea
            ps.setString(3, dia);//dia
            ps.setString(4, mes);//mes
            ps.setString(5, anio);//anio
            ps.setDouble(6, prod);//produccion
            ps.setDouble(7, tec);//tecnicas
            ps.setDouble(8, org);//organizacionales
            ps.setDouble(9, cal);//calidad
            ps.setDouble(10, cam);//cambio de modelo 
            ps.setDouble(11, des);//tiempo desempenio

            ps.executeUpdate();
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }
    
    public void modificarPercentDia(double prod, double tec, double org, double cal, double cam, double des, int id) throws Exception {
        try {
            this.conectar();            
            ps = this.conexion.prepareStatement("UPDATE OEEPercentDay SET prod ='"+prod+"', tec ='"+tec+"', org ='"+org+"', cal = '"+cal+"', cam ='"+cam+"', tDes ='"+des+"' WHERE id ='"+id+"'");           
            ps.executeUpdate();            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    } 
    
    
    
    @Override
    public void insertarRegistroHourly(ArrayList registroHourly) throws Exception {
        Object[] hourlyReg;
        try {
            this.conectar();
            for (int i = 0; i < registroHourly.size(); i++) {
                
            hourlyReg = (Object[]) registroHourly.get(i);
            ps = this.conexion.prepareStatement(INSERTA_REGISTRO_HOURLY);
            ps.setString(1, hourlyReg[12].toString());//fecha
            ps.setString(2, hourlyReg[13].toString());//linea
            ps.setInt(3, Integer.parseInt(hourlyReg[0].toString()));//hora
            ps.setString(4, hourlyReg[1].toString());//cantProd
            ps.setString(5, hourlyReg[2].toString());//noParteTC
            ps.setString(6, hourlyReg[3].toString());//scrap
            ps.setInt(7, Integer.parseInt(hourlyReg[4].toString()));//cambioDuracion
            ps.setInt(8, Integer.parseInt(hourlyReg[5].toString()));//tecnicaDuracion
            ps.setInt(9, Integer.parseInt(hourlyReg[6].toString()));//organizacionalDuracion
            ps.setInt(10, Integer.parseInt(hourlyReg[7].toString()));//tiempoPDuracion
            ps.setString(11, hourlyReg[8].toString());//operacion
            ps.setString(12, hourlyReg[9].toString());//problema
            ps.setString(13, hourlyReg[10].toString());//operacion
            ps.setString(14, hourlyReg[11].toString());//problema

            ps.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }

    @Override
    public void borrarRegistroHourly(int hIni, int hFin, String linea, String fecha) throws Exception {
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(BORRA_REGISTRO_HOURLY);
            ps.setInt(1, hIni);
            ps.setInt(2, hFin);
            ps.setString(3, linea);
            ps.setString(4, fecha);
            
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
            ps.close();
        }
    }
    
    public int edicionPorDia(String fecha, String linea) throws Exception {
        int registros = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(EXISTEN_TURNOS_DIA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros = rs.getInt(1);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
        return registros;
    }

    @Override
    public void numeroTurnoLinea(String fecha, String linea) throws Exception {
        int numTurno = 0;
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(EXISTEN_TURNOS_DIA);
            ps.setString(1, fecha);
            ps.setString(2, linea);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                numTurno = Integer.parseInt(rs.getString(1));
//                System.out.println("NUMERO DE TURNO: " + numTurno);
                
                ps2 = this.conexion.prepareStatement("SELECT NumTurnoLinea FROM TurnoLinea WHERE Linea LIKE ? AND Fecha = CONVERT(DATETIME, ?, 103)");
                ps2.setString(1, linea);
                ps2.setString(2, fecha);
                rs2 = ps2.executeQuery();
                //ps2.close();
                
                if (rs2.next()) {
                    
                    ps3 = this.conexion.prepareStatement("UPDATE TurnoLinea SET NumTurnoLinea = ? WHERE Linea like ? AND Fecha = CONVERT(DATETIME, ?, 103) AND Dia = DATEPART(DAY, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)) AND Mes = DATEPART(MONTH, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)) AND Anio = DATEPART(YEAR, CAST(CONVERT(DATETIME, ?, 103) as DATETIME))");
                    ps3.setInt(1, numTurno);
                    ps3.setString(2, linea);
                    ps3.setString(3, fecha);
                    ps3.setString(4, fecha);//MES
                    ps3.setString(5, fecha);//DIA
                    ps3.setString(6, fecha);//ANIO
                    ps3.executeUpdate();
                    //ps3.close();
                } else {
                    ps4 = this.conexion.prepareStatement("INSERT INTO TurnoLinea(NumTurnoLinea, Linea, Fecha, Dia, Mes, Anio) VALUES(?, ?, CONVERT(DATETIME, ?, 103), DATEPART(DAY, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)), DATEPART(MONTH, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)), DATEPART(YEAR, CAST(CONVERT(DATETIME, ?, 103) as DATETIME)) )");
                    ps4.setInt(1, numTurno);
                    ps4.setString(2, linea);
                    ps4.setString(3, fecha);
                    ps4.setString(4, fecha);//dia
                    ps4.setString(5, fecha);//mes
                    ps4.setString(6, fecha);//anio
                    ps4.executeUpdate();
                    //ps4.close();
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
    }
}
