package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ConexionBD;
import modelo.UsuariosDAO;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */

public class UsuariosDAOImpl extends ConexionBD implements UsuariosDAO {

    private PreparedStatement ps;
    private ResultSet rs;
    
    private final String PRIVILEGIOS_USUARIO = "SELECT privilegio FROM usuarios WHERE usuario LIKE ? AND contrasenia LIKE ? AND estado = 1 ";
    private final String AREA_USUARIO = "SELECT producto FROM usuarios WHERE usuario LIKE ? ";
    
    @Override
    public int nivelUsuario(String usuario, String contrasena) throws Exception { 
        int privilegio = 0; 
        try { 
            this.conectar(); 
            ps = this.conexion.prepareStatement(PRIVILEGIOS_USUARIO); 
            ps.setString(1, usuario); 
            ps.setString(2, contrasena); 
            rs = ps.executeQuery(); 
            
            if (rs.next()) { 
                privilegio = rs.getInt(1); 
            } 
        } catch (Exception e) { 
            throw e; 
        } finally { 
            ps.close(); 
            rs.close(); 
            this.cerrar(); 
        } 
        return privilegio; 
    } 
    
    //AREA DE USUARIO (MARCHA O ALTERNADOR)
    public String areaUsuario(String usuario) throws Exception {
        String area = "";
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(AREA_USUARIO);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                area = rs.getString(1);
            }else {
                area = "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return area;
    }
    
}
