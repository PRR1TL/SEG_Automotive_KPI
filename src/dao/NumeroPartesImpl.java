/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import modelo.ConexionBD;

/**
 *
 * @author PRR1TL
 */
public class NumeroPartesImpl extends ConexionBD {
    
    private static PreparedStatement ps;
    private static ResultSet rs;
    
    private Object[] filaTabla;
    
    private static final String TABLA_NUMERO_PARTES = "SELECT familia, cliente, noParte FROM ListaNumPartes WHERE linea = ?";
    private static final String NUEVO_NUMERO_PARTES = "INSERT INTO ListaNumPartes(noParte, LineaProduce, familia, Cliente)  VALUES ( ?, ?, ? , ?); ";      
    
    public DefaultTableModel listaTablaNoParte (String linea, DefaultTableModel tablaModel) throws Exception {
               
        try {
            this.conectar();        
            ps = this.conexion.prepareStatement(TABLA_NUMERO_PARTES);
            ps.setString(1, linea);

            rs = ps.executeQuery();

            while (rs.next()) {
                filaTabla[0] =  rs.getString(1);
                filaTabla[1] =  rs.getString(2);
                filaTabla[2] =  rs.getString(3);
                tablaModel.addRow(filaTabla);
                
                System.err.println("mod "+ tablaModel);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return tablaModel;
    }
    
    
    public DefaultTableModel listaLineasDesc(DefaultTableModel tablaModel) throws Exception {
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(TABLA_NUMERO_PARTES);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                filaTabla = new Object[2];
                filaTabla[0] = rs.getString(1);
                filaTabla[1] = rs.getString(2);
                tablaModel.addRow(filaTabla);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return tablaModel;
    }
    
    
    
    
    public void inserNuevoNoParte(String noParte, String linea, String familia, String cliente) throws Exception {
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(NUEVO_NUMERO_PARTES);
            ps.setString(1, noParte);
            ps.setString(2, linea);
            ps.setString(3, familia);
            ps.setString(4, cliente);
            ps.executeUpdate();
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }

    
    
}
