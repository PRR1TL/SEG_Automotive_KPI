/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ConexionBD;

/**
 *
 * @author PRR1TL
 */
public class ActivityLog extends ConexionBD {
    
    private PreparedStatement ps;
    private ResultSet rs;
    
    private final String REGISTRO_ACTIVIDAD = "INSERT INTO historialMovimientos(usuario, fechaMov ,linea, area, accion, detalle) VALUES (?, ?, ?, ?, ?, ?)" ;
    
    
    //public void resgistroActividad (String usuario, String fecha, String linea, String area, String accion, String detalle) throws Exception {
    public void resgistroActividad (Object[] perdida) throws Exception {
        this.conectar();
        ps = this.conexion.prepareStatement(REGISTRO_ACTIVIDAD);
        
        ps.setString(1, perdida[0].toString()); //usuario
        ps.setString(2, perdida[1].toString()); //fecha
        ps.setString(3, perdida[2].toString()); //linea
        ps.setString(4, perdida[3].toString()); //area
        ps.setString(5, perdida[4].toString()); //accion
        ps.setString(6, perdida[5].toString()); //detalle
        
        ps.executeUpdate();
//        ps.setString(1, usuario);
//        ps.setString(2, fecha);
//        ps.setString(3, linea);
//        ps.setString(4, area);
//        ps.setString(5, accion);
//        ps.setString(6, detalle);        
    }
    
}
