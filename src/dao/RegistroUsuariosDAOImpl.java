package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import modelo.ConexionBD;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class RegistroUsuariosDAOImpl extends ConexionBD { 
    private ArrayList<String> lineaArr; 
    private PreparedStatement ps; 
    private ResultSet rs;     
    
    private final String CONSULTA_EXISTE_USUARIO = "SELECT usuario FROM usuarios WHERE usuario LIKE ?";  
    private final String CREAR_USUARIO = "INSERT INTO usuarios (producto, linea, usuario, nombre, apellido, puesto, privilegio, correo, contrasenia, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 0)";    
    private final String CONSULTA_LINEA = "SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE linea = ? ";
    private final String CONSULTA_PUESTO = " SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE puesto = ? ";   
     
    //COMBINACIONES
    private final String CONSULTA_LINEA_PUESTO = " SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE linea = ? AND puesto = ?";  
    private final String CONSULTA_LINEA_USUARIO = "SELECT linea FROM usuarios WHERE usuario = ? ";
    private final String CONSULTA_SUP_LINEAS = "SELECT linea FROM lineas WHERE cValor = ? ORDER BY linea";
    private final String CONSULTA_LIST_CADENAVALOR = "SELECT cValor FROM Lineas WHERE producto = ? AND cValor <> '' GROUP BY cValor";
    private final String CONSULTA_JEFE_LINEAS = "SELECT linea FROM Lineas  WHERE product = ? ORDER BY linea";
    private final String CONSULTA_CADENAVALOR_USUARIO = "SELECT cValor FROM lineas WHERE linea = ? ";
    private final String CONSULTA_TABLA_ALL = "SELECT linea, puesto, usuario, nombre, correo FROM usuarios";
    private final String CONSULTA_SUP_ALLUSUARIOS_CADENAVALOR = "SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = ? GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo ";
    private final String CONSULTA_JP_ALLUSUARIOS_PRODUCTO = "SELECT u.linea, u.puesto, u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN ? AND ? AND u.producto = ? GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo ";
    
    //MODIFICACION
    private final String UPDATE_USUARIO_TOTAL = "UPDATE users SET producto = ? , linea = ? , nombre = ? , puesto = ?, privilegio = ?, contrasenia = ?, correo = ? WHERE usuario = ? ";
    private final String UPDATE_USUARIO = "UPDATE users SET producto = ? , linea = ? , nombre = ? , puesto = ?, privilegio = ?, correo = ? WHERE usuario = ? ";
    
    private final String BUSQUEDA_SUP_PUESTO = "SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = ? AND u.puesto = ? GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo ";
    private final String BUSQUEDA_JP_PUESTO = "SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN ? AND ? AND u.producto = ? AND u.puesto = ? GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo ";      
    
    public int insertarUsuario(String producto, String linea, String usuario, String nombre, String apellido, String puesto, String privilegios, String correo, String contrasena) throws Exception {
        int x = 0; 
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CREAR_USUARIO);
            ps.setString(1, producto); 
            ps.setString(2, linea);
            ps.setString(3, usuario); 
            ps.setString(4, nombre); 
            ps.setString(5, apellido); 
            ps.setString(6, puesto); 
            ps.setInt(7, Integer.parseUnsignedInt(privilegios)); 
            ps.setString(8, correo); 
            ps.setString(9, contrasena); 
            
            ps.executeUpdate();
            
            x = 1;
        } catch (Exception e) {
            x = 0;
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
        return x;
    }
    
    //MODIFICAR    
    public int modificaUsuarioTotal(String producto, String linea, String usuario, String nombre, String puesto, String privilegios, String contrasena, String correo ) throws Exception{
        int x = 0; 
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(UPDATE_USUARIO_TOTAL);
            ps.setString(1, producto);
            ps.setString(2, linea);            
            ps.setString(3, nombre);
            ps.setString(4, puesto);
            ps.setInt(5, Integer.parseInt(privilegios));
            ps.setString(6, contrasena);
            ps.setString(7, correo);
            ps.setString(8, usuario);
            
            ps.executeUpdate();
            x = 1;
        } catch (Exception e) {
            x = 0;
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
        return x;
    }
    
    public int modificaUsuario(String producto, String linea, String usuario, String nombre, String puesto, String privilegios, String correo ) throws Exception{
        int x = 0; 
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(UPDATE_USUARIO);
            ps.setString(1, producto);
            ps.setString(2, linea);            
            ps.setString(3, nombre);
            ps.setString(4, puesto);
            ps.setInt(5, Integer.parseUnsignedInt(privilegios));
            ps.setString(6, correo);
            ps.setString(7, usuario);
            
            ps.executeUpdate();
            x = 1;
        } catch (Exception e) {
            x = 0;
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
        return x;
    }

    //ELIMINAR
    public int eliminarUsuario( String usuario) throws Exception{
        int x = 0; 
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("DELETE FROM usuarios WHERE usuario = '"+usuario+"'");
            ps.executeUpdate();
            x = 1;
        } catch (Exception e) {
            x = 0;
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
        return x;
    }
    
    //CONSULTA LINEA USUARIO 
    public String consultaLineaUser( String usuario ) throws Exception {
        String linea = "";
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_LINEA_USUARIO);
            ps.setString(1, usuario);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                linea = rs.getString(1);
            }
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaUser()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return linea;
    }
    
    public int consultaExisteUser( String usuario ) throws Exception {
        int cont = 0;
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_EXISTE_USUARIO);
            ps.setString(1, usuario);
            
            rs = ps.executeQuery();
            while (rs.next()) { 
                cont++;
            } 
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaUser()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return cont;
    }
    
    public DefaultComboBoxModel consultaCadValorProducto( String cValor ) throws Exception {        
        lineaArr = new ArrayList<>();
        lineaArr.add("Selecciona");
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_SUP_LINEAS);
            ps.setString(1, cValor);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                lineaArr.add(rs.getString(1));
            } 
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.listaSupLineas()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return new DefaultComboBoxModel(lineaArr.toArray());
    }    
    
    //CONSULTA LINEA USUARIO (SIRVE PARA OBTENER CADENA DE VALOR DE LOS USUARIOE EN ESTE CASO PARA LOS SUPERVISORES)
    public DefaultComboBoxModel listaSupLineas( String cValor ) throws Exception {        
        lineaArr = new ArrayList<>();
        lineaArr.add("Selecciona");
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_SUP_LINEAS);
            ps.setString(1, cValor);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                lineaArr.add(rs.getString(1));
            }
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.listaSupLineas()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return new DefaultComboBoxModel(lineaArr.toArray());
    }    
    
    public DefaultComboBoxModel listaCadenaValor( String producto ) throws Exception {        
        lineaArr = new ArrayList<>();
        lineaArr.add("Selecciona");        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_LIST_CADENAVALOR);
            ps.setString(1, producto);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                lineaArr.add(rs.getString(1));
            }
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.listaCadenaValor()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return new DefaultComboBoxModel(lineaArr.toArray());
    } 
    
    
    //CONSULTAS PARA LA TABLA PRINCIPAL (DE ACUEROS A LOS USUARIOS QUE ENTRAN) 
    public DefaultComboBoxModel listaJefesLineas( String tipo ) throws Exception {        
        lineaArr = new ArrayList<>();
        lineaArr.add("Selecciona");
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_JEFE_LINEAS);
            ps.setString(1, tipo);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                lineaArr.add(rs.getString(1));
            }
            
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.listaJefesLineas()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return new DefaultComboBoxModel(lineaArr.toArray());
    }    
    
    //CONSULTA LINEA USUARIO 
    public String consultaCadenaValor( String linea ) throws Exception {
        String cValor = "";
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_CADENAVALOR_USUARIO);
            ps.setString(1, linea);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                cValor = rs.getString(1);
            }
            
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaCadenaValor()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return cValor;
    }
    
    
    //CONSULTA ADMINISTRADOR TOTAL   
    public DefaultTableModel consultaAllUsers( DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        //clearTable(model);
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_TABLA_ALL);
            rs = ps.executeQuery();
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaAllUsers()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }    
    
    //CONSULTA CADENA DE VALOR    
    public DefaultTableModel consultaSupUsuarios(DefaultTableModel model, String cadenaValor) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_SUP_ALLUSUARIOS_CADENAVALOR);
            ps.setString(1, cadenaValor);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaSupUsuarios()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    //CONSULTA JEFE DE PROCESOS  
    public DefaultTableModel consultaJPUsuarios(DefaultTableModel model,int v1, int v2, String cadenaValor) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_JP_ALLUSUARIOS_PRODUCTO);
            ps.setInt(1, v1); 
            ps.setInt(2, v2); 
            ps.setString(3, cadenaValor); 
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaJPUsuarios()");
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
      
    /*** APARTADO PARA BUSQUEDAS DE SUPERVISORES
     * @param model ***/
    public DefaultTableModel busquedaSupUsuario(DefaultTableModel model, String cadenaValor, String usuario) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.usuario LIKE '%"+usuario+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupPuesto(DefaultTableModel model, String cadenaValor, String puestoBusca) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(BUSQUEDA_SUP_PUESTO);
            ps.setString(1, cadenaValor); 
            ps.setString(2, puestoBusca); 
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupPuesto()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupNombre(DefaultTableModel model, String cadenaValor, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupPuestoUsuarioNombre(DefaultTableModel model, String cadenaValor, String puesto, String usuario, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.puesto = '"+puesto+"' AND u.usuario LIKE '%"+usuario+"%' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupPuestoUsuarioNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupUsuarioNombre(DefaultTableModel model, String cadenaValor, String usuario, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupUsuarioNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupPuestoUsuario(DefaultTableModel model, String cadenaValor, String puesto, String usuario) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.usuario LIKE '%"+usuario+"%' AND u.puesto = '"+puesto+"' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");

            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupPuestoUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaSupPuestoNombre(DefaultTableModel model, String cadenaValor, String puesto, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio < 3 AND l.cValor = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' AND u.puesto = '"+puesto+"' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");

            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaSupPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    /*** APARTADO PARA BUSQUEDAS DE JEFES DE PROCESOS
     * @param model ***/
    public DefaultTableModel busquedaJUsuario(DefaultTableModel model, int v1, int v2, String cadenaValor, String usuario) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto = '"+cadenaValor+"' AND u.usuario LIKE '%"+usuario+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJPuesto(DefaultTableModel model, int v1, int v2, String cadenaValor, String puestoBusca) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(BUSQUEDA_JP_PUESTO);
            ps.setInt(1, v1); 
            ps.setInt(2, v2); 
            ps.setString(3, cadenaValor); 
            ps.setString(4, puestoBusca); 
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJPuesto()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJNombre(DefaultTableModel model,int v1, int v2, String cadenaValor, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJPuestoUsuarioNombre(DefaultTableModel model, int v1, int v2, String cadenaValor, String puesto, String usuario, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto = '"+cadenaValor+"' AND u.puesto = '"+puesto+"' AND u.usuario LIKE '%"+usuario+"%' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJPuestoUsuarioNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJUsuarioNombre(DefaultTableModel model, int v1, int v2, String cadenaValor, String usuario, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' AND u.nombre LIKE '%"+nombre+"%' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJUsuarioNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJPuestoUsuario(DefaultTableModel model, int v1, int v2, String cadenaValor, String puesto, String usuario) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto ='"+cadenaValor+"' AND u.usuario LIKE '%"+usuario+"%' AND u.puesto = '"+puesto+"' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJPuestoUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel busquedaJPuestoNombre(DefaultTableModel model, int v1, int v2, String cadenaValor, String puesto, String nombre) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT u.linea, u.puesto ,u.usuario, u.nombre, u.correo FROM usuarios u, lineas l WHERE l.linea = u.linea AND u.privilegio BETWEEN '"+v1+"' AND '"+v2+"' AND u.producto = '"+cadenaValor+"' AND u.nombre LIKE '%"+nombre+"%' AND u.puesto = '"+puesto+"' GROUP BY u.linea, u.puesto ,u.usuario, u.nombre, u.correo");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.busquedaJPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    /***************** CONSULTAS PARA ADMINISTRADOR *********************/
    public DefaultTableModel consultaLinea(String linea, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_LINEA);
            ps.setString(1, linea);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            //throw e;
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLinea()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaPuesto(String puesto, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_PUESTO);
            ps.setString(1, puesto);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaPuesto()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaUsuario(String usuario, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE usuario LIKE '%" + usuario + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaNombre(String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    //COMBINACIONES
    //LINEA
    public DefaultTableModel consultaLineaPuesto(String linea, String puesto, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_LINEA_PUESTO);
            ps.setString(1, linea);
            ps.setString(2, puesto);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuesto()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }    
    
    public DefaultTableModel consultaLineaUsuario(String linea, String usuario, DefaultTableModel model) throws Exception {        
        Object[] usuarios = new Object[4];
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE linea = '"+ linea + "' AND usuario LIKE '%" + usuario + "%' ");
            rs = ps.executeQuery();
            
            while(rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaUsuario() "+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaLineaNombre(String linea, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE linea = '"+ linea + "' AND usuario LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaNombre() "+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaLineaPuestoUsuario(String linea, String puesto, String usuario, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[4];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE linea = '" + linea +"' AND puesto = '"+ puesto + "' AND usuario LIKE '%" + usuario + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaLineaPuestoNombre(String linea, String puesto, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE linea = '" + linea +"' AND puesto = '"+ puesto + "' AND nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaLineaUsuarioNombre(String linea, String usuario, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE linea = '" + linea +"' AND usuario LIKE '%" + usuario + "%' AND nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaLineaPuestoUsuarioNombre(String linea, String puesto, String usuario, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE linea = '" + linea +"' AND puesto = '"+ puesto + "' AND usuario LIKE '%" + usuario + "%' AND nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[5] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    //PUESTO
    public DefaultTableModel consultaPuestoUsuario(String puesto, String usuario, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre, correo FROM usuarios WHERE puesto = '"+ puesto + "' AND usuario LIKE '%" + usuario + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaPuestoNombre(String puesto, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE puesto = '"+ puesto + "' AND usuario LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoUsuario()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    public DefaultTableModel consultaPuestoUsuarioNombre(String puesto, String usuario, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE puesto = '"+ puesto + "' AND usuario LIKE '%" + usuario + "%' AND nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
    
    //USUARIO
    public DefaultTableModel consultaUsuarioNombre(String usuario, String nombre, DefaultTableModel model) throws Exception {
        Object[] usuarios = new Object[5];
        try {
            this.conectar();
            ps = this.conexion.prepareStatement("SELECT linea, puesto, usuario, nombre FROM usuarios WHERE usuario LIKE '%" + usuario + "%' AND nombre LIKE '%" + nombre + "%' ");
            rs = ps.executeQuery();
            
            while (rs.next()) {
                usuarios[0] = rs.getString(1);
                usuarios[1] = rs.getString(2);
                usuarios[2] = rs.getString(3);
                usuarios[3] = rs.getString(4);
                usuarios[4] = rs.getString(5);
                model.addRow(usuarios);
            }
            ps.close();
            rs.close();
        } catch (SQLException e) {
            System.err.println("dao.RegistroUsuariosDAOImpl.consultaLineaPuestoNombre()"+e);
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return model;
    }
}
