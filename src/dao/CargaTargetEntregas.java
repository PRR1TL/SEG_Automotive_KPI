package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import modelo.ConexionBD;

/**
 *
 * @author PRR1TL
 */
public class CargaTargetEntregas extends ConexionBD {
    private PreparedStatement ps;
    private ResultSet rs;
    
    private ArrayList<String> noParteArr;
    
    //metaTec, metaOrg, metaCalidad, metaScrap
    private final String INSERTAR_DIA_MES_ANIO = "INSERT INTO targets(linea, anio, mes, dia, producidas, tek, tecnicas, organizacionales, planeado,cambio, calidad, scrap, fecha, productividad) " + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final String CONSULTA_FECHA_REGISTRO = "SELECT COUNT(mes) FROM targets WHERE linea LIKE ? AND anio LIKE ? AND mes LIKE ?";
    private final String CONSULTA_NOPARTE_LINEA = "SELECT noParte FROM listaNumPartes WHERE lineaProduce LIKE ?";
    private final String CONSULTA_GENERAL = "SELECT dia, scrap, tecnicas, organizacionales, cambio, planeado, calidad, producidas, tek, productividad FROM targets WHERE linea LIKE ? AND mes LIKE ? AND anio LIKE ?";
    
//    public void insertarFechaYLinea(String linea, String anio, String mes, ArrayList dia, ArrayList programado, ArrayList tecnicas, ArrayList organizacionales, ArrayList calidad, ArrayList scrap, ArrayList cambios, ArrayList planeados) throws Exception {
    public void insertarFechaYLinea(String linea, String anio, String mes, String dia, String producidas, String tek, String tec, String org, String plan, String cambio,String cal, String scr, String fecha, String productividad) throws Exception {
        
        try {
            this.conectar();  
            
            ps = this.conexion.prepareStatement(INSERTAR_DIA_MES_ANIO);
            ps.setString(1, linea);//linea)
            ps.setString(2, anio);//anio
            ps.setString(3, mes);//mes
            ps.setString(4, dia);//dia
            ps.setString(5, producidas);
            ps.setString(6, tek);
            ps.setString(7, tec); 
            ps.setString(8, org);
            ps.setString(9, plan); 
            ps.setString(10, cambio);
            ps.setString(11, cal); 
            ps.setString(12, scr);
            ps.setString(13, fecha);
            ps.setString(14, productividad);
            
            //System.out.println(linea+anio+mes+dia+trek+programado+tecnicas+organizacionales+calidad+scrap+"\n");

            ps.executeUpdate();
        } catch (Exception e) {
            System.err.println("Error en INSERT " + e);
            throw e;
        } finally {
            ps.close();
            this.cerrar();
        }
    }   
        
    public int consultaFechaExistente(String linea, String anio, String mes) throws Exception{
        int bn = 0;
        try {   
            this.conectar();
            
            ps = this.conexion.prepareStatement(CONSULTA_FECHA_REGISTRO);
            ps.setString(1, linea);
            ps.setString(2, anio);
            ps.setString(3, mes);
            //ps.setString(4, dia);
            
            rs = ps.executeQuery();  
            if (rs.next()){  
                bn = rs.getInt(1);
            } else {
                bn = rs.getInt(1); ;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.cerrar();
        }
        return bn;
    }
    
    public DefaultComboBoxModel listaNoParte(String linea) throws Exception {
        noParteArr = new ArrayList<>();
        noParteArr.add("No. de parte");
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_NOPARTE_LINEA);
            ps.setString(1, linea);
            rs = ps.executeQuery();
            while (rs.next()) {
                noParteArr.add(rs.getString(1));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
        return new DefaultComboBoxModel(noParteArr.toArray());
    }
    
    public void consultaGeneral (String linea, String anio, String mes) throws Exception{
        try {
            this.conectar();
            ps = this.conexion.prepareStatement(CONSULTA_NOPARTE_LINEA);
            ps.setString(1, linea);
            ps.setString(2, anio);
            ps.setString(3, mes);
            rs = ps.executeQuery();
            
            
            
        } catch (Exception e) {
            throw e;
        } finally {
            ps.close();
            rs.close();
            this.cerrar();
        }
            
    }
    
}
