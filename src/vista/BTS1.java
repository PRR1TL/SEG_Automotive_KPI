package vista;

import static com.alee.managers.notification.NotificationIcon.calendar;
import static com.alee.managers.style.SupportedComponent.textField;
import dao.CargaTargetEntregas;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import jxl.write.DateTime;
import utils.PrincipalMetodos;

/**
 *
 * @author PRR1TL
 */
public class BTS1 extends javax.swing.JDialog {
    public static String linea = "";
    
    Object[] objProgramado = new Object[10];
    Object[] objCambio = new Object[10];
    Object[] objNoProgramado = new Object[10];
    
    Object[] objVolProgramado = new Object[10];
    Object[] objVolCambio = new Object[10];
    Object[] objVolNoProgramado = new Object[10];
    
    Object[] objDescCambio = new Object[10];
    Object[] objDescNoProgramado = new Object[10];
    
    ArrayList<String> arrProgramado;
    ArrayList<String> arrCambio;
    ArrayList<String> arrNoProgramado;
    
    ArrayList<String> arrVolProgramado;
    ArrayList<String> arrVolCambio;
    ArrayList<String> arrVolNoProgramado;
    
    String planeador;
    String familia;
    String supervisor;
    String fecha;
    String semana;
    String dia;
    String mes;
    String anio;
    
    
    int numberWeekOfYear;
    Calendar calendar = Calendar.getInstance();
    
    dao.BtsDAOImpl querysBTS = new dao.BtsDAOImpl();
    
    /**
     * Creates new form BTS
     */
    public BTS1(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage());
        
        this.setTitle("BTS");
        
        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        //int numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        dteFecha.setDate(new Date(System.currentTimeMillis()));
        txtSemana.setText(Integer.toString(numberWeekOfYear));

        cmbLinea.setModel(new PrincipalMetodos().listaLineas());
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtPlaneador = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtFamilia = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtSupervisor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtSemana = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbLinea = new javax.swing.JComboBox<>();
        dteFecha = new com.alee.extended.date.WebDateField();
        btnCargar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        cmbProgram1 = new javax.swing.JComboBox<>();
        cmbProgram2 = new javax.swing.JComboBox<>();
        cmbProgram3 = new javax.swing.JComboBox<>();
        cmbProgram4 = new javax.swing.JComboBox<>();
        cmbProgram5 = new javax.swing.JComboBox<>();
        cmbProgram6 = new javax.swing.JComboBox<>();
        cmbProgram7 = new javax.swing.JComboBox<>();
        cmbProgram8 = new javax.swing.JComboBox<>();
        cmbProgram9 = new javax.swing.JComboBox<>();
        cmbProgram10 = new javax.swing.JComboBox<>();
        txtVol1 = new javax.swing.JTextField();
        txtVol2 = new javax.swing.JTextField();
        txtVol3 = new javax.swing.JTextField();
        txtVol4 = new javax.swing.JTextField();
        txtVol5 = new javax.swing.JTextField();
        txtVol6 = new javax.swing.JTextField();
        txtVol7 = new javax.swing.JTextField();
        txtVol8 = new javax.swing.JTextField();
        txtVol9 = new javax.swing.JTextField();
        txtVol10 = new javax.swing.JTextField();
        jTextField31 = new javax.swing.JTextField();
        jTextField32 = new javax.swing.JTextField();
        jTextField33 = new javax.swing.JTextField();
        jTextField34 = new javax.swing.JTextField();
        jTextField35 = new javax.swing.JTextField();
        jTextField36 = new javax.swing.JTextField();
        jTextField37 = new javax.swing.JTextField();
        jTextField38 = new javax.swing.JTextField();
        jTextField39 = new javax.swing.JTextField();
        jTextField40 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel25 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        cmbProgram11 = new javax.swing.JComboBox<>();
        cmbProgram12 = new javax.swing.JComboBox<>();
        cmbProgram13 = new javax.swing.JComboBox<>();
        cmbProgram14 = new javax.swing.JComboBox<>();
        cmbProgram15 = new javax.swing.JComboBox<>();
        cmbProgram16 = new javax.swing.JComboBox<>();
        cmbProgram17 = new javax.swing.JComboBox<>();
        cmbProgram18 = new javax.swing.JComboBox<>();
        cmbProgram19 = new javax.swing.JComboBox<>();
        cmbProgram20 = new javax.swing.JComboBox<>();
        txtVol31 = new javax.swing.JTextField();
        txtVol32 = new javax.swing.JTextField();
        txtVol33 = new javax.swing.JTextField();
        txtVol34 = new javax.swing.JTextField();
        txtVol35 = new javax.swing.JTextField();
        txtVol36 = new javax.swing.JTextField();
        txtVol37 = new javax.swing.JTextField();
        txtVol38 = new javax.swing.JTextField();
        txtVol39 = new javax.swing.JTextField();
        txtVol40 = new javax.swing.JTextField();
        jTextField41 = new javax.swing.JTextField();
        jTextField42 = new javax.swing.JTextField();
        jTextField43 = new javax.swing.JTextField();
        jTextField44 = new javax.swing.JTextField();
        jTextField45 = new javax.swing.JTextField();
        jTextField46 = new javax.swing.JTextField();
        jTextField47 = new javax.swing.JTextField();
        jTextField48 = new javax.swing.JTextField();
        jTextField49 = new javax.swing.JTextField();
        jTextField50 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        txtDesc1 = new javax.swing.JTextField();
        txtDesc2 = new javax.swing.JTextField();
        txtDesc3 = new javax.swing.JTextField();
        txtDesc4 = new javax.swing.JTextField();
        txtDesc5 = new javax.swing.JTextField();
        txtDesc6 = new javax.swing.JTextField();
        txtDesc7 = new javax.swing.JTextField();
        txtDesc8 = new javax.swing.JTextField();
        txtDesc9 = new javax.swing.JTextField();
        txtDesc10 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        cmbCambio1 = new javax.swing.JComboBox<>();
        cmbCambio2 = new javax.swing.JComboBox<>();
        cmbCambio3 = new javax.swing.JComboBox<>();
        cmbCambio4 = new javax.swing.JComboBox<>();
        cmbCambio5 = new javax.swing.JComboBox<>();
        cmbCambio6 = new javax.swing.JComboBox<>();
        cmbCambio7 = new javax.swing.JComboBox<>();
        cmbCambio8 = new javax.swing.JComboBox<>();
        cmbCambio9 = new javax.swing.JComboBox<>();
        cmbCambio10 = new javax.swing.JComboBox<>();
        txtVol21 = new javax.swing.JTextField();
        txtVol22 = new javax.swing.JTextField();
        txtVol23 = new javax.swing.JTextField();
        txtVol24 = new javax.swing.JTextField();
        txtVol25 = new javax.swing.JTextField();
        txtVol26 = new javax.swing.JTextField();
        txtVol27 = new javax.swing.JTextField();
        txtVol28 = new javax.swing.JTextField();
        txtVol29 = new javax.swing.JTextField();
        txtVol30 = new javax.swing.JTextField();
        jTextField51 = new javax.swing.JTextField();
        jTextField52 = new javax.swing.JTextField();
        jTextField53 = new javax.swing.JTextField();
        jTextField54 = new javax.swing.JTextField();
        jTextField55 = new javax.swing.JTextField();
        jTextField56 = new javax.swing.JTextField();
        jTextField57 = new javax.swing.JTextField();
        jTextField58 = new javax.swing.JTextField();
        jTextField59 = new javax.swing.JTextField();
        jTextField60 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        txtDesc11 = new javax.swing.JTextField();
        txtDesc12 = new javax.swing.JTextField();
        txtDesc13 = new javax.swing.JTextField();
        txtDesc14 = new javax.swing.JTextField();
        txtDesc15 = new javax.swing.JTextField();
        txtDesc16 = new javax.swing.JTextField();
        txtDesc17 = new javax.swing.JTextField();
        txtDesc18 = new javax.swing.JTextField();
        txtDesc19 = new javax.swing.JTextField();
        txtDesc20 = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JSeparator();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTextField21 = new javax.swing.JTextField();
        jTextField22 = new javax.swing.JTextField();
        jTextField23 = new javax.swing.JTextField();
        jTextField24 = new javax.swing.JTextField();
        jTextField25 = new javax.swing.JTextField();
        jTextField26 = new javax.swing.JTextField();
        jTextField27 = new javax.swing.JTextField();
        jTextField28 = new javax.swing.JTextField();
        jTextField29 = new javax.swing.JTextField();
        jTextField30 = new javax.swing.JTextField();
        jTextField61 = new javax.swing.JTextField();
        jTextField62 = new javax.swing.JTextField();
        jTextField63 = new javax.swing.JTextField();
        jTextField64 = new javax.swing.JTextField();
        jTextField65 = new javax.swing.JTextField();
        jTextField66 = new javax.swing.JTextField();
        jTextField67 = new javax.swing.JTextField();
        jTextField68 = new javax.swing.JTextField();
        jTextField69 = new javax.swing.JTextField();
        jTextField70 = new javax.swing.JTextField();
        jTextField71 = new javax.swing.JTextField();
        jTextField72 = new javax.swing.JTextField();
        jTextField73 = new javax.swing.JTextField();
        jTextField74 = new javax.swing.JTextField();
        jTextField75 = new javax.swing.JTextField();
        jTextField76 = new javax.swing.JTextField();
        jTextField77 = new javax.swing.JTextField();
        jTextField78 = new javax.swing.JTextField();
        jTextField79 = new javax.swing.JTextField();
        jTextField80 = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        jSeparator15 = new javax.swing.JSeparator();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Personales Informativos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel1.setText("Planeador: ");

        txtPlaneador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlaneadorActionPerformed(evt);
            }
        });
        txtPlaneador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPlaneadorKeyTyped(evt);
            }
        });

        jLabel2.setText("Familia: ");

        txtFamilia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFamiliaKeyTyped(evt);
            }
        });

        jLabel3.setText("Linea: ");

        txtSupervisor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSupervisorKeyTyped(evt);
            }
        });

        jLabel4.setText("Supervisor: ");

        jLabel5.setText("Semana: ");

        jLabel6.setText("Fecha: ");

        cmbLinea.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbLinea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbLineaItemStateChanged(evt);
            }
        });

        dteFecha.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
        dteFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dteFechaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPlaneador, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(826, 826, 826)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(jLabel6))
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSupervisor, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dteFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txtSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(241, 241, 241)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSupervisor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4))
                            .addGap(35, 35, 35))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(dteFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtPlaneador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtFamilia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3)))))
                .addContainerGap())
        );

        btnCargar.setText("Guardar");
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(null, "PROGRAMA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION)));

        cmbProgram1.setEnabled(false);
        cmbProgram1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram1ItemStateChanged(evt);
            }
        });

        cmbProgram2.setEnabled(false);
        cmbProgram2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram2ItemStateChanged(evt);
            }
        });

        cmbProgram3.setEnabled(false);
        cmbProgram3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram3ItemStateChanged(evt);
            }
        });

        cmbProgram4.setEnabled(false);
        cmbProgram4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram4ItemStateChanged(evt);
            }
        });

        cmbProgram5.setEnabled(false);
        cmbProgram5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram5ItemStateChanged(evt);
            }
        });

        cmbProgram6.setEnabled(false);
        cmbProgram6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram6ItemStateChanged(evt);
            }
        });

        cmbProgram7.setEnabled(false);
        cmbProgram7.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram7ItemStateChanged(evt);
            }
        });

        cmbProgram8.setEnabled(false);
        cmbProgram8.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram8ItemStateChanged(evt);
            }
        });

        cmbProgram9.setEnabled(false);
        cmbProgram9.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram9ItemStateChanged(evt);
            }
        });

        cmbProgram10.setEnabled(false);
        cmbProgram10.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram10ItemStateChanged(evt);
            }
        });

        txtVol1.setEnabled(false);
        txtVol1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol1KeyTyped(evt);
            }
        });

        txtVol2.setEnabled(false);
        txtVol2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol2KeyTyped(evt);
            }
        });

        txtVol3.setEnabled(false);
        txtVol3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol3KeyTyped(evt);
            }
        });

        txtVol4.setEnabled(false);
        txtVol4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol4KeyTyped(evt);
            }
        });

        txtVol5.setEnabled(false);
        txtVol5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol5KeyTyped(evt);
            }
        });

        txtVol6.setEnabled(false);
        txtVol6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol6KeyTyped(evt);
            }
        });

        txtVol7.setEnabled(false);
        txtVol7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol7KeyTyped(evt);
            }
        });

        txtVol8.setEnabled(false);
        txtVol8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol8KeyTyped(evt);
            }
        });

        txtVol9.setEnabled(false);
        txtVol9.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol9KeyTyped(evt);
            }
        });

        txtVol10.setEnabled(false);
        txtVol10.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol10KeyTyped(evt);
            }
        });

        jTextField31.setEditable(false);
        jTextField31.setText("1");
        jTextField31.setEnabled(false);
        jTextField31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField31ActionPerformed(evt);
            }
        });

        jTextField32.setEditable(false);
        jTextField32.setText("2");
        jTextField32.setEnabled(false);

        jTextField33.setEditable(false);
        jTextField33.setText("3");
        jTextField33.setEnabled(false);

        jTextField34.setEditable(false);
        jTextField34.setText("4");
        jTextField34.setEnabled(false);

        jTextField35.setEditable(false);
        jTextField35.setText("5");
        jTextField35.setEnabled(false);

        jTextField36.setEditable(false);
        jTextField36.setText("6");
        jTextField36.setEnabled(false);

        jTextField37.setEditable(false);
        jTextField37.setText("7");
        jTextField37.setEnabled(false);

        jTextField38.setEditable(false);
        jTextField38.setText("8");
        jTextField38.setEnabled(false);

        jTextField39.setEditable(false);
        jTextField39.setText("9");
        jTextField39.setEnabled(false);

        jTextField40.setEditable(false);
        jTextField40.setText("10");
        jTextField40.setEnabled(false);

        jLabel7.setText("# de Parte");

        jLabel8.setText("Volumen");

        jLabel9.setText("Sec");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel25.setText("Planeado");

        jLabel27.setText("Planeada");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbProgram2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram5, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram7, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram8, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram9, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbProgram10, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtVol4)
                    .addComponent(txtVol6)
                    .addComponent(txtVol7)
                    .addComponent(txtVol8)
                    .addComponent(txtVol9)
                    .addComponent(txtVol10)
                    .addComponent(txtVol1)
                    .addComponent(txtVol2)
                    .addComponent(txtVol3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                    .addComponent(txtVol5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jTextField31, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField32, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField33, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField34, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField35, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField36, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField37, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField38, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField39, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField40, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(45, 45, 45)
                        .addComponent(jLabel25)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addGap(21, 21, 21))
                    .addComponent(jLabel27, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(jLabel27)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jTextField31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbProgram10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtVol10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CAMBIO DE PROGRAMA NO PLANEADO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 16), new java.awt.Color(204, 51, 0))); // NOI18N

        cmbProgram11.setEnabled(false);
        cmbProgram11.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram11ItemStateChanged(evt);
            }
        });

        cmbProgram12.setEnabled(false);
        cmbProgram12.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram12ItemStateChanged(evt);
            }
        });

        cmbProgram13.setEnabled(false);
        cmbProgram13.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram13ItemStateChanged(evt);
            }
        });

        cmbProgram14.setEnabled(false);
        cmbProgram14.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram14ItemStateChanged(evt);
            }
        });

        cmbProgram15.setEnabled(false);
        cmbProgram15.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram15ItemStateChanged(evt);
            }
        });

        cmbProgram16.setEnabled(false);
        cmbProgram16.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram16ItemStateChanged(evt);
            }
        });

        cmbProgram17.setEnabled(false);
        cmbProgram17.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram17ItemStateChanged(evt);
            }
        });

        cmbProgram18.setEnabled(false);
        cmbProgram18.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram18ItemStateChanged(evt);
            }
        });

        cmbProgram19.setEnabled(false);
        cmbProgram19.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram19ItemStateChanged(evt);
            }
        });

        cmbProgram20.setEnabled(false);
        cmbProgram20.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProgram20ItemStateChanged(evt);
            }
        });

        txtVol31.setEnabled(false);
        txtVol31.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol31KeyTyped(evt);
            }
        });

        txtVol32.setEnabled(false);
        txtVol32.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol32KeyTyped(evt);
            }
        });

        txtVol33.setEnabled(false);
        txtVol33.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol33KeyTyped(evt);
            }
        });

        txtVol34.setEnabled(false);
        txtVol34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol34KeyTyped(evt);
            }
        });

        txtVol35.setEnabled(false);
        txtVol35.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol35KeyTyped(evt);
            }
        });

        txtVol36.setEnabled(false);
        txtVol36.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol36KeyTyped(evt);
            }
        });

        txtVol37.setEnabled(false);
        txtVol37.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol37KeyTyped(evt);
            }
        });

        txtVol38.setEnabled(false);
        txtVol38.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol38KeyTyped(evt);
            }
        });

        txtVol39.setEnabled(false);
        txtVol39.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol39KeyTyped(evt);
            }
        });

        txtVol40.setEnabled(false);
        txtVol40.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol40KeyTyped(evt);
            }
        });

        jTextField41.setEditable(false);
        jTextField41.setText("1");
        jTextField41.setEnabled(false);
        jTextField41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField41ActionPerformed(evt);
            }
        });

        jTextField42.setEditable(false);
        jTextField42.setText("2");
        jTextField42.setEnabled(false);

        jTextField43.setEditable(false);
        jTextField43.setText("3");
        jTextField43.setEnabled(false);

        jTextField44.setEditable(false);
        jTextField44.setText("4");
        jTextField44.setEnabled(false);

        jTextField45.setEditable(false);
        jTextField45.setText("5");
        jTextField45.setEnabled(false);

        jTextField46.setEditable(false);
        jTextField46.setText("6");
        jTextField46.setEnabled(false);

        jTextField47.setEditable(false);
        jTextField47.setText("7");
        jTextField47.setEnabled(false);

        jTextField48.setEditable(false);
        jTextField48.setText("8");
        jTextField48.setEnabled(false);

        jTextField49.setEditable(false);
        jTextField49.setText("9");
        jTextField49.setEnabled(false);

        jTextField50.setEditable(false);
        jTextField50.setText("10");
        jTextField50.setEnabled(false);

        jLabel10.setForeground(new java.awt.Color(204, 0, 0));
        jLabel10.setText("# de Parte");

        jLabel11.setForeground(new java.awt.Color(204, 0, 0));
        jLabel11.setText("Volumen");

        jLabel12.setForeground(new java.awt.Color(204, 0, 0));
        jLabel12.setText("Sec");

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel13.setForeground(new java.awt.Color(204, 0, 0));
        jLabel13.setText("Descripción del cambio");

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txtDesc1.setEnabled(false);

        txtDesc2.setEnabled(false);

        txtDesc3.setEnabled(false);

        txtDesc4.setEnabled(false);

        txtDesc5.setEnabled(false);

        txtDesc6.setEnabled(false);

        txtDesc7.setEnabled(false);

        txtDesc8.setEnabled(false);

        txtDesc9.setEnabled(false);

        txtDesc10.setEnabled(false);

        jLabel26.setForeground(new java.awt.Color(204, 0, 0));
        jLabel26.setText("Planeado");

        jLabel30.setForeground(new java.awt.Color(204, 0, 0));
        jLabel30.setText("Planeada");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(30, 30, 30)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel11))
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel30))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(jLabel12)))
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cmbProgram12, 0, 102, Short.MAX_VALUE)
                                    .addComponent(cmbProgram11, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram13, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram14, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram15, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram16, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram17, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram18, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram19, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbProgram20, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtVol34)
                                    .addComponent(txtVol35)
                                    .addComponent(txtVol36)
                                    .addComponent(txtVol37)
                                    .addComponent(txtVol38)
                                    .addComponent(txtVol39)
                                    .addComponent(txtVol40)
                                    .addComponent(txtVol31, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                                    .addComponent(txtVol32)
                                    .addComponent(txtVol33))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextField41, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField42, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField43, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField44, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField45, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField46, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField47, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField48, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField49, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextField50, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtDesc3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtDesc2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDesc1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txtDesc4, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc5, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc6, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc7, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc8, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc9, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc10, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(32, 32, 32))))
                    .addComponent(jSeparator4))
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(jLabel13)
                    .addComponent(jLabel30)
                    .addComponent(jLabel10))
                .addGap(7, 7, 7)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(txtDesc1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtDesc10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbProgram20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVol40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jTextField41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jSeparator6))
                .addGap(6, 6, 6))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CAMBIO DE PROGRAMA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        cmbCambio1.setEnabled(false);
        cmbCambio1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio1ItemStateChanged(evt);
            }
        });

        cmbCambio2.setEnabled(false);
        cmbCambio2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio2ItemStateChanged(evt);
            }
        });

        cmbCambio3.setEnabled(false);
        cmbCambio3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio3ItemStateChanged(evt);
            }
        });

        cmbCambio4.setEnabled(false);
        cmbCambio4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio4ItemStateChanged(evt);
            }
        });

        cmbCambio5.setEnabled(false);
        cmbCambio5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio5ItemStateChanged(evt);
            }
        });

        cmbCambio6.setEnabled(false);
        cmbCambio6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio6ItemStateChanged(evt);
            }
        });

        cmbCambio7.setEnabled(false);
        cmbCambio7.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio7ItemStateChanged(evt);
            }
        });

        cmbCambio8.setEnabled(false);
        cmbCambio8.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio8ItemStateChanged(evt);
            }
        });

        cmbCambio9.setEnabled(false);
        cmbCambio9.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio9ItemStateChanged(evt);
            }
        });

        cmbCambio10.setEnabled(false);
        cmbCambio10.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCambio10ItemStateChanged(evt);
            }
        });

        txtVol21.setEnabled(false);
        txtVol21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol21KeyTyped(evt);
            }
        });

        txtVol22.setEnabled(false);
        txtVol22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol22KeyTyped(evt);
            }
        });

        txtVol23.setEnabled(false);
        txtVol23.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol23KeyTyped(evt);
            }
        });

        txtVol24.setEnabled(false);
        txtVol24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol24KeyTyped(evt);
            }
        });

        txtVol25.setEnabled(false);
        txtVol25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol25KeyTyped(evt);
            }
        });

        txtVol26.setEnabled(false);
        txtVol26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol26KeyTyped(evt);
            }
        });

        txtVol27.setEnabled(false);
        txtVol27.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol27KeyTyped(evt);
            }
        });

        txtVol28.setEnabled(false);
        txtVol28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol28KeyTyped(evt);
            }
        });

        txtVol29.setEnabled(false);
        txtVol29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol29KeyTyped(evt);
            }
        });

        txtVol30.setEnabled(false);
        txtVol30.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtVol30KeyTyped(evt);
            }
        });

        jTextField51.setEditable(false);
        jTextField51.setText("1");
        jTextField51.setEnabled(false);
        jTextField51.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField51ActionPerformed(evt);
            }
        });

        jTextField52.setEditable(false);
        jTextField52.setText("2");
        jTextField52.setEnabled(false);

        jTextField53.setEditable(false);
        jTextField53.setText("3");
        jTextField53.setEnabled(false);

        jTextField54.setEditable(false);
        jTextField54.setText("4");
        jTextField54.setEnabled(false);

        jTextField55.setEditable(false);
        jTextField55.setText("5");
        jTextField55.setEnabled(false);

        jTextField56.setEditable(false);
        jTextField56.setText("6");
        jTextField56.setEnabled(false);

        jTextField57.setEditable(false);
        jTextField57.setText("7");
        jTextField57.setEnabled(false);

        jTextField58.setEditable(false);
        jTextField58.setText("8");
        jTextField58.setEnabled(false);

        jTextField59.setEditable(false);
        jTextField59.setText("9");
        jTextField59.setEnabled(false);

        jTextField60.setEditable(false);
        jTextField60.setText("10");
        jTextField60.setEnabled(false);

        jLabel14.setText("# de Parte");

        jLabel15.setText("Volumen");

        jLabel16.setText("Sec");

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator10.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel17.setText("Descripción del cambio");

        jSeparator11.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txtDesc11.setEnabled(false);

        txtDesc12.setEnabled(false);

        txtDesc13.setEnabled(false);

        txtDesc14.setEnabled(false);

        txtDesc15.setEnabled(false);

        txtDesc16.setEnabled(false);

        txtDesc17.setEnabled(false);

        txtDesc18.setEnabled(false);

        txtDesc19.setEnabled(false);

        txtDesc20.setEnabled(false);

        jSeparator12.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel18.setText("Sec");

        jSeparator13.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel19.setText("Volumen");

        jLabel23.setText("Producido");

        jLabel24.setText("Planeado");

        jLabel28.setText("Planeada");

        jLabel29.setText("Real");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel14)
                        .addGap(45, 45, 45)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel24))
                        .addGap(21, 21, 21)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28)
                            .addComponent(jLabel16))
                        .addGap(53, 53, 53)
                        .addComponent(jLabel17)
                        .addGap(84, 84, 84)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addComponent(jLabel18))
                        .addGap(27, 27, 27)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel23)
                            .addComponent(jLabel19)))
                    .addComponent(jSeparator8, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbCambio2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio5, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio7, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio8, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio9, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbCambio10, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtVol24)
                            .addComponent(txtVol25)
                            .addComponent(txtVol26)
                            .addComponent(txtVol27)
                            .addComponent(txtVol28)
                            .addComponent(txtVol29)
                            .addComponent(txtVol30)
                            .addComponent(txtVol21)
                            .addComponent(txtVol22)
                            .addComponent(txtVol23, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextField51, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField52, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField53, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField54, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField55, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField56, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField57, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField58, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField59, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField60, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDesc13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txtDesc12, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtDesc11, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtDesc14, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc15, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc16, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc17, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc18, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc19, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDesc20, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, 0))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24)
                            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel14)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jLabel18))
                        .addGap(0, 0, 0)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(jLabel29)
                            .addComponent(jLabel17))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jSeparator10, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cmbCambio10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtVol30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jTextField11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                            .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jTextField10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                            .addGap(0, 0, Short.MAX_VALUE)
                                            .addComponent(txtDesc11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtDesc20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jSeparator12)
                                        .addComponent(jSeparator11, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jTextField51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextField60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CALIFICACIÓN", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jTextField23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField23ActionPerformed(evt);
            }
        });

        jTextField70.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField70ActionPerformed(evt);
            }
        });

        jSeparator15.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator16.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel20.setText("Volumen");

        jLabel21.setText("Secuencia");

        jLabel22.setText("Evaluación");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator14, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel21)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel22))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField22, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField23, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField24, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField25, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField26, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField27, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField28, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField29, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField30, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator15, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField70, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField69, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField68, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField67, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField66, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField65, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField64, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField63, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField62, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField61, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator16, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField71, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField72, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField73, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField74, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField75, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField76, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField77, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField78, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField79, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField80, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator15, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator14, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jTextField71, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField72, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField73, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField74, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField76, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField77, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField78, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(11, 11, 11)
                                    .addComponent(jTextField79, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTextField80, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(2, 2, 2)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                            .addComponent(jTextField70, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField69, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField68, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField67, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField66, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField64, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField63, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addComponent(jTextField21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addComponent(jTextField29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addComponent(jTextField62, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jTextField61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(jSeparator16, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCargar, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19)
                .addComponent(btnCargar)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
       
    private void txtPlaneadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlaneadorKeyTyped
        validarCaracter(evt);  
    }//GEN-LAST:event_txtPlaneadorKeyTyped

    private void txtSupervisorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSupervisorKeyTyped
        validarCaracter(evt);  
    }//GEN-LAST:event_txtSupervisorKeyTyped

    private void txtFamiliaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFamiliaKeyTyped
        validarCaracter(evt);
        if(txtFamilia.getText().length()>7){
            evt.consume();
        }
    }//GEN-LAST:event_txtFamiliaKeyTyped

    private void cmbLineaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbLineaItemStateChanged
        linea = cmbLinea.getSelectedItem().toString();        
        try {
            if (cmbLinea.getSelectedIndex() > 0){
                cmbProgram1.setEnabled(true);
                cmbProgram1.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram2.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram3.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram4.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram5.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram6.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram7.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram8.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram9.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram10.setModel(new CargaTargetEntregas().listaNoParte(linea));                
                //Para Cambios
                cmbCambio1.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio2.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio3.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio4.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio5.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio6.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio7.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio8.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio9.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbCambio10.setModel(new CargaTargetEntregas().listaNoParte(linea));                
                //Para Cambios No Planeados
                cmbProgram11.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram12.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram13.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram14.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram15.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram16.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram17.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram18.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram19.setModel(new CargaTargetEntregas().listaNoParte(linea));
                cmbProgram20.setModel(new CargaTargetEntregas().listaNoParte(linea));   
            } else {
                cmbProgram1.setEnabled(false);
            }            
        } catch (Exception ex) {
            Logger.getLogger(BTS1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cmbLineaItemStateChanged

    private void jTextField41ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField41ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField41ActionPerformed

    private void txtVol40KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol40KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol40KeyTyped

    private void txtVol39KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol39KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol39KeyTyped

    private void txtVol38KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol38KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol38KeyTyped

    private void txtVol37KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol37KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol37KeyTyped

    private void txtVol36KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol36KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol36KeyTyped

    private void txtVol35KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol35KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol35KeyTyped

    private void txtVol34KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol34KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol34KeyTyped

    private void txtVol33KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol33KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol33KeyTyped

    private void txtVol32KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol32KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol32KeyTyped

    private void txtVol31KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol31KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol31KeyTyped

    private void cmbProgram20ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram20ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram20ItemStateChanged

    private void cmbProgram19ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram19ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram19ItemStateChanged

    private void cmbProgram18ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram18ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram18ItemStateChanged

    private void cmbProgram17ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram17ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram17ItemStateChanged

    private void cmbProgram16ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram16ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram16ItemStateChanged

    private void cmbProgram15ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram15ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram15ItemStateChanged

    private void cmbProgram14ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram14ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram14ItemStateChanged

    private void cmbProgram13ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram13ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram13ItemStateChanged

    private void cmbProgram12ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram12ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram12ItemStateChanged

    private void cmbProgram11ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram11ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbProgram11ItemStateChanged

    private void txtPlaneadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlaneadorActionPerformed
        // TODO add your handling code here:
//        if(txtPlaneador.getSize() >0 ){
//            txtSupervisor.setEnabled(true);
//        }
    }//GEN-LAST:event_txtPlaneadorActionPerformed

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        // TODO add your handling code here:
        //System.out.println("reg: "+arrProgramado);
        //DATOS INFORMATIVOS
        supervisor = txtSupervisor.getText();
        planeador = txtPlaneador.getText();
        familia = txtFamilia.getText();
        fecha = dteFecha.getText();
        semana  = txtSemana.getText();
        
        String[] arrayFecha = fecha.split("/");
 
// En este momento tenemos un array en el que cada elemento es un color.
        for (int i = 0; i < arrayFecha.length; i++) {
            dia = arrayFecha[0];
            mes = arrayFecha[1];
            anio = arrayFecha[2];
            
            //System.out.println(arrayFecha[i]);
        }
        
        System.out.println("d: "+dia+" m:"+mes+" a:"+anio);
        System.err.println("p: "+planeador+", s: "+supervisor+", fe: "+fecha+", se: "+semana+", fa: "+familia+", L: "+linea);
        llenarObjetos();
        
        for (int i = 0; i<10; i++){    
            System.out.println(i+": "+objProgramado[i]);
            if (!objProgramado[i].equals("No. de parte")){                
                if(objCambio[i].equals("No. de parte")){
                    objCambio[i] = "";
                    objVolCambio[i] = "";
                    objDescCambio[i] = "";
                }
                if(objNoProgramado[i].equals("No. de parte")){
                    objNoProgramado[i] = "";
                    objVolNoProgramado[i] = "";
                    objDescNoProgramado[i] = "";
                }
                System.out.println("p"+i+" :"+objProgramado[i]+", "+objVolProgramado[i]+""
                        + ", c:"+objCambio[i]+", "+objVolCambio[i]+", "+objDescCambio[i]+""
                        + ", NP:"+objNoProgramado[i]+", "+objVolNoProgramado[i]+", "+objDescNoProgramado[i]);
                
                try {
                    querysBTS.InsertBTS(linea, fecha, dia, mes, anio, familia, planeador, supervisor, semana, objCambio[i], objVolProgramado[i], i, objCambio[i], objVolCambio[i],i, objDescCambio[i], objNoProgramado[i], objVolNoProgramado[i], i, objDescNoProgramado[i]);
                } catch (Exception ex) {
                    Logger.getLogger(BTS1.class.getName()).log(Level.SEVERE, null, ex);
                }

            } 
        }        
        
    }//GEN-LAST:event_btnCargarActionPerformed

    private void dteFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dteFechaActionPerformed
        // TODO add your handling code here:
        int anio = 0, mes = 0, dia = 0;
        String colores = "01.01.2018";
        String[] arrayColores = colores.split("/");

        // VAMOS CORTAR LA FECHA, POR ANIO, MES Y DIA.
        for (int i = 0; i < arrayColores.length; i++) {
            System.out.println(arrayColores[i]);
        }
        
        anio = Integer.parseInt(arrayColores[1]);
        mes = Integer.parseInt(arrayColores[2]);
        dia = Integer.parseInt(arrayColores[3]);
        
        System.out.println("a: "+anio+" m: "+mes+" d: "+dia);
        
        //calendar.set();
        numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
        txtSemana.setText(Integer.toString(numberWeekOfYear));
        System.out.println("f: "+dteFecha.getText());       
       
    }//GEN-LAST:event_dteFechaActionPerformed

    private void jTextField31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField31ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField31ActionPerformed

    private void txtVol10KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol10KeyTyped
        validarDigito(evt);
        if(txtVol10.getText().length() >= 0 && txtVol10.getText().length() < 4){
            cmbCambio10.setEnabled(true);
            txtVol30.setEnabled(true);
            txtDesc20.setEnabled(true);

            cmbProgram20.setEnabled(true);
            txtVol40.setEnabled(true);
            txtDesc10.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[9] = txtVol10.getText();
    }//GEN-LAST:event_txtVol10KeyTyped

    private void txtVol9KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol9KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol9.getText().length() >= 0 && txtVol9.getText().length() < 4){
            cmbProgram10.setEnabled(true);

            cmbCambio9.setEnabled(true);
            txtVol29.setEnabled(true);
            txtDesc19.setEnabled(true);

            cmbProgram19.setEnabled(true);
            txtVol39.setEnabled(true);
            txtDesc9.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[8] = txtVol9.getText();
    }//GEN-LAST:event_txtVol9KeyTyped

    private void txtVol8KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol8KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol8.getText().length() >= 0 && txtVol8.getText().length() < 4){
            cmbProgram9.setEnabled(true);

            cmbCambio8.setEnabled(true);
            txtVol28.setEnabled(true);
            txtDesc18.setEnabled(true);

            cmbProgram18.setEnabled(true);
            txtVol38.setEnabled(true);
            txtDesc8.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[7] = txtVol8.getText();
    }//GEN-LAST:event_txtVol8KeyTyped

    private void txtVol7KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol7KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol7.getText().length() >= 0 && txtVol7.getText().length() < 4){
            cmbProgram8.setEnabled(true);

            cmbCambio7.setEnabled(true);
            txtVol27.setEnabled(true);
            txtDesc17.setEnabled(true);

            cmbProgram17.setEnabled(true);
            txtVol37.setEnabled(true);
            txtDesc7.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[6] = txtVol7.getText();
    }//GEN-LAST:event_txtVol7KeyTyped

    private void txtVol6KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol6KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol6.getText().length() >= 0 && txtVol6.getText().length() < 4){
            cmbProgram7.setEnabled(true);

            cmbCambio6.setEnabled(true);
            txtVol26.setEnabled(true);
            txtDesc16.setEnabled(true);

            cmbProgram16.setEnabled(true);
            txtVol36.setEnabled(true);
            txtDesc6.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[5] = txtVol6.getText();
    }//GEN-LAST:event_txtVol6KeyTyped

    private void txtVol5KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol5KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol5.getText().length() >= 0 && txtVol5.getText().length() < 4){
            cmbProgram6.setEnabled(true);

            cmbCambio5.setEnabled(true);
            txtVol25.setEnabled(true);
            txtDesc15.setEnabled(true);

            cmbProgram15.setEnabled(true);
            txtVol35.setEnabled(true);
            txtDesc5.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }

        //objVolProgramado[4] = txtVol5.getText();
    }//GEN-LAST:event_txtVol5KeyTyped

    private void txtVol4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol4KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol4.getText().length() >= 0 && txtVol4.getText().length() < 4){
            cmbProgram5.setEnabled(true);

            cmbCambio4.setEnabled(true);
            txtVol24.setEnabled(true);
            txtDesc14.setEnabled(true);

            cmbProgram14.setEnabled(true);
            txtVol34.setEnabled(true);
            txtDesc4.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }

        //objVolProgramado[3] = txtVol4.getText();
    }//GEN-LAST:event_txtVol4KeyTyped

    private void txtVol3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol3KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol3.getText().length() >= 0 && txtVol3.getText().length() < 4){
            cmbProgram4.setEnabled(true);

            cmbCambio3.setEnabled(true);
            txtVol23.setEnabled(true);
            txtDesc13.setEnabled(true);

            cmbProgram13.setEnabled(true);
            txtVol33.setEnabled(true);
            txtDesc3.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[2] = txtVol3.getText();
    }//GEN-LAST:event_txtVol3KeyTyped

    private void txtVol2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol2KeyTyped
        // TODO add your handling code here:
        validarDigito(evt);
        if(txtVol2.getText().length() >= 0 && txtVol2.getText().length() < 4){
            cmbProgram3.setEnabled(true);

            cmbCambio2.setEnabled(true);
            txtVol22.setEnabled(true);
            txtDesc12.setEnabled(true);

            cmbProgram12.setEnabled(true);
            txtVol32.setEnabled(true);
            txtDesc2.setEnabled(true);

            btnCargar.setEnabled(true);
        } else {
            evt.consume();
        }
        //objVolProgramado[1] = txtVol2.getText();
    }//GEN-LAST:event_txtVol2KeyTyped

    private void txtVol1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol1KeyTyped
        validarDigito(evt);
        if(txtVol1.getText().length() >= 0 && txtVol1.getText().length() < 4){
            cmbProgram2.setEnabled(true);

            cmbCambio1.setEnabled(true);
            txtVol21.setEnabled(true);
            txtDesc11.setEnabled(true);

            cmbProgram11.setEnabled(true);
            txtVol31.setEnabled(true);
            txtDesc1.setEnabled(true);

            btnCargar.setEnabled(true);

        } else {
            evt.consume();
        }

        //objVolProgramado[0] = txtVol1.getText();
    }//GEN-LAST:event_txtVol1KeyTyped

    private void cmbProgram10ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram10ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram10.getSelectedIndex() > 0){
            txtVol10.setEnabled(true);
            //arrProgramado.add(cmbProgram10.getSelectedItem().toString());
            objProgramado[9] = cmbProgram10.getSelectedItem().toString();
        }else{
            txtVol10.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram10ItemStateChanged

    private void cmbProgram9ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram9ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram9.getSelectedIndex() > 0){
            txtVol9.setEnabled(true);
            //arrProgramado.add(cmbProgram9.getSelectedItem().toString());
            objProgramado[8] = cmbProgram9.getSelectedItem().toString();
        }else{
            txtVol9.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram9ItemStateChanged

    private void cmbProgram8ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram8ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram8.getSelectedIndex() > 0){
            txtVol8.setEnabled(true);
            //arrProgramado.add(cmbProgram8.getSelectedItem().toString());
            objProgramado[7] = cmbProgram8.getSelectedItem().toString();
        }else{
            txtVol8.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram8ItemStateChanged

    private void cmbProgram7ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram7ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram7.getSelectedIndex() > 0){
            txtVol7.setEnabled(true);
            //arrProgramado.add(cmbProgram7.getSelectedItem().toString());
            objProgramado[6] = cmbProgram7.getSelectedItem().toString();
        }else{
            txtVol7.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram7ItemStateChanged

    private void cmbProgram6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram6ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram6.getSelectedIndex() > 0){
            txtVol6.setEnabled(true);
            //arrProgramado.add(cmbProgram6.getSelectedItem().toString());
            objProgramado[5] = cmbProgram6.getSelectedItem().toString();
        }else{
            txtVol6.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram6ItemStateChanged

    private void cmbProgram5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram5ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram5.getSelectedIndex() > 0){
            txtVol5.setEnabled(true);
            objProgramado[4] = cmbProgram5.getSelectedItem().toString();
            //arrProgramado.add(cmbProgram5.getSelectedItem().toString());
        }else{
            txtVol5.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram5ItemStateChanged

    private void cmbProgram4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram4ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram4.getSelectedIndex() > 0){
            txtVol4.setEnabled(true);
            //arrProgramado.add(cmbProgram4.getSelectedItem().toString());
            objProgramado[3] = cmbProgram4.getSelectedItem().toString();
        }else{
            txtVol4.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram4ItemStateChanged

    private void cmbProgram3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram3ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram3.getSelectedIndex() > 0){
            txtVol3.setEnabled(true);
            //arrProgramado.add(cmbProgram3.getSelectedItem().toString());
            objProgramado[2] = cmbProgram3.getSelectedItem().toString();
        }else{
            txtVol3.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram3ItemStateChanged

    private void cmbProgram2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram2ItemStateChanged
        // TODO add your handling code here:
        if(cmbProgram2.getSelectedIndex() > 0){
            txtVol2.setEnabled(true);
            //arrProgramado.add(cmbProgram2.getSelectedItem().toString());
            objProgramado[1] = cmbProgram2.getSelectedItem().toString();
        }else{
            txtVol2.setText("");
            txtVol2.setEnabled(false);
            cmbProgram3.setSelectedIndex(0);
            cmbProgram3.setEnabled(false);
        }
    }//GEN-LAST:event_cmbProgram2ItemStateChanged

    private void cmbProgram1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProgram1ItemStateChanged
        // TODO add your handling code here:
        arrProgramado = new ArrayList<String>();
        if(cmbProgram1.getSelectedIndex() > 0){
            txtVol1.setEnabled(true);
            //txtVol1.setEnabled(true);            //arrProgramado.add(cmbProgram1.getSelectedItem().toString());
            objProgramado[0] = cmbProgram1.getSelectedItem().toString();
        }else{
            txtVol1.setText("");
            txtVol1.setEnabled(false);

            cmbCambio1.setSelectedIndex(0);
            cmbCambio1.setEnabled(false);

            txtVol21.setText("");
            txtVol21.setEnabled(false);

            txtDesc11.setText("");
            txtDesc11.setEnabled(false);

            cmbProgram11.setSelectedIndex(0);
            cmbProgram11.setEnabled(false);

            cmbProgram2.setSelectedIndex(0);
            cmbProgram2.setEnabled(false);

            txtVol31.setText("");
            txtVol31.setEnabled(false);

            txtDesc1.setText("");
            txtDesc1.setEnabled(false);

        }
    }//GEN-LAST:event_cmbProgram1ItemStateChanged

    private void jTextField51ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField51ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField51ActionPerformed

    private void txtVol30KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol30KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol30KeyTyped

    private void txtVol29KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol29KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol29KeyTyped

    private void txtVol28KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol28KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol28KeyTyped

    private void txtVol27KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol27KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol27KeyTyped

    private void txtVol26KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol26KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol26KeyTyped

    private void txtVol25KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol25KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol25KeyTyped

    private void txtVol24KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol24KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol24KeyTyped

    private void txtVol23KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol23KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol23KeyTyped

    private void txtVol22KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol22KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol22KeyTyped

    private void txtVol21KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVol21KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVol21KeyTyped

    private void cmbCambio10ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio10ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio10ItemStateChanged

    private void cmbCambio9ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio9ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio9ItemStateChanged

    private void cmbCambio8ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio8ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio8ItemStateChanged

    private void cmbCambio7ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio7ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio7ItemStateChanged

    private void cmbCambio6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio6ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio6ItemStateChanged

    private void cmbCambio5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio5ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio5ItemStateChanged

    private void cmbCambio4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio4ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio4ItemStateChanged

    private void cmbCambio3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio3ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio3ItemStateChanged

    private void cmbCambio2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio2ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio2ItemStateChanged

    private void cmbCambio1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCambio1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCambio1ItemStateChanged

    private void jTextField23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField23ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField23ActionPerformed

    private void jTextField70ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField70ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField70ActionPerformed

    public void validarCaracter(java.awt.event.KeyEvent evt){
        char c = evt.getKeyChar();
        
        if (c!= '-' && c!= ' ' && (c<'a' || c>'z') && (c<'A' || c>'Z') ){
            evt.consume();
        }
    }
    
    public void validarDigito(java.awt.event.KeyEvent evt){
        char c = evt.getKeyChar();
        
        if ((c<'0' || c>'9')){
            evt.consume();
        }
    }
    
    
    public void llenarObjetos (){    
    /****** PROGRAMADO  ******/
        objProgramado[0] = cmbProgram1.getSelectedItem().toString();
        objProgramado[1] = cmbProgram2.getSelectedItem().toString();
        objProgramado[2] = cmbProgram3.getSelectedItem().toString();
        objProgramado[3] = cmbProgram4.getSelectedItem().toString();
        objProgramado[4] = cmbProgram5.getSelectedItem().toString();
        objProgramado[5] = cmbProgram6.getSelectedItem().toString();
        objProgramado[6] = cmbProgram7.getSelectedItem().toString();
        objProgramado[7] = cmbProgram8.getSelectedItem().toString();
        objProgramado[8] = cmbProgram9.getSelectedItem().toString();
        objProgramado[9] = cmbProgram10.getSelectedItem().toString();        
        //VOLUMNE
        objVolProgramado[0] = txtVol1.getText();
        objVolProgramado[1] = txtVol2.getText();
        objVolProgramado[2] = txtVol3.getText();
        objVolProgramado[3] = txtVol4.getText();
        objVolProgramado[4] = txtVol5.getText();
        objVolProgramado[5] = txtVol6.getText();
        objVolProgramado[6] = txtVol7.getText();
        objVolProgramado[7] = txtVol8.getText();
        objVolProgramado[8] = txtVol9.getText();
        objVolProgramado[9] = txtVol10.getText();
    
    /**** CAMBIO ****/    
        objCambio[0] = cmbCambio1.getSelectedItem().toString();
        objCambio[1] = cmbCambio2.getSelectedItem().toString();
        objCambio[2] = cmbCambio3.getSelectedItem().toString();
        objCambio[3] = cmbCambio4.getSelectedItem().toString();
        objCambio[4] = cmbCambio5.getSelectedItem().toString();
        objCambio[5] = cmbCambio6.getSelectedItem().toString();
        objCambio[6] = cmbCambio7.getSelectedItem().toString();
        objCambio[7] = cmbCambio8.getSelectedItem().toString();
        objCambio[8] = cmbCambio9.getSelectedItem().toString();
        objCambio[9] = cmbCambio10.getSelectedItem().toString();        
        //VOLUMEN
        objVolCambio[0] = txtVol21.getText();
        objVolCambio[1] = txtVol22.getText();
        objVolCambio[2] = txtVol23.getText();
        objVolCambio[3] = txtVol24.getText();
        objVolCambio[4] = txtVol25.getText();
        objVolCambio[5] = txtVol26.getText();
        objVolCambio[6] = txtVol27.getText();
        objVolCambio[7] = txtVol28.getText();        
        objVolCambio[8] = txtVol29.getText();
        objVolCambio[9] = txtVol30.getText();
        //DECRIPCION
        objDescCambio[0] = txtDesc11.getText();
        objDescCambio[1] = txtDesc12.getText();
        objDescCambio[2] = txtDesc13.getText();
        objDescCambio[3] = txtDesc14.getText();
        objDescCambio[4] = txtDesc15.getText();
        objDescCambio[5] = txtDesc16.getText();
        objDescCambio[6] = txtDesc17.getText();
        objDescCambio[7] = txtDesc18.getText();
        objDescCambio[8] = txtDesc19.getText();
        objDescCambio[9] = txtDesc20.getText();       
        
    /***** CAMBIO NO PLANEADO ***/
        objNoProgramado[0] = cmbProgram11.getSelectedItem().toString();
        objNoProgramado[1] = cmbProgram12.getSelectedItem().toString();
        objNoProgramado[2] = cmbProgram13.getSelectedItem().toString();
        objNoProgramado[3] = cmbProgram14.getSelectedItem().toString();
        objNoProgramado[4] = cmbProgram15.getSelectedItem().toString();
        objNoProgramado[5] = cmbProgram16.getSelectedItem().toString();
        objNoProgramado[6] = cmbProgram17.getSelectedItem().toString();
        objNoProgramado[7] = cmbProgram18.getSelectedItem().toString();
        objNoProgramado[8] = cmbProgram19.getSelectedItem().toString();
        objNoProgramado[9] = cmbProgram20.getSelectedItem().toString();
        //VOLUMEN
        objVolNoProgramado[0] = txtVol31.getText();
        objVolNoProgramado[1] = txtVol32.getText();
        objVolNoProgramado[2] = txtVol33.getText();
        objVolNoProgramado[3] = txtVol34.getText();
        objVolNoProgramado[4] = txtVol35.getText();
        objVolNoProgramado[5] = txtVol36.getText();
        objVolNoProgramado[6] = txtVol37.getText();
        objVolNoProgramado[7] = txtVol38.getText();
        objVolNoProgramado[8] = txtVol39.getText();
        objVolNoProgramado[9] = txtVol40.getText();                
        //DESCRIPCION
        objDescNoProgramado[0] = txtDesc1.getText();
        objDescNoProgramado[1] = txtDesc2.getText();
        objDescNoProgramado[2] = txtDesc3.getText();
        objDescNoProgramado[3] = txtDesc4.getText();
        objDescNoProgramado[4] = txtDesc5.getText();
        objDescNoProgramado[5] = txtDesc6.getText();
        objDescNoProgramado[6] = txtDesc7.getText();
        objDescNoProgramado[7] = txtDesc8.getText();
        objDescNoProgramado[8] = txtDesc9.getText();
        objDescNoProgramado[9] = txtDesc10.getText();     
        
    }
                
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BTS1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BTS1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BTS1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BTS1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                BTS1 dialog = new BTS1(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargar;
    private javax.swing.JComboBox<String> cmbCambio1;
    private javax.swing.JComboBox<String> cmbCambio10;
    private javax.swing.JComboBox<String> cmbCambio2;
    private javax.swing.JComboBox<String> cmbCambio3;
    private javax.swing.JComboBox<String> cmbCambio4;
    private javax.swing.JComboBox<String> cmbCambio5;
    private javax.swing.JComboBox<String> cmbCambio6;
    private javax.swing.JComboBox<String> cmbCambio7;
    private javax.swing.JComboBox<String> cmbCambio8;
    private javax.swing.JComboBox<String> cmbCambio9;
    private javax.swing.JComboBox<String> cmbLinea;
    private javax.swing.JComboBox<String> cmbProgram1;
    private javax.swing.JComboBox<String> cmbProgram10;
    private javax.swing.JComboBox<String> cmbProgram11;
    private javax.swing.JComboBox<String> cmbProgram12;
    private javax.swing.JComboBox<String> cmbProgram13;
    private javax.swing.JComboBox<String> cmbProgram14;
    private javax.swing.JComboBox<String> cmbProgram15;
    private javax.swing.JComboBox<String> cmbProgram16;
    private javax.swing.JComboBox<String> cmbProgram17;
    private javax.swing.JComboBox<String> cmbProgram18;
    private javax.swing.JComboBox<String> cmbProgram19;
    private javax.swing.JComboBox<String> cmbProgram2;
    private javax.swing.JComboBox<String> cmbProgram20;
    private javax.swing.JComboBox<String> cmbProgram3;
    private javax.swing.JComboBox<String> cmbProgram4;
    private javax.swing.JComboBox<String> cmbProgram5;
    private javax.swing.JComboBox<String> cmbProgram6;
    private javax.swing.JComboBox<String> cmbProgram7;
    private javax.swing.JComboBox<String> cmbProgram8;
    private javax.swing.JComboBox<String> cmbProgram9;
    private com.alee.extended.date.WebDateField dteFecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField22;
    private javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField25;
    private javax.swing.JTextField jTextField26;
    private javax.swing.JTextField jTextField27;
    private javax.swing.JTextField jTextField28;
    private javax.swing.JTextField jTextField29;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField30;
    private javax.swing.JTextField jTextField31;
    private javax.swing.JTextField jTextField32;
    private javax.swing.JTextField jTextField33;
    private javax.swing.JTextField jTextField34;
    private javax.swing.JTextField jTextField35;
    private javax.swing.JTextField jTextField36;
    private javax.swing.JTextField jTextField37;
    private javax.swing.JTextField jTextField38;
    private javax.swing.JTextField jTextField39;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField40;
    private javax.swing.JTextField jTextField41;
    private javax.swing.JTextField jTextField42;
    private javax.swing.JTextField jTextField43;
    private javax.swing.JTextField jTextField44;
    private javax.swing.JTextField jTextField45;
    private javax.swing.JTextField jTextField46;
    private javax.swing.JTextField jTextField47;
    private javax.swing.JTextField jTextField48;
    private javax.swing.JTextField jTextField49;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField50;
    private javax.swing.JTextField jTextField51;
    private javax.swing.JTextField jTextField52;
    private javax.swing.JTextField jTextField53;
    private javax.swing.JTextField jTextField54;
    private javax.swing.JTextField jTextField55;
    private javax.swing.JTextField jTextField56;
    private javax.swing.JTextField jTextField57;
    private javax.swing.JTextField jTextField58;
    private javax.swing.JTextField jTextField59;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField60;
    private javax.swing.JTextField jTextField61;
    private javax.swing.JTextField jTextField62;
    private javax.swing.JTextField jTextField63;
    private javax.swing.JTextField jTextField64;
    private javax.swing.JTextField jTextField65;
    private javax.swing.JTextField jTextField66;
    private javax.swing.JTextField jTextField67;
    private javax.swing.JTextField jTextField68;
    private javax.swing.JTextField jTextField69;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField70;
    private javax.swing.JTextField jTextField71;
    private javax.swing.JTextField jTextField72;
    private javax.swing.JTextField jTextField73;
    private javax.swing.JTextField jTextField74;
    private javax.swing.JTextField jTextField75;
    private javax.swing.JTextField jTextField76;
    private javax.swing.JTextField jTextField77;
    private javax.swing.JTextField jTextField78;
    private javax.swing.JTextField jTextField79;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField80;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JTextField txtDesc1;
    private javax.swing.JTextField txtDesc10;
    private javax.swing.JTextField txtDesc11;
    private javax.swing.JTextField txtDesc12;
    private javax.swing.JTextField txtDesc13;
    private javax.swing.JTextField txtDesc14;
    private javax.swing.JTextField txtDesc15;
    private javax.swing.JTextField txtDesc16;
    private javax.swing.JTextField txtDesc17;
    private javax.swing.JTextField txtDesc18;
    private javax.swing.JTextField txtDesc19;
    private javax.swing.JTextField txtDesc2;
    private javax.swing.JTextField txtDesc20;
    private javax.swing.JTextField txtDesc3;
    private javax.swing.JTextField txtDesc4;
    private javax.swing.JTextField txtDesc5;
    private javax.swing.JTextField txtDesc6;
    private javax.swing.JTextField txtDesc7;
    private javax.swing.JTextField txtDesc8;
    private javax.swing.JTextField txtDesc9;
    private javax.swing.JTextField txtFamilia;
    private javax.swing.JTextField txtPlaneador;
    private javax.swing.JTextField txtSemana;
    private javax.swing.JTextField txtSupervisor;
    private javax.swing.JTextField txtVol1;
    private javax.swing.JTextField txtVol10;
    private javax.swing.JTextField txtVol2;
    private javax.swing.JTextField txtVol21;
    private javax.swing.JTextField txtVol22;
    private javax.swing.JTextField txtVol23;
    private javax.swing.JTextField txtVol24;
    private javax.swing.JTextField txtVol25;
    private javax.swing.JTextField txtVol26;
    private javax.swing.JTextField txtVol27;
    private javax.swing.JTextField txtVol28;
    private javax.swing.JTextField txtVol29;
    private javax.swing.JTextField txtVol3;
    private javax.swing.JTextField txtVol30;
    private javax.swing.JTextField txtVol31;
    private javax.swing.JTextField txtVol32;
    private javax.swing.JTextField txtVol33;
    private javax.swing.JTextField txtVol34;
    private javax.swing.JTextField txtVol35;
    private javax.swing.JTextField txtVol36;
    private javax.swing.JTextField txtVol37;
    private javax.swing.JTextField txtVol38;
    private javax.swing.JTextField txtVol39;
    private javax.swing.JTextField txtVol4;
    private javax.swing.JTextField txtVol40;
    private javax.swing.JTextField txtVol5;
    private javax.swing.JTextField txtVol6;
    private javax.swing.JTextField txtVol7;
    private javax.swing.JTextField txtVol8;
    private javax.swing.JTextField txtVol9;
    // End of variables declaration//GEN-END:variables
}
