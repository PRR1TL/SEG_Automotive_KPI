package vista;

import control.PrincipalControl;
import dao.RegistroUsuariosDAOImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import utils.PrincipalMetodos;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class EditarUsuario extends javax.swing.JDialog {
    
    RegistroUsuariosDAOImpl insert = new RegistroUsuariosDAOImpl();
    
    String cValor = "";
    String area = "";
    String linea = "";
    String puesto = "";
    String usuario = "";
    String nombre = "";
    String contrasena = "";
    String correo = "";
    
    String privilegios = "";
    /** Creates new form RegistroUsuarios */
    public EditarUsuario(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        if (PrincipalControl.privilegio > 6){
            cmbArea.setEnabled(true);
        }
        
        pasoDatos();
        this.setTitle("Edicion de Usuario Seleccionado");
        this.setLocationRelativeTo(null);
        //cmbLinea.setModel(new PrincipalMetodos().listaLineas());
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblVerificar = new javax.swing.JLabel();
        pwdVerificar = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        pnlBackground = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        lblTipoUsuario = new javax.swing.JLabel();
        cmbTipoUsuario = new javax.swing.JComboBox<>();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        cmbLinea = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        pwdContrasenia = new javax.swing.JPasswordField();
        lblArea = new javax.swing.JLabel();
        cmbArea = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        lblVerificar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblVerificar.setText("Verificar Contraseña: ");

        pwdVerificar.setEnabled(false);

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        pnlBackground.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Edición de Usuarios", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        pnlBackground.setToolTipText("");

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("USUARIO:");

        txtUsuario.setEnabled(false);
        txtUsuario.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtUsuarioCaretUpdate(evt);
            }
        });
        txtUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyTyped(evt);
            }
        });

        lblTipoUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTipoUsuario.setText("PUESTO:");

        cmbTipoUsuario.setActionCommand("_cmbTipoUsuario");
        cmbTipoUsuario.setEnabled(false);
        cmbTipoUsuario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoUsuarioItemStateChanged(evt);
            }
        });

        btnAceptar.setText("Aceptar");
        btnAceptar.setActionCommand("_btnAceptar");
        btnAceptar.setEnabled(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.setActionCommand("_btnCancelar");

        cmbLinea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbLineaItemStateChanged(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("LINEA:");

        txtNombre.setEnabled(false);
        txtNombre.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtNombreCaretUpdate(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("NOMBRE:");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("CONTRASEÑA:");

        pwdContrasenia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pwdContraseniaKeyTyped(evt);
            }
        });

        lblArea.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblArea.setText("ÁREA:");

        cmbArea.setEnabled(false);
        cmbArea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaItemStateChanged(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("CORREO:");

        txtCorreo.setEnabled(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(204, 51, 0));
        jLabel7.setText("*SOLO SI DESEA MODIFICAR LA CONTRASEÑA, LLENE EL CAMPO");

        javax.swing.GroupLayout pnlBackgroundLayout = new javax.swing.GroupLayout(pnlBackground);
        pnlBackground.setLayout(pnlBackgroundLayout);
        pnlBackgroundLayout.setHorizontalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addGroup(pnlBackgroundLayout.createSequentialGroup()
                        .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(lblTipoUsuario)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(29, 29, 29)
                                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblArea, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtUsuario)
                                    .addComponent(cmbArea, 0, 115, Short.MAX_VALUE)))
                            .addComponent(txtNombre)
                            .addComponent(pwdContrasenia, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCorreo))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        pnlBackgroundLayout.setVerticalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblArea)
                        .addComponent(cmbArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblTipoUsuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsuario)
                    .addComponent(jLabel2)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pwdContrasenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBackground, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        int bn = 0;
        try {  
            switch(cmbArea.getSelectedIndex()){
                case 1:
                    area = "GE";
                    break;
                case 2:
                    area = "ST";
                    break;
            }
            
            linea = cmbLinea.getSelectedItem().toString();
            nombre = txtNombre.getText();
            puesto = cmbTipoUsuario.getSelectedItem().toString();
            
            correo = txtCorreo.getText();
            switch (puesto){
                case "Ajustador":
                    privilegios = "1";
                    break;
                case "Team Lider":            
                    privilegios = "2";
                    break;
                case "Supervisor":
                    privilegios = "3";
                    break;
                case "Ing. Proceso":
                    privilegios = "4";
                    break;    
                case "J. Proceso":
                    privilegios = "5";
                    break;
                case "J. Manufactura":
                    privilegios = "6";
                    break;
            }
            
            if (pwdContrasenia.getText().isEmpty()){
                System.out.println(area+", "+linea+", "+usuario+", "+nombre+", "+puesto+", "+privilegios+", "+correo);
                bn = insert.modificaUsuario(area, linea, usuario, nombre, puesto, privilegios, correo);
            } else {
                System.out.println(area+", "+linea+", "+usuario+", "+nombre+", "+puesto+", "+privilegios+", "+correo);
                bn = insert.modificaUsuarioTotal(area, linea, usuario, nombre, puesto, privilegios,pwdContrasenia.getText(),correo);
            }
            
            if ( bn != 0 ){
                this.setVisible(false);
                this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                JOptionPane.showMessageDialog(this, "Usuario ( " + usuario+" ) modificado correctamente.");
            } else {
                JOptionPane.showMessageDialog(this, "El usuario que ingreso ya existe");
            }
        } catch (Exception ex) {
            Logger.getLogger(EditarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void txtUsuarioCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtUsuarioCaretUpdate
//        if (txtUsuario.getText().isEmpty()){
//            txtNombre.setEnabled(false);
//        } else if (txtUsuario.getText().length() >= 5) {
//            txtNombre.setEnabled(true);
//        } else if (txtUsuario.getText().length() < 5) {
//            txtNombre.setEnabled(false);
//        }
//            
    }//GEN-LAST:event_txtUsuarioCaretUpdate

    private void txtNombreCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtNombreCaretUpdate
        // TODO add your handling code here:
//        
//        if (txtNombre.getText().isEmpty()){
//            cmbTipoUsuario.setEnabled(false);
//        } else {
//            cmbTipoUsuario.setEnabled(true);
//        }
    }//GEN-LAST:event_txtNombreCaretUpdate

    private void cmbTipoUsuarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoUsuarioItemStateChanged
//        if (cmbTipoUsuario.getSelectedIndex() != 0){
//            btnAceptar.setEnabled(true);
//        } else {
//            btnAceptar.setEnabled(false);
//        }
    }//GEN-LAST:event_cmbTipoUsuarioItemStateChanged

    private void txtUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyTyped
        // TODO add your handling code here:
//        if (txtUsuario.getText().length() >= 6) {
//            evt.consume();
//        }
    }//GEN-LAST:event_txtUsuarioKeyTyped

    private void cmbLineaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbLineaItemStateChanged
        if (cmbLinea.getSelectedIndex() != 0){
            //txtNombre.setEnabled(true);
            cmbTipoUsuario.setEnabled(true);
            btnAceptar.setEnabled(true);
        } else {
            txtNombre.setEnabled(false);
            cmbTipoUsuario.setEnabled(false);
            btnAceptar.setEnabled(false);
        }
    }//GEN-LAST:event_cmbLineaItemStateChanged

    private void cmbAreaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaItemStateChanged
        // TODO add your handling code here:
        cmbTipoUsuario.setEnabled(false);
        if (cmbArea.getSelectedIndex() != 0){
            cmbTipoUsuario.setEnabled(true);
        }
    }//GEN-LAST:event_cmbAreaItemStateChanged

    private void pwdContraseniaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pwdContraseniaKeyTyped
        // TODO add your handling code here:
        Character c = evt.getKeyChar();
        if (Character.isSpace(c)){
            evt.consume();
        }
    }//GEN-LAST:event_pwdContraseniaKeyTyped

    public JComboBox<String> getCmbLinea() {
        return cmbLinea;
    }
    
    public JButton getBtnAceptar() {
        return btnAceptar;
    }

    public JButton getBtnCancelar() {
        return btnCancelar;
    }

    public JComboBox<String> getCmbTipoUsuario() {
        return cmbTipoUsuario;
    }

    public JPasswordField getPwdVerificar() {
        return pwdVerificar;
    }

    public JTextField getTxtUsuario() {
        return txtUsuario;
    }

    public JTextField getTxtNombre() {
        return txtNombre;
    }
    
    
    public void pasoDatos(){
        cmbArea.addItem("Seleccionar");
            cmbArea.addItem("Alternadores");
            cmbArea.addItem("Marchas");
            
        try {
            cmbTipoUsuario.addItem("Seleccionar");
            
            switch(PrincipalControl.privilegio){
                case 3:
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Team Lider");
                    cValor = new RegistroUsuariosDAOImpl().consultaLineaUser(PrincipalControl.usuario);

                    cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaSupLineas(cValor));
                    
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }                    
                    break;
                    
                case 5: //SOLO PUEDE MODIFICAR ING DE PROCESO Y LINEAS ASIGNADAS
                    cmbTipoUsuario.addItem("Ing. Proceso");
                    cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaJefesLineas(PrincipalControl.area));
                    
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }
                    break;
                    
                case 6: //SOLO PUEDE MODIFICAR DE SUS LINEAS
                    cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaJefesLineas(PrincipalControl.area));
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Team Lider");
                    cmbTipoUsuario.addItem("Supervisor");
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }
                    break;
                    
                case 7:
                    cmbLinea.setModel(new PrincipalMetodos().listaLineas());
                    cmbArea.setSelectedIndex(0);
                    cmbArea.setEnabled(true);
                    cmbTipoUsuario.setEnabled(false);
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Ing. Proceso");
                    cmbTipoUsuario.addItem("J. Manufactura");
                    cmbTipoUsuario.addItem("J. Proceso");
                    cmbTipoUsuario.addItem("Team Lider");
                    cmbTipoUsuario.addItem("Supervisor");
                    break; 
            }
            
            linea = AdministracionUsuarios.lineaT;
            puesto = AdministracionUsuarios.puestoT;
            usuario = AdministracionUsuarios.usuarioT;
            nombre = AdministracionUsuarios.nombreT;
            correo = AdministracionUsuarios.correoT;
            
            cmbLinea.setSelectedItem(linea);
            txtUsuario.setText(usuario);
            txtNombre.setText(nombre);
            cmbTipoUsuario.setSelectedItem(puesto);
            txtCorreo.setText(correo);
            
            //System.out.println("l: "+ linea + " p: "+ puesto +" us: "+usuario+" nom: "+nombre+" correo: "+correo);
        } catch (Exception ex) {
            Logger.getLogger(EditarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbArea;
    private javax.swing.JComboBox<String> cmbLinea;
    private javax.swing.JComboBox<String> cmbTipoUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lblArea;
    private javax.swing.JLabel lblTipoUsuario;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblVerificar;
    private javax.swing.JPanel pnlBackground;
    private javax.swing.JPasswordField pwdContrasenia;
    private javax.swing.JPasswordField pwdVerificar;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

}
