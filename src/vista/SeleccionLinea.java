package vista;

import utils.DatosNoParte;
import control.PrincipalControl;
import dao.LineasDAOImpl;
import java.awt.Cursor;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import utils.PrincipalMetodos;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class SeleccionLinea extends javax.swing.JDialog {

    private final Principal winPrincipal;
    
    /** Creates new form SeleccionLinea */
    public SeleccionLinea(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        winPrincipal = (Principal) parent;
        this.setTitle("SELECCION DE LINEA");
        cmbLinea.setModel(new PrincipalMetodos().listaLineas());
        //pnlLinea.setVisible(false);
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlLinea = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbLinea = new javax.swing.JComboBox<>();
        btnCambiarLinea = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        pnlLinea.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Lineas de producción", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Linea:");

        cmbLinea.setNextFocusableComponent(btnCambiarLinea);
        cmbLinea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbLineaItemStateChanged(evt);
            }
        });
        cmbLinea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbLineaKeyPressed(evt);
            }
        });

        btnCambiarLinea.setText("Aceptar");
        btnCambiarLinea.setToolTipText("");
        btnCambiarLinea.setEnabled(false);
        btnCambiarLinea.setFocusCycleRoot(true);
        btnCambiarLinea.setFocusTraversalPolicyProvider(true);
        btnCambiarLinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambiarLineaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlLineaLayout = new javax.swing.GroupLayout(pnlLinea);
        pnlLinea.setLayout(pnlLineaLayout);
        pnlLineaLayout.setHorizontalGroup(
            pnlLineaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLineaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCambiarLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlLineaLayout.setVerticalGroup(
            pnlLineaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLineaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLineaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCambiarLinea))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlLinea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlLinea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        switch (JOptionPane.showConfirmDialog(this, "¿Seguro que desea salir?",
                "Advertencia", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE)) {
            case 0:
                System.exit(0);
                break;
            case 1:
                this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                break;
        }
    }//GEN-LAST:event_formWindowClosing

    private void btnCambiarLineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambiarLineaActionPerformed
        validarLinea();
    }//GEN-LAST:event_btnCambiarLineaActionPerformed

    private void cmbLineaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbLineaItemStateChanged
        if(cmbLinea.getSelectedIndex() != 0){
            btnCambiarLinea.setEnabled(true);
        }else{
            btnCambiarLinea.setEnabled(false);
        }
    }//GEN-LAST:event_cmbLineaItemStateChanged

    private void cmbLineaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbLineaKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && cmbLinea.getSelectedIndex() != 0 ){
            validarLinea();
        }
    }//GEN-LAST:event_cmbLineaKeyPressed

    public void validarLinea(){
        winPrincipal.getCmbLinea().setSelectedItem(cmbLinea.getSelectedItem());
        DatosNoParte.linea = cmbLinea.getSelectedItem().toString();
        try {
            PrincipalControl.tipoEnsambleLinea = new LineasDAOImpl().tipoEnsambleLinea(cmbLinea.getSelectedItem().toString());
            PrincipalControl.linea = cmbLinea.getSelectedItem().toString();
            winPrincipal.getCmbCliente().setModel(new PrincipalMetodos().listaClientes(cmbLinea.getSelectedItem().toString()));

        } catch (Exception ex) {
            System.out.println("SeleccionLinea.LineasDAOImpl.tipoEnsambleLinea: " + ex);
        }
        this.dispose();
        winPrincipal.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCambiarLinea;
    private javax.swing.JComboBox<String> cmbLinea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel pnlLinea;
    // End of variables declaration//GEN-END:variables

}
