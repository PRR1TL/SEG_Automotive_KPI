package vista;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import control.PrincipalControl;
import dao.RegistroUsuariosDAOImpl;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import utils.PrincipalMetodos;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 * 
 * TIPOS DE USUARIOS
 * 1: AJUSTADOR
 * 2: TEAM LEADER
 * 3: SUPERVISOR
 * 4: ING.PROCESOS
 * 5: J. PROCESOS
 * 6: J. MANUFACTURA
 * 7: ADMIN TOTAL 
 * 
 */


public class RegistroUsuarios extends javax.swing.JDialog {

    RegistroUsuariosDAOImpl insert = new RegistroUsuariosDAOImpl();
    
    String cValor = "";    
    String linea = "";
    String puesto = "";
    String area = "";
    String usuario = "";
    String nombre = "";
    String apellido = "";
    String contrasena = "";
    String privilegios = "";
    String correo = "";
    
    /** Creates new form RegistroUsuarios */
    public RegistroUsuarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        try {
            initComponents();
            this.setLocationRelativeTo(null);
            this.setTitle("NUEVO USUARIO"); 
            
            setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage()); 
                        
            cmbArea.addItem("Seleccionar"); 
            cmbArea.addItem("Alternadores"); 
            cmbArea.addItem("Marchas"); 
            
            cmbArea.setEnabled(false); 
            //cmbTipoUsuario.setEnabled(true); 
            cmbTipoUsuario.addItem("Seleccionar"); 
            
            System.out.println("vista.RegistroUsuarios.<init>().Area ->  "+PrincipalControl.area); 
            //cmbCadenaValor.setModel(new RegistroUsuariosDAOImpl().listaCadenaValor(PrincipalControl.area));
            
            System.out.println("PrincipalControl.area: "+PrincipalControl.area);
            switch(PrincipalControl.privilegio){ 
                case 3: //SUPERVISORES 
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Team Lider"); 
                    cmbCadenaValor.setModel(new RegistroUsuariosDAOImpl().consultaCadValorProducto(PrincipalControl.area));
                    //cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaSupLineas(cValor));
                    
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }
                    
                    //cmbCadenaValor.setSelectedItem(cValor);
                    cmbCadenaValor.setEnabled(true);
                    break;
                case 5: //SOLO PUEDE MODIFICAR ING DE PROCESO Y LINEAS ASIGNADAS
                    cmbTipoUsuario.addItem("Ing. Proceso");
                    cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaJefesLineas(PrincipalControl.area));
                    
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }
                    break;
                case 6: //SOLO PUEDE MODIFICAR DE SUS LINEAS
                    cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaJefesLineas(PrincipalControl.area));
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Team Lider");
                    cmbTipoUsuario.addItem("Supervisor");
                    if (PrincipalControl.area.contentEquals("ST")){
                        cmbArea.setSelectedIndex(2);
                    } else {
                        cmbArea.setSelectedIndex(1);
                    }
                    break;
                case 7:
                    cmbLinea.setModel(new PrincipalMetodos().listaLineas());
                    cmbArea.setSelectedIndex(0);
                    cmbArea.setEnabled(true);
                    //cmbTipoUsuario.setEnabled(true);
                    cmbArea.setEnabled(true);
                    cmbTipoUsuario.addItem("Ajustador");
                    cmbTipoUsuario.addItem("Ing. Proceso");
                    cmbTipoUsuario.addItem("J. Manufactura");
                    cmbTipoUsuario.addItem("J. Proceso");
                    cmbTipoUsuario.addItem("Team Lider");
                    cmbTipoUsuario.addItem("Supervisor");
                    break; 
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBackground = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        lblContrasena = new javax.swing.JLabel();
        pwdContrasena = new javax.swing.JPasswordField();
        lblTipoUsuario = new javax.swing.JLabel();
        cmbTipoUsuario = new javax.swing.JComboBox<>();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        cmbLinea = new javax.swing.JComboBox<>();
        lblLinea = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        lblNombre = new javax.swing.JLabel();
        lblArea = new javax.swing.JLabel();
        cmbArea = new javax.swing.JComboBox<>();
        lblEmail = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lblArea1 = new javax.swing.JLabel();
        cmbCadenaValor = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        lblApellido = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        pnlBackground.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Registro de Usuarios", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));
        pnlBackground.setToolTipText("");

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUsuario.setText("USUARIO:");

        txtUsuario.setEnabled(false);
        txtUsuario.setNextFocusableComponent(txtNombre);
        txtUsuario.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtUsuarioCaretUpdate(evt);
            }
        });
        txtUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyTyped(evt);
            }
        });

        lblContrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblContrasena.setText("CONTRASEÑA:");

        pwdContrasena.setEnabled(false);
        pwdContrasena.setNextFocusableComponent(btnAceptar);
        pwdContrasena.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                pwdContrasenaCaretUpdate(evt);
            }
        });
        pwdContrasena.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pwdContrasenaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pwdContrasenaKeyTyped(evt);
            }
        });

        lblTipoUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTipoUsuario.setText("PUESTO:");

        cmbTipoUsuario.setActionCommand("_cmbTipoUsuario");
        cmbTipoUsuario.setEnabled(false);
        cmbTipoUsuario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoUsuarioItemStateChanged(evt);
            }
        });

        btnAceptar.setText("Aceptar");
        btnAceptar.setActionCommand("_btnAceptar");
        btnAceptar.setEnabled(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.setActionCommand("_btnCancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        cmbLinea.setEnabled(false);
        cmbLinea.setNextFocusableComponent(txtUsuario);
        cmbLinea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbLineaItemStateChanged(evt);
            }
        });

        lblLinea.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblLinea.setText("LINEA:");

        txtNombre.setEnabled(false);
        txtNombre.setNextFocusableComponent(txtApellido);
        txtNombre.setOpaque(false);
        txtNombre.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtNombreCaretUpdate(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNombreKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        lblNombre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNombre.setText("NOMBRE:");

        lblArea.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblArea.setText("ÁREA:");

        cmbArea.setEnabled(false);
        cmbArea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaItemStateChanged(evt);
            }
        });

        lblEmail.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEmail.setText("CORREO:");

        txtEmail.setEnabled(false);
        txtEmail.setNextFocusableComponent(pwdContrasena);
        txtEmail.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtEmailCaretUpdate(evt);
            }
        });
        txtEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtEmailKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtEmailKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        jLabel2.setText("@seg-automotive.com");

        lblArea1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblArea1.setText("CADENA DE VALOR:");

        cmbCadenaValor.setEnabled(false);
        cmbCadenaValor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCadenaValorItemStateChanged(evt);
            }
        });

        lblApellido.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblApellido.setText("APELLIDOS:");

        txtApellido.setEnabled(false);
        txtApellido.setNextFocusableComponent(txtEmail);
        txtApellido.setOpaque(false);
        txtApellido.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtApellidoCaretUpdate(evt);
            }
        });
        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtApellidoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtApellidoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout pnlBackgroundLayout = new javax.swing.GroupLayout(pnlBackground);
        pnlBackground.setLayout(pnlBackgroundLayout);
        pnlBackgroundLayout.setHorizontalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlBackgroundLayout.createSequentialGroup()
                        .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTipoUsuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblContrasena, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                                .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(pwdContrasena, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlBackgroundLayout.createSequentialGroup()
                        .addComponent(lblLinea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(270, 270, 270)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBackgroundLayout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(lblArea, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbArea, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblArea1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbCadenaValor, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlBackgroundLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlBackgroundLayout.setVerticalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBackgroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblArea1, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addComponent(lblArea)
                        .addComponent(cmbArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cmbCadenaValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLinea))
                .addGap(6, 6, 6)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsuario)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblApellido))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmail)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblContrasena)
                    .addComponent(pwdContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancelar))
                .addGap(66, 66, 66))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(7, 7, 7))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        try {
            validacion();
        } catch (Exception ex) {
            Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void cmbLineaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbLineaItemStateChanged
        // TODO add your handling code here: 
        cmbTipoUsuario.setSelectedIndex(0);
        if (cmbLinea.getItemCount() == 0){            
            cmbTipoUsuario.setEnabled(false);
        } else {
            cmbTipoUsuario.setEnabled(true);
        }
    }//GEN-LAST:event_cmbLineaItemStateChanged

    private void txtUsuarioCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtUsuarioCaretUpdate
        if (txtUsuario.getText().isEmpty()){
            txtNombre.setEnabled(false);
            txtNombre.setText("");
        } else if (txtUsuario.getText().length() >= 6) {
            txtNombre.setEnabled(true);
        } else if (txtUsuario.getText().length() <= 5) {
            txtNombre.setEnabled(false);
        }            
    }//GEN-LAST:event_txtUsuarioCaretUpdate

    private void txtNombreCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtNombreCaretUpdate
        // TODO add your handling code here:
        if (txtNombre.getText().length() > 3 ){ 
            txtApellido.setEnabled(true);                        
        } else {
            txtApellido.setText("");
            txtApellido.setEnabled(false);
        }
    }//GEN-LAST:event_txtNombreCaretUpdate

    private void pwdContrasenaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_pwdContrasenaCaretUpdate
        if (pwdContrasena.getText().isEmpty() || pwdContrasena.getText().length() <= 4 ){
            btnAceptar.setEnabled(false);
        } else {
            btnAceptar.setEnabled(true);
        }
    }//GEN-LAST:event_pwdContrasenaCaretUpdate

    private void cmbTipoUsuarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoUsuarioItemStateChanged
        txtUsuario.setText("");
        if (cmbTipoUsuario.getSelectedIndex() != 0){
            if (cmbTipoUsuario.getSelectedIndex() == 5 ){
                
            }
            txtUsuario.setEnabled(true);
        } else {
            txtUsuario.setEnabled(false);
        }
        
    }//GEN-LAST:event_cmbTipoUsuarioItemStateChanged

    private void txtUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyTyped
        // TODO add your handling code here:
        Character c = evt.getKeyChar();
        if (txtUsuario.getText().length() < 6 ) {
            if (txtUsuario.getText().length() == 3 && Character.isLetter(c)){ //(evt.getKeyChar() < '0'  ||  evt.getKeyChar() > '9') ){ //ke.getKeyChar() < '0' || ke.getKeyChar() > '9'
                evt.consume();
            } else if (txtUsuario.getText().length() != 3 && !Character.isLetter(c)) {
                evt.consume();
            }
        } else { 
            evt.consume();
        }
    }//GEN-LAST:event_txtUsuarioKeyTyped

    private void txtUsuarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyReleased
        // TODO add your handling code here:
        String cadena= (txtUsuario.getText()).toUpperCase();
        txtUsuario.setText(cadena);
    }//GEN-LAST:event_txtUsuarioKeyReleased

    private void txtNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyReleased
        // TODO add your handling code here:        
        String cadena= (txtNombre.getText()).toUpperCase();
        txtNombre.setText(cadena);   
    }//GEN-LAST:event_txtNombreKeyReleased

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar(); 
        if (!Character.isLetter(c) && !Character.isSpace(c)){
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void cmbAreaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaItemStateChanged
        // TODO add your handling code here: 
        
        cmbCadenaValor.setEnabled(false);
        if (cmbArea.getSelectedIndex() != 0){ 
            try { 
                cmbCadenaValor.setEnabled(true); 
                cmbCadenaValor.setModel(new RegistroUsuariosDAOImpl().listaCadenaValor(PrincipalControl.area));
                cmbCadenaValor.setSelectedIndex(0); 
            } catch (Exception ex) { 
                Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }//GEN-LAST:event_cmbAreaItemStateChanged

    private void txtEmailCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtEmailCaretUpdate
        // TODO add your handling code here:
        if (txtEmail.getText().isEmpty()){
            pwdContrasena.setEnabled(false);
            pwdContrasena.setText("");
        } else {
            pwdContrasena.setEnabled(false);
            if (txtEmail.getText().length() > 10){
                pwdContrasena.setEnabled(true);
            }
        }
    }//GEN-LAST:event_txtEmailCaretUpdate

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        cmbTipoUsuario.setSelectedIndex(0);
        cmbLinea.setSelectedIndex(0);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void pwdContrasenaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pwdContrasenaKeyTyped
        // TODO add your handling code here:
        Character c = evt.getKeyChar();
        if (Character.isSpace(c)){
            evt.consume();
        }
        
        if (evt.getKeyCode() == 10 && !pwdContrasena.getText().isEmpty()) {
            try {                      
                validacion();
            } catch (Exception ex) {
                Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }//GEN-LAST:event_pwdContrasenaKeyTyped

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && txtNombre.getText().length() > 10 ) {
            txtNombre.transferFocus(); 
        }
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtEmailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmailKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && txtEmail.getText().length() > 10 ) {
            txtEmail.transferFocus(); 
        }
    }//GEN-LAST:event_txtEmailKeyPressed

    private void txtUsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && txtUsuario.getText().length() > 5 ) {
            txtUsuario.transferFocus(); 
        }
    }//GEN-LAST:event_txtUsuarioKeyPressed

    private void pwdContrasenaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pwdContrasenaKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && pwdContrasena.getText().length() >= 5 ) {
            try {
                //pwdContrasena.transferFocus();
                validacion();
            } catch (Exception ex) {
                Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_pwdContrasenaKeyPressed

    private void txtEmailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmailKeyReleased
        // TODO add your handling code here:
        String cadena= (txtEmail.getText()).toLowerCase();
        txtEmail.setText(cadena); 
    }//GEN-LAST:event_txtEmailKeyReleased

    private void cmbCadenaValorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCadenaValorItemStateChanged
        // TODO add your handling code here: 
        cValor = cmbCadenaValor.getSelectedItem().toString(); 
        try { 
            if (cmbCadenaValor.getSelectedIndex() != 0){ 
                cmbLinea.setModel(new RegistroUsuariosDAOImpl().listaSupLineas(cValor)); 
                cmbLinea.setEnabled(true);
            } else {
                cmbLinea.setEnabled(false);
                cmbTipoUsuario.setEditable(false);
            }             
        } catch (Exception ex) { 
            Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex); 
        } 
    }//GEN-LAST:event_cmbCadenaValorItemStateChanged

    private void txtApellidoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtApellidoCaretUpdate
        // TODO add your handling code here:
        if (txtApellido.getText().length() > 3 ){ 
            txtEmail.setEnabled(true);                        
        } else {
            txtEmail.setText("");
            txtEmail.setEnabled(false);
        }
    }//GEN-LAST:event_txtApellidoCaretUpdate

    private void txtApellidoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10 && txtApellido.getText().length() > 10 ) {
            txtApellido.transferFocus(); 
        }
    }//GEN-LAST:event_txtApellidoKeyPressed

    private void txtApellidoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyReleased
        // TODO add your handling code here:
        String cadena= (txtApellido.getText()).toUpperCase();
        txtApellido.setText(cadena);   
    }//GEN-LAST:event_txtApellidoKeyReleased

    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
        // TODO add your handling code here:
        char c = evt.getKeyChar(); 
        if (!Character.isLetter(c) && !Character.isSpace(c)){
            evt.consume();
        }
    }//GEN-LAST:event_txtApellidoKeyTyped

    public void validacion() throws Exception{
        int bn = 0;
        int existe = 0;
        
        existe = insert.consultaExisteUser(txtUsuario.getText());
        if (existe != 0 ){
            JOptionPane.showMessageDialog(this, "EL USUARIO ("+txtUsuario.getText()+") SE ENCUENTRA REGISTRADO");
        } else { 
            try {           
                linea = cmbLinea.getSelectedItem().toString();
                usuario = txtUsuario.getText();
                nombre = txtNombre.getText();
                apellido = txtApellido.getText();
                puesto = cmbTipoUsuario.getSelectedItem().toString(); 

                switch (puesto){
                    case "Ajustador":
                        privilegios = "1";
                        break;
                    case "Team Lider":            
                        privilegios = "2";
                        break;
                    case "Supervisor":
                        privilegios = "3";
                        break;
                    case "Ing. Proceso":
                        privilegios = "4";
                        break;
                    case "J. Proceso":
                        privilegios = "5";
                        break;
                    case "J. Manufactura":
                        privilegios = "6";
                        break;
                }

                //PARA EL AREA
                switch (cmbArea.getSelectedIndex()){
                    case 1:
                        area = "GE";
                        break;
                    case 2:            
                        area = "ST";
                        break;                
                }

                contrasena = pwdContrasena.getText();
                //VALIDACION PARA COMPROBAR SI EL CORREO LO ESCRIBIERON CON @
                correo = txtEmail.getText()+"@seg-automotive.com"; 
                
                bn = insert.insertarUsuario(area, linea, usuario, nombre, apellido, puesto, privilegios, correo, contrasena); 
                if ( bn != 0 ){ 
                    this.setVisible(false); 
                    JOptionPane.showMessageDialog(this, "Usuario ( " + usuario+" ) creado correctamente."); 
                } 
            } catch (Exception ex) { 
                Logger.getLogger(RegistroUsuarios.class.getName()).log(Level.SEVERE, null, ex); 
            } 
        }
    }
    
    
    public JComboBox<String> getCmbLinea() {
        return cmbLinea;
    }    
    
    public JButton getBtnAceptar() {
        return btnAceptar;
    }

    public JButton getBtnCancelar() {
        return btnCancelar;
    }

    public JComboBox<String> getCmbTipoUsuario() {
        return cmbTipoUsuario;
    }

    public JPasswordField getPwdContraseña() {
        return pwdContrasena;
    }

    public JTextField getTxtUsuario() {
        return txtUsuario;
    }

    public JTextField getTxtNombre() {
        return txtNombre;
    }
    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox<String> cmbArea;
    private javax.swing.JComboBox<String> cmbCadenaValor;
    private javax.swing.JComboBox<String> cmbLinea;
    private javax.swing.JComboBox<String> cmbTipoUsuario;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblArea;
    private javax.swing.JLabel lblArea1;
    private javax.swing.JLabel lblContrasena;
    private javax.swing.JLabel lblEmail;
    private javax.swing.JLabel lblLinea;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblTipoUsuario;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JPanel pnlBackground;
    private javax.swing.JPasswordField pwdContrasena;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

}
