package vista;

import control.PrincipalControl;
import static control.PrincipalControl.fecha;
import static control.PrincipalControl.linea;
import dao.TiempoTurnoDAOImpl;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.prompt.PromptSupport;
import utils.PrincipalMetodos;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class TiempoTurno extends javax.swing.JDialog {

    public static int auxiliarTiempoTurno;
    private boolean existeHorario;
    private boolean cerrarVentana;
    private Principal winPrincipal;
    
    
    
    public static  ArrayList cmbTiempoModel = new ArrayList();
    
    /** Creates new form TiempoTurno */    
    public TiempoTurno(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        PromptSupport.setPrompt("0-24 hrs",txtHoraInicial);
        PromptSupport.setPrompt("0-24 hrs",txtHoraFinal);
        PromptSupport.setPrompt("máx 30", txtnNoPersonas);
        winPrincipal = (Principal) parent;
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage());
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbTurno = new javax.swing.JComboBox<>();
        txtHoraInicial = new javax.swing.JTextField();
        txtHoraFinal = new javax.swing.JTextField();
        txtnNoPersonas = new javax.swing.JTextField();
        pnlDatosProducir = new javax.swing.JPanel();
        lblCliente = new javax.swing.JLabel();
        cmbCliente = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        cmbNoParte = new javax.swing.JComboBox<>();
        txtTiempoCiclo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnValidar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TIEMPO DE TRABAJO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("TURNO:");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("HORA INICIO:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("HORA FIN:");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("NO. PERSONAS:");

        cmbTurno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Primer Turno", "Segundo Turno", "Segundo Complementario", "Tercer Turno", "Tercer Complementario" }));
        cmbTurno.setEnabled(false);
        cmbTurno.setNextFocusableComponent(txtHoraInicial);
        cmbTurno.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTurnoItemStateChanged(evt);
            }
        });
        cmbTurno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbTurnoKeyPressed(evt);
            }
        });

        txtHoraInicial.setEnabled(false);
        txtHoraInicial.setNextFocusableComponent(txtHoraFinal);
        txtHoraInicial.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtHoraInicialCaretUpdate(evt);
            }
        });
        txtHoraInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHoraInicialKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHoraInicialKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtHoraInicialKeyTyped(evt);
            }
        });

        txtHoraFinal.setEnabled(false);
        txtHoraFinal.setNextFocusableComponent(txtnNoPersonas);
        txtHoraFinal.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtHoraFinalCaretUpdate(evt);
            }
        });
        txtHoraFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHoraFinalKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHoraFinalKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtHoraFinalKeyTyped(evt);
            }
        });

        txtnNoPersonas.setEnabled(false);
        txtnNoPersonas.setNextFocusableComponent(btnValidar);
        txtnNoPersonas.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtnNoPersonasCaretUpdate(evt);
            }
        });
        txtnNoPersonas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtnNoPersonasKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnNoPersonasKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnNoPersonasKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtnNoPersonas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                        .addComponent(txtHoraFinal, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtHoraInicial, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtHoraInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtnNoPersonas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlDatosProducir.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DE PRODUCCIÓN", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        lblCliente.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCliente.setText("CLIENTE:");
        lblCliente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        cmbCliente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbClienteItemStateChanged(evt);
            }
        });
        cmbCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbClienteActionPerformed(evt);
            }
        });
        cmbCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbClienteKeyPressed(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("NO. PARTE:");
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        cmbNoParte.setEnabled(false);
        cmbNoParte.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNoParteItemStateChanged(evt);
            }
        });
        cmbNoParte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbNoParteKeyPressed(evt);
            }
        });

        txtTiempoCiclo.setEnabled(false);
        txtTiempoCiclo.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtTiempoCicloCaretUpdate(evt);
            }
        });
        txtTiempoCiclo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTiempoCicloKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTiempoCicloKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTiempoCicloKeyTyped(evt);
            }
        });

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("TIEMPO CICLO:");
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel7.setText("SEGUNDOS");

        javax.swing.GroupLayout pnlDatosProducirLayout = new javax.swing.GroupLayout(pnlDatosProducir);
        pnlDatosProducir.setLayout(pnlDatosProducirLayout);
        pnlDatosProducirLayout.setHorizontalGroup(
            pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosProducirLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbCliente, 0, 150, Short.MAX_VALUE)
                    .addComponent(cmbNoParte, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlDatosProducirLayout.createSequentialGroup()
                        .addComponent(txtTiempoCiclo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlDatosProducirLayout.setVerticalGroup(
            pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDatosProducirLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCliente))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbNoParte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDatosProducirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTiempoCiclo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(9, 9, 9))
        );

        btnValidar.setText("Aceptar");
        btnValidar.setEnabled(false);
        btnValidar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnValidar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                        .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pnlDatosProducir, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlDatosProducir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnValidar)
                    .addComponent(btnLimpiar))
                .addGap(9, 9, 9))
        );

        jPanel1.getAccessibleContext().setAccessibleName("");
        pnlDatosProducir.getAccessibleContext().setAccessibleName("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnValidarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarActionPerformed
        int ini = Integer.parseInt(txtHoraInicial.getText());
        int fin = Integer.parseInt(txtHoraFinal.getText());
        if (ini < fin){
            if (PrincipalControl.privilegio != 0){
                //ENTRA LA VALIDACION DE LOGIN
                validacionInsertar();
            }        
        } else {
            JOptionPane.showMessageDialog(this, "La hora final no puede ser mayor a la inicial");
        }        
    }//GEN-LAST:event_btnValidarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        cmbCliente.setSelectedIndex(0);
        //cmbTurno.setSelectedIndex(0);
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtnNoPersonasCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtnNoPersonasCaretUpdate
        if ((evt.getDot() + evt.getMark()) == 0) {
            btnValidar.setEnabled(false);
        } else if (evt.getDot() >= 1 && evt.getMark() >= 1) {
            btnValidar.setEnabled(true);
        } else if (txtnNoPersonas.getText().isEmpty()) {
            btnValidar.setEnabled(false);
        }
    }//GEN-LAST:event_txtnNoPersonasCaretUpdate

    private void cmbTurnoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTurnoItemStateChanged
        if (cmbTurno.getSelectedIndex() != 0) {
            txtHoraInicial.setText("");
            txtHoraInicial.setEnabled(true);
        } else {
            winPrincipal.getCmbCliente().setSelectedItem(cmbCliente.getSelectedItem().toString());
            winPrincipal.getCmbNoParte().setSelectedItem(cmbNoParte.getSelectedItem().toString());
            winPrincipal.getTxtTcP().setText(txtTiempoCiclo.getText());
            
            txtHoraInicial.setText("");
            txtHoraInicial.setEnabled(false);
        }
    }//GEN-LAST:event_cmbTurnoItemStateChanged

    private void txtHoraInicialCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtHoraInicialCaretUpdate
        if ((evt.getDot() + evt.getMark()) == 0) {
            txtHoraFinal.setEnabled(false);
            txtHoraFinal.setText("");
        } else if (evt.getDot() >= 1 && evt.getMark() >= 1) {
            txtHoraFinal.setEnabled(true);
        } else if (txtHoraInicial.getText().isEmpty()) {
            txtHoraFinal.setEnabled(false);
            txtHoraFinal.setText("");
        }
    }//GEN-LAST:event_txtHoraInicialCaretUpdate

    private void txtHoraFinalCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtHoraFinalCaretUpdate
        if ((evt.getDot() + evt.getMark()) == 0) {
            txtnNoPersonas.setEnabled(false);
            txtnNoPersonas.setText("");
        } else if (txtHoraInicial.getText().length() == txtHoraFinal.getText().length()){//evt.getDot() >= 1 && evt.getMark() >= 1) {
            txtnNoPersonas.setEnabled(true);
        } else if (txtHoraFinal.getText().isEmpty()) {
            txtnNoPersonas.setEnabled(false);
            txtnNoPersonas.setText("");
        }
    }//GEN-LAST:event_txtHoraFinalCaretUpdate

    private void txtHoraInicialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraInicialKeyTyped
        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9')) {
            evt.consume();
        } else if (txtHoraInicial.getText().length() >= 2) {
            evt.consume();
        }
    }//GEN-LAST:event_txtHoraInicialKeyTyped

    private void txtHoraFinalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraFinalKeyTyped
        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9')) {
            evt.consume();
        } else if (txtHoraFinal.getText().length() >= 2) {
            evt.consume();
        }
    }//GEN-LAST:event_txtHoraFinalKeyTyped

    private void txtnNoPersonasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnNoPersonasKeyTyped
        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9')) {
            evt.consume();
        } else if (txtnNoPersonas.getText().length() >= 2) {
            evt.consume();
        }
    }//GEN-LAST:event_txtnNoPersonasKeyTyped

    private void txtHoraInicialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraInicialKeyReleased
        if (!txtHoraInicial.getText().isEmpty()) {
            txtHoraFinal.setEnabled(true);
            if (Integer.parseInt(txtHoraInicial.getText()) > 23
                    || Integer.parseInt(txtHoraInicial.getText()) < 0 ) {
                txtHoraInicial.setText("");
                txtHoraFinal.setText("");
            }else if(!txtHoraFinal.getText().isEmpty()
                    && Integer.parseInt(txtHoraInicial.getText()) >= Integer.parseInt(txtHoraFinal.getText())
                    && txtHoraFinal.getText().length() == txtHoraInicial.getText().length()){
                txtHoraFinal.setText("");
            }
        }else {
            txtHoraFinal.setText("");
        }
    }//GEN-LAST:event_txtHoraInicialKeyReleased

    private void txtHoraFinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraFinalKeyReleased
        //String d
        String hF = txtHoraFinal.getText();
        if (txtHoraInicial.getText().length() <= 1){
            String val = txtHoraInicial.getText();
            txtHoraInicial.setText("0"+val);
            txtHoraFinal.setText(hF);
        }
        
        if (!txtHoraFinal.getText().isEmpty()) {
            if (Integer.parseInt(txtHoraFinal.getText()) > 24
                || Integer.parseInt(txtHoraFinal.getText()) < 0 ) {
                txtHoraFinal.setText("");
            }else if ( !txtHoraFinal.getText().isEmpty() && !txtHoraInicial.getText().isEmpty() && 
                    Integer.parseInt(txtHoraInicial.getText()) >= Integer.parseInt(txtHoraFinal.getText())
                    && txtHoraFinal.getText().length() == txtHoraInicial.getText().length()){
                txtHoraFinal.setText("");
            }
        }
    }//GEN-LAST:event_txtHoraFinalKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
  
    }//GEN-LAST:event_formWindowClosed

    private void txtnNoPersonasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnNoPersonasKeyReleased
        if (!txtnNoPersonas.getText().isEmpty()) {
            if (Integer.parseInt(txtnNoPersonas.getText()) > 30
                    || Integer.parseInt(txtnNoPersonas.getText()) < 1) {
                txtnNoPersonas.setText("");
            }
        }
    }//GEN-LAST:event_txtnNoPersonasKeyReleased

    private void cmbTurnoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbTurnoKeyPressed
        if (evt.getKeyCode() == 10 && cmbTurno.getSelectedIndex() != 0) {
            cmbTurno.transferFocus();
        }
    }//GEN-LAST:event_cmbTurnoKeyPressed

    private void txtHoraInicialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraInicialKeyPressed
        
        if (evt.getKeyCode() == 10 && !txtHoraInicial.getText().isEmpty()) {
            txtHoraInicial.transferFocus();
            if (txtHoraInicial.getText().length() <= 1){
                String val = txtHoraInicial.getText();
                txtHoraInicial.setText("0"+val);
            }
        }
    }//GEN-LAST:event_txtHoraInicialKeyPressed

    private void txtHoraFinalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraFinalKeyPressed
        if (evt.getKeyCode() == 10 && !txtHoraFinal.getText().isEmpty()) {
            txtHoraFinal.requestFocus();
        }
    }//GEN-LAST:event_txtHoraFinalKeyPressed

    private void txtnNoPersonasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnNoPersonasKeyPressed
        if (evt.getKeyCode() == 10 && !txtnNoPersonas.getText().isEmpty()) {
            //txtnNoPersonas.transferFocus();
            txtnNoPersonas.requestFocus();
            validacionInsertar();
        }
    }//GEN-LAST:event_txtnNoPersonasKeyPressed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (winPrincipal.getLblTurno().getText().isEmpty()) {
            if (JOptionPane.showConfirmDialog(this, "¿Seguro que desea salir?", "Mensaje",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
                this.setVisible(false);
                //System.exit(0);
            }
        } else {
            if (JOptionPane.showConfirmDialog(this, "¿Seguro que desea salir?", "Mensaje",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
                //System.exit(0);
                this.setVisible(false);
            }
        }
    }//GEN-LAST:event_formWindowClosing

    private void txtTiempoCicloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTiempoCicloKeyTyped
        // TODO add your handling code here:        
        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9') && evt.getKeyChar() != '.') {            
            evt.consume();
        } else {            
            
            switch(txtTiempoCiclo.getText().length()){
                case 0:
                case 1:
                case 3:
                    if (evt.getKeyChar() == '.'){
                        evt.consume();
                    }
                    break;
                case 2:
                    if (evt.getKeyChar() != '.'){
                        evt.consume();
                    }
                    break;
            }     

            if(txtTiempoCiclo.getText().length() > 3 ){
                evt.consume();
            }
        }
    }//GEN-LAST:event_txtTiempoCicloKeyTyped

    private void txtTiempoCicloKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTiempoCicloKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyChar() < '0' && evt.getKeyChar() > '9' && evt.getKeyChar() != '.' ){
            evt.consume();
        } else {
            if (evt.getKeyCode() == 10){
                validacionNoParte();
            }
        }
    }//GEN-LAST:event_txtTiempoCicloKeyPressed

    private void txtTiempoCicloCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtTiempoCicloCaretUpdate
        // TODO add your handling code here:
        if (txtTiempoCiclo.getText().isEmpty()){
            cmbTurno.setEnabled(false);
            cmbTurno.setSelectedIndex(0);
        } else {
//            if ( Integer.parseInt(txtTiempoCiclo.getText()) < 1 ){
//                txtTiempoCiclo.setText("");
//                JOptionPane.showMessageDialog(this, "REVISAR TIEMPO CICLO");
//            }
            
            if (txtTiempoCiclo.getText().length() == 2 && Double.parseDouble(txtTiempoCiclo.getText()) > 1.0 ){
                cmbTurno.setEnabled(true);  
            }
        }        
    }//GEN-LAST:event_txtTiempoCicloCaretUpdate

    private void cmbNoParteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbNoParteKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == 10){
            //validacionNoParte();
            txtTiempoCiclo.transferFocus();
        }
    }//GEN-LAST:event_cmbNoParteKeyPressed

    private void cmbNoParteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNoParteItemStateChanged
        // TODO add your handling code here:
        if (cmbNoParte.getSelectedIndex() != 0 ){
            txtTiempoCiclo.setEnabled(true);
        }else{
            txtTiempoCiclo.setEnabled(false);
        }
        txtTiempoCiclo.setText("");
    }//GEN-LAST:event_cmbNoParteItemStateChanged

    private void cmbClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbClienteKeyPressed

    }//GEN-LAST:event_cmbClienteKeyPressed

    private void cmbClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbClienteActionPerformed
        // TODO add your handling code here:

        //cmbNoParte.transferFocus();
    }//GEN-LAST:event_cmbClienteActionPerformed

    private void cmbClienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbClienteItemStateChanged
        // TODO add your handling code here:
        //System.out.println("vista.DatosNoParte.cmbClienteItemStateChanged()"+cmbCliente.getItemCount());
        if (cmbCliente.getSelectedIndex() != 0 ){
            cmbNoParte.setModel(new PrincipalMetodos().listaNoPartes(linea, cmbCliente.getSelectedItem().toString()));            
            cmbNoParte.setEnabled(true);
        } else {
            cmbNoParte.setSelectedIndex(0);
            cmbNoParte.setEnabled(false);
        }
        winPrincipal.getCmbNoParte().setModel(new PrincipalMetodos().listaNoPartes(linea, cmbCliente.getSelectedItem().toString()));
        txtTiempoCiclo.setEnabled(false);
        txtTiempoCiclo.setText("");
    }//GEN-LAST:event_cmbClienteItemStateChanged

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        fecha = winPrincipal.getDteFecha().getText();
        //System.out.println("vista.TiempoTurno.formWindowOpened(): "+fecha);
        
        String linea = winPrincipal.getCmbLinea().getSelectedItem().toString();
        
        cmbCliente.setModel(new PrincipalMetodos().listaClientes(linea));
        if (winPrincipal.getCmbCliente().getItemCount() != 0 && winPrincipal.getCmbCliente().getSelectedIndex() != 0){
            cmbCliente.setSelectedItem(winPrincipal.getCmbCliente().getSelectedItem());
        } else {
            //PrincipalMetodos().listaClientes(linea));
            winPrincipal.getCmbCliente().setModel(new PrincipalMetodos().listaClientes(linea));
            cmbTurno.setEnabled(false);
        }           
    }//GEN-LAST:event_formWindowOpened

    private void txtTiempoCicloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTiempoCicloKeyReleased
        // TODO add your handling code here:
        if (txtTiempoCiclo.getText().length() == 2){
            if ( Double.parseDouble(txtTiempoCiclo.getText()) < 1.0 ){
                txtTiempoCiclo.setText("");
                //JOptionPane.showMessageDialog(this, "REVISAR TIEMPO CICLO");
            }
        }
    }//GEN-LAST:event_txtTiempoCicloKeyReleased

     public void validacionNoParte(){
        if(!txtTiempoCiclo.getText().isEmpty()){
            winPrincipal.getCmbCliente().setSelectedItem(cmbCliente.getSelectedItem().toString());
            winPrincipal.getCmbNoParte().setSelectedItem(cmbNoParte.getSelectedItem().toString());
            winPrincipal.getTxtTcP().setText(txtTiempoCiclo.getText());
            winPrincipal.getTxtTcP().setEnabled(true);  
            PrincipalControl.bnEntrada = 0;
            //this.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(this, "Debe tener todos los datos");
        }        
    }    
    
    public void validacionInsertar(){          
        try {
            existeHorario = new TiempoTurnoDAOImpl().horarioExisteBitacora(
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText(),
                    cmbTurno.getSelectedItem().toString()                        
            );
            if (!existeHorario) { 
                int horaInicial = Integer.parseInt(txtHoraInicial.getText());
                int horaFinal = Integer.parseInt(txtHoraFinal.getText());
                //MANDAMOS LA LEYENDA DEL TURNO ACTUAL
                winPrincipal.getLblTurno().setText(cmbTurno.getSelectedItem().toString()
                        + " <> " + horaInicial + " - " + horaFinal);
                winPrincipal.getCmbCliente().setSelectedItem(cmbCliente.getSelectedItem().toString());
                winPrincipal.getCmbNoParte().setSelectedItem(cmbNoParte.getSelectedItem().toString());
                winPrincipal.getCmbNoParte().setEnabled(false);
                winPrincipal.getTxtTcP().setText(txtTiempoCiclo.getText());
                winPrincipal.getTxtTcP().setEnabled(true);

                new TiempoTurnoDAOImpl().registrarHorarioBitacora(
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText(),
                    String.valueOf(cmbTurno.getSelectedItem()),
                    Integer.parseInt(txtHoraInicial.getText()),
                    Integer.parseInt(txtHoraFinal.getText()),
                    Integer.parseInt(txtnNoPersonas.getText())
                );

                //APARTADO PARA MANDAR LAS HORAS AL COMBO DE HORAS
                if (horaInicial > horaFinal) {
                    cmbTiempoModel.clear();
                    cmbTiempoModel.add("Seleccionar");
                    for (int i = horaInicial; i != 0; i++) {
                        if (i == 24) {
                            i = 0;
                        } else if (i == horaFinal) {
                            break;
                        }
                        cmbTiempoModel.add(i);
                    }
                } else {
                    int duracionTurno = horaFinal - horaInicial;
                    cmbTiempoModel.clear();
                    cmbTiempoModel.add("Seleccionar");
                    for (int i = 0, j = horaInicial; i < duracionTurno; i++, j++) {
                        cmbTiempoModel.add(j);
                    }
                }                 

                //MANDAMOS LA LEYENDA DEL TURNO ACTUAL
                winPrincipal.getLblTurno().setText(cmbTurno.getSelectedItem().toString()
                        + " <> " + horaInicial + " - " + horaFinal);

                winPrincipal.getCmbHora().setModel(new DefaultComboBoxModel(cmbTiempoModel.toArray()));
                winPrincipal.getCmbTema().setEnabled(true);
                winPrincipal.getMniOperaciones().setEnabled(true);
                winPrincipal.getMniCargaMasiva().setEnabled(true);
                cerrarVentana = true;

                winPrincipal.getDteFecha().setEditable(false);
                winPrincipal.getDteFecha().setEnabled(false);

                winPrincipal.getBtnCancelar().setVisible(true);
                winPrincipal.getBtnCambiarLinea().setVisible(true);

                this.dispose();

            } else {   
                String[] options2 = {"Si", "No"};

                int seleccion =  JOptionPane.showOptionDialog(this,"Turno ya registrado en la bitacora\n"+"Desea editar los registros del turno ("+cmbTurno.getSelectedItem().toString()+")", 
                        "Advertencia", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options2, options2[0]);

                if (seleccion == 0){
                    this.dispose();
                    fecha = winPrincipal.getDteFecha().getText();
                    linea = winPrincipal.getCmbLinea().getSelectedItem().toString();
                    new SelecTurno(winPrincipal, true).setVisible(true);
                }
                cmbTurno.setSelectedIndex(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "TiempoTurno.btnIniciarActionPerformed()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }  
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnValidar;
    private javax.swing.JComboBox<String> cmbCliente;
    private javax.swing.JComboBox<String> cmbNoParte;
    private javax.swing.JComboBox<String> cmbTurno;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblCliente;
    private javax.swing.JPanel pnlDatosProducir;
    private javax.swing.JTextField txtHoraFinal;
    private javax.swing.JTextField txtHoraInicial;
    private javax.swing.JTextField txtTiempoCiclo;
    private javax.swing.JTextField txtnNoPersonas;
    // End of variables declaration//GEN-END:variables

}
