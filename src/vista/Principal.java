package vista;

import com.alee.extended.date.WebDateField;
import com.alee.extended.panel.WebCollapsiblePane;
import control.PrincipalControl;
import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.JTextField;
import org.jdesktop.swingx.prompt.PromptSupport;
import utils.PrincipalMetodos;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class Principal extends javax.swing.JFrame {
    private final PrincipalMetodos principalMetodos = new PrincipalMetodos();
    
    int contEDateFecha = 0;
    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        PromptSupport.setPrompt("Minuto Inicio",txtTiempoInicio);
        PromptSupport.setPrompt("Minuto Fin",txtTiempoFin);
        //btnAceptarNoParte.setVisible(false);
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        setIconImage(new ImageIcon(getClass().getResource("/imagenes/bs2.png")).getImage());
       
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pmnClickMenu = new javax.swing.JPopupMenu();
        mniEliminar = new javax.swing.JMenuItem();
        pnlGeneral = new javax.swing.JPanel();
        lblCantPzas = new javax.swing.JLabel();
        txtCantPzas = new javax.swing.JTextField();
        lblOperacion1 = new javax.swing.JLabel();
        cmbOperacion1 = new javax.swing.JComboBox<>();
        lblArea1 = new javax.swing.JLabel();
        cmbArea1 = new javax.swing.JComboBox<>();
        lblProblema1 = new javax.swing.JLabel();
        cmbProblema1 = new javax.swing.JComboBox<>();
        lblScrap1 = new javax.swing.JLabel();
        txtScrap1 = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        lblCliente1 = new javax.swing.JLabel();
        cmbCliente1 = new javax.swing.JComboBox<>();
        lblNoParte1 = new javax.swing.JLabel();
        cmbNoParte1 = new javax.swing.JComboBox<>();
        lblMatFaltante1 = new javax.swing.JLabel();
        txtMatFaltante1 = new javax.swing.JTextField();
        pnl2 = new javax.swing.JPanel();
        lblProblema2 = new javax.swing.JLabel();
        lblArea2 = new javax.swing.JLabel();
        txtScrap2 = new javax.swing.JTextField();
        cmbProblema2 = new javax.swing.JComboBox<>();
        lblScrap2 = new javax.swing.JLabel();
        lblOperacion2 = new javax.swing.JLabel();
        cmbOperacion2 = new javax.swing.JComboBox<>();
        cmbArea2 = new javax.swing.JComboBox<>();
        lblCliente2 = new javax.swing.JLabel();
        cmbCliente2 = new javax.swing.JComboBox<>();
        lblNoParte2 = new javax.swing.JLabel();
        cmbNoParte2 = new javax.swing.JComboBox<>();
        lblMatFaltante2 = new javax.swing.JLabel();
        txtMatFaltante2 = new javax.swing.JTextField();
        lblTema2 = new javax.swing.JLabel();
        cmbTema2 = new javax.swing.JComboBox<>();
        jSeparator7 = new javax.swing.JSeparator();
        lblDur2 = new javax.swing.JLabel();
        txtDur2 = new javax.swing.JTextField();
        pnl3 = new javax.swing.JPanel();
        lblProblema3 = new javax.swing.JLabel();
        lblArea3 = new javax.swing.JLabel();
        txtScrap3 = new javax.swing.JTextField();
        cmbProblema3 = new javax.swing.JComboBox<>();
        lblScrap3 = new javax.swing.JLabel();
        lblOperacion3 = new javax.swing.JLabel();
        cmbOperacion3 = new javax.swing.JComboBox<>();
        cmbArea3 = new javax.swing.JComboBox<>();
        lblCliente3 = new javax.swing.JLabel();
        cmbCliente3 = new javax.swing.JComboBox<>();
        lblNoParte3 = new javax.swing.JLabel();
        cmbNoParte3 = new javax.swing.JComboBox<>();
        lblMatFaltante3 = new javax.swing.JLabel();
        txtMatFaltante3 = new javax.swing.JTextField();
        lblTema3 = new javax.swing.JLabel();
        cmbTema3 = new javax.swing.JComboBox<>();
        jSeparator11 = new javax.swing.JSeparator();
        lblDur3 = new javax.swing.JLabel();
        txtDur3 = new javax.swing.JTextField();
        pnl4 = new javax.swing.JPanel();
        lblProblema4 = new javax.swing.JLabel();
        lblArea4 = new javax.swing.JLabel();
        txtScrap4 = new javax.swing.JTextField();
        cmbProblema4 = new javax.swing.JComboBox<>();
        lblScrap4 = new javax.swing.JLabel();
        lblOperacion4 = new javax.swing.JLabel();
        cmbOperacion4 = new javax.swing.JComboBox<>();
        cmbArea4 = new javax.swing.JComboBox<>();
        lblCliente4 = new javax.swing.JLabel();
        cmbCliente4 = new javax.swing.JComboBox<>();
        lblNoParte4 = new javax.swing.JLabel();
        cmbNoParte4 = new javax.swing.JComboBox<>();
        lblMatFaltante4 = new javax.swing.JLabel();
        txtMatFaltante4 = new javax.swing.JTextField();
        lblTema4 = new javax.swing.JLabel();
        cmbTema4 = new javax.swing.JComboBox<>();
        jSeparator16 = new javax.swing.JSeparator();
        lblDur4 = new javax.swing.JLabel();
        txtDur4 = new javax.swing.JTextField();
        pnlBackground = new javax.swing.JPanel();
        pnlBitacora = new javax.swing.JPanel();
        scrBitacora = new javax.swing.JScrollPane();
        tblBitacora = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        };
        tblBitacora = new javax.swing.JTable();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        pnlBorderNorth = new javax.swing.JPanel();
        lblTema = new javax.swing.JLabel();
        cmbTema = new javax.swing.JComboBox();
        lblLinea = new javax.swing.JLabel();
        cmbLinea = new javax.swing.JComboBox();
        lblFecha = new javax.swing.JLabel();
        dteFecha = new com.alee.extended.date.WebDateField();
        btnCambiarLinea = new javax.swing.JButton();
        lblTurno = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbCliente = new javax.swing.JComboBox<>();
        cmbNoParte = new javax.swing.JComboBox<>();
        txtTcP = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        pnlProduccionCollapsible = new com.alee.extended.panel.WebCollapsiblePane();
        pnlTiempoIncidencia = new javax.swing.JPanel();
        btnAgregarBitacora = new javax.swing.JButton();
        btnRevisarHoras = new javax.swing.JButton();
        btnParoPeriodo = new javax.swing.JButton();
        pnlTiempoIncidenciaHora = new javax.swing.JPanel();
        lblHora = new javax.swing.JLabel();
        cmbHora = new javax.swing.JComboBox();
        lblInicio = new javax.swing.JLabel();
        txtTiempoInicio = new javax.swing.JTextField();
        lblFin = new javax.swing.JLabel();
        txtTiempoFin = new javax.swing.JTextField();
        lblDuracion = new javax.swing.JLabel();
        txtDuracion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        mnuBarra = new javax.swing.JMenuBar();
        mnuAdministrar = new javax.swing.JMenu();
        mniBTS = new javax.swing.JMenuItem();
        jSeparator18 = new javax.swing.JPopupMenu.Separator();
        mniCargaMasiva = new javax.swing.JMenuItem();
        jSeparator19 = new javax.swing.JPopupMenu.Separator();
        mniOperaciones = new javax.swing.JMenuItem();
        jSeparator13 = new javax.swing.JPopupMenu.Separator();
        mniCrearUsuario = new javax.swing.JMenuItem();
        jSeparator14 = new javax.swing.JPopupMenu.Separator();
        jMenu1 = new javax.swing.JMenu();
        mniLineas = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mniNewNoParte = new javax.swing.JMenuItem();
        mnuEditar = new javax.swing.JMenu();
        mniEditarPorDia = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        mniEditarPorTurno = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mniERangoTurno = new javax.swing.JMenuItem();
        mnuGraficas = new javax.swing.JMenu();
        mniListarDia = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        mniHourlyCount = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        mniIndicadores = new javax.swing.JMenuItem();
        mnuOtros = new javax.swing.JMenu();
        mniAbout = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        mniManuales = new javax.swing.JMenuItem();
        mnuSesion = new javax.swing.JMenu();
        mniSesion = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mniAdminU = new javax.swing.JMenuItem();

        mniEliminar.setText("Eliminar");
        mniEliminar.setActionCommand("_mniEliminar");
        pmnClickMenu.add(mniEliminar);

        pnlGeneral.setFocusable(false);

        lblCantPzas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCantPzas.setText("cant");

        txtCantPzas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        txtCantPzas.setNextFocusableComponent(cmbHora);

        lblOperacion1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOperacion1.setText("op");

        lblArea1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblArea1.setText("are");

        lblProblema1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProblema1.setText("prob");

        lblScrap1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblScrap1.setText("sc");

        txtScrap1.setText(" ");

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/addB.png"))); // NOI18N
        btnAdd.setActionCommand("_btnAdd");

        btnRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/deleteB.png"))); // NOI18N
        btnRemove.setActionCommand("_btnRemove");

        lblCliente1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCliente1.setText("cl");
        lblCliente1.setPreferredSize(null);

        lblNoParte1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNoParte1.setText("No");
        lblNoParte1.setPreferredSize(null);

        lblMatFaltante1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMatFaltante1.setText("mat");

        txtMatFaltante1.setText(" ");

        lblProblema2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProblema2.setText("prob");

        lblArea2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblArea2.setText("are");

        txtScrap2.setText(" ");

        lblScrap2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblScrap2.setText("sc");

        lblOperacion2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOperacion2.setText("op");

        lblCliente2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCliente2.setText("cl");

        lblNoParte2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNoParte2.setText("No");

        lblMatFaltante2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMatFaltante2.setText("mat");

        txtMatFaltante2.setText(" ");

        lblTema2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTema2.setText("tem");

        cmbTema2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Calidad", "Tecnicas", "Organizacionales", "Cambio de Modelo" }));
        cmbTema2.setActionCommand("_cmbTema2");

        lblDur2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDur2.setText("Dur");

        txtDur2.setText(" ");

        javax.swing.GroupLayout pnl2Layout = new javax.swing.GroupLayout(pnl2);
        pnl2.setLayout(pnl2Layout);
        pnl2Layout.setHorizontalGroup(
            pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTema2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbTema2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblOperacion2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbOperacion2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblArea2, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbArea2, 0, 150, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblProblema2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbProblema2, 0, 240, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMatFaltante2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtMatFaltante2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCliente2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbCliente2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblNoParte2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbNoParte2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblScrap2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtScrap2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDur2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtDur2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
            .addComponent(jSeparator7)
        );
        pnl2Layout.setVerticalGroup(
            pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl2Layout.createSequentialGroup()
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDur2)
                        .addComponent(txtDur2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblScrap2)
                        .addComponent(txtScrap2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblNoParte2)
                        .addComponent(cmbNoParte2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCliente2)
                        .addComponent(cmbCliente2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMatFaltante2)
                        .addComponent(txtMatFaltante2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProblema2)
                        .addComponent(cmbProblema2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblArea2)
                        .addComponent(cmbArea2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblTema2)
                        .addComponent(cmbTema2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblOperacion2)
                        .addComponent(cmbOperacion2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        lblProblema3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProblema3.setText("prob");

        lblArea3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblArea3.setText("are");

        txtScrap3.setText(" ");

        lblScrap3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblScrap3.setText("sc");

        lblOperacion3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOperacion3.setText("op");

        lblCliente3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCliente3.setText("cl");

        lblNoParte3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNoParte3.setText("No");

        lblMatFaltante3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMatFaltante3.setText("mat");

        txtMatFaltante3.setText(" ");

        lblTema3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTema3.setText("tem");

        cmbTema3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Calidad", "Tecnicas", "Organizacionales", "Cambio de Modelo" }));
        cmbTema3.setActionCommand("_cmbTema3");

        lblDur3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDur3.setText("Dur");

        txtDur3.setText(" ");

        javax.swing.GroupLayout pnl3Layout = new javax.swing.GroupLayout(pnl3);
        pnl3.setLayout(pnl3Layout);
        pnl3Layout.setHorizontalGroup(
            pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator11)
            .addGroup(pnl3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTema3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbTema3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblOperacion3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbOperacion3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblArea3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbArea3, 0, 150, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblProblema3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbProblema3, 0, 240, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMatFaltante3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtMatFaltante3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCliente3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbCliente3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblNoParte3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbNoParte3, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblScrap3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtScrap3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDur3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtDur3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnl3Layout.setVerticalGroup(
            pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl3Layout.createSequentialGroup()
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCliente3)
                        .addComponent(cmbCliente3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProblema3)
                        .addComponent(cmbProblema3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMatFaltante3)
                        .addComponent(txtMatFaltante3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblScrap3)
                        .addComponent(txtScrap3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDur3)
                        .addComponent(txtDur3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblOperacion3)
                        .addComponent(cmbOperacion3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTema3)
                        .addComponent(cmbTema3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblArea3)
                        .addComponent(cmbArea3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblNoParte3)
                        .addComponent(cmbNoParte3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17))
        );

        lblProblema4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProblema4.setText("prob");

        lblArea4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblArea4.setText("are");

        txtScrap4.setText(" ");

        lblScrap4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblScrap4.setText("sc");

        lblOperacion4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOperacion4.setText("op");

        lblCliente4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCliente4.setText("cl");

        lblNoParte4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNoParte4.setText("No");

        lblMatFaltante4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMatFaltante4.setText("mat");

        txtMatFaltante4.setText(" ");

        lblTema4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTema4.setText("tem");

        cmbTema4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Calidad", "Tecnicas", "Organizacionales", "Cambio de Modelo" }));
        cmbTema4.setActionCommand("_cmbTema4");

        lblDur4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDur4.setText("Dur");

        txtDur4.setText(" ");

        javax.swing.GroupLayout pnl4Layout = new javax.swing.GroupLayout(pnl4);
        pnl4.setLayout(pnl4Layout);
        pnl4Layout.setHorizontalGroup(
            pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator16)
            .addGroup(pnl4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTema4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbTema4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(lblOperacion4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbOperacion4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblArea4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbArea4, 0, 150, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblProblema4, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbProblema4, 0, 240, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMatFaltante4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtMatFaltante4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCliente4, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbCliente4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblNoParte4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbNoParte4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblScrap4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtScrap4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDur4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtDur4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnl4Layout.setVerticalGroup(
            pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl4Layout.createSequentialGroup()
                .addComponent(jSeparator16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblOperacion4)
                        .addComponent(cmbOperacion4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblArea4)
                        .addComponent(cmbArea4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProblema4)
                        .addComponent(cmbProblema4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTema4)
                        .addComponent(cmbTema4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblMatFaltante4)
                        .addComponent(txtMatFaltante4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblScrap4)
                        .addComponent(txtScrap4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCliente4)
                        .addComponent(cmbCliente4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblNoParte4)
                        .addComponent(cmbNoParte4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDur4)
                        .addComponent(txtDur4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnlGeneralLayout = new javax.swing.GroupLayout(pnlGeneral);
        pnlGeneral.setLayout(pnlGeneralLayout);
        pnlGeneralLayout.setHorizontalGroup(
            pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGeneralLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCantPzas, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtCantPzas, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblOperacion1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbOperacion1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblArea1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbArea1, 0, 150, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblProblema1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbProblema1, 0, 248, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMatFaltante1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtMatFaltante1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(lblNoParte1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(cmbNoParte1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(lblScrap1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtScrap1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
            .addGroup(pnlGeneralLayout.createSequentialGroup()
                .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnl3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnl2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnl4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlGeneralLayout.setVerticalGroup(
            pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGeneralLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMatFaltante1)
                        .addComponent(txtMatFaltante1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblScrap1)
                        .addComponent(txtScrap1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmbCliente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblNoParte1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cmbNoParte1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCantPzas)
                        .addComponent(txtCantPzas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblOperacion1)
                        .addComponent(cmbOperacion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblArea1)
                        .addComponent(cmbArea1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblProblema1)
                        .addComponent(cmbProblema1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Programa para Bítacora y reportes de producción");
        setIconImage(getIconImage());
        addWindowStateListener(new java.awt.event.WindowStateListener() {
            public void windowStateChanged(java.awt.event.WindowEvent evt) {
                formWindowStateChanged(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        pnlBackground.setFocusable(false);
        pnlBackground.setLayout(new java.awt.BorderLayout(0, 5));

        pnlBitacora.setBorder(javax.swing.BorderFactory.createTitledBorder("Bítacora:"));
        pnlBitacora.setToolTipText("");
        pnlBitacora.setFocusable(false);
        pnlBitacora.setName(""); // NOI18N

        scrBitacora.setFocusable(false);

        tblBitacora.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "LINEA", "FECHA", "HORA", "INICIO", "FIN", "DURACION", "TEMA", "OPERACION", "AREA", "PROBLEMA", "CLIENTE", "# PARTE", "CANTIDAD", "# PARTE CAMBIO", "SCRAP", "D. MATERIAL", "T. CICLO", "*"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        JTableHeader header = tblBitacora.getTableHeader();
        header.setPreferredSize(new Dimension(
            header.getPreferredSize().width, header.getPreferredSize().height * 2
        )
    );
    tblBitacora.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
    tblBitacora.setComponentPopupMenu(pmnClickMenu);
    tblBitacora.setFocusable(false);
    tblBitacora.setIntercellSpacing(new java.awt.Dimension(10, 10));
    scrBitacora.setViewportView(tblBitacora);
    if (tblBitacora.getColumnModel().getColumnCount() > 0) {
        tblBitacora.getColumnModel().getColumn(0).setMinWidth(60);
        tblBitacora.getColumnModel().getColumn(0).setPreferredWidth(60);
        tblBitacora.getColumnModel().getColumn(0).setMaxWidth(90);
        tblBitacora.getColumnModel().getColumn(1).setMinWidth(85);
        tblBitacora.getColumnModel().getColumn(1).setPreferredWidth(85);
        tblBitacora.getColumnModel().getColumn(1).setMaxWidth(115);
        tblBitacora.getColumnModel().getColumn(2).setMinWidth(58);
        tblBitacora.getColumnModel().getColumn(2).setPreferredWidth(58);
        tblBitacora.getColumnModel().getColumn(2).setMaxWidth(58);
        tblBitacora.getColumnModel().getColumn(3).setMinWidth(57);
        tblBitacora.getColumnModel().getColumn(3).setPreferredWidth(57);
        tblBitacora.getColumnModel().getColumn(3).setMaxWidth(60);
        tblBitacora.getColumnModel().getColumn(4).setMinWidth(50);
        tblBitacora.getColumnModel().getColumn(4).setPreferredWidth(50);
        tblBitacora.getColumnModel().getColumn(4).setMaxWidth(65);
        tblBitacora.getColumnModel().getColumn(5).setMinWidth(80);
        tblBitacora.getColumnModel().getColumn(5).setPreferredWidth(80);
        tblBitacora.getColumnModel().getColumn(5).setMaxWidth(80);
        tblBitacora.getColumnModel().getColumn(6).setMinWidth(20);
        tblBitacora.getColumnModel().getColumn(6).setPreferredWidth(20);
        tblBitacora.getColumnModel().getColumn(7).setMinWidth(100);
        tblBitacora.getColumnModel().getColumn(7).setPreferredWidth(100);
        tblBitacora.getColumnModel().getColumn(7).setMaxWidth(100);
        tblBitacora.getColumnModel().getColumn(8).setMinWidth(20);
        tblBitacora.getColumnModel().getColumn(8).setPreferredWidth(20);
        tblBitacora.getColumnModel().getColumn(10).setMinWidth(73);
        tblBitacora.getColumnModel().getColumn(10).setPreferredWidth(73);
        tblBitacora.getColumnModel().getColumn(10).setMaxWidth(73);
        tblBitacora.getColumnModel().getColumn(11).setMinWidth(105);
        tblBitacora.getColumnModel().getColumn(11).setPreferredWidth(105);
        tblBitacora.getColumnModel().getColumn(11).setMaxWidth(105);
        tblBitacora.getColumnModel().getColumn(12).setMinWidth(95);
        tblBitacora.getColumnModel().getColumn(12).setPreferredWidth(95);
        tblBitacora.getColumnModel().getColumn(12).setMaxWidth(95);
        tblBitacora.getColumnModel().getColumn(13).setMinWidth(120);
        tblBitacora.getColumnModel().getColumn(13).setPreferredWidth(120);
        tblBitacora.getColumnModel().getColumn(13).setMaxWidth(120);
        tblBitacora.getColumnModel().getColumn(14).setMinWidth(60);
        tblBitacora.getColumnModel().getColumn(14).setPreferredWidth(60);
        tblBitacora.getColumnModel().getColumn(14).setMaxWidth(60);
        tblBitacora.getColumnModel().getColumn(15).setMinWidth(105);
        tblBitacora.getColumnModel().getColumn(15).setPreferredWidth(105);
        tblBitacora.getColumnModel().getColumn(15).setMaxWidth(105);
        tblBitacora.getColumnModel().getColumn(16).setMinWidth(76);
        tblBitacora.getColumnModel().getColumn(16).setPreferredWidth(76);
        tblBitacora.getColumnModel().getColumn(16).setMaxWidth(76);
        tblBitacora.getColumnModel().getColumn(17).setMinWidth(22);
        tblBitacora.getColumnModel().getColumn(17).setPreferredWidth(22);
        tblBitacora.getColumnModel().getColumn(17).setMaxWidth(22);
    }

    btnGuardar.setText("Guardar Bitacora");
    btnGuardar.setActionCommand("_btnGuardar");
    btnGuardar.setFocusable(false);
    btnGuardar.setNextFocusableComponent(cmbLinea);

    btnCancelar.setText("Cancelar");
    btnCancelar.setActionCommand("_btnCancelar");
    btnCancelar.setFocusable(false);
    btnCancelar.setNextFocusableComponent(cmbLinea);

    javax.swing.GroupLayout pnlBitacoraLayout = new javax.swing.GroupLayout(pnlBitacora);
    pnlBitacora.setLayout(pnlBitacoraLayout);
    pnlBitacoraLayout.setHorizontalGroup(
        pnlBitacoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlBitacoraLayout.createSequentialGroup()
            .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(1096, Short.MAX_VALUE))
        .addComponent(scrBitacora)
    );
    pnlBitacoraLayout.setVerticalGroup(
        pnlBitacoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBitacoraLayout.createSequentialGroup()
            .addGap(5, 5, 5)
            .addComponent(scrBitacora, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBitacoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnGuardar)
                .addComponent(btnCancelar))
            .addGap(0, 0, 0))
    );

    pnlBorderNorth.setFocusable(false);

    lblTema.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblTema.setText("TEMA");

    cmbTema.setActionCommand("_cmbTema");
    cmbTema.setEnabled(false);
    cmbTema.setFocusTraversalPolicyProvider(true);
    cmbTema.setNextFocusableComponent(txtCantPzas);
    cmbTema.setVerifyInputWhenFocusTarget(false);
    cmbTema.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            cmbTemaActionPerformed(evt);
        }
    });

    lblLinea.setText("LINEA");

    cmbLinea.setEditable(true);
    cmbLinea.setActionCommand("_cmbLinea");
    cmbLinea.setEnabled(false);
    cmbLinea.setFocusable(false);

    lblFecha.setText("FECHA");

    dteFecha.setEnabled(false);
    dteFecha.addCaretListener(new javax.swing.event.CaretListener() {
        public void caretUpdate(javax.swing.event.CaretEvent evt) {
            dteFechaCaretUpdate(evt);
        }
    });
    dteFecha.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusGained(java.awt.event.FocusEvent evt) {
            dteFechaFocusGained(evt);
        }
        public void focusLost(java.awt.event.FocusEvent evt) {
            dteFechaFocusLost(evt);
        }
    });

    btnCambiarLinea.setText("Iniciar Turno");
    btnCambiarLinea.setActionCommand("_btnCambiarLinea");
    btnCambiarLinea.setFocusable(false);
    btnCambiarLinea.setNextFocusableComponent(cmbLinea);

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS DE NUMERO DE PARTE"));

    jLabel3.setText("CLIENTE");

    jLabel4.setText("NO. PARTE");

    cmbCliente.setActionCommand("_cmbCliente");
    cmbCliente.setEnabled(false);

    cmbNoParte.setActionCommand("_cmbNoParte");
    cmbNoParte.setEnabled(false);

    txtTcP.setEnabled(false);

    jLabel1.setText("TIEMPO CICLO");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jLabel1)
                .addComponent(jLabel4)
                .addComponent(jLabel3))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cmbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cmbNoParte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtTcP, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel3)
                .addComponent(cmbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel4)
                .addComponent(cmbNoParte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtTcP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel1))
            .addContainerGap())
    );

    pnlProduccionCollapsible.setFocusable(false);
    pnlProduccionCollapsible.setNextFocusableComponent(pnlProduccionCollapsible);
    pnlProduccionCollapsible.setTitle("Datos de producción");

    pnlTiempoIncidencia.setFocusable(false);

    btnAgregarBitacora.setText("Agregar a Bitacora");
    btnAgregarBitacora.setActionCommand("_btnAgregarBitacora");
    btnAgregarBitacora.setEnabled(false);

    btnRevisarHoras.setText("Revisar Horas");
    btnRevisarHoras.setActionCommand("_btnRevisarHoras");
    btnRevisarHoras.setEnabled(false);
    btnRevisarHoras.setFocusable(false);

    btnParoPeriodo.setText("Paro por Periodo");
    btnParoPeriodo.setActionCommand("cmbParoPeriodo");

    pnlTiempoIncidenciaHora.setBorder(javax.swing.BorderFactory.createTitledBorder("Tiempo de Incidencia:"));
    pnlTiempoIncidenciaHora.setFocusable(false);

    lblHora.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    lblHora.setText("HORA");

    cmbHora.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccionar", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));
    cmbHora.setActionCommand("_cmbHora");
    cmbHora.setEnabled(false);

    lblInicio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    lblInicio.setText("INICIO");

    txtTiempoInicio.setEnabled(false);

    lblFin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    lblFin.setText("FIN");

    txtTiempoFin.setEnabled(false);

    lblDuracion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
    lblDuracion.setText("DURACIÓN");

    txtDuracion.setEditable(false);
    txtDuracion.setEnabled(false);

    javax.swing.GroupLayout pnlTiempoIncidenciaHoraLayout = new javax.swing.GroupLayout(pnlTiempoIncidenciaHora);
    pnlTiempoIncidenciaHora.setLayout(pnlTiempoIncidenciaHoraLayout);
    pnlTiempoIncidenciaHoraLayout.setHorizontalGroup(
        pnlTiempoIncidenciaHoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTiempoIncidenciaHoraLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(lblHora)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cmbHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(lblInicio)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txtTiempoInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(lblFin)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txtTiempoFin, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(lblDuracion)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txtDuracion, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );
    pnlTiempoIncidenciaHoraLayout.setVerticalGroup(
        pnlTiempoIncidenciaHoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTiempoIncidenciaHoraLayout.createSequentialGroup()
            .addGroup(pnlTiempoIncidenciaHoraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblHora)
                .addComponent(cmbHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblInicio)
                .addComponent(txtTiempoInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFin)
                .addComponent(txtTiempoFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblDuracion)
                .addComponent(txtDuracion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    javax.swing.GroupLayout pnlTiempoIncidenciaLayout = new javax.swing.GroupLayout(pnlTiempoIncidencia);
    pnlTiempoIncidencia.setLayout(pnlTiempoIncidenciaLayout);
    pnlTiempoIncidenciaLayout.setHorizontalGroup(
        pnlTiempoIncidenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTiempoIncidenciaLayout.createSequentialGroup()
            .addGap(0, 0, 0)
            .addComponent(pnlTiempoIncidenciaHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnParoPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnAgregarBitacora, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnRevisarHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(232, Short.MAX_VALUE))
    );
    pnlTiempoIncidenciaLayout.setVerticalGroup(
        pnlTiempoIncidenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTiempoIncidenciaLayout.createSequentialGroup()
            .addGap(5, 5, 5)
            .addGroup(pnlTiempoIncidenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlTiempoIncidenciaHora, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTiempoIncidenciaLayout.createSequentialGroup()
                    .addGroup(pnlTiempoIncidenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAgregarBitacora)
                        .addComponent(btnRevisarHoras)
                        .addComponent(btnParoPeriodo))
                    .addGap(17, 17, 17))))
    );

    jLabel5.setText("USUARIO:");

    javax.swing.GroupLayout pnlBorderNorthLayout = new javax.swing.GroupLayout(pnlBorderNorth);
    pnlBorderNorth.setLayout(pnlBorderNorthLayout);
    pnlBorderNorthLayout.setHorizontalGroup(
        pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlBorderNorthLayout.createSequentialGroup()
            .addGap(0, 0, 0)
            .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(pnlBorderNorthLayout.createSequentialGroup()
                    .addComponent(lblTema, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(cmbTema, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlTiempoIncidencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlProduccionCollapsible, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlBorderNorthLayout.createSequentialGroup()
                    .addComponent(lblLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, Short.MAX_VALUE)
                    .addComponent(lblTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCambiarLinea)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5)
                    .addGap(2, 2, 2)
                    .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(dteFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(5, 5, 5))
    );
    pnlBorderNorthLayout.setVerticalGroup(
        pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlBorderNorthLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTema, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblLinea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbLinea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(lblTurno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFecha)
                    .addComponent(dteFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCambiarLinea)
                    .addComponent(jLabel5)
                    .addComponent(lblUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBorderNorthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlBorderNorthLayout.createSequentialGroup()
                    .addComponent(pnlProduccionCollapsible, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlTiempoIncidencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(2, 2, 2))
    );

    mnuAdministrar.setText("Administar");

    mniBTS.setText("BTS");
    mniBTS.setActionCommand("_mniBTS");
    mniBTS.setFocusable(true);
    mnuAdministrar.add(mniBTS);
    mnuAdministrar.add(jSeparator18);

    mniCargaMasiva.setText("Carga Masiva");
    mniCargaMasiva.setActionCommand("_mniCargaMasiva");
    mnuAdministrar.add(mniCargaMasiva);
    mnuAdministrar.add(jSeparator19);

    mniOperaciones.setText("Fallas");
    mniOperaciones.setActionCommand("_mniOperaciones");
    mniOperaciones.setFocusable(true);
    mnuAdministrar.add(mniOperaciones);
    mnuAdministrar.add(jSeparator13);

    mniCrearUsuario.setText("Usuarios");
    mniCrearUsuario.setActionCommand("_mniCrearUsuario");
    mniCrearUsuario.setFocusable(true);
    mnuAdministrar.add(mniCrearUsuario);
    mnuAdministrar.add(jSeparator14);

    jMenu1.setText("Lineas");

    mniLineas.setText("Nueva Linea");
    mniLineas.setActionCommand("_mniLineas");
    mniLineas.setFocusable(true);
    jMenu1.add(mniLineas);
    jMenu1.add(jSeparator2);

    mniNewNoParte.setText("Nuevo No de Parte");
    mniNewNoParte.setActionCommand("_mniNewNoParte");
    jMenu1.add(mniNewNoParte);

    mnuAdministrar.add(jMenu1);

    mnuBarra.add(mnuAdministrar);

    mnuEditar.setText("Edicion");
    mnuEditar.setFocusable(false);

    mniEditarPorDia.setText("Bitácora por día");
    mniEditarPorDia.setActionCommand("_mniEditarPorDia");
    mniEditarPorDia.setFocusable(true);
    mnuEditar.add(mniEditarPorDia);
    mnuEditar.add(jSeparator8);

    mniEditarPorTurno.setText("Bitácora por turno");
    mniEditarPorTurno.setActionCommand("_mniEditarPorTurno");
    mnuEditar.add(mniEditarPorTurno);
    mnuEditar.add(jSeparator1);

    mniERangoTurno.setText("Rango de Turno");
    mniERangoTurno.setActionCommand("_mniERangoTurno");
    mnuEditar.add(mniERangoTurno);

    mnuBarra.add(mnuEditar);

    mnuGraficas.setText("Visualizar");

    mniListarDia.setText("Ver Bitácora Día");
    mniListarDia.setActionCommand("_mniListarDia");
    mnuGraficas.add(mniListarDia);
    mnuGraficas.add(jSeparator10);

    mniHourlyCount.setText("Hourly Count");
    mniHourlyCount.setActionCommand("_mniHourlyCount");
    mnuGraficas.add(mniHourlyCount);
    mnuGraficas.add(jSeparator12);

    mniIndicadores.setText("Indicadores");
    mniIndicadores.setActionCommand("_mniIndicadores");
    mniIndicadores.setFocusable(true);
    mnuGraficas.add(mniIndicadores);

    mnuBarra.add(mnuGraficas);

    mnuOtros.setText("Otros");

    mniAbout.setText("Acerca de");
    mniAbout.setActionCommand("_mniAbout");
    mniAbout.setFocusable(true);
    mnuOtros.add(mniAbout);
    mnuOtros.add(jSeparator9);

    mniManuales.setText("Manuales");
    mniManuales.setActionCommand("_mniManuales");
    mniManuales.setFocusable(true);
    mnuOtros.add(mniManuales);

    mnuBarra.add(mnuOtros);

    mnuSesion.setText("IniciarSesion");
    mnuSesion.setActionCommand("_mnuSesion");

    mniSesion.setText("Sesion");
    mniSesion.setActionCommand("_mniSesion");
    mnuSesion.add(mniSesion);
    mnuSesion.add(jSeparator3);

    mniAdminU.setText("Administrar Usuario");
    mniAdminU.setActionCommand("_mniAdminU");
    mnuSesion.add(mniAdminU);

    mnuBarra.add(mnuSesion);

    setJMenuBar(mnuBarra);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGap(0, 0, 0)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlBorderNorth, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlBitacora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 1440, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGap(0, 0, 0)
            .addComponent(pnlBorderNorth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(pnlBitacora, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGap(5, 5, 5))
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 675, Short.MAX_VALUE)))
    );

    pack();
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        //vA PREGUNTAR SI DECEA ACTUALIZAR DATOS 
        //new Languaje(this, true).setVisible(true);
        new SeleccionLinea(this, true).setVisible(true);
        //new DatosNoParte(this, true).setVisible(true);
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (getTblBitacora().getRowCount() != 0) {
            if (JOptionPane.showConfirmDialog(this, "Existe(n) " + getTblBitacora().getRowCount()
                    + " registro(s) en la bitacora que se perderán si continua\n¿Seguro que desea salir?", "Advertencia",
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
                try {
//                    new PrincipalMetodos().eliminarTurnoVacio(this);
//                    new PrincipalMetodos().eliminarRegistroTiempoVacio(this);
                    //new BitacoraDAOImpl().borrarRegistroHourly(this.getCmbLinea().getSelectedItem().toString());
                } catch (Exception ex) { 
                    JOptionPane.showMessageDialog(this, "1.Principal.eliminarHourlyCount()\n" + ex,"Error", JOptionPane.ERROR_MESSAGE);
                } 
                System.exit(0); 
            }
        } else if (!lblTurno.getText().isEmpty()) { 
            try { 
                //new PrincipalMetodos().eliminarTurnoVacio(this);
                //new BitacoraDAOImpl().borrarRegistroHourly(this.getCmbLinea().getSelectedItem().toString());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "2.Principal.eliminarHourlyCount()\n" + e,"Error", JOptionPane.ERROR_MESSAGE);
            } 
            System.exit(0); 
        } else {
            System.out.println("vista.Principal.formWindowClosing(): "+PrincipalControl.insercionesAccess);
            if (PrincipalControl.insercionesAccess == 0 && !lblTurno.getText().isEmpty()) {
                try {
                    //new PrincipalMetodos().eliminarTurnoVacio(this);
                    //new PrincipalMetodos().eliminarRegistroTiempo(this);
                    //new BitacoraDAOImpl().borrarRegistroHourly(this.getCmbLinea().getSelectedItem().toString());
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "3.PrincipalMetodos.eliminarTurnoVacio()\n" + e,"Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowClosing

    private void dteFechaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dteFechaFocusLost
        // TODO add your handling code here:
        
    }//GEN-LAST:event_dteFechaFocusLost

    private void dteFechaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_dteFechaCaretUpdate
        // TODO add your handling code here:
        //System.out.println("vista.Principal.dteFechaCaretUpdate(): bnEdicion "+ bnEdicion);
    }//GEN-LAST:event_dteFechaCaretUpdate

    private void dteFechaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dteFechaFocusGained
        // TODO add your handling code here:
        //System.out.println("vista.Principal.dteFechaFocusGained(): bnEdicion "+ bnEdicion);
//        contEDateFecha ++;
//        System.out.println("cont: "+contEDateFecha);
//        if (contEDateFecha > 1){            
//            PrincipalControl.linea = winPrincipal.getCmbLinea().getSelectedItem().toString();
//            PrincipalControl.fecha = dteFecha.getText();
//            //System.out.println("vista.Principal.dteFechaFocusLost() f: "+winPrincipal.getCmbLinea().getSelectedItem().toString() );
//            switch (bnEdicion) {
//                case 0:
//                    principalMetodos.consultarBitacoraPorDia(winPrincipal);
//                    break;
//                case 2:
//                    new SelecTurno(winPrincipal, true).setVisible(true);
//                    break;
//            }
//            //System.out.println("control.Principal..dteFechaFocusLost(): bnEdicion "+ bnEdicion);
//            contEDateFecha = 0;
//        }
    }//GEN-LAST:event_dteFechaFocusGained

    private void formWindowStateChanged(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowStateChanged
        this.validate();
        this.repaint();
    }//GEN-LAST:event_formWindowStateChanged

    private void cmbTemaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTemaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTemaActionPerformed
//
    public JMenu getMnuSesion() {
        return mnuSesion;
    }

    public JMenuItem getMniSesion() {
        return mniSesion;
    }    
    
    // GETS DE PANEL GENERAL
    public JMenuItem getMniERangoTurno() {
        return mniERangoTurno;
    }

    public JMenuItem getMniHourlyCount() {
        return mniHourlyCount;
    }
    
    public JMenuItem getMniListarDia() {
        return mniListarDia;
    }

    public JMenuItem getMniBTS() {
        return mniBTS;
    }

    public JMenuItem getMniIndicadores() {
        return mniIndicadores;
    }

    public JMenuItem getMniManuales() {
        return mniManuales;
    }

    public JMenuItem getMniAbout() {
        return mniAbout;
    }

    public JMenuItem getMniNewNoParte() {
        return mniNewNoParte;
    }

    public JMenuItem getMniEditarPorTurno() {
        return mniEditarPorTurno;
    }

    public JButton getBtnParoPeriodo() {
        return btnParoPeriodo;
    }
    
    public JLabel getLblTurno() {
        return lblTurno;
    }

    public JLabel getLblUsuario() {
        return lblUsuario;
    }
    
    public JButton getBtnGuardar(){
        return btnGuardar;
    }
    
    public JButton getBtnCancelar() {
        return btnCancelar;
    }
    
    public JMenuItem getMniEditarPorDia() {
        return mniEditarPorDia;
    }
    
    public JPanel getPnlBitacora() {
        return pnlBitacora;
    }
    
    public WebCollapsiblePane getPnlProduccionCollapsible() {
        return pnlProduccionCollapsible;
    }

    public JComboBox getCmbTema() {
        return cmbTema;
    }

    public JComboBox getCmbLinea() {
        return cmbLinea;
    }

    public JButton getBtnCambiarLinea() {
        return btnCambiarLinea;
    }
    
    public WebDateField getDteFecha(){
        return dteFecha;
    }

    public JComboBox getCmbHora() {
        return cmbHora;
    }

    public JTextField getTxtDuracion() {
        return txtDuracion;
    }

    public JTextField getTxtTiempoInicio() {
        return txtTiempoInicio;
    }

    public JTextField getTxtTiempoFin() {
        return txtTiempoFin;
    }

    public JButton getBtnAgregarBitacora() {
        return btnAgregarBitacora;
    }

    public JButton getBtnRevisarHoras() {
        return btnRevisarHoras;
    }
    
    public JComboBox<String> getCmbCliente() {
        return cmbCliente;
    }

    public JComboBox<String> getCmbNoParte() {
        return cmbNoParte;
    }

    public JTextField getTxtTcP() {
        return txtTcP;
    } 
    
    //PANEL BITACORA
    public JScrollPane getScrBitacora() {
        return scrBitacora;
    }
    public JTable getTblBitacora() {
        return tblBitacora;
    }

    public JMenuItem getMniEliminar() {
        return mniEliminar;
    }
    
    public JMenuItem getMniCrearUsuario() {
        return mniCrearUsuario;
    }

    public JMenuItem getMniCargaMasiva() {
        return mniCargaMasiva;
    }
    
    //OPERACIONES    
    public JMenuItem getMniOperaciones() {
        return mniOperaciones;
    }

    public JMenuItem getMniLineas() {
        return mniLineas;
    }
          
    //PANEL GENERAL 

    public JPanel getPnlGeneral() {
        return pnlGeneral;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    public JButton getBtnRemove() {
        return btnRemove;
    }

    public JTextField getTxtCantPzas() {
        return txtCantPzas;
    }
    
    public JComboBox<String> getCmbArea1() {
        return cmbArea1;
    }

    public JComboBox<String> getCmbArea2() {
        return cmbArea2;
    }

    public JComboBox<String> getCmbArea3() {
        return cmbArea3;
    }

    public JComboBox<String> getCmbArea4() {
        return cmbArea4;
    }

    public JComboBox<String> getCmbCliente1() {
        return cmbCliente1;
    }

    public JComboBox<String> getCmbCliente2() {
        return cmbCliente2;
    }

    public JComboBox<String> getCmbCliente3() {
        return cmbCliente3;
    }

    public JComboBox<String> getCmbCliente4() {
        return cmbCliente4;
    }

    public JComboBox<String> getCmbNoParte1() {
        return cmbNoParte1;
    }

    public JComboBox<String> getCmbNoParte2() {
        return cmbNoParte2;
    }

    public JComboBox<String> getCmbNoParte3() {
        return cmbNoParte3;
    }

    public JComboBox<String> getCmbOperacion1() {
        return cmbOperacion1;
    }

    public JComboBox<String> getCmbOperacion2() {
        return cmbOperacion2;
    }

    public JComboBox<String> getCmbOperacion3() {
        return cmbOperacion3;
    }

    public JComboBox<String> getCmbNoParte4() {
        return cmbNoParte4;
    }

    public JComboBox<String> getCmbOperacion4() {
        return cmbOperacion4;
    }

    public JComboBox<String> getCmbProblema2() {
        return cmbProblema2;
    }

    public JLabel getLblCantPzas() {
        return lblCantPzas;
    }
    
    
    public JLabel getLblArea1() {
        return lblArea1;
    }

    public JLabel getLblArea2() {
        return lblArea2;
    }

    public JLabel getLblArea3() {
        return lblArea3;
    }

    public JLabel getLblArea4() {
        return lblArea4;
    }

    public JLabel getLblCliente3() {
        return lblCliente3;
    }

    public JLabel getLblOperacion1() {
        return lblOperacion1;
    }

    public JLabel getLblOperacion2() {
        return lblOperacion2;
    }

    public JLabel getLblOperacion3() {
        return lblOperacion3;
    }

    public JLabel getLblOperacion4() {
        return lblOperacion4;
    }

    public JPanel getPnl4() {
        return pnl4;
    }
    
    public JComboBox<String> getCmbProblema1() {
        return cmbProblema1;
    }

    public JComboBox<String> getCmbProblema3() {
        return cmbProblema3;
    }

    public JComboBox<String> getCmbProblema4() {
        return cmbProblema4;
    }

    public JLabel getLblCliente1() {
        return lblCliente1;
    }

    public JLabel getLblCliente2() {
        return lblCliente2;
    }

    public JLabel getLblCliente4() {
        return lblCliente4;
    }

    public JLabel getLblMatFaltante1() {
        return lblMatFaltante1;
    }

    public JLabel getLblMatFaltante2() {
        return lblMatFaltante2;
    }

    public JLabel getLblMatFaltante3() {
        return lblMatFaltante3;
    }

    public JLabel getLblMatFaltante4() {
        return lblMatFaltante4;
    }

    public JLabel getLblNoParte1() {
        return lblNoParte1;
    }

    public JLabel getLblNoParte2() {
        return lblNoParte2;
    }

    public JLabel getLblNoParte3() {
        return lblNoParte3;
    }

    public JLabel getLblNoParte4() {
        return lblNoParte4;
    }

    public JLabel getLblProblema1() {
        return lblProblema1;
    }

    public JLabel getLblProblema2() {
        return lblProblema2;
    }

    public JLabel getLblProblema3() {
        return lblProblema3;
    }

    public JLabel getLblProblema4() {
        return lblProblema4;
    }

    public JLabel getLblScrap1() {
        return lblScrap1;
    }

    public JLabel getLblScrap2() {
        return lblScrap2;
    }

    public JLabel getLblScrap3() {
        return lblScrap3;
    }

    public JLabel getLblScrap4() {
        return lblScrap4;
    }

    public JLabel getLblTema1() {
        return lblTema2;
    }

    public JLabel getLblTema2() {
        return lblTema3;
    }

    public JLabel getLblTema3() {
        return lblTema4;
    }

    public JPanel getPnl2() {
        return pnl2;
    }

    public JPanel getPnl3() {
        return pnl3;
    }

    public JTextField getTxtMatFaltante1() {
        return txtMatFaltante1;
    }

    public JTextField getTxtMatFaltante2() {
        return txtMatFaltante2;
    }

    public JTextField getTxtMatFaltante3() {
        return txtMatFaltante3;
    }

    public JTextField getTxtMatFaltante4() {
        return txtMatFaltante4;
    }

    public JTextField getTxtScrap1() {
        return txtScrap1;
    }

    public JTextField getTxtScrap2() {
        return txtScrap2;
    }

    public JTextField getTxtScrap3() {
        return txtScrap3;
    }

    public JTextField getTxtScrap4() {
        return txtScrap4;
    }

    public JComboBox<String> getCmbTema2() {
        return cmbTema2;
    }

    public JComboBox<String> getCmbTema3() {
        return cmbTema3;
    }

    public JComboBox<String> getCmbTema4() {
        return cmbTema4;
    }

    public JLabel getLblDur2() {
        return lblDur2;
    }

    public JLabel getLblDur3() {
        return lblDur3;
    }

    public JLabel getLblDur4() {
        return lblDur4;
    }

    public JTextField getTxtDur2() {
        return txtDur2;
    }

    public JTextField getTxtDur3() {
        return txtDur3;
    }

    public JTextField getTxtDur4() {
        return txtDur4;
    }

    public JLabel getLblTema4() {
        return lblTema4;
    }

    public JMenuItem getMniAdminU() {
        return mniAdminU;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAgregarBitacora;
    private javax.swing.JButton btnCambiarLinea;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnParoPeriodo;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnRevisarHoras;
    private javax.swing.JComboBox<String> cmbArea1;
    private javax.swing.JComboBox<String> cmbArea2;
    private javax.swing.JComboBox<String> cmbArea3;
    private javax.swing.JComboBox<String> cmbArea4;
    private javax.swing.JComboBox<String> cmbCliente;
    private javax.swing.JComboBox<String> cmbCliente1;
    private javax.swing.JComboBox<String> cmbCliente2;
    private javax.swing.JComboBox<String> cmbCliente3;
    private javax.swing.JComboBox<String> cmbCliente4;
    private javax.swing.JComboBox cmbHora;
    private javax.swing.JComboBox cmbLinea;
    private javax.swing.JComboBox<String> cmbNoParte;
    private javax.swing.JComboBox<String> cmbNoParte1;
    private javax.swing.JComboBox<String> cmbNoParte2;
    private javax.swing.JComboBox<String> cmbNoParte3;
    private javax.swing.JComboBox<String> cmbNoParte4;
    private javax.swing.JComboBox<String> cmbOperacion1;
    private javax.swing.JComboBox<String> cmbOperacion2;
    private javax.swing.JComboBox<String> cmbOperacion3;
    private javax.swing.JComboBox<String> cmbOperacion4;
    private javax.swing.JComboBox<String> cmbProblema1;
    private javax.swing.JComboBox<String> cmbProblema2;
    private javax.swing.JComboBox<String> cmbProblema3;
    private javax.swing.JComboBox<String> cmbProblema4;
    private javax.swing.JComboBox cmbTema;
    private javax.swing.JComboBox<String> cmbTema2;
    private javax.swing.JComboBox<String> cmbTema3;
    private javax.swing.JComboBox<String> cmbTema4;
    private com.alee.extended.date.WebDateField dteFecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator13;
    private javax.swing.JPopupMenu.Separator jSeparator14;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JPopupMenu.Separator jSeparator18;
    private javax.swing.JPopupMenu.Separator jSeparator19;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JLabel lblArea1;
    private javax.swing.JLabel lblArea2;
    private javax.swing.JLabel lblArea3;
    private javax.swing.JLabel lblArea4;
    private javax.swing.JLabel lblCantPzas;
    private javax.swing.JLabel lblCliente1;
    private javax.swing.JLabel lblCliente2;
    private javax.swing.JLabel lblCliente3;
    private javax.swing.JLabel lblCliente4;
    private javax.swing.JLabel lblDur2;
    private javax.swing.JLabel lblDur3;
    private javax.swing.JLabel lblDur4;
    private javax.swing.JLabel lblDuracion;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblFin;
    private javax.swing.JLabel lblHora;
    private javax.swing.JLabel lblInicio;
    private javax.swing.JLabel lblLinea;
    private javax.swing.JLabel lblMatFaltante1;
    private javax.swing.JLabel lblMatFaltante2;
    private javax.swing.JLabel lblMatFaltante3;
    private javax.swing.JLabel lblMatFaltante4;
    private javax.swing.JLabel lblNoParte1;
    private javax.swing.JLabel lblNoParte2;
    private javax.swing.JLabel lblNoParte3;
    private javax.swing.JLabel lblNoParte4;
    private javax.swing.JLabel lblOperacion1;
    private javax.swing.JLabel lblOperacion2;
    private javax.swing.JLabel lblOperacion3;
    private javax.swing.JLabel lblOperacion4;
    private javax.swing.JLabel lblProblema1;
    private javax.swing.JLabel lblProblema2;
    private javax.swing.JLabel lblProblema3;
    private javax.swing.JLabel lblProblema4;
    private javax.swing.JLabel lblScrap1;
    private javax.swing.JLabel lblScrap2;
    private javax.swing.JLabel lblScrap3;
    private javax.swing.JLabel lblScrap4;
    private javax.swing.JLabel lblTema;
    private javax.swing.JLabel lblTema2;
    private javax.swing.JLabel lblTema3;
    private javax.swing.JLabel lblTema4;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JMenuItem mniAbout;
    private javax.swing.JMenuItem mniAdminU;
    private javax.swing.JMenuItem mniBTS;
    private javax.swing.JMenuItem mniCargaMasiva;
    private javax.swing.JMenuItem mniCrearUsuario;
    private javax.swing.JMenuItem mniERangoTurno;
    private javax.swing.JMenuItem mniEditarPorDia;
    private javax.swing.JMenuItem mniEditarPorTurno;
    private javax.swing.JMenuItem mniEliminar;
    private javax.swing.JMenuItem mniHourlyCount;
    private javax.swing.JMenuItem mniIndicadores;
    private javax.swing.JMenuItem mniLineas;
    private javax.swing.JMenuItem mniListarDia;
    private javax.swing.JMenuItem mniManuales;
    private javax.swing.JMenuItem mniNewNoParte;
    private javax.swing.JMenuItem mniOperaciones;
    private javax.swing.JMenuItem mniSesion;
    private javax.swing.JMenu mnuAdministrar;
    private javax.swing.JMenuBar mnuBarra;
    private javax.swing.JMenu mnuEditar;
    private javax.swing.JMenu mnuGraficas;
    private javax.swing.JMenu mnuOtros;
    private javax.swing.JMenu mnuSesion;
    private javax.swing.JPopupMenu pmnClickMenu;
    private javax.swing.JPanel pnl2;
    private javax.swing.JPanel pnl3;
    private javax.swing.JPanel pnl4;
    private javax.swing.JPanel pnlBackground;
    private javax.swing.JPanel pnlBitacora;
    private javax.swing.JPanel pnlBorderNorth;
    private javax.swing.JPanel pnlGeneral;
    private com.alee.extended.panel.WebCollapsiblePane pnlProduccionCollapsible;
    private javax.swing.JPanel pnlTiempoIncidencia;
    private javax.swing.JPanel pnlTiempoIncidenciaHora;
    private javax.swing.JScrollPane scrBitacora;
    private javax.swing.JTable tblBitacora;
    private javax.swing.JTextField txtCantPzas;
    private javax.swing.JTextField txtDur2;
    private javax.swing.JTextField txtDur3;
    private javax.swing.JTextField txtDur4;
    private javax.swing.JTextField txtDuracion;
    private javax.swing.JTextField txtMatFaltante1;
    private javax.swing.JTextField txtMatFaltante2;
    private javax.swing.JTextField txtMatFaltante3;
    private javax.swing.JTextField txtMatFaltante4;
    private javax.swing.JTextField txtScrap1;
    private javax.swing.JTextField txtScrap2;
    private javax.swing.JTextField txtScrap3;
    private javax.swing.JTextField txtScrap4;
    private javax.swing.JTextField txtTcP;
    private javax.swing.JTextField txtTiempoFin;
    private javax.swing.JTextField txtTiempoInicio;
    // End of variables declaration//GEN-END:variables
}
