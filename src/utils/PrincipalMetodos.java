package utils;

import control.PrincipalControl;
import dao.BitacoraDAOImpl;
import dao.CalidadDAOImpl;
import dao.LineasDAOImpl;
import dao.OrganizacionalesDAOImpl;
import dao.PiezasProducidasDAOImpl;
import dao.TiempoTurnoDAOImpl;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import vista.Principal;
import vista.SelecTurno;
import vista.TiemposFaltantes;

/**
 * Hecho con <3 
 * @author GarciHard
 * @boschUsr GJA5TL
 * 
 */

public class PrincipalMetodos {

    private ArrayList tiempoHora;
    public DefaultComboBoxModel listaArea;
    public DefaultComboBoxModel listaAreaC;
    public DefaultComboBoxModel listaCliente;
    public DefaultComboBoxModel listaLinea;
    public DefaultComboBoxModel listaNoParte;
    public DefaultComboBoxModel listaOperacion;
    public DefaultComboBoxModel listaProblema;
    public DefaultComboBoxModel listaProblemaC;
    private DefaultTableModel modeloTabla;
    public int contadorFila = 0;
    private JFrame form;
    private List tablaObj = new ArrayList();
    private Object[] registroBitacora = new Object[18];
    private Object[] registroBitacoraAux;
    private Object[] registroBitacoraTmp;
    private Object[] registroBitacoraTmpAux;
    private Object[] tiempos;

    int hora = 0;
    int minIni = 0;
    int minFin = 0;
    
    String newCliente = "";
    String newFamilia = "";
    String newNoParte = "";
    String clienteP = "";
    String noParteP = "";

    String problemaCambios = "";

    //double lineTackDia = 0;
   int contTCProd = 0;
    
    double tcPonderado = 0.0;
    double percentTotal = 100;

    private DefaultTableModel bitacoraModeloCero,
            tablaHorasCero = new DefaultTableModel();

    public DefaultComboBoxModel listaLineas() {
        try {
            listaLinea = new LineasDAOImpl().listaLineas();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaLineas()\n" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }

        return listaLinea;
    }

    public DefaultComboBoxModel listaClientes(String linea) {
        try {
            listaCliente = new PiezasProducidasDAOImpl().listaClientes(linea);
            if (listaCliente.getSize() == 0) {
                JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaClientes()\n"
                        + "No hay clientes para la linea seleccionada", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaClientes()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaCliente;
    }

    public DefaultComboBoxModel listaFamilias(String linea) {
        try {
            listaCliente = new PiezasProducidasDAOImpl().listaFamilias(linea);
            if (listaCliente.getSize() == 0) {
                JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaFamilias()\n"
                        + "No hay familias para la linea seleccionada", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaFamilias()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaCliente;
    }

    public DefaultComboBoxModel listaFamiliasClientes(String linea, String familia) {
        try {
            listaCliente = new PiezasProducidasDAOImpl().listaFamiliasClientes(linea, familia);
            if (listaCliente.getSize() == 0) {
                JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaFamiliasClientes()\n"
                        + "No hay clientes para la linea seleccionada", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaFamiliasClientes()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaCliente;
    }

    public DefaultComboBoxModel listaNoPartes(String linea, String cliente) {
        try {
            listaNoParte = new PiezasProducidasDAOImpl().listaNoParte(linea, cliente);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaNoParte()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaNoParte;
    }

    public DefaultComboBoxModel listaNoPartesFamilia(String linea, String familia, String cliente) {
        try {
            listaNoParte = new PiezasProducidasDAOImpl().listaNoParteFamilia(linea, familia, cliente);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaNoParteFamilia()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaNoParte;
    }

    public DefaultComboBoxModel listaOperacionesCalidad(String linea) {
        try {
            listaOperacion = new CalidadDAOImpl().listaOperacionCalidad(linea);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaOperacionesCalidad()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaOperacion;
    }

    public DefaultComboBoxModel listaAreasCalidad(String linea, String tema, String operacion) {
        try {
            listaAreaC = new CalidadDAOImpl().listaAreaCalidad(linea, tema, operacion);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaAreasCalidad()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaAreaC;
    }

    public DefaultComboBoxModel listaProblemasCalidad(String linea, String tema, String operacion, String area) {
        try {
            listaProblemaC = new CalidadDAOImpl().listaProblemaCalidad(linea, tema, operacion, area);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaProblemasCalidad()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaProblemaC;
    }

    public DefaultComboBoxModel listaAreasOrganizacional(String linea, String tema) {
        try {
            listaArea = new OrganizacionalesDAOImpl().listaAreasOrganizacional(linea, tema);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaAreaOrganizacional()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaArea;
    }

    public DefaultComboBoxModel listaProblemasOrganizacional(String linea, String tema, String area) {
        try {
            listaProblema = new OrganizacionalesDAOImpl().listaProblemasOrganizacional(linea, tema, area);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, "PrincipalMetodos.listaProblemaOrganizacional()\n"
                    + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return listaProblema;
    }

    private void limpiarTabla(DefaultTableModel tablaBitacora) {
        for (int i = 0; i < tablaBitacora.getRowCount(); i++) {
            tablaBitacora.removeRow(i);
            i -= 1;
        }
    }

    public void panelDatosProduccion(Principal winPrincipal) {
        winPrincipal.getCmbCliente().setEnabled(false);
        winPrincipal.getCmbNoParte().setEnabled(false);
    }

    public void panelCliente(Principal winPrincipal) {
        if (winPrincipal.getCmbCliente().getSelectedIndex() != 0) {
            winPrincipal.getCmbNoParte().setModel(
                    new PrincipalMetodos().listaNoPartes(winPrincipal.getCmbLinea().getSelectedItem().toString(), winPrincipal.getCmbCliente().getSelectedItem().toString()));
            winPrincipal.getCmbNoParte().setEnabled(true);
        } else {
            winPrincipal.getCmbNoParte().setEnabled(false);
        }
    }

    public void panelNoParte(Principal winPrincipal) {
        if (winPrincipal.getCmbNoParte().getSelectedIndex() != 0) {
            winPrincipal.getCmbTema().setEnabled(true);
        } else {
            winPrincipal.getCmbTema().setEnabled(true);
        }
    }
    
    public void limpiaPanelGeneral(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
        //winPrincipal.getLblCantPzas().setVisible(true);
        winPrincipal.getTxtCantPzas().setText("");
        
        if (winPrincipal.getCmbTema2().getItemCount() != 0){
            winPrincipal.getCmbTema2().setSelectedIndex(-1);
        }
        
        if (winPrincipal.getCmbTema3().getItemCount() != 0){
            winPrincipal.getCmbTema3().setSelectedIndex(-1);
        }
        
        if (winPrincipal.getCmbTema4().getItemCount() != 0){
            winPrincipal.getCmbTema4().setSelectedIndex(-1);
        }        

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }
    

    public void panelPiezasProducidas(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
        winPrincipal.getLblCantPzas().setVisible(true);
        winPrincipal.getTxtCantPzas().setText("");
        winPrincipal.getTxtCantPzas().setEnabled(true);
        winPrincipal.getTxtCantPzas().setVisible(true);

        winPrincipal.getLblOperacion1().setVisible(false);
        winPrincipal.getCmbOperacion1().setVisible(false);

        winPrincipal.getLblArea1().setVisible(false);
        winPrincipal.getCmbArea1().setVisible(false);

        winPrincipal.getLblProblema1().setVisible(false);
        winPrincipal.getCmbProblema1().setVisible(false);

        winPrincipal.getLblScrap1().setVisible(false);
        winPrincipal.getTxtScrap1().setVisible(false);

        winPrincipal.getLblCliente1().setVisible(false);
        winPrincipal.getCmbCliente1().setVisible(false);

        winPrincipal.getLblNoParte1().setVisible(false);
        winPrincipal.getCmbNoParte1().setVisible(false);

        winPrincipal.getLblMatFaltante1().setVisible(false);
        winPrincipal.getTxtMatFaltante1().setVisible(false);

        winPrincipal.getBtnAdd().setVisible(false);
        winPrincipal.getBtnRemove().setVisible(false);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelCalidad1(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
        winPrincipal.getCmbOperacion1().setModel(listaOperacionesCalidad(winPrincipal.getCmbLinea().getSelectedItem().toString()));
        winPrincipal.getCmbOperacion1().setEnabled(true);
        
        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");
        winPrincipal.getTxtCantPzas().setVisible(false);

        winPrincipal.getLblOperacion1().setVisible(true);
        winPrincipal.getCmbOperacion1().setVisible(true);

        if (winPrincipal.getCmbArea1().getItemCount() != 0) {
            winPrincipal.getCmbArea1().setSelectedIndex(-1);
        }
        winPrincipal.getLblArea1().setVisible(true);
        winPrincipal.getCmbArea1().setVisible(true);
        winPrincipal.getCmbArea1().setEnabled(false);

        if (winPrincipal.getCmbProblema1().getItemCount() != 0) {
            winPrincipal.getCmbProblema1().setSelectedIndex(-1);
        }
        winPrincipal.getLblProblema1().setVisible(true);
        winPrincipal.getCmbProblema1().setVisible(true);
        winPrincipal.getCmbProblema1().setEnabled(false);

        winPrincipal.getLblScrap1().setVisible(true);
        winPrincipal.getTxtScrap1().setVisible(true);

        winPrincipal.getLblCliente1().setVisible(false);
        winPrincipal.getCmbCliente1().setVisible(false);

        winPrincipal.getLblNoParte1().setVisible(false);
        winPrincipal.getCmbNoParte1().setVisible(false);

        winPrincipal.getLblMatFaltante1().setVisible(false);
        winPrincipal.getTxtMatFaltante1().setVisible(false);

        winPrincipal.getBtnAdd().setVisible(true);
        winPrincipal.getBtnRemove().setVisible(true);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelOrganizacionales(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
        winPrincipal.getCmbArea1().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema().getSelectedItem().toString()));

        winPrincipal.getCmbArea1().setEnabled(true);
        winPrincipal.getCmbProblema1().setEnabled(false);

        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");
        winPrincipal.getTxtCantPzas().setVisible(false);

        winPrincipal.getLblOperacion1().setVisible(false);
        winPrincipal.getCmbOperacion1().setVisible(false);

        winPrincipal.getLblArea1().setVisible(true);
        winPrincipal.getCmbArea1().setVisible(true);

        if (winPrincipal.getCmbProblema1().getItemCount() != 0) {
            winPrincipal.getCmbProblema1().setSelectedIndex(0);
        }
        winPrincipal.getLblProblema1().setVisible(true);
        winPrincipal.getCmbProblema1().setVisible(true);
        winPrincipal.getCmbProblema1().setEnabled(false);

        winPrincipal.getLblScrap1().setVisible(false);
        winPrincipal.getTxtScrap1().setVisible(false);
        winPrincipal.getTxtScrap1().setEnabled(false);

        winPrincipal.getLblCliente1().setVisible(false);
        winPrincipal.getCmbCliente1().setVisible(false);

        winPrincipal.getLblNoParte1().setVisible(false);
        winPrincipal.getCmbNoParte1().setVisible(false);

        winPrincipal.getLblMatFaltante1().setVisible(false);
        winPrincipal.getTxtMatFaltante1().setVisible(false);

        winPrincipal.getBtnAdd().setVisible(true);
        winPrincipal.getBtnRemove().setVisible(true);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelCambios(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);

        winPrincipal.getCmbArea1().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema().getSelectedItem().toString()));

        winPrincipal.getCmbArea1().setEnabled(true);
        winPrincipal.getCmbProblema1().setEnabled(false);

        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");
        winPrincipal.getTxtCantPzas().setVisible(false);

        winPrincipal.getLblOperacion1().setVisible(false);
        winPrincipal.getCmbOperacion1().setVisible(false);

        winPrincipal.getLblArea1().setVisible(true);
        winPrincipal.getCmbArea1().setVisible(true);

        if (winPrincipal.getCmbProblema1().getItemCount() != 0) {
            winPrincipal.getCmbProblema1().setSelectedIndex(0);
        }
        
        winPrincipal.getLblProblema1().setVisible(false);
        winPrincipal.getCmbProblema1().setVisible(false);

        winPrincipal.getLblScrap1().setVisible(true);
        winPrincipal.getTxtScrap1().setVisible(true);
        winPrincipal.getTxtScrap1().setEnabled(false);

        winPrincipal.getLblCliente1().setVisible(true);
        winPrincipal.getCmbCliente1().setVisible(true);
        winPrincipal.getCmbCliente1().setEnabled(false);

        winPrincipal.getLblNoParte1().setVisible(true);
        winPrincipal.getCmbNoParte1().setVisible(true);

        winPrincipal.getLblMatFaltante1().setVisible(false);
        winPrincipal.getTxtMatFaltante1().setVisible(false);

        winPrincipal.getBtnAdd().setVisible(true);
        winPrincipal.getBtnRemove().setVisible(true);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelPlaneados(Principal winPrincipal) {
        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);

        winPrincipal.getCmbArea1().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema().getSelectedItem().toString()));
        winPrincipal.getCmbArea1().setEnabled(true);

        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");
        winPrincipal.getTxtCantPzas().setVisible(false);

        winPrincipal.getLblOperacion1().setVisible(false);
        winPrincipal.getCmbOperacion1().setVisible(false);

        winPrincipal.getLblArea1().setVisible(true);
        winPrincipal.getCmbArea1().setVisible(true);

        if (winPrincipal.getCmbProblema1().getItemCount() != 0) {
            winPrincipal.getCmbProblema1().setSelectedIndex(0);
        }
        
        winPrincipal.getLblProblema1().setVisible(false);
        winPrincipal.getCmbProblema1().setVisible(false);

        winPrincipal.getLblScrap1().setVisible(false);
        winPrincipal.getTxtScrap1().setVisible(false);
        winPrincipal.getTxtScrap1().setEnabled(false);

        winPrincipal.getLblCliente1().setVisible(false);
        winPrincipal.getCmbCliente1().setVisible(false);

        winPrincipal.getLblNoParte1().setVisible(false);
        winPrincipal.getCmbNoParte1().setVisible(false);

        winPrincipal.getLblMatFaltante1().setVisible(false);
        winPrincipal.getTxtMatFaltante1().setVisible(false);

        winPrincipal.getBtnAdd().setEnabled(false);
        winPrincipal.getBtnRemove().setEnabled(true);

        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
        
        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl2().setVisible(false);
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panel2(Principal winPrincipal) {        
        winPrincipal.getCmbArea1().setEnabled(false);
        winPrincipal.getCmbOperacion1().setEnabled(false);
        winPrincipal.getCmbArea1().setEnabled(false);
        winPrincipal.getCmbProblema1().setEnabled(false);
        winPrincipal.getTxtMatFaltante1().setEnabled(false);
        winPrincipal.getTxtScrap1().setEnabled(false);

        winPrincipal.getCmbCliente1().setEnabled(false);
        winPrincipal.getCmbNoParte1().setEnabled(false);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        
        winPrincipal.getLblDur2().setVisible(false);
        winPrincipal.getTxtDur2().setVisible(false);    
        winPrincipal.getTxtDur2().setEnabled(false);

        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getTxtTiempoFin().setEnabled(false);    
        
        winPrincipal.getBtnAgregarBitacora().setEnabled(false);        
        
        winPrincipal.getPnl2().setVisible(true);        
        winPrincipal.getCmbTema2().setSelectedIndex(0);        
        winPrincipal.getCmbTema2().setEnabled(true);
        
        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getLblOperacion2().setVisible(false);
        winPrincipal.getCmbOperacion2().setVisible(false);

        winPrincipal.getLblArea2().setVisible(false);
        winPrincipal.getCmbArea2().setVisible(false);

        winPrincipal.getLblProblema2().setVisible(false);
        winPrincipal.getCmbProblema2().setVisible(false);

        winPrincipal.getLblScrap2().setVisible(false);
        winPrincipal.getTxtScrap2().setVisible(false);

        winPrincipal.getLblCliente2().setVisible(false);
        winPrincipal.getCmbCliente2().setVisible(false);

        winPrincipal.getLblNoParte2().setVisible(false);
        winPrincipal.getCmbNoParte2().setVisible(false);

        winPrincipal.getLblMatFaltante2().setVisible(false);
        winPrincipal.getTxtMatFaltante2().setVisible(false);
    
        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);

        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelCalidad2(Principal winPrincipal) {     
        //APARTADO DE MODULOS QUE SE MUESTRAN        
        winPrincipal.getCmbOperacion2().setModel(listaOperacionesCalidad(winPrincipal.getCmbLinea().getSelectedItem().toString()));
        winPrincipal.getCmbOperacion2().setEnabled(true);
        winPrincipal.getCmbOperacion2().setVisible(true);
        
        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");        
        winPrincipal.getTxtCantPzas().setVisible(false);   
        
        winPrincipal.getLblOperacion2().setVisible(true);
        winPrincipal.getCmbOperacion2().setVisible(true);
        
        if (winPrincipal.getCmbArea2().getItemCount() != 0){
            winPrincipal.getCmbArea2().setSelectedIndex(0);
        }
        winPrincipal.getLblArea2().setVisible(true);
        winPrincipal.getCmbArea2().setVisible(true);
        winPrincipal.getCmbArea2().setEnabled(false);
        
        if (winPrincipal.getCmbProblema2().getItemCount() != 0){
            winPrincipal.getCmbProblema2().setSelectedIndex(-1);
        }
        winPrincipal.getLblProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setEnabled(false);
        
        winPrincipal.getLblScrap2().setVisible(true);
        winPrincipal.getTxtScrap2().setVisible(true);
        
        winPrincipal.getLblCliente2().setVisible(false);
        winPrincipal.getCmbCliente2().setVisible(false);
        
        winPrincipal.getLblNoParte2().setVisible(false);
        winPrincipal.getCmbNoParte2().setVisible(false);
        
        winPrincipal.getLblMatFaltante2().setVisible(false);
        winPrincipal.getTxtMatFaltante2().setVisible(false);
        
        winPrincipal.getLblDur2().setVisible(true);
        winPrincipal.getTxtDur2().setVisible(true); 
        winPrincipal.getTxtDur2().setEnabled(false); 
        winPrincipal.getTxtDur2().setText(""); 
        
        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }
    
    public void panelOrganizacionales2(Principal winPrincipal) {        
        winPrincipal.getCmbArea2().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema2().getSelectedItem().toString()));
        
        winPrincipal.getCmbArea2().setSelectedIndex(0);

        winPrincipal.getLblArea2().setVisible(true);
        winPrincipal.getCmbArea2().setVisible(true);
        winPrincipal.getCmbArea2().setEnabled(true);
        
        if (winPrincipal.getCmbProblema2().getItemCount() != 0){
            winPrincipal.getCmbProblema2().setSelectedIndex(-1);
        }
        
        winPrincipal.getLblProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setEnabled(false);       

        winPrincipal.getLblOperacion2().setVisible(false);
        winPrincipal.getCmbOperacion2().setVisible(false);

        winPrincipal.getLblArea2().setVisible(true);
        winPrincipal.getCmbArea2().setVisible(true);
        
        winPrincipal.getLblProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setVisible(true);
        winPrincipal.getCmbProblema2().setEnabled(false);

        winPrincipal.getLblScrap2().setVisible(false);
        winPrincipal.getTxtScrap2().setVisible(false);
        winPrincipal.getTxtScrap2().setEnabled(false);

        if (winPrincipal.getCmbCliente2().getItemCount() != 0) {
            winPrincipal.getCmbCliente2().setSelectedIndex(0);
        }
        
        winPrincipal.getLblCliente2().setVisible(false);
        winPrincipal.getCmbCliente2().setVisible(false);
        winPrincipal.getCmbCliente2().setEnabled(false);

        if (winPrincipal.getCmbNoParte2().getItemCount() != 0) {
            winPrincipal.getCmbNoParte2().setSelectedIndex(0);
        }
        winPrincipal.getLblNoParte2().setVisible(false);
        winPrincipal.getCmbNoParte2().setVisible(false);
        winPrincipal.getCmbNoParte2().setEditable(false);

        winPrincipal.getLblMatFaltante2().setVisible(false);
        winPrincipal.getTxtMatFaltante2().setVisible(false);
        
        winPrincipal.getLblDur2().setVisible(true);
        winPrincipal.getTxtDur2().setVisible(true); 
        winPrincipal.getTxtDur2().setEnabled(false); 
        winPrincipal.getTxtDur2().setText("");

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }
    
    public void panelCambios2(Principal winPrincipal) {

        winPrincipal.getCmbArea2().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema2().getSelectedItem().toString()));

        winPrincipal.getCmbArea2().setEnabled(true);
        winPrincipal.getCmbProblema2().setEnabled(false);

        winPrincipal.getLblOperacion2().setVisible(false);
        winPrincipal.getCmbOperacion2().setVisible(false);

        winPrincipal.getLblArea2().setVisible(true);
        winPrincipal.getCmbArea2().setVisible(true);

        if (winPrincipal.getCmbProblema2().getItemCount() != 0){
            winPrincipal.getCmbProblema2().setSelectedIndex(-1);
        }
        winPrincipal.getLblProblema2().setVisible(false);
        winPrincipal.getCmbProblema2().setVisible(false);

        winPrincipal.getLblScrap2().setVisible(true);
        winPrincipal.getTxtScrap2().setVisible(true);
        winPrincipal.getTxtScrap2().setEnabled(false);
        
        if (winPrincipal.getCmbCliente2().getItemCount() != 0){
            winPrincipal.getCmbCliente2().setSelectedIndex(0);
        }

        winPrincipal.getLblCliente2().setVisible(true);
        winPrincipal.getCmbCliente2().setVisible(true);
        winPrincipal.getCmbCliente2().setEnabled(false);

        if (winPrincipal.getCmbNoParte2().getItemCount() != 0){
            winPrincipal.getCmbNoParte2().setSelectedIndex(0);
        }
        
        winPrincipal.getLblNoParte2().setVisible(true);
        winPrincipal.getCmbNoParte2().setVisible(true);
        winPrincipal.getCmbNoParte2().setEnabled(false);
        
        winPrincipal.getLblDur2().setVisible(true);
        winPrincipal.getTxtDur2().setVisible(true); 
        winPrincipal.getTxtDur2().setEnabled(false); 
        winPrincipal.getTxtDur2().setText("");

        winPrincipal.getLblMatFaltante2().setVisible(false);
        winPrincipal.getTxtMatFaltante2().setVisible(false);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl3().setVisible(false);
        winPrincipal.getPnl4().setVisible(false);
    }
    
    //******** APARTADO PARA EL PANEL 3
    public void panel3(Principal winPrincipal) {        
        winPrincipal.getCmbTema3().setSelectedIndex(0);
        
        winPrincipal.getCmbTema2().setEnabled(false);
        winPrincipal.getCmbArea2().setEnabled(true);
        winPrincipal.getCmbOperacion2().setEnabled(false);
        winPrincipal.getCmbArea2().setEnabled(false);
        winPrincipal.getCmbProblema2().setEnabled(false);
        winPrincipal.getTxtMatFaltante2().setEnabled(false);
        winPrincipal.getTxtScrap2().setEnabled(false);

        winPrincipal.getCmbCliente2().setEnabled(false);
        winPrincipal.getCmbNoParte2().setEnabled(false);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        
        winPrincipal.getTxtDur2().setEnabled(false);
        winPrincipal.getLblDur3().setVisible(false);
        winPrincipal.getTxtDur3().setVisible(false); 
        winPrincipal.getTxtDur3().setEnabled(false);
        winPrincipal.getTxtDur3().setText("");

        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getTxtTiempoFin().setEnabled(false);         
        
        winPrincipal.getBtnAgregarBitacora().setEnabled(false);        
        
        winPrincipal.getPnl3().setVisible(true);
        winPrincipal.getCmbTema3().setEnabled(true);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getLblOperacion3().setVisible(false);
        winPrincipal.getCmbOperacion3().setVisible(false);

        winPrincipal.getLblArea3().setVisible(false);
        winPrincipal.getCmbArea3().setVisible(false);

        if (winPrincipal.getCmbProblema3().getItemCount() != 0){
            winPrincipal.getCmbProblema3().setSelectedIndex(-1);
        }
        
        winPrincipal.getLblProblema3().setVisible(false);
        winPrincipal.getCmbProblema3().setVisible(false);

        winPrincipal.getLblScrap3().setVisible(false);
        winPrincipal.getTxtScrap3().setVisible(false);

        winPrincipal.getLblCliente3().setVisible(false);
        winPrincipal.getCmbCliente3().setVisible(false);

        winPrincipal.getLblNoParte3().setVisible(false);
        winPrincipal.getCmbNoParte3().setVisible(false);

        winPrincipal.getLblMatFaltante3().setVisible(false);
        winPrincipal.getTxtMatFaltante3().setVisible(false);
    
        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);

        winPrincipal.getPnl4().setVisible(false);
    }

    public void panelCalidad3(Principal winPrincipal) {     
        //APARTADO DE MODULOS QUE SE MUESTRAN        
        winPrincipal.getCmbOperacion3().setModel(listaOperacionesCalidad(winPrincipal.getCmbLinea().getSelectedItem().toString()));
        winPrincipal.getCmbOperacion3().setEnabled(true);
        winPrincipal.getCmbOperacion3().setVisible(true);
        
        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");        
        winPrincipal.getTxtCantPzas().setVisible(false);   
        
        winPrincipal.getLblOperacion3().setVisible(true);
        winPrincipal.getCmbOperacion3().setVisible(true);
        
        if (winPrincipal.getCmbArea3().getItemCount() != 0){
            winPrincipal.getCmbArea3().setSelectedIndex(0);
        }
        winPrincipal.getLblArea3().setVisible(true);
        winPrincipal.getCmbArea3().setVisible(true);
        winPrincipal.getCmbArea3().setEnabled(false);
        
        if (winPrincipal.getCmbProblema3().getItemCount() != 0){
            winPrincipal.getCmbProblema3().setSelectedIndex(-1);
        }
        winPrincipal.getLblProblema3().setVisible(true);
        winPrincipal.getCmbProblema3().setVisible(true);
        winPrincipal.getCmbProblema3().setEnabled(false);
        
        winPrincipal.getLblScrap3().setVisible(true);
        winPrincipal.getTxtScrap3().setVisible(true);
        
        winPrincipal.getLblCliente3().setVisible(false);
        winPrincipal.getCmbCliente3().setVisible(false);
        
        winPrincipal.getLblNoParte3().setVisible(false);
        winPrincipal.getCmbNoParte3().setVisible(false);
        
        winPrincipal.getLblDur3().setVisible(true);
        winPrincipal.getTxtDur3().setVisible(true); 
        winPrincipal.getTxtDur3().setEnabled(false);
        winPrincipal.getTxtDur3().setText("");
        
        winPrincipal.getLblMatFaltante3().setVisible(false);
        winPrincipal.getTxtMatFaltante3().setVisible(false);
        
        winPrincipal.getLblDur3().setVisible(true);
        winPrincipal.getTxtDur3().setVisible(true); 
        
        winPrincipal.getPnl4().setVisible(false);
    }
    
    public void panelOrganizacionales3(Principal winPrincipal) {        
        winPrincipal.getCmbArea3().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema3().getSelectedItem().toString()));

        //if (winPrincipal.getCmbArea3().getItemCount() != 0){
            winPrincipal.getCmbArea3().setSelectedIndex(0);
        //}

        winPrincipal.getLblArea3().setVisible(true);
        winPrincipal.getCmbArea3().setVisible(true);
        winPrincipal.getCmbArea3().setEnabled(true);
        
        if (winPrincipal.getCmbProblema3().getItemCount() != 0){
            winPrincipal.getCmbProblema3().setSelectedIndex(-1);
        }
        
        winPrincipal.getLblProblema3().setVisible(true);
        winPrincipal.getCmbProblema3().setVisible(true);
        winPrincipal.getCmbProblema3().setEnabled(false);       

        winPrincipal.getLblOperacion3().setVisible(false);
        winPrincipal.getCmbOperacion3().setVisible(false);

        winPrincipal.getLblArea3().setVisible(true);
        winPrincipal.getCmbArea3().setVisible(true);

        winPrincipal.getLblScrap3().setVisible(false);
        winPrincipal.getTxtScrap3().setVisible(false);
        winPrincipal.getTxtScrap3().setEnabled(false);

        if (winPrincipal.getCmbCliente3().getItemCount() != 0) {
            winPrincipal.getCmbCliente3().setSelectedIndex(0);
        }
        
        winPrincipal.getLblCliente3().setVisible(false);
        winPrincipal.getCmbCliente3().setVisible(false);
        winPrincipal.getCmbCliente3().setEnabled(false);

        if (winPrincipal.getCmbNoParte3().getItemCount() != 0) {
            winPrincipal.getCmbNoParte3().setSelectedIndex(0);
        }
        winPrincipal.getLblNoParte3().setVisible(false);
        winPrincipal.getCmbNoParte3().setVisible(false);
        winPrincipal.getCmbNoParte3().setEditable(false);

        winPrincipal.getLblDur3().setVisible(true);
        winPrincipal.getTxtDur3().setVisible(true); 
        winPrincipal.getTxtDur3().setEnabled(false);
        winPrincipal.getTxtDur3().setText("");
        
        winPrincipal.getLblMatFaltante3().setVisible(false);
        winPrincipal.getTxtMatFaltante3().setVisible(false);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);

        winPrincipal.getPnl4().setVisible(false);
    }
    
    public void panelCambios3(Principal winPrincipal) {

        winPrincipal.getCmbArea3().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema3().getSelectedItem().toString()));

        winPrincipal.getCmbArea3().setEnabled(true);

        winPrincipal.getLblOperacion3().setVisible(false);
        winPrincipal.getCmbOperacion3().setVisible(false);

        winPrincipal.getLblArea3().setVisible(true);
        winPrincipal.getCmbArea3().setVisible(true);
        
        if (winPrincipal.getCmbProblema3().getItemCount() != 0){
            winPrincipal.getCmbProblema3().setSelectedIndex(-1);
        }

        winPrincipal.getLblProblema3().setVisible(false);
        winPrincipal.getCmbProblema3().setVisible(false);

        winPrincipal.getLblScrap3().setVisible(true);
        winPrincipal.getTxtScrap3().setVisible(true);
        winPrincipal.getTxtScrap3().setEnabled(false);

        winPrincipal.getLblCliente3().setVisible(true);
        winPrincipal.getCmbCliente3().setVisible(true);
        winPrincipal.getCmbCliente3().setEnabled(false);

        winPrincipal.getLblNoParte3().setVisible(true);
        winPrincipal.getCmbNoParte3().setVisible(true);
        
        winPrincipal.getLblDur3().setVisible(true);
        winPrincipal.getTxtDur3().setVisible(true); 
        winPrincipal.getTxtDur3().setEnabled(false);
        winPrincipal.getTxtDur3().setText("");

        winPrincipal.getLblMatFaltante3().setVisible(false);
        winPrincipal.getTxtMatFaltante3().setVisible(false);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getPnl4().setVisible(false);
    }
    
    
    //**************APARTADO PANEL 4
    public void panel4(Principal winPrincipal) {
        
        winPrincipal.getCmbTema3().setEnabled(false);
        winPrincipal.getCmbArea3().setEnabled(false);
        winPrincipal.getCmbOperacion3().setEnabled(false);
        winPrincipal.getCmbArea3().setEnabled(false);
        
        winPrincipal.getCmbProblema3().setEnabled(false);
        winPrincipal.getTxtMatFaltante3().setEnabled(false);
        winPrincipal.getTxtScrap3().setEnabled(false);

        winPrincipal.getCmbCliente3().setEnabled(false);
        winPrincipal.getCmbNoParte3().setEnabled(false);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        
        winPrincipal.getLblDur4().setVisible(false);
        winPrincipal.getTxtDur4().setVisible(false);        

        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getTxtTiempoFin().setEnabled(false); 
        
        winPrincipal.getTxtDur3().setEnabled(false);
        winPrincipal.getBtnAgregarBitacora().setEnabled(false);        
        
        winPrincipal.getPnl4().setVisible(true);
        winPrincipal.getCmbTema4().setSelectedIndex(0);
        winPrincipal.getCmbTema4().setEnabled(true);

        //OCULTAMOS LOS COMPONENTES QUE NO NECESITAMOS 
        winPrincipal.getLblOperacion4().setVisible(false);
        winPrincipal.getCmbOperacion4().setVisible(false);

        winPrincipal.getLblArea4().setVisible(false);
        winPrincipal.getCmbArea4().setVisible(false);

        winPrincipal.getLblProblema4().setVisible(false);
        winPrincipal.getCmbProblema4().setVisible(false);

        winPrincipal.getLblScrap4().setVisible(false);
        winPrincipal.getTxtScrap4().setVisible(false);

        winPrincipal.getLblCliente4().setVisible(false);
        winPrincipal.getCmbCliente4().setVisible(false);

        winPrincipal.getLblNoParte4().setVisible(false);
        winPrincipal.getCmbNoParte4().setVisible(false);

        winPrincipal.getLblMatFaltante4().setVisible(false);
        winPrincipal.getTxtMatFaltante4().setVisible(false);
    
        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);
    }

    public void panelCalidad4(Principal winPrincipal) {     
        //APARTADO DE MODULOS QUE SE MUESTRAN        
        winPrincipal.getCmbOperacion4().setModel(listaOperacionesCalidad(winPrincipal.getCmbLinea().getSelectedItem().toString()));
        winPrincipal.getCmbOperacion4().setEnabled(true);
        winPrincipal.getCmbOperacion4().setVisible(true);
        
        winPrincipal.getLblCantPzas().setVisible(false);
        winPrincipal.getTxtCantPzas().setText("");        
        winPrincipal.getTxtCantPzas().setVisible(false);   
        
        winPrincipal.getLblOperacion4().setVisible(true);
        winPrincipal.getCmbOperacion4().setVisible(true);
        
        if (winPrincipal.getCmbArea4().getItemCount() != 0){
            winPrincipal.getCmbArea4().setSelectedIndex(0);
        }
        winPrincipal.getLblArea4().setVisible(true);
        winPrincipal.getCmbArea4().setVisible(true);
        winPrincipal.getCmbArea4().setEnabled(false);
        
        if (winPrincipal.getCmbProblema4().getItemCount() != 0){
            winPrincipal.getCmbProblema4().setSelectedIndex(-1);
        }
        winPrincipal.getLblProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setEnabled(false);
        
        winPrincipal.getLblScrap4().setVisible(true);
        winPrincipal.getTxtScrap4().setVisible(true);
        
        winPrincipal.getLblCliente4().setVisible(false);
        winPrincipal.getCmbCliente4().setVisible(false);
        
        winPrincipal.getLblNoParte4().setVisible(false);
        winPrincipal.getCmbNoParte4().setVisible(false);
        
        winPrincipal.getLblDur4().setVisible(true);
        winPrincipal.getTxtDur4().setVisible(true); 
        winPrincipal.getTxtDur4().setEnabled(false);
        winPrincipal.getTxtDur4().setText("");
        
        winPrincipal.getLblMatFaltante4().setVisible(false);
        winPrincipal.getTxtMatFaltante4().setVisible(false);
        
        winPrincipal.getLblDur4().setVisible(true);
        winPrincipal.getTxtDur4().setVisible(true); 
    }
    
    public void panelOrganizacionales4(Principal winPrincipal) {        
        winPrincipal.getCmbArea4().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema4().getSelectedItem().toString()));

        winPrincipal.getCmbArea4().setSelectedIndex(0);

        winPrincipal.getLblArea4().setVisible(true);
        winPrincipal.getCmbArea4().setVisible(true);
        winPrincipal.getCmbArea4().setEnabled(true);
        
        if (winPrincipal.getCmbProblema4().getItemCount() != 0){
            winPrincipal.getCmbProblema4().setSelectedIndex(-1);
        }
        
        winPrincipal.getLblProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setEnabled(false);       

        winPrincipal.getLblOperacion4().setVisible(false);
        winPrincipal.getCmbOperacion4().setVisible(false);

        winPrincipal.getLblArea4().setVisible(true);
        winPrincipal.getCmbArea4().setVisible(true);
        
        winPrincipal.getLblProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setVisible(true);
        winPrincipal.getCmbProblema4().setEnabled(false);

        winPrincipal.getLblScrap4().setVisible(false);
        winPrincipal.getTxtScrap4().setVisible(false);
        winPrincipal.getTxtScrap4().setEnabled(false);

        if (winPrincipal.getCmbCliente4().getItemCount() != 0) {
            winPrincipal.getCmbCliente4().setSelectedIndex(0);
        }
        
        winPrincipal.getLblCliente4().setVisible(false);
        winPrincipal.getCmbCliente4().setVisible(false);
        winPrincipal.getCmbCliente4().setEnabled(false);

        if (winPrincipal.getCmbNoParte4().getItemCount() != 0) {
            winPrincipal.getCmbNoParte4().setSelectedIndex(0);
        }
        winPrincipal.getLblNoParte4().setVisible(false);
        winPrincipal.getCmbNoParte4().setVisible(false);
        winPrincipal.getCmbNoParte4().setEditable(false);

        winPrincipal.getLblDur4().setVisible(true);
        winPrincipal.getTxtDur4().setVisible(true); 
        winPrincipal.getTxtDur4().setEnabled(false);
        winPrincipal.getTxtDur4().setText("");
        
        winPrincipal.getLblMatFaltante4().setVisible(false);
        winPrincipal.getTxtMatFaltante4().setVisible(false);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        winPrincipal.getBtnAdd().setEnabled(false);
    }
    
    public void panelCambios4(Principal winPrincipal) {

        winPrincipal.getCmbArea4().setModel(new PrincipalMetodos().listaAreasOrganizacional(
                PrincipalControl.linea, winPrincipal.getCmbTema4().getSelectedItem().toString()));

        winPrincipal.getCmbArea4().setEnabled(true);
        winPrincipal.getCmbProblema4().setEnabled(false);

        winPrincipal.getLblOperacion4().setVisible(false);
        winPrincipal.getCmbOperacion4().setVisible(false);

        winPrincipal.getLblArea4().setVisible(true);
        winPrincipal.getCmbArea4().setVisible(true);

        if (winPrincipal.getCmbProblema4().getItemCount() != 0){
            winPrincipal.getCmbProblema4().setSelectedIndex(-1);
        }
        
        winPrincipal.getLblProblema4().setVisible(false);
        winPrincipal.getCmbProblema4().setVisible(false);

        winPrincipal.getLblScrap4().setVisible(true);
        winPrincipal.getTxtScrap4().setVisible(true);
        winPrincipal.getTxtScrap4().setEnabled(false);

        winPrincipal.getLblCliente4().setVisible(true);
        winPrincipal.getCmbCliente4().setVisible(true);
        winPrincipal.getCmbCliente4().setEnabled(false);

        winPrincipal.getLblNoParte4().setVisible(true);
        winPrincipal.getCmbNoParte4().setVisible(true);
        winPrincipal.getCmbNoParte4().setEnabled(false);
        
        winPrincipal.getLblDur4().setVisible(true);
        winPrincipal.getTxtDur4().setVisible(true); 
        winPrincipal.getTxtDur4().setEnabled(false);
        winPrincipal.getTxtDur4().setText("");

        winPrincipal.getLblMatFaltante4().setVisible(false);
        winPrincipal.getTxtMatFaltante4().setVisible(false);

    }
    
    //*******´PANELS PARA BOTON REMOVE
    
    //REMOVE 4 
    public void panel4R (Principal winPrincipal) {      
        PrincipalControl.contAdd = 2;        
        //LIMPIA LOS COMPONENTES DEL PANEL 4
        
        //HABILITAMOS LOS COMPONENTES DEL PANEL 3
        winPrincipal.getCmbTema3().setEnabled(true);
        winPrincipal.getCmbArea3().setEnabled(true);
        winPrincipal.getCmbOperacion3().setEnabled(true);
                
        winPrincipal.getCmbProblema3().setEnabled(true);
        winPrincipal.getTxtMatFaltante3().setEnabled(true);
        winPrincipal.getTxtScrap3().setEnabled(true);

        winPrincipal.getCmbCliente3().setEnabled(true);
        winPrincipal.getCmbNoParte3().setEnabled(true);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
        
        //winPrincipal.getTxtDur4().setVisible(true);        
        winPrincipal.getBtnAdd().setEnabled(true);
        
        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getTxtTiempoFin().setEnabled(false); 
        
        winPrincipal.getTxtDur3().setEnabled(true);
        winPrincipal.getBtnAgregarBitacora().setEnabled(true);        
        
        winPrincipal.getPnl4().setVisible(false);
    }
    
    //REMOVE 3    
    public void panel3R (Principal winPrincipal) {      
        PrincipalControl.contAdd = 1;
        
        //LIMPIA LOS COMPONENTES DEL PANEL 4
        
        //HABILITAMOS LOS COMPONENTES DEL PANEL 3
        winPrincipal.getCmbTema2().setEnabled(true);
        winPrincipal.getCmbArea2().setEnabled(true);
        winPrincipal.getCmbOperacion2().setEnabled(true);
                
        winPrincipal.getCmbProblema2().setEnabled(true);
        winPrincipal.getTxtMatFaltante2().setEnabled(true);
        winPrincipal.getTxtScrap2().setEnabled(true);

        winPrincipal.getCmbCliente2().setEnabled(true);
        winPrincipal.getCmbNoParte2().setEnabled(true);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);
          
        winPrincipal.getBtnAdd().setEnabled(true);

        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getTxtTiempoFin().setEnabled(false); 
        
        winPrincipal.getTxtDur2().setEnabled(true);
        winPrincipal.getBtnAgregarBitacora().setEnabled(true);        
        
        winPrincipal.getPnl3().setVisible(false);
    }
    
    //REMOVE 2
    public void panel2R (Principal winPrincipal) {      
        PrincipalControl.contAdd = 0;
        
        //LIMPIA LOS COMPONENTES DEL PANEL 4        
        //HABILITAMOS LOS COMPONENTES DEL PANEL 3
        winPrincipal.getCmbArea1().setEnabled(true);
        winPrincipal.getCmbOperacion1().setEnabled(true);
                
        winPrincipal.getCmbProblema1().setEnabled(true);
        winPrincipal.getTxtMatFaltante1().setEnabled(true);
        winPrincipal.getTxtScrap1().setEnabled(true);

        winPrincipal.getCmbCliente1().setEnabled(true);
        winPrincipal.getCmbNoParte1().setEnabled(true);

        winPrincipal.getBtnRemove().setVisible(true);
        winPrincipal.getBtnRemove().setEnabled(true);     
        
        winPrincipal.getCmbHora().setEnabled(true);
        winPrincipal.getTxtTiempoInicio().setEnabled(true);
        winPrincipal.getTxtTiempoFin().setEnabled(true); 
        
        winPrincipal.getBtnAgregarBitacora().setEnabled(true);
        
        winPrincipal.getBtnAdd().setEnabled(true);
        winPrincipal.getBtnRemove().setEnabled(false);
        
        winPrincipal.getPnl2().setVisible(false);
    }
    
    //LIMPIEZA A APARTIR DE ESTE PUNTO        
    public void panelTiempoFaltante(Principal winPrincipal) {
        if (winPrincipal.getTblBitacora().getRowCount() <= 0) {
            JOptionPane.showMessageDialog(
                    winPrincipal, "No hay horas por justificar", "Mensaje",
                    JOptionPane.INFORMATION_MESSAGE
            );
        } else {
            PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel());
            winPrincipal.getPnlProduccionCollapsible().repaint();
            winPrincipal.getCmbHora().setEnabled(true);
        }
    }

    public void agregarRegistroBitacora(Principal winPrincipal) {
        clienteP = winPrincipal.getCmbCliente().getSelectedItem().toString();        
        noParteP = winPrincipal.getCmbNoParte().getSelectedItem().toString();
        hora = Integer.parseInt(winPrincipal.getCmbHora().getSelectedItem().toString());
        
        if (winPrincipal.getTxtTiempoInicio().getText().length() < 1){
            minIni = 0;
        } else {
            minIni = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
        }
        
        if(winPrincipal.getTxtTiempoFin().getText().length() < 1){
            minFin = 0;
        } else {
            minFin = Integer.parseInt(winPrincipal.getTxtTiempoFin().getText());
        }
            
        int valorTema = winPrincipal.getCmbTema().getSelectedIndex();
        modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
        registroBitacora = modeloRegistroBitacora(winPrincipal, registroBitacora);
        winPrincipal.getBtnGuardar().setEnabled(true);
        
        //System.out.println("1. utils.PrincipalMetodos.agregarRegistroBitacora().row "+winPrincipal.getTblBitacora().getRowCount()+" contAdd: "+PrincipalControl.contAdd);
        if (winPrincipal.getTblBitacora().getRowCount() == 0) {
            switch(PrincipalControl.contAdd){   
                case 0:
                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    break;
                case 1:
                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    break;
                case 2:
                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    break;
                case 3:
                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                    ordenarTabla(modeloTabla);
                    insertarRegistroFilaAccess(winPrincipal);
                    break;    
            } 
            winPrincipal.getCmbTema().setSelectedIndex(valorTema);
        } else {
            //AQUI ENTRA
            tiempoHora = new ArrayList();
            for (int c = 0; c < modeloTabla.getRowCount(); c++) {
                if (modeloTabla.getValueAt(c, 2).toString().equals(registroBitacora[2].toString())) {
                    tiempos = new Object[4];
                    tiempos[0] = modeloTabla.getValueAt(c, 2);
                    tiempos[1] = modeloTabla.getValueAt(c, 3);
                    tiempos[2] = modeloTabla.getValueAt(c, 4);
                    tiempos[3] = modeloTabla.getValueAt(c, 5);
                    tiempoHora.add(tiempos);
                }
            }
            
            if (!tiempoHora.isEmpty()) { //INSERT DE ACUERDO A LOS PANELS ACTIVADOS
                //System.out.println("utils.PrincipalMetodos.agregarRegistroBitacora().!tiempoHora.isEmpty(): "+tiempoHora.size());
                for (int i = 0; i < tiempoHora.size(); i++) {
                    registroBitacoraTmp = (Object[]) tiempoHora.get(i);
                    registroBitacoraTmpAux = null;
                    if (tiempoHora.size() > 1 && i < tiempoHora.size() - 1) {
                        registroBitacoraTmpAux = (Object[]) tiempoHora.get(i + 1);
                    }
                    //System.out.println(registroBitacora[3].toString()+"- "+Integer.parseInt(registroBitacoraTmp[1].toString()));
                    if (Integer.parseInt(registroBitacora[3].toString()) >= Integer.parseInt(registroBitacoraTmp[1].toString())) {
                        if (Integer.parseInt(registroBitacora[3].toString()) <= Integer.parseInt(registroBitacoraTmp[2].toString()) && winPrincipal.getCmbTema().getSelectedIndex() != 2 ) {                            
                            JOptionPane.showMessageDialog(
                                    winPrincipal, "Registro dentro de intervalo no válido\n Revisar tiempo de inicio y fin",
                                    "Advertencia", JOptionPane.WARNING_MESSAGE
                            );
                            winPrincipal.getCmbHora().setEnabled(true);
                            winPrincipal.getTxtTiempoInicio().setEnabled(true);
                            winPrincipal.getTxtTiempoFin().setEnabled(true);
                            winPrincipal.getTxtTiempoInicio().requestFocus();
                            break;
                        } else if ((Integer.parseInt(registroBitacora[3].toString()) > Integer.parseInt(registroBitacoraTmp[2].toString()) ||
                                     Integer.parseInt(registroBitacora[5].toString()) == 0 && winPrincipal.getCmbTema().getSelectedItem().equals("Calidad"))) {
                            if (registroBitacoraTmpAux != null) {
                                if (Integer.parseInt(registroBitacora[4].toString()) < Integer.parseInt(registroBitacoraTmpAux[1].toString())) {                                    
                                    if (winPrincipal.getCmbTema().getSelectedItem().equals("Tiempo Faltante")) {
                                        if ((Integer.parseInt(registroBitacora[5].toString()) + getValueTblBitacoraTiempoFaltante(winPrincipal)) <= 20) {
                                            System.out.println("2. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                                            switch(PrincipalControl.contAdd){   
                                                case 0:
                                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    limpiaPanelGeneral(winPrincipal);
                                                    break;
                                                case 1:
                                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    limpiaPanelGeneral(winPrincipal);
                                                    break;
                                                case 2:
                                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    limpiaPanelGeneral(winPrincipal);
                                                    break;
                                                case 3:
                                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                                    ordenarTabla(modeloTabla);
                                                    insertarRegistroFilaAccess(winPrincipal);
                                                    limpiaPanelGeneral(winPrincipal);
                                                    break;    
                                            } 
                                            winPrincipal.getCmbTema().setSelectedIndex(0);
                                            winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                                            break;
                                        } else {
                                            JOptionPane.showMessageDialog(
                                                    winPrincipal, "El tiempo faltante ingresado\nexcede el límite permitido",
                                                    "Advertencia", JOptionPane.WARNING_MESSAGE
                                            );
                                        }
                                    } else {
                                        System.out.println("3. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                                        switch(PrincipalControl.contAdd){   
                                            case 0:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 1:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 2:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 3:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;    
                                        } 
                                        //winPrincipal.getCmbHora().setModel(actualizaComboHoras(winPrincipal));
                                        winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                                        break;
                                    }
                                }
                            } else {
                                if (winPrincipal.getCmbTema().getSelectedItem().equals("Tiempo Faltante")) {
                                    if ((Integer.parseInt(registroBitacora[5].toString()) + getValueTblBitacoraTiempoFaltante(winPrincipal)) <= 20) {
                                        System.out.println("5. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                                        switch(PrincipalControl.contAdd){   
                                            case 0:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 1:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 2:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;
                                            case 3:
                                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                                ordenarTabla(modeloTabla);
                                                insertarRegistroFilaAccess(winPrincipal);
                                                limpiaPanelGeneral(winPrincipal);
                                                break;    
                                        } 
                                        winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                                        break;
                                    } else {
                                        JOptionPane.showMessageDialog(
                                                winPrincipal, "El tiempo faltante ingresado\nexcede el límite permitido",
                                                "Advertencia", JOptionPane.WARNING_MESSAGE
                                        );
                                    }
                                } else {
                                    System.out.println("4. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                                    switch(PrincipalControl.contAdd){   
                                        case 0:
                                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            limpiaPanelGeneral(winPrincipal);
                                            break;
                                        case 1:
                                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            limpiaPanelGeneral(winPrincipal);
                                            break;
                                        case 2:
                                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            limpiaPanelGeneral(winPrincipal);
                                            break;
                                        case 3:
                                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                            ordenarTabla(modeloTabla);
                                            insertarRegistroFilaAccess(winPrincipal);
                                            limpiaPanelGeneral(winPrincipal);
                                            break;    
                                    } 
                                    winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                                    break;
                                }
                            }
                        }
                    } else if (Integer.parseInt(registroBitacora[4].toString()) < Integer.parseInt(registroBitacoraTmp[1].toString())) {
                        if (winPrincipal.getCmbTema().getSelectedItem().equals("Tiempo Faltante")) {
                            if ((Integer.parseInt(registroBitacora[5].toString()) + getValueTblBitacoraTiempoFaltante(winPrincipal)) <= 20) {
                                System.out.println("6. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                                switch(PrincipalControl.contAdd){   
                                    case 0:
                                        modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        limpiaPanelGeneral(winPrincipal);
                                        break;
                                    case 1:
                                        modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        limpiaPanelGeneral(winPrincipal);
                                        break;
                                    case 2:
                                        modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        limpiaPanelGeneral(winPrincipal);
                                        break;
                                    case 3:
                                        modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                        ordenarTabla(modeloTabla);
                                        insertarRegistroFilaAccess(winPrincipal);
                                        limpiaPanelGeneral(winPrincipal);
                                        break;    
                                } 
                                winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                                break;
                            } else {
                                JOptionPane.showMessageDialog(
                                        winPrincipal, "El tiempo faltante ingresado\nexcede el límite permitido",
                                        "Advertencia", JOptionPane.WARNING_MESSAGE
                                );
                                winPrincipal.getCmbHora().setEnabled(true);
                                winPrincipal.getTxtTiempoInicio().setEnabled(true);
                                winPrincipal.getTxtTiempoFin().setEnabled(true);
                                winPrincipal.getTxtTiempoInicio().requestFocus();
                            }
                        } else {
                            System.out.println("7. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                            switch(PrincipalControl.contAdd){   
                                case 0:
                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    limpiaPanelGeneral(winPrincipal);
                                    break;
                                case 1:
                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    limpiaPanelGeneral(winPrincipal);
                                    break;
                                case 2:
                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    limpiaPanelGeneral(winPrincipal);
                                    break;
                                case 3:
                                    modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                    ordenarTabla(modeloTabla);
                                    insertarRegistroFilaAccess(winPrincipal);
                                    limpiaPanelGeneral(winPrincipal);
                                    break;    
                            } 
                            winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                            break;
                        }
                    } else {
                        JOptionPane.showMessageDialog(
                                winPrincipal, "Tiempo de registro duplicado \n Revisar tiempo de inicio y fin",
                                "Advertencia", JOptionPane.WARNING_MESSAGE
                        );
                        winPrincipal.getCmbHora().setEnabled(true);
                        winPrincipal.getTxtTiempoInicio().setEnabled(true);
                        winPrincipal.getTxtTiempoFin().setEnabled(true);
                        winPrincipal.getTxtTiempoInicio().requestFocus();
                    }
                }
            } else {
                System.out.println("utils.PrincipalMetodos.agregarRegistroBitacora().tiempoHora.isEmpty(): ");
                if (winPrincipal.getCmbTema().getSelectedItem().equals("Tiempo Faltante")) {
                    if ((Integer.parseInt(registroBitacora[5].toString()) + getValueTblBitacoraTiempoFaltante(winPrincipal)) <= 20) {    
                        System.out.println("8. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                        switch(PrincipalControl.contAdd){   
                            case 0:
                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                limpiaPanelGeneral(winPrincipal);
                                break;
                            case 1:
                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                limpiaPanelGeneral(winPrincipal);
                                break;
                            case 2:
                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                limpiaPanelGeneral(winPrincipal);
                                break;
                            case 3:
                                modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                                ordenarTabla(modeloTabla);
                                insertarRegistroFilaAccess(winPrincipal);
                                limpiaPanelGeneral(winPrincipal);
                                break;    
                        } 
                        winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                    } else {
                        JOptionPane.showMessageDialog(
                                winPrincipal, "El tiempo faltante ingresado\nexcede el límite permitido",
                                "Advertencia", JOptionPane.WARNING_MESSAGE
                        );
                    }
                } else {
                    System.out.println("9. utils.PrincipalMetodos.agregarRegistroBitacora() contAdd: "+PrincipalControl.contAdd);
                    switch(PrincipalControl.contAdd){   
                        case 0:
                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            limpiaPanelGeneral(winPrincipal);
                            break;
                        case 1:
                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            limpiaPanelGeneral(winPrincipal);
                            break;
                        case 2:
                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            limpiaPanelGeneral(winPrincipal);
                            break;
                        case 3:
                            modeloTabla.addRow(modeloRegistroBitacora(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora2(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora3(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            modeloTabla.addRow(modeloRegistroBitacora4(winPrincipal, registroBitacora));
                            ordenarTabla(modeloTabla);
                            insertarRegistroFilaAccess(winPrincipal);
                            limpiaPanelGeneral(winPrincipal);
                            break;  
                    }                      
                    winPrincipal.getCmbTema().setSelectedIndex(valorTema);
                }
            }
        }        
        //VAMOS A LIMPIAR LOS COMBOS  DESPUES DE QUE AGREGEN EL/LOS PROBLEMAS
        actualizaOEEBitacoraHora(registroBitacora[1].toString(), registroBitacora[0].toString(), Integer.parseInt(registroBitacora[2].toString()));
    }

    public void actualizaOEEBitacoraHora(String fecha, String linea, int hora) {    
        //CORTES DE LA FECHA        
        String[] date = fecha.split("/");
        String d = date[0];
        String m = date[1];
        String y = date[2];

        //INICIAMOS VARIBALES PARA LOS CORRESPONDIENTES PORCENTAJES        
        int minDesc = 0; 
        int pzasProd = 0; 
        int minTec = 0;
        int minOrg = 0; 
        double minCali = 0; 
        int minCamb = 0; 
        int minTFal = 0;
        double minTPerdida = 0;
        int minHTotal = 0;
        int metaPzas = 0;
        int existReg = 0;
        int minRepHora = 0;

        double percentRest = 100.00;
        double percentProd = 0.0;
        double percentTec = 0.0;
        double percentOrg = 0.0;
        double percentPlan = 0.0;
        double percentCali = 0.0;
        double percentCamb = 0.0;
        double percentFal = 0.0;        
        try {
            //OBTENEMOS LA CANTIDAD DE PIEZAS
            pzasProd = new BitacoraDAOImpl().pzasProdHora(fecha, linea, hora);

            if (pzasProd != 0 && pzasProd > 0) {
                //TIEMPO CICLO PONDERADO, CUANDO SE TIENE PRODUCCION 
                tcPonderado = new BitacoraDAOImpl().tcPonderadoDia(fecha, linea, hora);                            
            } else {
                //TIEMPO CICLO PONDERADO, CUANDO TODA LA HORA SE TIENEN FALLAS Y NO SE PRODUCE 
                tcPonderado = new BitacoraDAOImpl().tcPonderadoDiaNoProduccion(fecha, linea, hora);                
            }

            minDesc = new BitacoraDAOImpl().minDesHora(fecha, linea, hora); 
            minRepHora = new BitacoraDAOImpl().minRepHora(fecha, linea, hora);
            if (hora == 23) {                 
                minHTotal = minRepHora - minDesc;                
                System.out.println("h23: "+minRepHora);
            } else {
                minHTotal = 60 - minDesc;
            }
            
            //TEMA DE PRODUCCION
            pzasProd = new BitacoraDAOImpl().pzasProdHora(fecha, linea, hora);
            
            //HACEMOS LA CONSULTA DE TIEMPOS POR TEMA
            minTec = new BitacoraDAOImpl().minTecHora(fecha, linea, hora);
            minOrg = new BitacoraDAOImpl().minOrgHora(fecha, linea, hora);
            minCali = new BitacoraDAOImpl().minCaliHora(fecha, linea, hora);
            
            minCamb = new BitacoraDAOImpl().minCamHora(fecha, linea, hora);
            minTFal = new BitacoraDAOImpl().minTFalHora(fecha, linea, hora);
            
            minTPerdida = minTec + minOrg + minCali + minCamb + minTFal;            
            System.out.println("utils.PrincipalMetodos.actualizaOEEBitacoraHora("+fecha+", "+linea+","+hora+")"+minCali+"<br>");            
            //HACEMOS EL CALCULO DE PORCENTAJE DE PRODUCCION
            //System.out.println("tcPonderado: "+tcPonderado);            
            if (tcPonderado == 0) {
                metaPzas = 0;
            } else {
                metaPzas = (int) ((minHTotal * 60) / tcPonderado);
            }
            
            //PORCENTAJES
            percentProd = (float) (pzasProd * 100) / metaPzas;

            if (percentProd > 0.0) {
                percentRest = percentTotal - percentProd;
            } else {
                percentRest = 100;
                percentProd = 0.0;
            }

            if (minTec == 0 && minOrg == 0 && minCali == 0 && minTFal == 0) {
                percentTec = 0.0;
                percentOrg = 0.0;
                percentPlan = 0.0;
                percentCali = 0.0;
                percentCamb = 0.0;
                percentFal = 0.0;
            } else {
                percentTec = (minTec * percentRest) / minTPerdida;
                percentOrg = (minOrg * percentRest) / minTPerdida;
                percentCali = (minCali * percentRest) / minTPerdida;
                percentCamb = (minCamb * percentRest) / minTPerdida;
                percentFal = (minTFal * percentRest) / minTPerdida;
            } 
            
            //APARTADO DE CORROBORACION DE PORCENTAJES 
            existReg = new BitacoraDAOImpl().regPercentsHora(fecha, linea, hora);
            //APARTADO PARA INSERT 
            if (existReg == 0) { //INSERTAR LOS REGISTROS PARA NUEVA HORA 
                new BitacoraDAOImpl().insertarPercentHoraDia(linea, fecha, d, m, y, hora, percentProd, percentTec, percentOrg, percentCali, percentCamb, percentFal, metaPzas);
                //System.out.println("1. h: " + hora + "%Rest: " + percentRest + " minPerdidos: " + minDesc + " \n metPzas: " + metaPzas + " pzasReal: " + pzasProd + " tc: " + tcPonderado + "\n tiempos \n tec:" + minTec + " org: " + minOrg + " cali: " + minCali + " cam: " + minCamb + " tFal: " + minTFal + " \n porcentajes \n prod: " + percentProd + " tec: " + percentTec + " org: " + percentOrg + " plan: " + percentPlan + " cali: " + percentCali + " camb: " + percentCamb + " tFal: " + percentFal);
            } else { //MODIFICAR LOS REGITROS DE HORA 
                //System.out.println("2. h: " + hora + "%Rest: " + percentRest + " minPerdidos: " + minDesc + " \n metPzas: " + metaPzas + " pzasReal: " + pzasProd + " tc: " + tcPonderado + "\n tiempos \n tec:" + minTec + " org: " + minOrg + " cali: " + minCali + " cam: " + minCamb + " tFal: " + minTFal + " \n porcentajes \n prod: " + percentProd + " tec: " + percentTec + " org: " + percentOrg + " plan: " + percentPlan + " cali: " + percentCali + " camb: " + percentCamb + " tFal: " + percentFal);
                new BitacoraDAOImpl().modificarPercentHoraDia(percentProd, percentTec, percentOrg, percentCali, percentCamb, percentFal, metaPzas, existReg);
            }
            
            //System.out.println(" --------------------- h: " + hora + " ------------------------- \n minRep"+minRepHora+"  %Rest: " + percentRest + " minPerdidos: " + minDesc + " \n metPzas: " + metaPzas + " pzasReal: " + pzasProd + " tc: " + tcPonderado + "\n tiempos \n tec:" + minTec + " org: " + minOrg + " cali: " + minCali + " cam: " + minCamb + " tFal: " + minTFal + " \n porcentajes \n prod: " + percentProd + " tec: " + percentTec + " org: " + percentOrg + " plan: " + percentPlan + " cali: " + percentCali + " camb: " + percentCamb + " tFal: " + percentFal);
            actualizaOEEBitacoraDia(fecha, linea); 
        } catch (Exception ex) {
            Logger.getLogger(PrincipalMetodos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void actualizaOEEBitacoraDia(String fecha, String linea) {
        //CORTES DE LA FECHA 
        String[] date = fecha.split("/");
        String d = date[0];
        String m = date[1];
        String y = date[2];

        //INICIAMOS VARIBALES PARA LOS CORRESPONDIENTES PORCENTAJES        
        int minDescD = 0;
        int minTrabajadosD = 0;
        int pzasProdD = 0;
        int minTecD = 0;
        int minOrgD = 0;
        int minPlanD = 0;
        double minCaliD = 0.0;
        int minCambD = 0;
        int minTFalD = 0; 
        double minTPerdidaD = 0.0; 
        int minDTotal = 0; 
        int metaPzasD = 0; 
        int existRegD = 0; 

        double percentRestD = 0;
        double percentProdD = 0;
        double percentTecD = 0;
        double percentOrgD = 0; 
        double percentCaliD = 0;
        double percentCambD = 0;
        double percentFalD = 0;

        try {
            minDescD = new BitacoraDAOImpl().minDesDia(fecha, linea);

            minTrabajadosD = new BitacoraDAOImpl().minTrabajadosDia(fecha, linea);
            minDTotal = minTrabajadosD - minDescD;

            //TEMA DE PRODUCCION            
            pzasProdD = new BitacoraDAOImpl().pzasProdDia(fecha, linea);
            metaPzasD = new BitacoraDAOImpl().pzasEspDia(fecha, linea);
            //tcProdD = new BitacoraDAOImpl().tcHora(fecha, linea);

            //HACEMOS LA CONSULTA DE TIEMPOS POR TEMA
            minTecD = new BitacoraDAOImpl().minTecDia(fecha, linea);
            minOrgD = new BitacoraDAOImpl().minOrgDia(fecha, linea);
            minCaliD = new BitacoraDAOImpl().minCaliDia(fecha, linea);
            System.out.println("utils.PrincipalMetodos.actualizaOEEBitacoraDIA().CALIDAD ("+fecha+","+linea+ "): "+minCaliD);
            minCambD = new BitacoraDAOImpl().minCamDia(fecha, linea);
            minTFalD = new BitacoraDAOImpl().minTFalDia(fecha, linea);

            minTPerdidaD = minTecD + minOrgD + minCaliD + minCambD + minTFalD;

            //HACEMOS EL CALCULO DE PORCENTAJE DE PRODUCCION
            //PORCENTAJES
            percentProdD = (float) (pzasProdD * 100) / metaPzasD;
            if (!(percentProdD >= 0.0)) {
                percentProdD = 0.0;
            }

            percentRestD = 100 - percentProdD; 
            if (minTecD == 0 && minOrgD == 0 && minCaliD == 0 && minTFalD == 0 && minCambD == 0 ) {
                percentTecD = 0;
                percentOrgD = 0;
                percentCaliD = 0;
                percentCambD = 0;
                percentFalD = 0;
            } else {
                percentTecD = (minTecD * percentRestD) / minTPerdidaD;
                percentOrgD = (minOrgD * percentRestD) / minTPerdidaD;
                percentCaliD = (minCaliD * percentRestD) / minTPerdidaD;
                percentCambD = (minCambD * percentRestD) / minTPerdidaD;
                percentFalD = (minTFalD * percentRestD) / minTPerdidaD;
            } 

            //APARTADO DE CORROBORACION DE PORCENTAJES
//            //System.out.println("\n****************************************************** DIA *******************************************************************\n "
//                    + "minPerdidos: " + minDescD + " minTotales: " + minDTotal + " \n metPzas: " + metaPzasD + " pzasReal: " + pzasProdD + "\n tiempos \n tec:" + minTecD + " org: " + minOrgD + " plan: " + minPlanD + " cali: " + minCaliD + " cam: " + minCambD + " tFal: " + minTFalD + " \n porcentajes \n prod: " + percentProdD + " tec: " + percentTecD + " org: " + percentOrgD + " cali: " + percentCaliD + " camb: " + percentCambD + " tFal: " + percentFalD + "\n");
//            System.out.println("dat: " + pzasProdD + " mta: " + metaPzasD + " %:" + percentProdD+ " cali: "+percentCaliD );
            existRegD = new BitacoraDAOImpl().regPercentsDia(fecha, linea);
            
            //APARTADO PARA INSERT 
            if (existRegD == 0) { //INSERTAR LOS REGISTROS PARA NUEVA HORA
                new BitacoraDAOImpl().insertarPercentDia(linea, fecha, d, m, y, percentProdD, percentTecD, percentOrgD, percentCaliD, percentCambD, percentFalD);
            } else { //MODIFICAR LOS REGITROS DE HORA  
                new BitacoraDAOImpl().modificarPercentDia(percentProdD, percentTecD, percentOrgD, percentCaliD, percentCambD, percentFalD, existRegD);
            }
        } catch (Exception ex) {
            Logger.getLogger(PrincipalMetodos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ordenarTabla(DefaultTableModel modeloTabla) {
        for (int i = modeloTabla.getRowCount() - 1; i >= 0; i--) {
            registroBitacoraAux = new Object[18];
            for (int j = 0; j < registroBitacoraAux.length; j++) {
                registroBitacoraAux[j] = modeloTabla.getValueAt(i, j);
            }
            tablaObj.add(registroBitacoraAux);
            modeloTabla.removeRow(i);
        }
        Collections.sort(tablaObj, new OrdenaTablaTiempo());

        for (int i = 0; i < tablaObj.size(); i++) {
            modeloTabla.addRow((Object[]) tablaObj.get(i));
        }
        tablaObj.clear();
    }

    private Object[] modeloRegistroBitacora(Principal winPrincipal, Object[] reg) { 
        String oArea = "";
        String oProblema = "";         
        
        if (winPrincipal.getCmbTema().getSelectedItem().toString().equals("Paros Planeados")){ 
            if (winPrincipal.getCmbArea1().getSelectedItem().toString().equals("Otros")){
                oArea = winPrincipal.getCmbProblema1().getSelectedItem().toString();                     
                oProblema = winPrincipal.getCmbProblema1().getSelectedItem().toString(); 
            } else { 
                oArea = winPrincipal.getCmbArea1().getSelectedItem().toString(); 
                oProblema = ""; 
            }              
        } else if (winPrincipal.getCmbTema().getSelectedItem().toString().equals("Organizacionales")){ 
            //PARA PAROS ORGANIZACIONALES 
            if(winPrincipal.getCmbArea1().getSelectedItem().toString().equals("Logistica")){ 
                if (winPrincipal.getCmbProblema1().getSelectedItem().toString().equals("Cambios NO programados por Logistica")){ 
                    oArea = winPrincipal.getCmbProblema1().getSelectedItem().toString(); 
                    oProblema = winPrincipal.getCmbProblema1().getSelectedItem().toString(); 
                } else { 
                    oArea = winPrincipal.getCmbProblema1().getSelectedItem().toString(); 
                    oProblema = "Material: "+winPrincipal.getTxtMatFaltante1().getText(); 
                } 
            } else { 
                oArea = winPrincipal.getCmbArea1().getSelectedItem().toString();
                oProblema = winPrincipal.getCmbProblema1().getSelectedItem().toString();
            }
        } 
        
        switch (winPrincipal.getCmbTema().getSelectedItem().toString()) {
            case "Tiempo Faltante":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
                reg[4] = winPrincipal.getTxtTiempoFin().getText();
                reg[5] = winPrincipal.getTxtDuracion().getText();
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();
                reg[7] = "";
                reg[8] = "";
                reg[9] = "";
                reg[10] = "";
                reg[11] = "";
                reg[12] = "0";
                reg[13] = "";
                reg[14] = "0";
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";
                break;
                
            case "Piezas Producidas":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = winPrincipal.getTxtTiempoInicio().getText();
                reg[4] = winPrincipal.getTxtTiempoFin().getText();
                reg[5] = winPrincipal.getTxtDuracion().getText();
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();//cmbOperacion.getSelectedItem();
                reg[8] = "";
                reg[9] = "";
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem();//winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = winPrincipal.getTxtCantPzas().getText();
                reg[13] = "";
                reg[14] = "0";
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";
                break;
                
            case "Calidad":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = "0";
                reg[4] = "0";
                reg[5] = "0";
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion1().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea1().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema1().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap1().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";
                break;
                
            case "Tecnicas":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
                reg[4] = winPrincipal.getTxtTiempoFin().getText();
                reg[5] = winPrincipal.getTxtDuracion().getText();
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion1().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea1().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema1().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap1().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";
                break;

            case "Organizacionales":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
                reg[4] = winPrincipal.getTxtTiempoFin().getText();
                reg[5] = winPrincipal.getTxtDuracion().getText();
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                //reg[8] = winPrincipal.getCmbArea1().getSelectedItem();
                reg[8] = oArea;
                //reg[9] = winPrincipal.getCmbProblema1().getSelectedItem();
                reg[9] = oProblema; 
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = "";
                reg[14] = "0";
                reg[15] = winPrincipal.getTxtMatFaltante1().getText().toUpperCase();
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";
                break;
                
            case "Cambio de Modelo":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();
                reg[3] = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
                reg[4] = winPrincipal.getTxtTiempoFin().getText();
                reg[5] = winPrincipal.getTxtDuracion().getText();
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea1().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema1().getSelectedItem();//winPrincipal.getCmbProblemaCambios().getSelectedItem().toString();
                reg[10] = clienteP;//winPrincipal.getTxtCliente().getText(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = noParteP;//winPrincipal.getTxtNoParte().getText(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = winPrincipal.getCmbNoParte1().getSelectedItem();
                reg[14] = winPrincipal.getTxtScrap1().getText();
                reg[15] = winPrincipal.getTxtMatFaltante1().getText().toUpperCase(); //DETALLE MATERIAL
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "-";

                //System.out.println("RCambioModelo: "+noParteP+" a "+ winPrincipal.getCmbNoParte1().getSelectedItem()+"\n");
                winPrincipal.getCmbCliente().setSelectedItem(winPrincipal.getCmbCliente1().getSelectedItem());
                winPrincipal.getCmbNoParte().setSelectedItem(winPrincipal.getCmbNoParte1().getSelectedItem());
                winPrincipal.getCmbCliente().setEnabled(true);
                winPrincipal.getCmbNoParte().setEnabled(true);
                winPrincipal.getTxtTcP().setEnabled(true);
                break;

            case "Paros Planeados":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem(); //linea
                reg[1] = winPrincipal.getDteFecha().getText(); //fecha
                reg[2] = winPrincipal.getCmbHora().getSelectedItem();//hora
                reg[3] = Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());//inicio
                reg[4] = winPrincipal.getTxtTiempoFin().getText();//fin
                reg[5] = winPrincipal.getTxtDuracion().getText();//duracion
                reg[6] = winPrincipal.getCmbTema().getSelectedItem();//tema
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();//operacion
                //reg[8] = winPrincipal.getCmbArea1().getSelectedItem();//area
                reg[8] = oArea;//area 
                reg[9] = oProblema; //problema 
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem();//winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//cantidad
                reg[13] = "";//parte nuevo
                reg[14] = "0";//scrap
                reg[15] = "";//detalle material
                reg[16] = winPrincipal.getTxtTcP().getText(); //tc
                reg[17] = "-"; 
                break; 
        } 
        return reg;
    }
    
    //METODOS PARA INSERT DE LOS OTROS PANELS
    //PARA DATOS DEL PANEL 2
    private Object[] modeloRegistroBitacora2(Principal winPrincipal, Object[] reg) {
        System.err.println("Tema 2 ");
        switch (winPrincipal.getCmbTema2().getSelectedItem().toString()) {            
            case "Calidad":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = "0";
                reg[4] = "0";
                reg[5] = "0";
                reg[6] = winPrincipal.getCmbTema2().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion2().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea2().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema2().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap2().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;
                
            case "Tecnicas":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur2().getText();
                reg[6] = winPrincipal.getCmbTema2().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion2().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea2().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema2().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap2().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;

            case "Organizacionales":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur2().getText();
                reg[6] = winPrincipal.getCmbTema2().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea2().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema2().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = "";
                reg[14] = "0";
                reg[15] = winPrincipal.getTxtMatFaltante2().getText().toUpperCase();
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";
                break;
                
            case "Cambio de Modelo":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur2().getText();
                reg[6] = winPrincipal.getCmbTema2().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea2().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema2().getSelectedItem();//winPrincipal.getCmbProblemaCambios().getSelectedItem().toString();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem();//winPrincipal.getTxtCliente().getText(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem();//winPrincipal.getTxtNoParte().getText(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = winPrincipal.getCmbNoParte2().getSelectedItem();
                reg[14] = winPrincipal.getTxtScrap2().getText();
                reg[15] = winPrincipal.getTxtMatFaltante2().getText().toUpperCase(); //DETALLE MATERIAL
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";

                winPrincipal.getCmbCliente().setSelectedItem(winPrincipal.getCmbCliente2().getSelectedItem());
                winPrincipal.getCmbNoParte().setSelectedItem(winPrincipal.getCmbNoParte2().getSelectedItem());
                winPrincipal.getCmbCliente().setEnabled(true);
                winPrincipal.getCmbNoParte().setEnabled(true);
                winPrincipal.getTxtTcP().setEnabled(true);

                break;
        }
        return reg;
    }
    
    //PARA DATOS DEL PANEL 3
    private Object[] modeloRegistroBitacora3(Principal winPrincipal, Object[] reg) {
        switch (winPrincipal.getCmbTema3().getSelectedItem().toString()) {
            
            case "Calidad":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = "0";
                reg[4] = "0";
                reg[5] = "0";
                reg[6] = winPrincipal.getCmbTema3().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion3().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea3().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema3().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap3().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;
                
            case "Tecnicas":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur3().getText();
                reg[6] = winPrincipal.getCmbTema3().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion3().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea3().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema3().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap3().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;

            case "Organizacionales":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur3().getText();
                reg[6] = winPrincipal.getCmbTema3().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea3().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema3().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = "";
                reg[14] = "0";
                reg[15] = winPrincipal.getTxtMatFaltante3().getText().toUpperCase();
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";
                break;
                
            case "Cambio de Modelo":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur3().getText();
                reg[6] = winPrincipal.getCmbTema3().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea3().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema3().getSelectedItem();//winPrincipal.getCmbProblemaCambios().getSelectedItem().toString();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem();//winPrincipal.getTxtCliente().getText(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem();//winPrincipal.getTxtNoParte().getText(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = winPrincipal.getCmbNoParte3().getSelectedItem();
                reg[14] = winPrincipal.getTxtScrap3().getText();
                reg[15] = winPrincipal.getTxtMatFaltante3().getText().toUpperCase(); //DETALLE MATERIAL
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";

                winPrincipal.getCmbCliente().setSelectedItem(winPrincipal.getCmbCliente3().getSelectedItem());
                winPrincipal.getCmbNoParte().setSelectedItem(winPrincipal.getCmbNoParte3().getSelectedItem());
                winPrincipal.getCmbCliente().setEnabled(true);
                winPrincipal.getCmbNoParte().setEnabled(true);
                winPrincipal.getTxtTcP().setEnabled(true);
                break;
        }
        return reg;
    }

    //PARA DATOS DEL PANEL 4
    private Object[] modeloRegistroBitacora4(Principal winPrincipal, Object[] reg) {
        switch (winPrincipal.getCmbTema4().getSelectedItem().toString()) {
            
            case "Calidad":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = "0";
                reg[4] = "0";
                reg[5] = "0";
                reg[6] = winPrincipal.getCmbTema4().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion4().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea4().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema4().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap4().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;
                
            case "Tecnicas":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur4().getText();
                reg[6] = winPrincipal.getCmbTema4().getSelectedItem();
                reg[7] = winPrincipal.getCmbOperacion4().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea4().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema4().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";//winPrincipal.getTxtCantidadProducidaCalidad().getText();
                reg[13] = "";
                reg[14] = winPrincipal.getTxtScrap4().getText();
                reg[15] = "";
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X"; 
                break;

            case "Organizacionales":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur4().getText();
                reg[6] = winPrincipal.getCmbTema4().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea4().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema4().getSelectedItem();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = "";
                reg[14] = "0";
                reg[15] = winPrincipal.getTxtMatFaltante4().getText().toUpperCase();
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";
                break;
                
            case "Cambio de Modelo":
                reg[0] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[1] = winPrincipal.getDteFecha().getText();
                reg[2] = hora;
                reg[3] = minIni;
                reg[4] = minFin;
                reg[5] = winPrincipal.getTxtDur4().getText();
                reg[6] = winPrincipal.getCmbTema4().getSelectedItem();//
                reg[7] = winPrincipal.getCmbLinea().getSelectedItem();
                reg[8] = winPrincipal.getCmbArea4().getSelectedItem();
                reg[9] = winPrincipal.getCmbProblema4().getSelectedItem();//winPrincipal.getCmbProblemaCambios().getSelectedItem().toString();
                reg[10] = winPrincipal.getCmbCliente().getSelectedItem();//winPrincipal.getTxtCliente().getText(); //winPrincipal.getCmbFamiliaPzasProd().getSelectedItem();
                reg[11] = winPrincipal.getCmbNoParte().getSelectedItem();//winPrincipal.getTxtNoParte().getText(); //winPrincipal.getCmbNoPartePzasProd().getSelectedItem();
                reg[12] = "0";
                reg[13] = winPrincipal.getCmbNoParte4().getSelectedItem();
                reg[14] = winPrincipal.getTxtScrap4().getText();
                reg[15] = winPrincipal.getTxtMatFaltante4().getText().toUpperCase(); //DETALLE MATERIAL
                reg[16] = winPrincipal.getTxtTcP().getText();
                reg[17] = "X";

                winPrincipal.getCmbCliente().setSelectedItem(winPrincipal.getCmbCliente4().getSelectedItem());
                winPrincipal.getCmbNoParte().setSelectedItem(winPrincipal.getCmbNoParte4().getSelectedItem());
                winPrincipal.getCmbCliente().setEnabled(true);
                winPrincipal.getCmbNoParte().setEnabled(true);
                winPrincipal.getTxtTcP().setEnabled(true);
                break;
        }
        return reg;
    }
    

    public void eliminarRegistroBitacora(Principal winPrincipal) {
        String fech;
        String line;
        int hora;
        DefaultTableModel reg = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
        int posicionFila = winPrincipal.getTblBitacora().getSelectedRow();
        if (posicionFila >= 0) {
            try {
                int columnas = winPrincipal.getTblBitacora().getColumnCount();
                ArrayList obj = new ArrayList();

                line = (String) winPrincipal.getTblBitacora().getValueAt(posicionFila, 0);
                fech = (String) winPrincipal.getTblBitacora().getValueAt(posicionFila, 1);
                hora = (int) winPrincipal.getTblBitacora().getValueAt(posicionFila, 2);

                for (int j = 0; j < columnas; j++) {
                    obj.add(winPrincipal.getTblBitacora().getValueAt(posicionFila, j));
                }
                new BitacoraDAOImpl().borrarFilaRegistro(obj);
                reg.removeRow(winPrincipal.getTblBitacora().getSelectedRow());
                new PrincipalMetodos().actualizaOEEBitacoraHora(fech, line, hora);
                contadorFila--;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.eliminarRegistroBitacora()\n" + e,
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(winPrincipal, "Seleccionar", "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void revisarTiemposFaltantes(Principal winPrincipal, int opcion) {
        
        System.out.println("utils.PrincipalMetodos.revisarTiemposFaltantes(): "+opcion);
        switch (opcion) {
            case 1:
                DefaultTableModel bitacoraModelo,
                tablaHoras = new DefaultTableModel();
                tablaHoras.addColumn("Hora");
                tablaHoras.addColumn("Tiempo Faltante");
                Object[] r = new Object[2];
                bitacoraModelo = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                DefaultComboBoxModel horasModelo = (DefaultComboBoxModel) winPrincipal.getCmbHora().getModel();
                
                int hora,
                sum,
                faltante;
                
                for (int i = 1; i < winPrincipal.getCmbHora().getItemCount(); i++) {
                    hora = Integer.parseInt(winPrincipal.getCmbHora().getItemAt(i).toString());
                    r[0] = hora;
                    r[1] = 60;
                    tablaHoras.addRow(r);
                }
                for (int i = 1; i < horasModelo.getSize(); i++) {
                    hora = Integer.parseInt(winPrincipal.getCmbHora().getItemAt(i).toString());
                    sum = 0;
                    for (int c = 0; c < bitacoraModelo.getRowCount(); c++) {
                        if (Integer.parseInt(bitacoraModelo.getValueAt(c, 2).toString()) == hora) {                         
                            //SEGUNDA OPCION PARA CUANDO SEA 60                             
                            if (!bitacoraModelo.getValueAt(c, 17).toString().equals("X") ){
                                sum += Integer.parseInt(bitacoraModelo.getValueAt(c, 5).toString());  
                            }         
                            faltante = 60 - sum;
                            tablaHoras.setValueAt(faltante, i - 1, 1);
                        }
                    }
                }
                TiemposFaltantes tiemposFaltantes = new TiemposFaltantes(winPrincipal, true);
                tiemposFaltantes.getTblTiemposFaltantes().setModel(tablaHoras);
                tiemposFaltantes.setVisible(true);
                break;
            case 2:
            case 3:
                DefaultTableModel bitacora,
                 tablaDia = new DefaultTableModel();
                tablaDia.addColumn("Hora");
                tablaDia.addColumn("Tiempo Faltante");
                Object[] d = new Object[2];
                bitacoraModelo = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
//                int horaVis,
//                 sumVis,
//                 faltanteVis;
                for (int i = 0; i < 24; i++) {
                    hora = i;
                    d[0] = hora;
                    d[1] = 60;
                    tablaDia.addRow(d);
                }
                for (int i = 0; i < 24; i++) {
                    hora = i;
                    sum = 0;
                    for (int c = 0; c < bitacoraModelo.getRowCount(); c++) {
                        if (Integer.parseInt(bitacoraModelo.getValueAt(c, 2).toString()) == hora) {
                            if(!bitacoraModelo.getValueAt(c, 17).toString().equals("X")){
                                sum += Integer.parseInt(bitacoraModelo.getValueAt(c, 5).toString());  
                            }                            
                            faltante = 60 - sum;
                            tablaDia.setValueAt(faltante, i, 1);
                        }
                    }
                }
                TiemposFaltantes tiemposFaltantesVis = new TiemposFaltantes(winPrincipal, true);
                tiemposFaltantesVis.getTblTiemposFaltantes().setModel(tablaDia);
                tiemposFaltantesVis.setVisible(true);
                break;
        }
    }

    public void guardarRegistroAccess(Principal winPrincipal) {
        try {
            int tiempoFaltante = 0;
            for (int i = 0; i < tablaHorasCero.getRowCount(); i++) {
                if (Integer.parseInt(tablaHorasCero.getValueAt(i, 1).toString()) != 0) {
                    tiempoFaltante += Integer.parseInt(
                            tablaHorasCero.getValueAt(i, 1).toString()
                    );
                }
            }
            if (tiempoFaltante == 0) {
                new PrincipalMetodos().eliminarRegistroTiempo(winPrincipal);
                int columnas = winPrincipal.getTblBitacora().getColumnCount();
                ArrayList reg;
                for (int i = 0; i < winPrincipal.getTblBitacora().getRowCount(); i++) {
                    reg = new ArrayList();
                    for (int j = 0; j < columnas; j++) {
                        reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                    }
                    new BitacoraDAOImpl().insertarRegistroAccess(reg);
                }
                limpiarTabla((DefaultTableModel) winPrincipal.getTblBitacora().getModel());
                PrincipalControl.insercionesAccess++;
                contadorFila = 0;
                JOptionPane.showMessageDialog(winPrincipal, "Bitacora Guardada Correctamente",
                        "Guardar", JOptionPane.INFORMATION_MESSAGE);
            } else if (tiempoFaltante < 0 || tiempoFaltante > 0) {
                String[] options = {"Si"};
                int seleccion = JOptionPane.showOptionDialog(null, "La bitacora aun no esta llena." + "\nTiempo faltante: " + getValueTblTiempoFaltante(winPrincipal) + " minutos"
                        + "\n¿Desea continuar con el guardado de informacion?", "Peticion", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (seleccion == 0) {
                    new PrincipalMetodos().eliminarRegistroTiempo(winPrincipal);
                    int columnas = winPrincipal.getTblBitacora().getColumnCount();
                    ArrayList reg;
                    for (int i = 0; i < winPrincipal.getTblBitacora().getRowCount(); i++) {
                        reg = new ArrayList();
                        for (int j = 0; j < columnas; j++) {
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        }
                        new BitacoraDAOImpl().insertarRegistroAccess(reg);
                    }
                    limpiarTabla((DefaultTableModel) winPrincipal.getTblBitacora().getModel());
                    PrincipalControl.insercionesAccess++;
                    contadorFila = 0;
                    JOptionPane.showMessageDialog(winPrincipal, "Bitacora Guardada Correctamente",
                            "Guardar", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (Exception e) {
            guardaTemporalTXT(winPrincipal);
            JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.guardarRegistroAccess()\n"
                    + "Ocurrio un error : " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void editarBitacoraPorDia(Principal winPrincipal) {
        if (winPrincipal.getCmbLinea().getSelectedIndex() != 0) {
            winPrincipal.getDteFecha().setEnabled(true);
            winPrincipal.getBtnCancelar().setVisible(true);
        } else {
            JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.editarBitacoraPorDia()\n Debe seleccionar una linea", "Mensaje", JOptionPane.INFORMATION_MESSAGE
            );
        }
    }

    public void consultarBitacoraPorDia(Principal winPrincipal) {
        String fecha = PrincipalControl.fecha;
        String linea = PrincipalControl.linea;

        if (winPrincipal.getTblBitacora().getRowCount() != 0) {
            switch (JOptionPane.showConfirmDialog(winPrincipal, "En caso de tener registros sin guardar, estos se perderán.\n¿Seguro que desea continuar?", "Mensaje",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
                case 0:
                    limpiarTabla((DefaultTableModel) winPrincipal.getTblBitacora().getModel());

                    winPrincipal.getLblTurno().setForeground(Color.red);
                    winPrincipal.getLblTurno().setText("Visualización día / No edición");
                    winPrincipal.getDteFecha().setEnabled(false);
                    winPrincipal.getBtnGuardar().setEnabled(false);
                    winPrincipal.getBtnCambiarLinea().setEnabled(false);
                    Object[] bitacoraObj;
                    try {
                        ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasPorFecha(fecha, linea);

                        if (!bitacoraArr.isEmpty()) {
                            modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                            for (int i = 0; i < bitacoraArr.size(); i++) {
                                bitacoraObj = (Object[]) bitacoraArr.get(i);
                                modeloTabla.addRow(bitacoraObj);
                            }
                        }
                    } catch (Exception e) {
                        guardaTemporalTXT(winPrincipal);
                        JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.consultarBitacoraPorDia()\n" + e,
                                "Error", JOptionPane.ERROR_MESSAGE
                        );
                    }
                    break;
                case 1:
                    cancelarEdicion(winPrincipal);
                    break;
            }
        } else {
            winPrincipal.getLblTurno().setForeground(Color.red);
            winPrincipal.getLblTurno().setText("Visualización día / No edición");
            winPrincipal.getDteFecha().setEnabled(false);
            winPrincipal.getBtnGuardar().setEnabled(false);
            winPrincipal.getBtnCambiarLinea().setEnabled(false);
            Object[] bitacoraObj;
            try {
                ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasPorFecha(fecha, linea);

                if (!bitacoraArr.isEmpty()) {
                    modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                    for (int i = 0; i < bitacoraArr.size(); i++) {
                        bitacoraObj = (Object[]) bitacoraArr.get(i);
                        modeloTabla.addRow(bitacoraObj);
                    }
                } else {
                    JOptionPane.showMessageDialog(
                            winPrincipal, "Bitácora no asignada para el día seleccionado\n"
                            + "Favor de seleccionar un turno & realizar los registros pendientes",
                            "Mensaje", JOptionPane.INFORMATION_MESSAGE
                    );
                }
            } catch (Exception e) {
                guardaTemporalTXT(winPrincipal);
                JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.consultarBitacoraPorDia()\n" + e,
                        "Error", JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }

    public void consultarBitacoraPorTurno(Principal winPrincipal) {
        int inicio = SelecTurno.inicioTurno;
        int fin = SelecTurno.finTurno;
        String fecha = SelecTurno.fecha;
        String linea = SelecTurno.seleccionTurnoLinea;
        
        if (winPrincipal.getTblBitacora().getRowCount() != 0) {
            switch (JOptionPane.showConfirmDialog(winPrincipal, "En caso de tener registros sin guardar, estos se perderán.\n¿Seguro que desea continuar?", "Mensaje",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
                case 0:
                    limpiarTabla((DefaultTableModel) winPrincipal.getTblBitacora().getModel());

                    winPrincipal.getDteFecha().setEnabled(false);
                    winPrincipal.getBtnGuardar().setText("Cerrar Turno");
                    Object[] bitacoraObj;
                    try {
                        if (inicio > fin) {
                            ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasTurnoNocturno(fecha, linea, inicio, fecha, linea, fin);
                            if (!bitacoraArr.isEmpty()) {
                                modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                                //System.out.println("1. i: "+inicio+" f: "+fin+" size: "+bitacoraArr.size());
                                
                                for (int i = 0; i < bitacoraArr.size(); i++) {
                                    bitacoraObj = (Object[]) bitacoraArr.get(i);
                                    modeloTabla.addRow(bitacoraObj);
                                }
                            }
                        } else {
                            ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasTurno(fecha, linea, inicio, fin);
                            if (!bitacoraArr.isEmpty()) {
                                modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                                //System.out.println("2. i: "+inicio+" f: "+fin+" size: "+bitacoraArr.size());
                                for (int i = 0; i < bitacoraArr.size(); i++) {
                                    bitacoraObj = (Object[]) bitacoraArr.get(i);
                                    modeloTabla.addRow(bitacoraObj);
                                }
                            }
                        }
                    } catch (Exception e) {
                        guardaTemporalTXT(winPrincipal);
                        JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.consultarBitacoraPorDia()\n" + e,
                                "Error", JOptionPane.ERROR_MESSAGE
                        );
                    }
                    break;
                case 1:
                    cancelarEdicion(winPrincipal);
                    break;
            }
        } else {
            winPrincipal.getDteFecha().setEnabled(false);
            winPrincipal.getBtnGuardar().setText("Cerrar Turno");
            Object[] bitacoraObj;
            try {
                if (inicio > fin) {
                    ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasTurnoNocturno(fecha, linea, inicio, fecha, linea, fin);
                    if (!bitacoraArr.isEmpty()) {
                        modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                        //System.out.println("3. i: "+inicio+" f: "+fin+" size: "+bitacoraArr.size());
                        
                        for (int i = 0; i < bitacoraArr.size(); i++) {
                            bitacoraObj = (Object[]) bitacoraArr.get(i);
                            modeloTabla.addRow(bitacoraObj);
                        }
                    }
                } else {
                    ArrayList bitacoraArr = new BitacoraDAOImpl().listarBitacorasTurno(fecha, linea, inicio, fin);
                    if (!bitacoraArr.isEmpty()) {
                        modeloTabla = (DefaultTableModel) winPrincipal.getTblBitacora().getModel();
                        //System.out.println("4. i: "+inicio+" f: "+fin+" size: "+bitacoraArr.size());
                        for (int i = 0; i < bitacoraArr.size(); i++) {
                            bitacoraObj = (Object[]) bitacoraArr.get(i);
                            modeloTabla.addRow(bitacoraObj);
                        }
                    }
                }
            } catch (Exception e) {
                guardaTemporalTXT(winPrincipal);
                JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.consultarBitacoraPorDia()\n" + e,
                        "Error", JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }

    public void cancelarEdicion(Principal winPrincipal) {
        winPrincipal.getDteFecha().setEnabled(false);
        winPrincipal.getDteFecha().setDate(new java.util.Date(System.currentTimeMillis()));
        winPrincipal.getLblTurno().setText("");
        winPrincipal.getCmbTema().setSelectedIndex(0);
        winPrincipal.getCmbTema().setEnabled(false);
        limpiarTabla((DefaultTableModel) winPrincipal.getTblBitacora().getModel());
        winPrincipal.getBtnGuardar().setText("Guardar Bitacora");
        winPrincipal.getBtnGuardar().setVisible(false);
        winPrincipal.getBtnCancelar().setVisible(false);
        winPrincipal.getBtnCambiarLinea().setEnabled(true);
    }

    public void actualizarRegistroFechaAccess(Principal winPrincipal) {
        try {
            if (winPrincipal.getTblBitacora().getModel().getRowCount() != 0) {
                eliminarRegistroTiempoActualizar(winPrincipal);
                ArrayList reg;
                for (int i = 0; i < winPrincipal.getTblBitacora().getRowCount(); i++) {
                    reg = new ArrayList();
                    for (int j = 0; j < winPrincipal.getTblBitacora().getColumnCount(); j++) {
                        Object ob = winPrincipal.getTblBitacora().getValueAt(i, j);
                        if ((j == 12 || j == 16) && (ob == null || ob.toString().isEmpty())) {
                            winPrincipal.getTblBitacora().setValueAt("0", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else if (ob == null || ob.toString().isEmpty()) {
                            winPrincipal.getTblBitacora().setValueAt("", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else {
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        }
                    }
                    new BitacoraDAOImpl().insertarRegistroAccess(reg);
                }
                cancelarEdicion(winPrincipal);
                winPrincipal.getLblTurno().setText("");
                winPrincipal.getCmbTema().setSelectedIndex(0);
                winPrincipal.getCmbTema().setEnabled(false);
                PrincipalControl.insercionesAccess = 1;
                JOptionPane.showMessageDialog(winPrincipal, "Bitacora Actualizada Correctamente",
                        "Actualizar", JOptionPane.INFORMATION_MESSAGE);
                contadorFila = 0;
            } else {
                JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.actualizarRegistroFechaAccess()\nLa bitacora no puede estar vacia",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            guardaTemporalTXT(winPrincipal);
            JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.actualizarRegistroFechaAccess()\n" + e,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminarTurnoVacio(Principal winPrincipal) throws Exception {
        try {
            String turno = "";
            for (int i = 1; i != 0; i++) {
                if (!(winPrincipal.getLblTurno().getText().charAt(i - 1) == ' '
                        && winPrincipal.getLblTurno().getText().charAt(i) == '<')) {
                    turno += winPrincipal.getLblTurno().getText().charAt(i - 1);
                } else {
                    break;
                }
            }
            
            new TiempoTurnoDAOImpl().eliminarTurnoVacio(
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText(),
                    turno
            );
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminarRegistroTiempo(Principal winPrincipal) throws Exception {
        winPrincipal.getCmbHora().setSelectedIndex(1);
        int horaInicial = Integer.parseInt(winPrincipal.getCmbHora().getSelectedItem().toString());
        int finalAux = winPrincipal.getCmbHora().getItemCount();
        winPrincipal.getCmbHora().setSelectedIndex(finalAux - 1);
        int horaFinal = Integer.parseInt(winPrincipal.getCmbHora().getSelectedItem().toString());
        winPrincipal.getCmbHora().setSelectedIndex(0);
        try {
            new BitacoraDAOImpl().borrarRegistroTiempo(
                    horaInicial,
                    horaFinal,
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText()
            );
            
//            //ACTUALIZAMOS LO DEL DIA            
//            actualizaOEEBitacoraDia(winPrincipal.getDteFecha().getText(), winPrincipal.getCmbLinea().getSelectedItem().toString());            
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminarRegistroTiempoVacio(Principal winPrincipal) throws Exception {
        winPrincipal.getCmbHora().setSelectedIndex(1);
        int horaInicial = Integer.parseInt(winPrincipal.getCmbHora().getSelectedItem().toString());
        int finalAux = winPrincipal.getCmbHora().getItemCount();
        winPrincipal.getCmbHora().setSelectedIndex(finalAux - 1);
        int horaFinal = Integer.parseInt(winPrincipal.getCmbHora().getSelectedItem().toString());
        winPrincipal.getCmbHora().setSelectedIndex(0);
        try {
            new BitacoraDAOImpl().borrarRegistroTiempo(
                    horaInicial,
                    horaFinal,
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText()
            );
            
            //ELIMINAMOS HOURLY 
            new BitacoraDAOImpl().borrarRegistroHourly(horaInicial,
                    horaFinal,
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText());
            
            //ACTUALIZAMOS LO DEL DIA            
            actualizaOEEBitacoraDia(winPrincipal.getDteFecha().getText(), winPrincipal.getCmbLinea().getSelectedItem().toString());            
        } catch (Exception e) {
            throw e;
        }
    }
    
    
    public void eliminarRegistroTiempoActualizar(Principal winPrincipal) throws Exception {
        int horaInicial = Integer.parseInt(winPrincipal.getTblBitacora().getValueAt(0, 2).toString());
        int horaFinal = Integer.parseInt(winPrincipal.getTblBitacora().getValueAt(
                winPrincipal.getTblBitacora().getRowCount() - 1, 2).toString());

        try {
            new BitacoraDAOImpl().borrarRegistroTiempo(
                    horaInicial,
                    horaFinal,
                    winPrincipal.getCmbLinea().getSelectedItem().toString(),
                    winPrincipal.getDteFecha().getText()
            );
        } catch (Exception e) {
            throw e;
        }
    }

    private void insertarRegistroFilaAccess(Principal winPrincipal) {        
        System.out.println("utils.PrincipalMetodos.insertarRegistroFilaAccess().RowCount: "+winPrincipal.getTblBitacora().getRowCount());
        
        try {
            if (PrincipalControl.bnEdicion == 2) {
                ArrayList reg;
                eliminarRegistroTiempo(winPrincipal);
                for (int i = 0; i < winPrincipal.getTblBitacora().getRowCount(); i++) {
                    reg = new ArrayList();
                    for (int j = 0; j < winPrincipal.getTblBitacora().getColumnCount(); j++) {
                        Object ob = winPrincipal.getTblBitacora().getValueAt(i, j);
                        if ((j == 12 || j == 16) && (ob == null || ob.toString().isEmpty())) {
                            winPrincipal.getTblBitacora().setValueAt("0", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else if (ob == null || ob.toString().isEmpty()) {
                            winPrincipal.getTblBitacora().setValueAt("", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else {
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        }
                    }
                    new BitacoraDAOImpl().insertarFilaRegistro(reg);
                }
            } else {
                ArrayList reg;
                eliminarRegistroTiempo(winPrincipal);
                for (int i = 0; i < winPrincipal.getTblBitacora().getRowCount(); i++) {
                    reg = new ArrayList();
                    for (int j = 0; j < winPrincipal.getTblBitacora().getColumnCount(); j++) {
                        Object ob = winPrincipal.getTblBitacora().getValueAt(i, j);
                        if ((j == 12 || j == 16) && (ob == null || ob.toString().isEmpty())) {
                            winPrincipal.getTblBitacora().setValueAt("0", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else if (ob == null || ob.toString().isEmpty()) {
                            winPrincipal.getTblBitacora().setValueAt("", i, j);
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        } else {
                            reg.add(winPrincipal.getTblBitacora().getValueAt(i, j));
                        }
                    }
                    new BitacoraDAOImpl().insertarFilaRegistro(reg);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(winPrincipal, "PrincipalMetodos.insertarRegistroFilaAccess()\n" + e,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void guardaTemporalTXT(Principal winPrincipal) {
        try {
            String sucursalesCSVFile = "tmp/" + winPrincipal.getCmbLinea().getSelectedItem() + ".txt";
            BufferedWriter bfw = new BufferedWriter(new FileWriter(sucursalesCSVFile));

            for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                for (int j = 0; j < modeloTabla.getColumnCount(); j++) {
                    bfw.write((String) (modeloTabla.getValueAt(i, j)));
                    if (j < modeloTabla.getColumnCount() - 1) {
                        bfw.write(",");
                    }
                }
                bfw.newLine();
            }
            bfw.close();
            JOptionPane.showMessageDialog(null, "Archivo salvador salio al rescate");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El archivo salvador no pudo asistir");
        }
    }

    private int getValueTblTiempoFaltante(Principal winPrincipal) {
        int tiempoFaltante = 0;
        for (int b = 0; b < tablaHorasCero.getRowCount(); b++) {
            if (Integer.parseInt(tablaHorasCero.getValueAt(b, 1).toString()) != 0) {
                tiempoFaltante += Integer.parseInt(
                        tablaHorasCero.getValueAt(b, 1).toString()
                );
            }
        }
        return tiempoFaltante;
    }

    private int getValueTblBitacoraTiempoFaltante(Principal winPrincipal) {
        int tiempoFaltante = 0;
        for (int a = 0; a < winPrincipal.getTblBitacora().getRowCount(); a++) {
            if (winPrincipal.getTblBitacora().getValueAt(a, 6).equals("Tiempo Faltante")) {
                tiempoFaltante += Integer.parseInt(
                        winPrincipal.getTblBitacora().getValueAt(a, 5).toString()
                );
            }
        }
        return tiempoFaltante;
    }

    public DefaultComboBoxModel actualizaComboHoras(Principal winPrincipal) {
        //revisarTiemposFaltantes(winPrincipal, 3);
        ArrayList listaHorasActualizada = new ArrayList();
        listaHorasActualizada.add("Seleccionar");
        for (int i = 0; i < tablaHorasCero.getRowCount(); i++) {
            if (Integer.parseInt(tablaHorasCero.getValueAt(i, 1).toString()) != 0) {
                listaHorasActualizada.add(tablaHorasCero.getValueAt(i, 0).toString());
            }
        }
        return new DefaultComboBoxModel(listaHorasActualizada.toArray());
    }

}
