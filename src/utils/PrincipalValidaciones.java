package utils;

import control.PrincipalControl;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import javax.swing.event.CaretEvent;
import javax.swing.event.TableModelEvent;
import vista.Principal;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */

public class PrincipalValidaciones {
    private static int bn = 0;
    private static char[] noParte = new char[14];
    private static int[] num = new int[14];
    
    public static void validarCaretUpdate(Principal winPrincipal, CaretEvent e) {
        //JTextField Cantidad Producida -> Panel Piezas Producidas
        if (winPrincipal.getTxtDur2().equals(e.getSource())) {            
            if (winPrincipal.getTxtDur2().getText().isEmpty()) {                
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                winPrincipal.getBtnAdd().setEnabled(false);
            }
        } if (winPrincipal.getTxtDur3().equals(e.getSource())) {            
            if (winPrincipal.getTxtDur3().getText().isEmpty()) {
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                winPrincipal.getBtnAdd().setEnabled(false);
            }
        } if (winPrincipal.getTxtDur4().equals(e.getSource())) {           
            if (winPrincipal.getTxtDur4().getText().isEmpty()) {
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                winPrincipal.getBtnAdd().setEnabled(false);
            }
        } else if (winPrincipal.getTxtCantPzas().equals(e.getSource())) {
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                if (Integer.parseInt(winPrincipal.getTxtCantPzas().getText()) < 1){
                    winPrincipal.getTxtCantPzas().setText("");
                }else {
                    winPrincipal.getCmbHora().setEnabled(true);
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                }
            }
            if (winPrincipal.getTxtCantPzas().getText().isEmpty()) {
                winPrincipal.getCmbHora().setSelectedIndex(0);
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
        } else if (winPrincipal.getTxtScrap1().equals(e.getSource())) { //JTextField Scrap -> Panel Calidad
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) { 
                //VALIDACION PARA EL BOTON DE PARO POR PERIODO
                winPrincipal.getCmbHora().setSelectedIndex(0);
                switch(winPrincipal.getCmbTema().getSelectedIndex()){ 
                    case 0: 
                    case 1: 
                    case 2:
                    case 5:
                    case 7:
                        winPrincipal.getBtnParoPeriodo().setVisible(false);
                        winPrincipal.getBtnParoPeriodo().setVisible(false);
                        break;
                    case 3: 
                    case 4: 
                    case 6:
                        winPrincipal.getCmbHora().setEnabled(true);
                        winPrincipal.getBtnParoPeriodo().setVisible(true);
                        break;
                } 
                winPrincipal.getCmbHora().setEnabled(true);                 
            }
            if (winPrincipal.getTxtScrap1().getText().isEmpty()) {
                winPrincipal.getCmbHora().setSelectedIndex(0);
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
        } else if (winPrincipal.getTxtScrap2().equals(e.getSource())) { //JTextField Scrap -> Panel Calidad
            winPrincipal.getTxtDur2().setText("");
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                winPrincipal.getTxtDur2().setEnabled(true);                  
            }
            if (winPrincipal.getTxtScrap2().getText().isEmpty()) {
                winPrincipal.getTxtDur2().setEnabled(false);                
            }
        } else if (winPrincipal.getTxtScrap3().equals(e.getSource())) { //JTextField Scrap -> Panel Calidad
            winPrincipal.getTxtDur3().setText("");
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                winPrincipal.getTxtDur3().setEnabled(true);                  
            }
            if (winPrincipal.getTxtScrap3().getText().isEmpty()) {
                winPrincipal.getTxtDur3().setEnabled(false);                
            }
        } else if (winPrincipal.getTxtScrap4().equals(e.getSource())) { //JTextField Scrap -> Panel Calidad
            winPrincipal.getTxtDur4().setText("");
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getBtnParoPeriodo().setVisible(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                winPrincipal.getTxtDur4().setEnabled(true);                  
            }
            if (winPrincipal.getTxtScrap4().getText().isEmpty()) {
                winPrincipal.getTxtDur4().setEnabled(false);                
            }
        } else if (winPrincipal.getTxtMatFaltante1().equals(e.getSource())){//JTextField Material Faltante -> Panel ORGANIZACIONALES
            //VALIDACION PARA QUE SE BLOQUE SCRAP SI SE TIENEN MENOS DE 10 DIGITOS
            if (e.getDot() >= 10 && e.getMark() >= 10) {
                switch(winPrincipal.getCmbTema().getSelectedIndex()){
                    case 4:
                        winPrincipal.getCmbHora().setEnabled(true);
                        winPrincipal.getCmbHora().setSelectedIndex(0);
                        break;
                    case 5:
                        winPrincipal.getCmbCliente1().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        winPrincipal.getCmbCliente1().setEnabled(true);
                        break;
                }                
            }else {
                winPrincipal.getTxtScrap1().setText("");
                winPrincipal.getTxtScrap1().setEnabled(true);
                winPrincipal.getCmbHora().setEnabled(false);
                winPrincipal.getCmbHora().setSelectedIndex(0);
            }
        } else if (winPrincipal.getTxtMatFaltante2().equals(e.getSource())){//JTextField Material Faltante -> Panel ORGANIZACIONALES
            //VALIDACION PARA QUE SE BLOQUE SCRAP SI SE TIENEN MENOS DE 10 DIGITOS
            winPrincipal.getTxtDur2().setText("");
            winPrincipal.getTxtDur2().setEnabled(false);
            
            if (winPrincipal.getCmbCliente2().getItemCount() > 0){
                winPrincipal.getCmbCliente2().setSelectedIndex(0);
            }
            winPrincipal.getCmbCliente2().setEnabled(false);
            
            if (e.getDot() >= 10 && e.getMark() >= 10) {
                if (e.getDot() >= 10 && e.getMark() >= 10) {
                    switch(winPrincipal.getCmbTema2().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur2().setEnabled(true);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente2().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                            winPrincipal.getCmbCliente2().setEnabled(true);
                            break;
                    }                
                }else {
                    switch(winPrincipal.getCmbTema2().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur2().setEnabled(false);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente2().setEnabled(true);
                            break;
                    }
                }
            }
        } else if (winPrincipal.getTxtMatFaltante3().equals(e.getSource())){//JTextField Material Faltante -> Panel ORGANIZACIONALES
            //VALIDACION PARA QUE SE BLOQUE SCRAP SI SE TIENEN MENOS DE 10 DIGITOS
            winPrincipal.getTxtDur3().setText("");
            winPrincipal.getTxtDur3().setEnabled(false);
            
            if (winPrincipal.getCmbCliente3().getItemCount() != 0){
                winPrincipal.getCmbCliente3().setSelectedIndex(0);
            }
            winPrincipal.getCmbCliente3().setEnabled(false);
            
            if (winPrincipal.getCmbNoParte3().getItemCount() != 0){ 
                winPrincipal.getCmbNoParte3().setSelectedIndex(0);
            }
            winPrincipal.getCmbNoParte3().setEnabled(false); 
            
            if (e.getDot() >= 10 && e.getMark() >= 10) {
                if (e.getDot() >= 10 && e.getMark() >= 10) {
                    switch(winPrincipal.getCmbTema3().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur3().setEnabled(true);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente3().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                            winPrincipal.getCmbCliente3().setEnabled(true);
                            break;
                    }                
                }else {
                    switch(winPrincipal.getCmbTema3().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur3().setEnabled(false);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente3().setEnabled(true);
                            break;
                    }
                }
            }
        } else if (winPrincipal.getTxtMatFaltante4().equals(e.getSource())){//JTextField Material Faltante -> Panel ORGANIZACIONALES
            //VALIDACION PARA QUE SE BLOQUE SCRAP SI SE TIENEN MENOS DE 10 DIGITOS
            winPrincipal.getTxtDur4().setText("");
            winPrincipal.getTxtDur4().setEnabled(false);
            
            if (winPrincipal.getCmbCliente4().getItemCount() != 0){
                winPrincipal.getCmbCliente4().setSelectedIndex(0);
            }
            winPrincipal.getCmbCliente4().setEnabled(false);
            
            if (winPrincipal.getCmbNoParte4().getItemCount() != 0){
                winPrincipal.getCmbNoParte4().setSelectedIndex(0);
            }
            winPrincipal.getCmbNoParte4().setEnabled(false);
            
            if (e.getDot() >= 10 && e.getMark() >= 10) {
                if (e.getDot() >= 10 && e.getMark() >= 10) {
                    switch(winPrincipal.getCmbTema4().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur4().setEnabled(true);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente4().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                            winPrincipal.getCmbCliente4().setEnabled(true);
                            break;
                    }                
                }else {
                    switch(winPrincipal.getCmbTema4().getSelectedIndex()){
                        case 3:
                            winPrincipal.getTxtDur4().setEnabled(false);
                            break;
                        case 4:
                            winPrincipal.getCmbCliente4().setEnabled(true);
                            break;
                    }
                }
            }
        } else if (winPrincipal.getTxtTiempoInicio().equals(e.getSource())) { //JTextField TiempoInicio
            if ((e.getDot() + e.getMark()) == 0 || winPrincipal.getTxtTiempoInicio().getText().isEmpty()) {                
                winPrincipal.getTxtTiempoFin().setText("");
                winPrincipal.getTxtTiempoFin().setEnabled(false);                
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                winPrincipal.getTxtTiempoFin().setEnabled(true);
            }
        } else if (winPrincipal.getTxtTiempoFin().equals(e.getSource())) { //JTextField TiempoFin            
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getTxtDuracion().setText("");
            }
            if (winPrincipal.getTxtTiempoFin().getText().isEmpty()) {
                winPrincipal.getTxtDuracion().setText("");
            }
        } else if (winPrincipal.getTxtDuracion().equals(e.getSource())) { //JTextField Duracion
            if ((e.getDot() + e.getMark()) == 0) {
                winPrincipal.getTxtDuracion().setText("");
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                winPrincipal.getBtnAdd().setEnabled(false);
            }
            if (e.getDot() == 1 && e.getMark() == 1) {
                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                winPrincipal.getBtnAdd().setEnabled(true);
            }
            if (winPrincipal.getTxtDuracion().getText().isEmpty()) {
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                winPrincipal.getBtnAdd().setEnabled(false);
            }
            if (!winPrincipal.getTxtDuracion().getText().isEmpty() && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                winPrincipal.getBtnAdd().setEnabled(true);
            }
        } 
    }
    
    public static void validarItemStateChanged(Principal winPrincipal, ItemEvent e) { 
        if (winPrincipal.getCmbCliente().equals(e.getSource())) { 
                winPrincipal.getCmbNoParte().setModel(new PrincipalMetodos().listaNoPartes(
                    PrincipalControl.linea, winPrincipal.getCmbCliente().getSelectedItem().toString()));
                winPrincipal.getCmbNoParte().setEnabled(true);
                
        } else if (winPrincipal.getCmbLinea().equals(e.getSource())) {
            if (winPrincipal.getCmbLinea().getSelectedIndex() == 0) {
                winPrincipal.getCmbTema().setEnabled(false);
            }
        } else if (winPrincipal.getCmbHora().equals(e.getSource())) { //ComboBox Hora
            if (winPrincipal.getCmbHora().getSelectedIndex() == 0
                    || !winPrincipal.getTxtTiempoInicio().getText().isEmpty()) {
                winPrincipal.getTxtTiempoInicio().setText("");
                winPrincipal.getTxtTiempoInicio().setEnabled(false);
            }
        } else if (winPrincipal.getCmbCliente().equals(e.getSource())){
            //AREA
            if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte().setSelectedIndex(-1);
            }
            winPrincipal.getCmbNoParte().setEnabled(false);
            //PROBLEMA
            if (winPrincipal.getCmbCliente().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte().setModel(new PrincipalMetodos().listaNoPartes(
                    winPrincipal.getCmbLinea().getSelectedItem().toString(), winPrincipal.getCmbCliente().getSelectedItem().toString()));
                winPrincipal.getCmbNoParte().setEnabled(true);
            }
        } else if (winPrincipal.getCmbOperacion1().equals(e.getSource())){
            //AREA
            if (winPrincipal.getCmbArea1().getItemCount() > 0){
                winPrincipal.getCmbArea1().setSelectedIndex(0);
            }
            //PROBLEMA
            if (winPrincipal.getCmbProblema1().getItemCount() > 0){
                winPrincipal.getCmbProblema1().setSelectedIndex(0);
            }
            //BOTONES                    
            winPrincipal.getCmbArea1().setEnabled(false);
            winPrincipal.getCmbProblema1().setEnabled(false);
            //SCRAP
            winPrincipal.getTxtScrap1().setText("");
            winPrincipal.getTxtScrap1().setEnabled(false);
            //ADD PROBLEMAS
            winPrincipal.getBtnAdd().setEnabled(false);
            winPrincipal.getBtnRemove().setEnabled(false);
            switch (winPrincipal.getCmbTema().getSelectedIndex()){                
                case 2:
                case 3:   
                    if (winPrincipal.getCmbOperacion1().getSelectedIndex() > 0){
                        winPrincipal.getCmbArea1().setModel(new PrincipalMetodos().listaAreasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion1().getSelectedItem().toString()));
                        winPrincipal.getCmbArea1().setEnabled(true);
                    }                     
                    break;
            }                
        } else if (winPrincipal.getCmbArea1().equals(e.getSource())){
            if (winPrincipal.getCmbProblema1().getSelectedIndex() > 0){
                winPrincipal.getCmbProblema1().setSelectedIndex(0);
            }
            
            winPrincipal.getLblProblema1().setVisible(false);
            winPrincipal.getCmbProblema1().setVisible(false);
            winPrincipal.getCmbProblema1().setEnabled(false);
            winPrincipal.getCmbCliente1().setEnabled(false);
            
            //NO PARTE
            if (winPrincipal.getCmbNoParte1().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte1().setSelectedIndex(0);
            }            
            
            //BOTONES                    
            winPrincipal.getCmbNoParte1().setEnabled(false);            
            winPrincipal.getTxtMatFaltante1().setText("");
            winPrincipal.getLblMatFaltante1().setVisible(false);
            winPrincipal.getTxtMatFaltante1().setVisible(false);
            winPrincipal.getTxtMatFaltante1().setEnabled(false);  
            
            //SCRAP
            winPrincipal.getTxtScrap1().setText("");
            winPrincipal.getTxtScrap1().setEnabled(false);
            
            //ADD PROBLEMAS
            winPrincipal.getBtnAdd().setEnabled(false);
            winPrincipal.getBtnRemove().setEnabled(false);
            
            switch (winPrincipal.getCmbTema().getSelectedIndex()){ 
                case 2:
                case 3:
                    winPrincipal.getLblProblema1().setVisible(true);
                    winPrincipal.getCmbProblema1().setVisible(true);
                    
                    if (winPrincipal.getCmbArea1().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema1().setModel(new PrincipalMetodos().listaProblemasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion1().getSelectedItem().toString(),winPrincipal.getCmbArea1().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema1().setEnabled(true);
                    } 
                    break;
                case 4: //ORGANIZACIONAL 
                    //System.out.println("utils.PrincipalValidaciones.validarItemStateChanged()"+winPrincipal.getCmbTema().getSelectedItem());
                    winPrincipal.getLblProblema1().setVisible(true);
                    winPrincipal.getCmbProblema1().setVisible(true);
                    if (winPrincipal.getCmbArea1().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema1().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                PrincipalControl.linea,winPrincipal.getCmbTema().getSelectedItem().toString(), winPrincipal.getCmbArea1().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema1().setEnabled(true);
                    }  else {
                        winPrincipal.getCmbProblema1().setSelectedIndex(-1);
                    }
                    break;
                case 5: // CAMBIO DE MODELO
                    if (winPrincipal.getCmbProblema1().getItemCount() >= 0){
                        winPrincipal.getCmbProblema1().setSelectedIndex(-1);
                    } 
                    if (winPrincipal.getCmbArea1().getSelectedIndex() == 1 ){
                        winPrincipal.getCmbProblema1().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                            PrincipalControl.linea,winPrincipal.getCmbTema().getSelectedItem().toString(), winPrincipal.getCmbArea1().getSelectedItem().toString()));
                        winPrincipal.getLblProblema1().setVisible(true);
                        winPrincipal.getCmbProblema1().setVisible(true);
                        winPrincipal.getCmbProblema1().setEnabled(true);   
                        winPrincipal.getCmbCliente1().setEnabled(false);
                    } else if (winPrincipal.getCmbArea1().getSelectedIndex() > 1){
                        winPrincipal.getCmbCliente1().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        winPrincipal.getCmbCliente1().setEnabled(true);   
                        winPrincipal.getTxtScrap1().setEditable(true);
                        winPrincipal.getTxtScrap1().setEnabled(true);
                        winPrincipal.getCmbProblema1().setEnabled(true);                                
                    } else {
                        if (winPrincipal.getCmbProblema1().getSelectedIndex() > 0){
                            winPrincipal.getCmbProblema1().setSelectedIndex(0);
                        }
                        winPrincipal.getCmbProblema1().setEnabled(false);
                    }
                    break;
                case 6: // PAROS PLANEADOS
                    winPrincipal.getCmbHora().setEnabled(false);
                    if (!winPrincipal.getCmbArea1().getSelectedItem().toString().equals("Otros")){
                        winPrincipal.getCmbHora().setEnabled(true);
                    } else { 
                        winPrincipal.getCmbProblema1().setVisible(true);
                        winPrincipal.getLblProblema1().setVisible(true);
                        winPrincipal.getCmbProblema1().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                PrincipalControl.linea,winPrincipal.getCmbTema().getSelectedItem().toString(), winPrincipal.getCmbArea1().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema1().setEnabled(true);
                    }
                    break;        
            }                            
        } else if (winPrincipal.getCmbProblema1().equals(e.getSource())){
            winPrincipal.getLblMatFaltante1().setVisible(false);
            winPrincipal.getTxtMatFaltante1().setVisible(false);
            winPrincipal.getTxtMatFaltante1().setEnabled(false);
            winPrincipal.getTxtMatFaltante1().setText("");
            
            //SCRAP
            winPrincipal.getTxtScrap1().setText("");
            winPrincipal.getTxtScrap1().setEnabled(false);
            
            //ADD PROBLEMAS
            winPrincipal.getBtnAdd().setEnabled(false);
            winPrincipal.getBtnRemove().setEnabled(false);

            limpiarTiemposIncidencia(winPrincipal);
            switch (winPrincipal.getCmbTema().getSelectedIndex()){                
                case 2:
                case 3:
                    if (winPrincipal.getCmbProblema1().getSelectedIndex() > 0 ){
                        winPrincipal.getTxtScrap1().setEditable(true);
                        winPrincipal.getTxtScrap1().setEnabled(true);
                    }                
                    break;
                case 4: //ORGANIZACIONALES
                    limpiarTiemposIncidencia(winPrincipal);
                    if (winPrincipal.getCmbArea1().getSelectedIndex() > 0) {                       
                        if(winPrincipal.getCmbArea1().getSelectedItem().toString().equals("Logistica")) { 
                            if ( winPrincipal.getCmbProblema1().getSelectedIndex() > 1) {
                                winPrincipal.getLblMatFaltante1().setVisible(true);
                                winPrincipal.getTxtMatFaltante1().setVisible(true);
                                winPrincipal.getTxtMatFaltante1().setEnabled(true); 
                                winPrincipal.getCmbHora().setEnabled(true); 
                            } else {
                                winPrincipal.getTxtMatFaltante1().setText("");
                                winPrincipal.getLblMatFaltante1().setVisible(false);
                                winPrincipal.getTxtMatFaltante1().setVisible(false);
                                winPrincipal.getTxtMatFaltante1().setEnabled(false);
                                winPrincipal.getCmbHora().setEnabled(true);
                            }  
                        } else {
                            if ( winPrincipal.getCmbProblema1().getSelectedIndex() > 0) {
                                winPrincipal.getCmbHora().setEnabled(true);
                            } else {
                                winPrincipal.getCmbHora().setEnabled(false);
                            }
                        } 
                    } 
                    break;
                case 5: // CAMBIO DE MODELO 
                    winPrincipal.getCmbCliente1().setEnabled(false);
                    if (winPrincipal.getCmbArea1().getSelectedIndex() > 0) {
                        winPrincipal.getCmbCliente1().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        if(winPrincipal.getCmbArea1().getSelectedIndex() == 1 ) {
                            if ( winPrincipal.getCmbProblema1().getSelectedIndex() > 1) {
                                winPrincipal.getLblMatFaltante1().setVisible(true);
                                winPrincipal.getTxtMatFaltante1().setVisible(true);
                                winPrincipal.getTxtMatFaltante1().setEnabled(true);
                            } else {
                                winPrincipal.getTxtMatFaltante1().setText("");
                                winPrincipal.getLblMatFaltante1().setVisible(false);
                                winPrincipal.getTxtMatFaltante1().setVisible(false);
                                winPrincipal.getTxtMatFaltante1().setEnabled(false);
                                winPrincipal.getCmbCliente1().setEnabled(true);
                            }  
                        } else {
                            winPrincipal.getCmbCliente1().setEnabled(true);
                            if ( winPrincipal.getCmbProblema1().getSelectedIndex() > 0) {
                                winPrincipal.getCmbHora().setEnabled(true);
                            } else {
                                winPrincipal.getCmbHora().setEnabled(false);
                            } 
                        } 
                    } 
                    break;
                case 6: // PARO PLANEADO                 
                    winPrincipal.getCmbHora().setEnabled(false);
                    if (winPrincipal.getCmbProblema1().getSelectedIndex() != 0 ){
                        winPrincipal.getCmbHora().setEnabled(true);
                    } 
                    break;
            }                 
        } else if (winPrincipal.getCmbCliente1().equals(e.getSource())){
            
            //NO PARTE
            if (winPrincipal.getCmbNoParte1().getItemCount()!= 0){
                winPrincipal.getCmbNoParte1().setSelectedIndex(0);
            }
            
            //BOTONES                    
            winPrincipal.getCmbNoParte1().setEnabled(false); 
            if (winPrincipal.getCmbCliente1().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte1().setModel(
                        new PrincipalMetodos().listaNoPartes(
                                PrincipalControl.linea,
                                winPrincipal.getCmbCliente1().getSelectedItem().toString()
                        ));
                winPrincipal.getCmbNoParte1().setEnabled(true);
            }
        } else if (winPrincipal.getCmbNoParte1().equals(e.getSource())){
            if (winPrincipal.getCmbNoParte1().getSelectedIndex() > 0){
                winPrincipal.getTxtScrap1().setEnabled(true);
            } else {
                winPrincipal.getTxtScrap1().setText("");
                winPrincipal.getTxtScrap1().setEnabled(false);
                limpiarTiemposIncidencia(winPrincipal);
            }
        }
        //APARTADO PARA EL SEGUNDO PANEL
        else if (winPrincipal.getCmbOperacion2().equals(e.getSource())){
            //AREA
            if (winPrincipal.getCmbArea2().getItemCount() > 0) {
                winPrincipal.getCmbArea2().setSelectedIndex(0);
            }
            //PROBLEMA
            if (winPrincipal.getCmbProblema2().getItemCount() > 0){
                winPrincipal.getCmbProblema2().setSelectedIndex(0);
            }
            //BOTONES                    
            winPrincipal.getCmbArea2().setEnabled(false);
            winPrincipal.getCmbProblema2().setEnabled(false);
            //SCRAP
            winPrincipal.getTxtScrap2().setText("");
            winPrincipal.getTxtScrap2().setEnabled(false);
            //ADD PROBLEMAS           
            switch (winPrincipal.getCmbTema2().getSelectedIndex()){                
                case 1:
                case 2:   
                    if (winPrincipal.getCmbOperacion2().getSelectedIndex() > 0){
                        winPrincipal.getCmbArea2().setModel(new PrincipalMetodos().listaAreasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema2().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion2().getSelectedItem().toString()));
                        winPrincipal.getCmbArea2().setEnabled(true);
                    }                     
                    break;
            }                
        } else if (winPrincipal.getCmbArea2().equals(e.getSource())){  
            if (winPrincipal.getCmbProblema2().getItemCount() > 0){
                winPrincipal.getCmbProblema2().setSelectedIndex(0);
            }
            
            winPrincipal.getLblProblema2().setVisible(false);
            winPrincipal.getCmbProblema2().setVisible(false);
            winPrincipal.getCmbProblema2().setEnabled(false);            
          
            if (winPrincipal.getCmbCliente2().getItemCount()!= 0){
                winPrincipal.getCmbCliente2().setSelectedIndex(0);
            } 

            winPrincipal.getCmbCliente2().setEnabled(false);
            //NO PARTE
            if (winPrincipal.getCmbNoParte2().getItemCount() != 0){
                winPrincipal.getCmbNoParte2().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte2().setEnabled(false);
            
            winPrincipal.getTxtMatFaltante2().setText("");
            winPrincipal.getLblMatFaltante2().setVisible(false);
            winPrincipal.getTxtMatFaltante2().setVisible(false);
            winPrincipal.getTxtMatFaltante2().setEnabled(false);  
            
            //SCRAP
            winPrincipal.getTxtScrap2().setText("");
            winPrincipal.getTxtScrap2().setEnabled(false);
            //ADD PROBLEMAS            
            
            switch (winPrincipal.getCmbTema2().getSelectedIndex()){                
                case 1:
                case 2:
                    winPrincipal.getLblProblema2().setVisible(true);
                    winPrincipal.getCmbProblema2().setVisible(true);
                    
                    if (winPrincipal.getCmbArea2().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema2().setModel(new PrincipalMetodos().listaProblemasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema2().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion2().getSelectedItem().toString(),winPrincipal.getCmbArea2().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema2().setEnabled(true);
                    } 
                    break;
                    
                case 3:
                    winPrincipal.getLblProblema2().setVisible(true);
                    winPrincipal.getCmbProblema2().setVisible(true);
                    if (winPrincipal.getCmbArea2().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema2().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                PrincipalControl.linea,winPrincipal.getCmbTema2().getSelectedItem().toString(), winPrincipal.getCmbArea2().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema2().setEnabled(true);
                    }                     
                    break;
                    
                case 4:                    
                    if (winPrincipal.getCmbProblema2().getItemCount() != 0){
                        winPrincipal.getCmbProblema2().setSelectedIndex(-1);
                    }
                    winPrincipal.getCmbCliente2().setEnabled(false);
                    
                    if (winPrincipal.getCmbArea2().getSelectedIndex() == 1) {
                        winPrincipal.getLblProblema2().setVisible(true);
                        winPrincipal.getCmbProblema2().setVisible(true);
                        if (winPrincipal.getCmbArea2().getSelectedIndex() > 0){
                            winPrincipal.getCmbProblema2().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                    PrincipalControl.linea,winPrincipal.getCmbTema2().getSelectedItem().toString(), winPrincipal.getCmbArea2().getSelectedItem().toString()));
                            winPrincipal.getCmbProblema2().setSelectedIndex(0);
                            winPrincipal.getCmbProblema2().setEnabled(true);
                        }                     
                    } else if (winPrincipal.getCmbArea2().getSelectedIndex() > 0){
                        winPrincipal.getCmbCliente2().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        winPrincipal.getCmbCliente2().setEnabled(true);
                    }                     
                    break;
            }                            
        } else if (winPrincipal.getCmbProblema2().equals(e.getSource())){
            winPrincipal.getLblMatFaltante2().setVisible(false);
            winPrincipal.getTxtMatFaltante2().setVisible(false);
            winPrincipal.getTxtMatFaltante2().setEnabled(false);
            winPrincipal.getTxtMatFaltante2().setText("");
            //SCRAP
            winPrincipal.getTxtScrap2().setText("");
            winPrincipal.getTxtScrap2().setEnabled(false);
            winPrincipal.getTxtDur2().setText("");
            winPrincipal.getTxtDur2().setEnabled(false);
            winPrincipal.getCmbCliente2().setEnabled(false);
            //ADD PROBLEMAS            
            switch (winPrincipal.getCmbTema2().getSelectedIndex()){                
                case 1:
                case 2:
                    if (winPrincipal.getCmbProblema2().getSelectedIndex() > 0 ){
                        winPrincipal.getTxtScrap2().setEditable(true);
                        winPrincipal.getTxtScrap2().setEnabled(true);
                    }                
                    break;
                case 3:  
                    winPrincipal.getTxtMatFaltante2().setText("");
                    winPrincipal.getLblMatFaltante2().setVisible(false);
                    winPrincipal.getTxtMatFaltante2().setVisible(false);
                    winPrincipal.getTxtMatFaltante2().setEnabled(false);
                    winPrincipal.getTxtDur2().setEnabled(false);                        
                    if(winPrincipal.getCmbArea2().getSelectedIndex() == 1 ) {
                        if ( winPrincipal.getCmbProblema2().getSelectedIndex() > 1) {
                            winPrincipal.getLblMatFaltante2().setVisible(true);
                            winPrincipal.getTxtMatFaltante2().setVisible(true);
                            winPrincipal.getTxtMatFaltante2().setEnabled(true);
                        } else if(winPrincipal.getCmbProblema2().getSelectedIndex() > 0){                                                           
                            winPrincipal.getTxtDur2().setEnabled(true);                                
                        }  
                    } else {
                        if ( winPrincipal.getCmbProblema2().getSelectedIndex() > 0) {
                            winPrincipal.getTxtDur2().setEnabled(true);
                        } else {
                            winPrincipal.getTxtDur2().setEnabled(false);
                        }
                    } 
                    break;
                case 4:                    
                    if (winPrincipal.getCmbCliente2().getItemCount()!= 0){
                        winPrincipal.getCmbCliente2().setSelectedIndex(0);
                    }
                    if (winPrincipal.getCmbNoParte2().getItemCount()!= 0){
                        winPrincipal.getCmbNoParte2().setSelectedIndex(0);
                    }
                    
                    winPrincipal.getCmbCliente2().setEnabled(false);
                    winPrincipal.getCmbNoParte2().setEnabled(false);
                                      
                    if(winPrincipal.getCmbArea2().getSelectedIndex() == 1 ) {
                        if ( winPrincipal.getCmbProblema2().getSelectedIndex() > 1) {
                            winPrincipal.getLblMatFaltante2().setVisible(true);
                            winPrincipal.getTxtMatFaltante2().setVisible(true);
                            winPrincipal.getTxtMatFaltante2().setEnabled(true);
                        } else if (winPrincipal.getCmbProblema2().getSelectedIndex() > 0){
                            winPrincipal.getTxtMatFaltante2().setText("");
                            winPrincipal.getLblMatFaltante2().setVisible(false);
                            winPrincipal.getTxtMatFaltante2().setVisible(false);
                            winPrincipal.getTxtMatFaltante2().setEnabled(false);    
                            winPrincipal.getCmbCliente2().setEnabled(true);
                        }  
                    } 
                    break;
                case 6:                    
                    if (winPrincipal.getCmbProblema2().getSelectedIndex() == 1){
                        winPrincipal.getLblMatFaltante2().setVisible(true);
                        winPrincipal.getTxtMatFaltante2().setVisible(true);
                        winPrincipal.getTxtMatFaltante2().setEnabled(true);
                    } 
                    break;
            }                 
        } else if (winPrincipal.getCmbCliente2().equals(e.getSource())){
            //NO PARTE
            if (winPrincipal.getCmbNoParte2().getItemCount() != 0){
                winPrincipal.getCmbNoParte2().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte2().setEnabled(false); 
            
            if (winPrincipal.getCmbCliente2().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte2().setModel(
                        new PrincipalMetodos().listaNoPartes(
                                PrincipalControl.linea,
                                winPrincipal.getCmbCliente2().getSelectedItem().toString()
                        ));                    
                winPrincipal.getCmbNoParte2().setEnabled(true);
            }             
        } else if (winPrincipal.getCmbNoParte2().equals(e.getSource())){            
            //PROBLEMA
            winPrincipal.getTxtScrap2().setText("");
            if (winPrincipal.getCmbNoParte2().getSelectedIndex() > 0){                
                winPrincipal.getTxtScrap2().setEditable(true);
                winPrincipal.getTxtScrap2().setEnabled(true);
            } else {
                winPrincipal.getTxtScrap2().setEditable(false);
                winPrincipal.getTxtScrap2().setEnabled(false);
            }                         
        } 
        //*****APARTADO TERCER PANEL
        else if (winPrincipal.getCmbOperacion3().equals(e.getSource())){
            
            //AREA
            if (winPrincipal.getCmbArea3().getItemCount() > 0){
                winPrincipal.getCmbArea3().setSelectedIndex(0);
            }
            //PROBLEMA
            if (winPrincipal.getCmbProblema3().getItemCount() > 0){
                winPrincipal.getCmbProblema3().setSelectedIndex(0);
            }
            //BOTONES                    
            winPrincipal.getCmbArea3().setEnabled(false);
            winPrincipal.getCmbProblema3().setEnabled(false);
            //SCRAP
            winPrincipal.getTxtScrap3().setText("");
            winPrincipal.getTxtScrap3().setEnabled(false);
            //ADD PROBLEMAS           
            switch (winPrincipal.getCmbTema3().getSelectedIndex()){                
                case 1:
                case 2:   
                    if (winPrincipal.getCmbOperacion3().getSelectedIndex() > 0){
                        winPrincipal.getCmbArea3().setModel(new PrincipalMetodos().listaAreasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema3().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion3().getSelectedItem().toString()));
                        winPrincipal.getCmbArea3().setEnabled(true);
                    }                     
                    break;
            }                
        } else if (winPrincipal.getCmbArea3().equals(e.getSource())){  
            if (winPrincipal.getCmbProblema3().getItemCount() > 0){
                winPrincipal.getCmbProblema3().setSelectedIndex(0);
            }
//            
            winPrincipal.getLblProblema3().setVisible(false);
            winPrincipal.getCmbProblema3().setVisible(false);
            winPrincipal.getCmbProblema3().setEnabled(false);            
//          
            if (winPrincipal.getCmbCliente3().getItemCount()!= 0){
                winPrincipal.getCmbCliente3().setSelectedIndex(0);
            } 

            winPrincipal.getCmbCliente3().setEnabled(false);
            //NO PARTE
            if (winPrincipal.getCmbNoParte3().getItemCount() != 0){
                winPrincipal.getCmbNoParte3().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte3().setEnabled(false);
            
            winPrincipal.getTxtMatFaltante3().setText("");
            winPrincipal.getLblMatFaltante3().setVisible(false);
            winPrincipal.getTxtMatFaltante3().setVisible(false);
            winPrincipal.getTxtMatFaltante3().setEnabled(false);  
//            
            //SCRAP
            winPrincipal.getTxtScrap3().setText("");
            winPrincipal.getTxtScrap3().setEnabled(false);
//            //ADD PROBLEMAS            
//            
            switch (winPrincipal.getCmbTema3().getSelectedIndex()){                
                case 1:
                case 2:
                    winPrincipal.getLblProblema3().setVisible(true);
                    winPrincipal.getCmbProblema3().setVisible(true);
                    
                    if (winPrincipal.getCmbArea3().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema3().setModel(new PrincipalMetodos().listaProblemasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema3().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion3().getSelectedItem().toString(),winPrincipal.getCmbArea3().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema3().setEnabled(true);
                    } 
                    break;
                    
                case 3:
                    winPrincipal.getLblProblema3().setVisible(true);
                    winPrincipal.getCmbProblema3().setVisible(true);
                    if (winPrincipal.getCmbArea3().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema3().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                PrincipalControl.linea,winPrincipal.getCmbTema3().getSelectedItem().toString(), winPrincipal.getCmbArea3().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema3().setEnabled(true);
                    }                     
                    break;
                    
                case 4:                    
                    if (winPrincipal.getCmbProblema3().getItemCount() != 0){
                        winPrincipal.getCmbProblema3().setSelectedIndex(-1);
                    }
                    winPrincipal.getCmbCliente3().setEnabled(false);
                    
                    if (winPrincipal.getCmbArea3().getSelectedIndex() == 1) {
                        winPrincipal.getLblProblema3().setVisible(true);
                        winPrincipal.getCmbProblema3().setVisible(true);
                        if (winPrincipal.getCmbArea3().getSelectedIndex() > 0){
                            winPrincipal.getCmbProblema3().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                    PrincipalControl.linea,winPrincipal.getCmbTema3().getSelectedItem().toString(), winPrincipal.getCmbArea3().getSelectedItem().toString()));
                            winPrincipal.getCmbProblema3().setSelectedIndex(0);
                            winPrincipal.getCmbProblema3().setEnabled(true);
                        }                     
                    } else if (winPrincipal.getCmbArea3().getSelectedIndex() > 0) {
                        winPrincipal.getCmbCliente3().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        winPrincipal.getCmbCliente3().setEnabled(true);
                    }                     
                    break;
            }                            
        } else if (winPrincipal.getCmbProblema3().equals(e.getSource())){
            winPrincipal.getLblMatFaltante3().setVisible(false);
            winPrincipal.getTxtMatFaltante3().setVisible(false);
            winPrincipal.getTxtMatFaltante3().setEnabled(false);
            winPrincipal.getTxtMatFaltante3().setText("");
            //SCRAP
            winPrincipal.getTxtScrap3().setText("");
            winPrincipal.getTxtScrap3().setEnabled(false);
            winPrincipal.getTxtDur3().setText("");
            winPrincipal.getTxtDur3().setEnabled(false);
            //ADD PROBLEMAS            
            switch (winPrincipal.getCmbTema3().getSelectedIndex()){                
                case 1:
                case 2:
                    if (winPrincipal.getCmbProblema3().getSelectedIndex() > 0 ){
                        winPrincipal.getTxtScrap3().setEditable(true);
                        winPrincipal.getTxtScrap3().setEnabled(true);
                    }                
                    break;
                case 3:  
                    winPrincipal.getTxtMatFaltante3().setText("");
                    winPrincipal.getLblMatFaltante3().setVisible(false);
                    winPrincipal.getTxtMatFaltante3().setVisible(false);
                    winPrincipal.getTxtMatFaltante3().setEnabled(false);   
                    winPrincipal.getTxtDur2().setEnabled(false);                    
                    if(winPrincipal.getCmbArea3().getSelectedIndex() == 1 ) {
                        if ( winPrincipal.getCmbProblema3().getSelectedIndex() > 1) {
                            winPrincipal.getLblMatFaltante3().setVisible(true);
                            winPrincipal.getTxtMatFaltante3().setVisible(true);
                            winPrincipal.getTxtMatFaltante3().setEnabled(true);
                        } else if (winPrincipal.getCmbProblema3().getSelectedIndex() > 0){                                  
                            winPrincipal.getTxtDur3().setEnabled(true);
                        }  
                    } else {
                        if ( winPrincipal.getCmbProblema3().getSelectedIndex() > 0) {
                            winPrincipal.getTxtDur3().setEnabled(true);
                        } else {
                            winPrincipal.getTxtDur3().setEnabled(false);
                        }
                    } 
                    break;
                case 4:   
                    winPrincipal.getCmbCliente3().setEnabled(false);
                    if (winPrincipal.getCmbCliente3().getItemCount()!= 0){
                        winPrincipal.getCmbCliente3().setSelectedIndex(0);
                    }
                    if (winPrincipal.getCmbNoParte3().getItemCount()!= 0){
                        winPrincipal.getCmbNoParte3().setSelectedIndex(0);
                    }
                    
                    winPrincipal.getCmbCliente3().setEnabled(false);
                    winPrincipal.getCmbNoParte3().setEnabled(false);
                                       
                    if(winPrincipal.getCmbArea3().getSelectedIndex() == 1 ) {
                        if ( winPrincipal.getCmbProblema3().getSelectedIndex() > 1) {
                            winPrincipal.getLblMatFaltante3().setVisible(true);
                            winPrincipal.getTxtMatFaltante3().setVisible(true);
                            winPrincipal.getTxtMatFaltante3().setEnabled(true);
                        } else if ( winPrincipal.getCmbProblema3().getSelectedIndex() > 0){
                            winPrincipal.getTxtMatFaltante3().setText("");
                            winPrincipal.getLblMatFaltante3().setVisible(false);
                            winPrincipal.getTxtMatFaltante3().setVisible(false);
                            winPrincipal.getTxtMatFaltante3().setEnabled(false);    
                            winPrincipal.getCmbCliente3().setEnabled(true);
                            winPrincipal.getCmbCliente3().setModel( new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        }  
                    } 
                    break;
                case 6:                    
                    if (winPrincipal.getCmbProblema3().getSelectedIndex() == 1){
                        winPrincipal.getLblMatFaltante3().setVisible(true);
                        winPrincipal.getTxtMatFaltante3().setVisible(true);
                        winPrincipal.getTxtMatFaltante3().setEnabled(true);
                    } 
                    break;
            }                 
        } else if (winPrincipal.getCmbCliente3().equals(e.getSource())){
            //NO PARTE
            if (winPrincipal.getCmbNoParte3().getItemCount() != 0){
                winPrincipal.getCmbNoParte3().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte3().setEnabled(false);           
            
            if (winPrincipal.getCmbCliente3().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte3().setModel(
                        new PrincipalMetodos().listaNoPartes(
                                PrincipalControl.linea,
                                winPrincipal.getCmbCliente3().getSelectedItem().toString()
                        ));                    
                winPrincipal.getCmbNoParte3().setEnabled(true);
            }             
        } else if (winPrincipal.getCmbNoParte3().equals(e.getSource())){            
            //PROBLEMA
            winPrincipal.getTxtScrap3().setText("");
            if (winPrincipal.getCmbNoParte3().getSelectedIndex() > 0){                
                winPrincipal.getTxtScrap3().setEditable(true);
                winPrincipal.getTxtScrap3().setEnabled(true);
            } else {
                winPrincipal.getTxtScrap3().setEditable(false);
                winPrincipal.getTxtScrap3().setEnabled(false);
            }                         
        }
        //*********APARTADO PARA EL TERCER PANEL
        else if (winPrincipal.getCmbOperacion4().equals(e.getSource())){            
            //AREA
            if (winPrincipal.getCmbArea4().getItemCount() > 0){
                winPrincipal.getCmbArea4().setSelectedIndex(0);
            }
            //PROBLEMA
            if (winPrincipal.getCmbProblema4().getItemCount() > 0){
                winPrincipal.getCmbProblema4().setSelectedIndex(-1);
            }
            //BOTONES                    
            winPrincipal.getCmbArea4().setEnabled(false);
            winPrincipal.getCmbProblema4().setEnabled(false);
            //SCRAP
            winPrincipal.getTxtScrap4().setText("");
            winPrincipal.getTxtScrap4().setEnabled(false);
            //ADD PROBLEMAS           
            switch (winPrincipal.getCmbTema4().getSelectedIndex()){                
                case 1:
                case 2:   
                    if (winPrincipal.getCmbOperacion4().getSelectedIndex() > 0){
                        winPrincipal.getCmbArea4().setModel(new PrincipalMetodos().listaAreasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema4().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion4().getSelectedItem().toString()));
                        winPrincipal.getCmbArea4().setEnabled(true);
                    }                     
                    break;
            }                
        } else if (winPrincipal.getCmbArea4().equals(e.getSource())){  
            if (winPrincipal.getCmbProblema4().getItemCount() > 0){
                winPrincipal.getCmbProblema4().setSelectedIndex(-1);
            }
//            
            winPrincipal.getLblProblema4().setVisible(false);
            winPrincipal.getCmbProblema4().setVisible(false);
            winPrincipal.getCmbProblema4().setEnabled(false);            
//          
            if (winPrincipal.getCmbCliente4().getItemCount()!= 0){
                winPrincipal.getCmbCliente4().setSelectedIndex(0);
            } 

            winPrincipal.getCmbCliente4().setEnabled(false);
            //NO PARTE
            if (winPrincipal.getCmbNoParte4().getItemCount() != 0){
                winPrincipal.getCmbNoParte4().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte4().setEnabled(false);
            
            winPrincipal.getTxtMatFaltante4().setText("");
            winPrincipal.getLblMatFaltante4().setVisible(false);
            winPrincipal.getTxtMatFaltante4().setVisible(false);
            winPrincipal.getTxtMatFaltante4().setEnabled(false);  
//            
            //SCRAP
            winPrincipal.getTxtScrap4().setText("");
            winPrincipal.getTxtScrap4().setEnabled(false);
//            //ADD PROBLEMAS            
//            
            switch (winPrincipal.getCmbTema4().getSelectedIndex()){                
                case 1:
                case 2:
                    winPrincipal.getLblProblema4().setVisible(true);
                    winPrincipal.getCmbProblema4().setVisible(true);
                    
                    if (winPrincipal.getCmbArea4().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema4().setModel(new PrincipalMetodos().listaProblemasCalidad(
                            PrincipalControl.linea,winPrincipal.getCmbTema4().getSelectedItem().toString(),
                            winPrincipal.getCmbOperacion4().getSelectedItem().toString(),winPrincipal.getCmbArea4().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema4().setEnabled(true);
                    } 
                    break;
                    
                case 3:
                    winPrincipal.getLblProblema4().setVisible(true);
                    winPrincipal.getCmbProblema4().setVisible(true);
                    if (winPrincipal.getCmbArea4().getSelectedIndex() > 0){
                        winPrincipal.getCmbProblema4().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                PrincipalControl.linea,winPrincipal.getCmbTema4().getSelectedItem().toString(), winPrincipal.getCmbArea4().getSelectedItem().toString()));
                        winPrincipal.getCmbProblema4().setEnabled(true);
                    }                     
                    break;
                    
                case 4:                    
                    if (winPrincipal.getCmbProblema4().getItemCount() != 0){
                        winPrincipal.getCmbProblema4().setSelectedIndex(-1);
                    }
                    winPrincipal.getCmbCliente4().setEnabled(false);
                    
                    if (winPrincipal.getCmbArea4().getSelectedIndex() == 1) {
                        winPrincipal.getLblProblema4().setVisible(true);
                        winPrincipal.getCmbProblema4().setVisible(true);
                        if (winPrincipal.getCmbArea4().getSelectedIndex() > 0){
                            winPrincipal.getCmbProblema4().setModel(new PrincipalMetodos().listaProblemasOrganizacional(
                                    PrincipalControl.linea,winPrincipal.getCmbTema4().getSelectedItem().toString(), winPrincipal.getCmbArea4().getSelectedItem().toString()));
                            winPrincipal.getCmbProblema4().setSelectedIndex(-1);
                            winPrincipal.getCmbProblema4().setEnabled(true);
                        }                     
                    } else if (winPrincipal.getCmbArea4().getSelectedIndex() > 0){
                        winPrincipal.getCmbCliente4().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                        winPrincipal.getCmbCliente4().setEnabled(true);
                    }                     
                    break;
            }                            
        } else if (winPrincipal.getCmbProblema4().equals(e.getSource())){
            winPrincipal.getLblMatFaltante4().setVisible(false);
            winPrincipal.getTxtMatFaltante4().setVisible(false);
            winPrincipal.getTxtMatFaltante4().setEnabled(false);
            winPrincipal.getTxtMatFaltante4().setText("");
            //SCRAP
            winPrincipal.getTxtScrap4().setText("");
            winPrincipal.getTxtScrap4().setEnabled(false);
            winPrincipal.getTxtDur4().setText("");
            winPrincipal.getTxtDur4().setEnabled(false);
            //ADD PROBLEMAS            
            switch (winPrincipal.getCmbTema4().getSelectedIndex()){                
                case 1:
                case 2:
                    if (winPrincipal.getCmbProblema4().getSelectedIndex() > 0 ){
                        winPrincipal.getTxtScrap4().setEditable(true);
                        winPrincipal.getTxtScrap4().setEnabled(true);
                    }                
                    break;
                case 3:  
                    //if (winPrincipal.getCmbArea4().getSelectedIndex() > 0) {                       
                        if(winPrincipal.getCmbArea4().getSelectedIndex() == 1 ) {
                            if ( winPrincipal.getCmbProblema4().getSelectedIndex() > 1) {
                                winPrincipal.getLblMatFaltante4().setVisible(true);
                                winPrincipal.getTxtMatFaltante4().setVisible(true);
                                winPrincipal.getTxtMatFaltante4().setEnabled(true);
                            } else if ( winPrincipal.getCmbProblema4().getSelectedIndex() > 0) {    
                                winPrincipal.getTxtDur4().setEnabled(true);
                            }  
                        } else {
                            if ( winPrincipal.getCmbProblema4().getSelectedIndex() > 0) {
                                winPrincipal.getTxtDur4().setEnabled(true);
                            } else {
                                winPrincipal.getTxtDur4().setEnabled(false);
                            }
                        } 
                    //} 
                    break;
                case 4:                    
                    if (winPrincipal.getCmbCliente4().getItemCount()!= 0){
                        winPrincipal.getCmbCliente4().setSelectedIndex(0);
                    }
                    if (winPrincipal.getCmbNoParte4().getItemCount()!= 0){
                        winPrincipal.getCmbNoParte4().setSelectedIndex(0);
                    }
                    
                    winPrincipal.getCmbCliente4().setEnabled(false);
                    winPrincipal.getCmbNoParte4().setEnabled(false);
                    
                    winPrincipal.getTxtMatFaltante4().setText("");
                    winPrincipal.getLblMatFaltante4().setVisible(false);
                    winPrincipal.getTxtMatFaltante4().setVisible(false);
                    winPrincipal.getTxtMatFaltante4().setEnabled(false);    
                                        
                    if(winPrincipal.getCmbArea4().getSelectedIndex() == 1 ) {
                        if ( winPrincipal.getCmbProblema4().getSelectedIndex() > 1) {
                            winPrincipal.getLblMatFaltante4().setVisible(true);
                            winPrincipal.getTxtMatFaltante4().setVisible(true);
                            winPrincipal.getTxtMatFaltante4().setEnabled(true);
                        } else if (winPrincipal.getCmbProblema4().getSelectedIndex() > 0){
                            winPrincipal.getCmbCliente4().setModel(new PrincipalMetodos().listaClientes(PrincipalControl.linea));
                            winPrincipal.getCmbCliente4().setEnabled(true);
                        }  
                    } 
                    break;
                case 6:                    
                    if (winPrincipal.getCmbProblema4().getSelectedIndex() == 1){
                        winPrincipal.getLblMatFaltante4().setVisible(true);
                        winPrincipal.getTxtMatFaltante4().setVisible(true);
                        winPrincipal.getTxtMatFaltante4().setEnabled(true);
                    } 
                    break;
            }                 
        } else if (winPrincipal.getCmbCliente4().equals(e.getSource())){
            //NO PARTE
            if (winPrincipal.getCmbNoParte4().getItemCount() != 0){
                winPrincipal.getCmbNoParte4().setSelectedIndex(0);
            }            
            //BOTONES                    
            winPrincipal.getCmbNoParte4().setEnabled(false);           
            
            if (winPrincipal.getCmbCliente4().getSelectedIndex() > 0){
                winPrincipal.getCmbNoParte4().setModel(
                        new PrincipalMetodos().listaNoPartes(
                                PrincipalControl.linea,
                                winPrincipal.getCmbCliente4().getSelectedItem().toString()
                        ));                    
                winPrincipal.getCmbNoParte4().setEnabled(true);
            }             
        } else if (winPrincipal.getCmbNoParte4().equals(e.getSource())){            
            //PROBLEMA
            winPrincipal.getTxtScrap4().setText("");
            if (winPrincipal.getCmbNoParte4().getSelectedIndex() > 0){                
                winPrincipal.getTxtScrap4().setEditable(true);
                winPrincipal.getTxtScrap4().setEnabled(true);
            } else {
                winPrincipal.getTxtScrap4().setEditable(false);
                winPrincipal.getTxtScrap4().setEnabled(false);
            }                         
        }
    }

    public static void validarKeyReleased(Principal winPrincipal, KeyEvent ke) {
        //JTextField Tiempo Inicio
        int duracion = 0;
        if (winPrincipal.getTxtTiempoInicio().equals(ke.getSource())) {
            if (!winPrincipal.getTxtTiempoInicio().getText().isEmpty()) {
                if (Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText()) > 59
                        || Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText()) < 0) {
                    winPrincipal.getTxtTiempoInicio().setText("");
                }
                if (!winPrincipal.getTxtTiempoFin().getText().isEmpty() && 
                    Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText()) <= Integer.parseInt(winPrincipal.getTxtTiempoFin().getText())){
                    //Actualiza cuando se hace un cambio en el minuto inicial
                    duracion = duracion = Integer.parseInt(winPrincipal.getTxtTiempoFin().getText())
                                - Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());                    
                    winPrincipal.getTxtDuracion().setText(String.valueOf(duracion + 1));                    
                }else {
                    winPrincipal.getTxtTiempoFin().setText("");
                }                
            }
        } else if (winPrincipal.getTxtTiempoFin().equals(ke.getSource())) { //AQUI VALIDACION DE FIN PARA 2 DIGITOS            
            if(winPrincipal.getTxtTiempoInicio().getText().length() == 1){ //VALIDACION PARA LOS DIGITOS, PARA QUE TE DEJE ESCRIBIR EJEMPLO: Inicio:9 y fin:12, para que no te borre 
                String f = winPrincipal.getTxtTiempoFin().getText();
                winPrincipal.getTxtTiempoInicio().setText("0"+winPrincipal.getTxtTiempoInicio().getText());                
                winPrincipal.getTxtTiempoFin().setEditable(true);
                winPrincipal.getTxtTiempoFin().setEnabled(true);
                winPrincipal.getTxtTiempoFin().setText(f);
                winPrincipal.getTxtTiempoFin().requestFocus();
            }
            
            if (!winPrincipal.getTxtTiempoFin().getText().isEmpty()) {
                if (Integer.parseInt(winPrincipal.getTxtTiempoFin().getText()) > 59 || Integer.parseInt(winPrincipal.getTxtTiempoFin().getText()) < 0) {
                    winPrincipal.getTxtTiempoFin().setText("");
                } else {
                    if(winPrincipal.getTxtTiempoInicio().getText().length() < winPrincipal.getTxtTiempoFin().getText().length() 
                        && Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText()) < Integer.parseInt(winPrincipal.getTxtTiempoFin().getText())){
                            duracion = Integer.parseInt(winPrincipal.getTxtTiempoFin().getText()) - Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());
                    }
                    if (winPrincipal.getTxtTiempoFin().getText().length() == winPrincipal.getTxtTiempoInicio().getText().length()
                            && Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText()) > Integer.parseInt(winPrincipal.getTxtTiempoFin().getText())) {
                        winPrincipal.getTxtTiempoFin().setText("");
                        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                        winPrincipal.getTxtDuracion().setText("");

                    } else{
                        duracion = Integer.parseInt(winPrincipal.getTxtTiempoFin().getText()) - Integer.parseInt(winPrincipal.getTxtTiempoInicio().getText());                            
                    }
                }
                if (!(duracion < 0)) {
                    winPrincipal.getTxtDuracion().setText(String.valueOf(duracion + 1));
                }                    
            }
        } else if (winPrincipal.getTxtDur2().equals(ke.getSource())) { //AQUI VALIDACION DE FIN PARA 2 DIGITOS             
            if (!winPrincipal.getTxtDur2().getText().isEmpty()) {                
                if (Integer.parseInt(winPrincipal.getTxtDur2().getText()) > 59 || Integer.parseInt(winPrincipal.getTxtDur2().getText()) < 1) {
                    winPrincipal.getTxtDur2().setText("");
                    winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                } else {
                    if ( winPrincipal.getTxtDuracion().getText().length() < winPrincipal.getTxtDur2().getText().length()){
                        winPrincipal.getTxtDur2().setText("");
                        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                    } else if (winPrincipal.getTxtDuracion().getText().length() == winPrincipal.getTxtDur2().getText().length()){
                        if (Integer.parseInt(winPrincipal.getTxtDuracion().getText()) < Integer.parseInt(winPrincipal.getTxtDur2().getText()) ){
                            winPrincipal.getTxtDur2().setText("");
                            winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                            //winPrincipal.getTxtDur2().setText(winPrincipal.getTxtDuracion().getText());
                        } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                            winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            winPrincipal.getBtnAdd().setEnabled(true);
                        }
                    } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                        winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                        winPrincipal.getBtnAdd().setEnabled(true);
                    }
                }
            }             
        } else if (winPrincipal.getTxtDur3().equals(ke.getSource())) { //AQUI VALIDACION DE FIN PARA 2 DIGITOS             
            if (!winPrincipal.getTxtDur3().getText().isEmpty()) {
                if (Integer.parseInt(winPrincipal.getTxtDur3().getText()) > 59 || Integer.parseInt(winPrincipal.getTxtDur3().getText()) < 1) {
                    winPrincipal.getTxtDur3().setText("");
                    winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                } else {
                    if ( winPrincipal.getTxtDuracion().getText().length() < winPrincipal.getTxtDur3().getText().length()){
                        winPrincipal.getTxtDur3().setText("");
                        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                    } else if (winPrincipal.getTxtDuracion().getText().length() == winPrincipal.getTxtDur3().getText().length()){
                        if (Integer.parseInt(winPrincipal.getTxtDuracion().getText()) < Integer.parseInt(winPrincipal.getTxtDur3().getText()) ){
                            winPrincipal.getTxtDur3().setText("");
                            winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                            //winPrincipal.getTxtDur3().setText(winPrincipal.getTxtDuracion().getText());
                        } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                            winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            winPrincipal.getBtnAdd().setEnabled(true);
                        }
                    } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                        winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                        winPrincipal.getBtnAdd().setEnabled(true);
                    }
                }                   
            }
        } else if (winPrincipal.getTxtDur4().equals(ke.getSource())) { //AQUI VALIDACION DE FIN PARA 2 DIGITOS             
            if (!winPrincipal.getTxtDur4().getText().isEmpty()) {
                if (Integer.parseInt(winPrincipal.getTxtDur4().getText()) > 59 || Integer.parseInt(winPrincipal.getTxtDur4().getText()) < 1) {
                    winPrincipal.getTxtDur4().setText("");
                    winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                } else {
                    if ( winPrincipal.getTxtDuracion().getText().length() < winPrincipal.getTxtDur4().getText().length()){
                        winPrincipal.getTxtDur4().setText("");
                        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                    } else if (winPrincipal.getTxtDuracion().getText().length() == winPrincipal.getTxtDur4().getText().length()){
                        if (Integer.parseInt(winPrincipal.getTxtDuracion().getText()) < Integer.parseInt(winPrincipal.getTxtDur4().getText()) ){
                            winPrincipal.getTxtDur4().setText("");
                            winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                        } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                            winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                        }
                    } else if (!winPrincipal.getTxtTcP().getText().isEmpty()){
                        winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                    }
                }                   
            }
        } else if (winPrincipal.getTxtTcP().equals(ke.getSource())) { //AQUI VALIDACION DE FIN PARA 2 DIGITOS 
            if (winPrincipal.getTxtTcP().getText().length() == 2){
                if(Integer.parseInt(winPrincipal.getTxtTcP().getText()) == 0){
                    winPrincipal.getTxtTcP().setText("");
                }
            }
        }
    }

    public static void validarKeyTyped(Principal winPrincipal, KeyEvent ke) {        
        if(winPrincipal.getTxtDur2().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9' )) {
                    ke.consume();
                }
                if(winPrincipal.getTxtDur2().getText().length()>= 2){
                    ke.consume();
                }
        }else if(winPrincipal.getTxtDur3().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9' )) {
                    ke.consume();
                }
                if(winPrincipal.getTxtDur3().getText().length()>= 2){
                    ke.consume();
                }
        }else if(winPrincipal.getTxtDur4().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9' )) {
                    ke.consume();
                }
                if(winPrincipal.getTxtDur4().getText().length()>= 2){
                    ke.consume();
                }
        }else if(winPrincipal.getTxtTcP().equals(ke.getSource())) {                
                
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9') && ke.getKeyChar() != '.') {
                    ke.consume();
                } else {                    
                    switch(winPrincipal.getTxtTcP().getText().length()){
                        case 0:
                        case 1:
                        case 3:
                            if (ke.getKeyChar() == '.'){
                                ke.consume();
                            }
                            break;
                        case 2:
                            if (ke.getKeyChar() != '.'){
                                ke.consume();
                            }
                            break;
                    }                   
                    
                    if(winPrincipal.getTxtTcP().getText().length() > 3){
                        ke.consume();
                    }  
                }    
                
                if (winPrincipal.getTxtTcP().getText().isEmpty()){      
                    winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                }else {
                    switch(PrincipalControl.contAdd){
                        case 0:
                            if (!winPrincipal.getTxtTiempoFin().getText().isEmpty() ){
                                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            }
                            break;
                        case 1:
                            if (!winPrincipal.getTxtDur2().getText().isEmpty() ){
                                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            }
                            break;
                        case 2:
                            if (!winPrincipal.getTxtDur3().getText().isEmpty() ){
                                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            }
                            break;
                        case 3:
                            if (!winPrincipal.getTxtDur4().getText().isEmpty() ){
                                winPrincipal.getBtnAgregarBitacora().setEnabled(true);
                            }
                            break;
                    }
                }
        }else if (winPrincipal.getTxtCantPzas().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                    ke.consume();
                }
                if (winPrincipal.getTxtCantPzas().getText().length() >= 3) {
                    ke.consume();
                }
                if (winPrincipal.getTxtCantPzas().getText().isEmpty()) {
                    winPrincipal.getCmbHora().setSelectedIndex(0);
                    winPrincipal.getCmbHora().setEnabled(false);
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                }
        } else if (winPrincipal.getTxtScrap1().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap1().getText().length() >= 3) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap1().getText().isEmpty()) {
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                    switch(winPrincipal.getCmbTema().getSelectedIndex()){
                        case 2:
                        case 3:
                        case 4:
                            winPrincipal.getCmbHora().setSelectedIndex(0);
                            winPrincipal.getCmbHora().setEnabled(true);
                            break;
                        case 5:
                            winPrincipal.getCmbCliente1().setEnabled(true);
                            break;  
                    }                
                }
        } else if (winPrincipal.getTxtScrap2().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap2().getText().length() >= 3) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap2().getText().isEmpty()) {
                    winPrincipal.getCmbHora().setEnabled(false);
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                }
        } else if (winPrincipal.getTxtScrap3().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap3().getText().length() >= 3) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap3().getText().isEmpty()) {
                    winPrincipal.getCmbHora().setEnabled(false);
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                }
        } else if (winPrincipal.getTxtScrap4().equals(ke.getSource())) {
                if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap4().getText().length() >= 3) {
                    ke.consume();
                }
                if (winPrincipal.getTxtScrap4().getText().isEmpty()) {
                    winPrincipal.getCmbHora().setEnabled(false);
                    winPrincipal.getBtnParoPeriodo().setVisible(false);
                }
        } else if (winPrincipal.getTxtMatFaltante1().equals(ke.getSource())) {
                
            int x = winPrincipal.getTxtMatFaltante1().getText().length();
            num[x] = (int) (ke.getKeyCode());            
            if (winPrincipal.getTxtMatFaltante1().getText().length() > 9){
                ke.consume();                     
            }else {
                if(ke.getKeyChar() != '0' && 
                    ke.getKeyChar() != '1' && 
                    ke.getKeyChar() != '2' && 
                    ke.getKeyChar() != '3' && 
                    ke.getKeyChar() != '4' && 
                    ke.getKeyChar() != '5' && 
                    ke.getKeyChar() != '6' && 
                    ke.getKeyChar() != '7' && 
                    ke.getKeyChar() != '8' && 
                    ke.getKeyChar() != '9' &&
                    ke.getKeyChar() != 'A' && 
                    ke.getKeyChar() != 'B' &&
                    ke.getKeyChar() != 'C' && 
                    ke.getKeyChar() != 'D' &&
                    ke.getKeyChar() != 'E' && 
                    ke.getKeyChar() != 'F' && 
                    ke.getKeyChar() != 'L' && 
                    ke.getKeyChar() != 'M' && 
                    ke.getKeyChar() != 'W' && 
                    ke.getKeyChar() != 'X' &&
                    ke.getKeyChar() != 'U' &&    
                    ke.getKeyChar() != 'Z' &&    
                    ke.getKeyChar() != 'a' && 
                    ke.getKeyChar() != 'b' && 
                    ke.getKeyChar() != 'c' && 
                    ke.getKeyChar() != 'd' && 
                    ke.getKeyChar() != 'e' && 
                    ke.getKeyChar() != 'f' && 
                    ke.getKeyChar() != 'l' && 
                    ke.getKeyChar() != 'm' &&
                    ke.getKeyChar() != 'w' && 
                    ke.getKeyChar() != 'x' && 
                    ke.getKeyChar() != 'u' && 
                    ke.getKeyChar() != 'z'
                    ) {
                    ke.consume();
                } else {
                    noParte[x] = (char) ke.getKeyChar();                    
                    if ( x > 2){  
                        if (noParte[x] == noParte[x-3] && noParte[x] == noParte[x-2] ){
                            ke.consume();
                        }
                        if (num[x] == num[x-1]+1 && num[x-1]-1 == num[x-2]+2   ){
                            ke.consume();
                        }
                    }
                }
            }            
            
            if (winPrincipal.getTxtMatFaltante1().getText().isEmpty() || 
                    winPrincipal.getTxtMatFaltante1().getText().length() < 9 ) {
                    switch(winPrincipal.getCmbTema().getSelectedIndex()){
                        case 5:
                            winPrincipal.getCmbCliente1().setEnabled(true);
                            break;
                    }                    
            } else {
                winPrincipal.getTxtScrap1().setText("");
            }            
        } else 
            if (winPrincipal.getTxtMatFaltante2().equals(ke.getSource())) {
            int x = winPrincipal.getTxtMatFaltante2().getText().length();
            num[x] = (int) (ke.getKeyCode());            
            if (winPrincipal.getTxtMatFaltante2().getText().length() > 9){
                ke.consume();                     
            }else {
                if(ke.getKeyChar() != '0' && 
                    ke.getKeyChar() != '1' && 
                    ke.getKeyChar() != '2' && 
                    ke.getKeyChar() != '3' && 
                    ke.getKeyChar() != '4' && 
                    ke.getKeyChar() != '5' && 
                    ke.getKeyChar() != '6' && 
                    ke.getKeyChar() != '7' && 
                    ke.getKeyChar() != '8' && 
                    ke.getKeyChar() != '9' &&
                    ke.getKeyChar() != 'A' && 
                    ke.getKeyChar() != 'B' &&
                    ke.getKeyChar() != 'C' && 
                    ke.getKeyChar() != 'D' &&
                    ke.getKeyChar() != 'E' && 
                    ke.getKeyChar() != 'F' && 
                    ke.getKeyChar() != 'L' && 
                    ke.getKeyChar() != 'M' && 
                    ke.getKeyChar() != 'W' && 
                    ke.getKeyChar() != 'X' &&
                    ke.getKeyChar() != 'U' &&    
                    ke.getKeyChar() != 'Z' &&    
                    ke.getKeyChar() != 'a' && 
                    ke.getKeyChar() != 'b' && 
                    ke.getKeyChar() != 'c' && 
                    ke.getKeyChar() != 'd' && 
                    ke.getKeyChar() != 'e' && 
                    ke.getKeyChar() != 'f' && 
                    ke.getKeyChar() != 'l' && 
                    ke.getKeyChar() != 'm' &&
                    ke.getKeyChar() != 'w' && 
                    ke.getKeyChar() != 'x' && 
                    ke.getKeyChar() != 'u' && 
                    ke.getKeyChar() != 'z'
                    ) {
                    ke.consume();
                } else {
                    noParte[x] = (char) ke.getKeyChar();                    
                    if ( x > 2){  
                        if (noParte[x] == noParte[x-3] && noParte[x] == noParte[x-2] ){
                            ke.consume();
                        }
                        if (num[x] == num[x-1]+1 && num[x-1]-1 == num[x-2]+2   ){
                            ke.consume();
                        }
                    }
                }
            }
        } else 
            if (winPrincipal.getTxtMatFaltante3().equals(ke.getSource())) {
            int x = winPrincipal.getTxtMatFaltante3().getText().length();
            num[x] = (int) (ke.getKeyCode());            
            if (winPrincipal.getTxtMatFaltante3().getText().length() > 9){
                ke.consume();                     
            }else {
                if(ke.getKeyChar() != '0' && 
                    ke.getKeyChar() != '1' && 
                    ke.getKeyChar() != '2' && 
                    ke.getKeyChar() != '3' && 
                    ke.getKeyChar() != '4' && 
                    ke.getKeyChar() != '5' && 
                    ke.getKeyChar() != '6' && 
                    ke.getKeyChar() != '7' && 
                    ke.getKeyChar() != '8' && 
                    ke.getKeyChar() != '9' &&
                    ke.getKeyChar() != 'A' && 
                    ke.getKeyChar() != 'B' &&
                    ke.getKeyChar() != 'C' && 
                    ke.getKeyChar() != 'D' &&
                    ke.getKeyChar() != 'E' && 
                    ke.getKeyChar() != 'F' && 
                    ke.getKeyChar() != 'L' && 
                    ke.getKeyChar() != 'M' && 
                    ke.getKeyChar() != 'W' && 
                    ke.getKeyChar() != 'X' &&
                    ke.getKeyChar() != 'U' &&    
                    ke.getKeyChar() != 'Z' &&    
                    ke.getKeyChar() != 'a' && 
                    ke.getKeyChar() != 'b' && 
                    ke.getKeyChar() != 'c' && 
                    ke.getKeyChar() != 'd' && 
                    ke.getKeyChar() != 'e' && 
                    ke.getKeyChar() != 'f' && 
                    ke.getKeyChar() != 'l' && 
                    ke.getKeyChar() != 'm' &&
                    ke.getKeyChar() != 'w' && 
                    ke.getKeyChar() != 'x' && 
                    ke.getKeyChar() != 'u' && 
                    ke.getKeyChar() != 'z'
                    ) {
                    ke.consume();
                } else {
                    noParte[x] = (char) ke.getKeyChar();                    
                    if ( x > 2){  
                        if (noParte[x] == noParte[x-3] && noParte[x] == noParte[x-2] ){
                            ke.consume();
                        }
                        if (num[x] == num[x-1]+1 && num[x-1]-1 == num[x-2]+2   ){
                            ke.consume();
                        }
                    }
                }
            }
        } else
            if (winPrincipal.getTxtMatFaltante4().equals(ke.getSource())) {
            int x = winPrincipal.getTxtMatFaltante4().getText().length();
            num[x] = (int) (ke.getKeyCode());            
            if (winPrincipal.getTxtMatFaltante4().getText().length() > 9){
                ke.consume();                     
            }else {
                if(ke.getKeyChar() != '0' && 
                    ke.getKeyChar() != '1' && 
                    ke.getKeyChar() != '2' && 
                    ke.getKeyChar() != '3' && 
                    ke.getKeyChar() != '4' && 
                    ke.getKeyChar() != '5' && 
                    ke.getKeyChar() != '6' && 
                    ke.getKeyChar() != '7' && 
                    ke.getKeyChar() != '8' && 
                    ke.getKeyChar() != '9' &&
                    ke.getKeyChar() != 'A' && 
                    ke.getKeyChar() != 'B' &&
                    ke.getKeyChar() != 'C' && 
                    ke.getKeyChar() != 'D' &&
                    ke.getKeyChar() != 'E' && 
                    ke.getKeyChar() != 'F' && 
                    ke.getKeyChar() != 'L' && 
                    ke.getKeyChar() != 'M' && 
                    ke.getKeyChar() != 'W' && 
                    ke.getKeyChar() != 'X' &&
                    ke.getKeyChar() != 'U' &&    
                    ke.getKeyChar() != 'Z' &&    
                    ke.getKeyChar() != 'a' && 
                    ke.getKeyChar() != 'b' && 
                    ke.getKeyChar() != 'c' && 
                    ke.getKeyChar() != 'd' && 
                    ke.getKeyChar() != 'e' && 
                    ke.getKeyChar() != 'f' && 
                    ke.getKeyChar() != 'l' && 
                    ke.getKeyChar() != 'm' &&
                    ke.getKeyChar() != 'w' && 
                    ke.getKeyChar() != 'x' && 
                    ke.getKeyChar() != 'u' && 
                    ke.getKeyChar() != 'z'
                    ) {
                    ke.consume();
                } else {
                    noParte[x] = (char) ke.getKeyChar();                    
                    if ( x > 2){  
                        if (noParte[x] == noParte[x-3] && noParte[x] == noParte[x-2] ){
                            ke.consume();
                        }
                        if (num[x] == num[x-1]+1 && num[x-1]-1 == num[x-2]+2   ){
                            ke.consume();
                        }
                    }
                }
            }
        } else
        if (winPrincipal.getTxtTiempoInicio().equals(ke.getSource())) {
            if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                ke.consume();
            }
            if (winPrincipal.getTxtTiempoInicio().getText().length() >= 2) {
                ke.consume();
            }
            if (winPrincipal.getTxtTiempoInicio().getText().isEmpty()) {
                winPrincipal.getTxtTiempoFin().setText("");
                winPrincipal.getTxtTiempoFin().setEnabled(false);
            }
        } else //JTextField Tiempo Fin
        if (winPrincipal.getTxtTiempoFin().equals(ke.getSource())) {
            if ((ke.getKeyChar() < '0' || ke.getKeyChar() > '9')) {
                ke.consume();
            }
            if (winPrincipal.getTxtTiempoFin().getText().length() >= 2) {
                ke.consume();
            }
            if (winPrincipal.getTxtDuracion().getText().isEmpty()) {
                winPrincipal.getTxtDuracion().setText("");
                winPrincipal.getTxtDuracion().setEnabled(false);
            }
        }
    }
    
    public static void limpiarTiemposIncidencia(Principal winPrincipal) {
        winPrincipal.getCmbHora().setSelectedIndex(0);
        winPrincipal.getCmbHora().setEnabled(false);
        winPrincipal.getTxtTiempoInicio().setEnabled(false);
        winPrincipal.getBtnParoPeriodo().setVisible(false);
    }
  
    public static void validarTableModelListener(Principal winPrincipal, TableModelEvent e) {
        if (e.getType() == TableModelEvent.DELETE
                && winPrincipal.getTblBitacora().getRowCount() == 0) {
            winPrincipal.getBtnRevisarHoras().setEnabled(false);
            winPrincipal.getBtnGuardar().setVisible(false);
        } else {
            winPrincipal.getBtnRevisarHoras().setEnabled(true);
            winPrincipal.getBtnGuardar().setVisible(true);
        }
    }
}