package utils;

import control.PrincipalControl;
import dao.UsuariosDAOImpl;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import vista.Principal;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class LoginMetodos {

    private JFrame form;
    Principal winPrincipal = new Principal();
    
    public boolean cambiarLinea(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());  
            //System.err.println(" p: "+privilegio);
            if (privilegio > 1) {
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase(); 
                respuesta = true; 
            } else if (privilegio < 1) { 
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaSupervisor(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            if (privilegio == 3) {
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();
                //winPrincipal.getLblUsuario().setText(usuario.getText());
                //winPrincipal.getBtnCerrarSesion().setVisible(true);
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaTeamLider(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        String area;
        
        try {
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            if (privilegio == 2) {
                respuesta = true;                
                
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                PrincipalControl.privilegio = privilegio; 
                PrincipalControl.usuario = usuario.getText().toUpperCase(); 
            }else if (privilegio < 1) { 
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaAjustador(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            //winPrincipal.getLblUsuario().setText(usuario.getText());
            if (privilegio == 2) {
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();                
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaTeamYSupervisor(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            //winPrincipal.getLblUsuario().setText(usuario.getText());
            if (privilegio > 1) {
                
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();
                
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaJProcesos(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            
            System.out.println("utils.LoginMetodos.validaJProcesos(): "+usuario+" pass:"+contrasena.getText());
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            //winPrincipal.getLblUsuario().setText(usuario.getText());
            if (privilegio == 4) {
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();
                
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    
    public boolean validaJManufactura (JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            
            System.out.println("utils.LoginMetodos.validaJManufactura(): "+usuario+" pass:"+contrasena.getText());
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            
            //winPrincipal.getLblUsuario().setText(usuario.getText());
            if (privilegio == 5) {
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();
                
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public boolean validaFull(JTextField usuario, JPasswordField contrasena) {
        boolean respuesta = false;
        int privilegio;
        try {
            
            privilegio = new UsuariosDAOImpl().nivelUsuario(usuario.getText(), contrasena.getText());
            if (privilegio > 0) {
                PrincipalControl.area = new UsuariosDAOImpl().areaUsuario(usuario.getText()); 
                PrincipalControl.privilegio = privilegio;
                PrincipalControl.usuario = usuario.getText().toUpperCase();
                
                respuesta = true;
            } else if (privilegio < 1) {
                JOptionPane.showMessageDialog(form, "Usuario inválido\nVerifique Usuario y Contraseña", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(form, "Usuario inválido:\nProbablemente no se tienen Permisos para esta acción", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(form, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }    
}
