package control;

import static control.PrincipalControl.winPrincipal;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import utils.LoginMetodos;
import vista.Login;
import vista.TiempoTurno;

/*
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */

public class LoginControl implements ActionListener, KeyListener {
    public static int auxiliarLogin = 0;
    public static int privilegio = 0;
    
    private final Login winLogin;
    private LoginMetodos loginMetodos = new LoginMetodos();
        
    public LoginControl(Login login) {
        this.winLogin = login;
        initFrame();
    }
    
    private void initFrame() {
        winLogin.getTxtUsuario().addKeyListener(this);
        winLogin.getPwdContrasena().addKeyListener(this);
        winLogin.getBtnAceptar().addActionListener(this);
        winLogin.getBtnCancelar().addActionListener(this);        
        winLogin.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent evt) {
        auxiliarLogin = 0;
        switch (evt.getActionCommand()) {            
            case "_btnAceptar": // validaciones para aceptar de Login    
                validacionAceptarLogin();                
                break;
            case "_btnCancelar":
                if (winPrincipal.getCmbLinea().getSelectedIndex() != 0) {
                    winPrincipal.getCmbTema().setEnabled(true);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true);
                break;
        }
    }  
    
    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 10 && !winLogin.getTxtUsuario().getText().isEmpty()) {
            winLogin.getTxtUsuario().transferFocus();
        }
        if (e.getKeyCode() == 10 && !winLogin.getPwdContrasena().getText().isEmpty()) {
            //winLogin.getBtnAceptar().transferFocus();   
            validacionAceptarLogin();                      
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}    
    
    public void validacionAceptarLogin(){      
        winLogin.setCursor(new Cursor(Cursor.WAIT_CURSOR)); //cambia cursor
        winLogin.getTxtUsuario().setEnabled(false);
        winLogin.getPwdContrasena().setEnabled(false);
      
        switch (PrincipalControl.auxiliarPrincipal) {
            case 1:
                if (loginMetodos.cambiarLinea(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {
                    PrincipalControl.winPrincipal.getBtnCambiarLinea().setText("Aceptar");
                    PrincipalControl.winPrincipal.getCmbLinea().setEnabled(true);
                    winLogin.dispose();
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                } 
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); 
                winLogin.getTxtUsuario().setEnabled(true); 
                winLogin.getPwdContrasena().setEnabled(true); 
                break; 
            case 2: //SUPERVISOR Y TEAM LIDER 
                if (loginMetodos.validaTeamYSupervisor(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {
                    auxiliarLogin = 1; 
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario); 
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); 
                    winLogin.getTxtUsuario().setEnabled(true); 
                    winLogin.getPwdContrasena().setEnabled(true); 
                    winLogin.dispose(); 
                } 
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); 
                winLogin.getTxtUsuario().setEnabled(true); 
                winLogin.getPwdContrasena().setEnabled(true); 
                break; 
            case 3:// AJUSTADOR
                if (loginMetodos.validaAjustador(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {
                    auxiliarLogin = 1;
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                    winLogin.dispose();
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true);
                break; 
                
            case 4://TEAM LIDER
                if (loginMetodos.validaTeamLider(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {
                    auxiliarLogin = 1;
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                    winLogin.dispose();
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true);
                break; 
                
            case 5://SUPERVISOR
                if (loginMetodos.validaSupervisor(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {                    
                    auxiliarLogin = 1;
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                    winLogin.dispose();
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true); 
                break; 
                
            case 6://TODOS LOS USUARIOS REGISTRADOS 
                if (loginMetodos.validaFull(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {                    
                    auxiliarLogin = 1;
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                    winLogin.dispose();
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true);
                break;
                
            case 7://TODOS LOS USUARIOS REGISTRADOS 
                if (loginMetodos.validaFull(winLogin.getTxtUsuario(), winLogin.getPwdContrasena())) {                    
                    auxiliarLogin = 1;
                    winPrincipal.getLblUsuario().setText(PrincipalControl.usuario);
                    winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    winLogin.getTxtUsuario().setEnabled(true);
                    winLogin.getPwdContrasena().setEnabled(true);
                    winLogin.dispose();
                }
                winLogin.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                winLogin.getTxtUsuario().setEnabled(true);
                winLogin.getPwdContrasena().setEnabled(true);
                break;
        }
        winLogin.getTxtUsuario().transferFocus();        
    }    
}
