package control;

import dao.ActivityLog;
import dao.BitacoraDAOImpl;
import dao.TemasDAOImpl;
import java.awt.Color;
import java.awt.Desktop;
import vista.Principal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import utils.PrincipalMetodos;
import utils.PrincipalValidaciones;
import vista.AboutKPI;
import vista.AdministracionUsuarios;
import vista.Cargas;
import vista.EdicionRangoTurno;
import vista.Lineas;
import vista.ListaNumerosParte;
import vista.Login; 
import vista.Operaciones;
import vista.ParoRango;
import vista.SelecTurno;
import vista.TiempoTurno;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class PrincipalControl implements ActionListener, CaretListener, ItemListener, KeyListener, TableModelListener {
    
    public static int insercionesAccess = 0;
    public static String fecha;
    public static int auxiliarPrincipal;
    private final SimpleDateFormat fechaFormato = new SimpleDateFormat("dd/MM/yyyy");
    protected static Principal winPrincipal;
    private final PrincipalMetodos principalMetodos = new PrincipalMetodos();
    public static String linea;
    protected static Operaciones winOperaciones;
    public static int bnEdicion = 0;
    public static int bnEntrada;
    public static int tipoEnsambleLinea; //3. ENSAMBLE FINAL 
    
    //VARIABLES PARA DATOS DE USUARIO
    public static int privilegio = 0;
    public static String usuario = "";
    public static String area = "";
    
    //VARIABLE PARA LOS ADD
    public static int contAdd = 0;
    
    public PrincipalControl(Principal principal) {
        PrincipalControl.winPrincipal = principal;
        initFrame();  
        
        //IMPORTAMOS EL LENGUAJE
        lenguaje();        
    }
    
    private void lenguaje(){
        //MENU SUPERIOR
        winPrincipal.getMniSesion().setText("IniciarSesion");
        
        //PRIMER PANEL
        winPrincipal.getLblCantPzas().setText("CantPzas");
        winPrincipal.getLblOperacion1().setText("Operacion");
        winPrincipal.getLblArea1().setText("Area");
        winPrincipal.getLblProblema1().setText("Problema");
        winPrincipal.getLblScrap1().setText("Scrap");
        winPrincipal.getLblCliente1().setText("NuevoCliente");
        winPrincipal.getLblNoParte1().setText("NoParte");
        winPrincipal.getLblMatFaltante1().setText("MaterialFaltante");
        
        //SEGUNDO PANEL
        winPrincipal.getLblOperacion2().setText("Operacion");
        winPrincipal.getLblArea2().setText("Area");
        winPrincipal.getLblProblema2().setText("Problema");
        winPrincipal.getLblScrap2().setText("Scrap");
        winPrincipal.getLblCliente2().setText("NuevoCliente");
        winPrincipal.getLblNoParte2().setText("NoParte");
        winPrincipal.getLblMatFaltante2().setText("MaterialFaltante");
        
        //TERCER PANEL
        winPrincipal.getLblOperacion3().setText("Operacion");
        winPrincipal.getLblArea3().setText("Area");
        winPrincipal.getLblProblema3().setText("Problema");
        winPrincipal.getLblScrap3().setText("Scrap");
        winPrincipal.getLblCliente3().setText("NuevoCliente");
        winPrincipal.getLblNoParte3().setText("NoParte");
        winPrincipal.getLblMatFaltante3().setText("MaterialFaltante");
        
        //CUARTO PANEL
        winPrincipal.getLblOperacion4().setText("Operacion");
        winPrincipal.getLblArea4().setText("Area");
        winPrincipal.getLblProblema4().setText("Problema");
        winPrincipal.getLblScrap4().setText("Scrap");
        winPrincipal.getLblCliente4().setText("NuevoCliente");
        winPrincipal.getLblNoParte4().setText("NoParte");
        winPrincipal.getLblMatFaltante4().setText("MaterialFaltante");
        
        //SESION
        //winPrincipal.getMniSesion().setText("IniciarSesion");
        //winPrincipal.getMniSesion().setText("CerrarSesion");        
    }
    
    private void initFrame() {        
        winPrincipal.getMniAdminU().setVisible(false);
        
        winPrincipal.getMniAdminU().addActionListener(this);        
        winPrincipal.getCmbCliente().addActionListener(this);
        winPrincipal.getCmbNoParte().addActionListener(this);   
        
        winPrincipal.getCmbCliente().addItemListener(this);
        winPrincipal.getCmbNoParte().addItemListener(this);   
        winPrincipal.getTxtTcP().addActionListener(this);   
        
        winPrincipal.getDteFecha().setDateFormat(fechaFormato);
        winPrincipal.getDteFecha().setDate(new Date(System.currentTimeMillis()));
        winPrincipal.getDteFecha().addActionListener(this);
        winPrincipal.getCmbLinea().addActionListener(this);
        winPrincipal.getCmbLinea().addItemListener(this);
        winPrincipal.getCmbTema().setModel(TemasDAOImpl.listaTema());
        winPrincipal.getCmbTema().addActionListener(this); 
        winPrincipal.getCmbTema().addItemListener(this);
        
        winPrincipal.getCmbHora().addActionListener(this);
        winPrincipal.getCmbHora().addItemListener(this);
        winPrincipal.getTxtTiempoInicio().addCaretListener(this);
        winPrincipal.getTxtTiempoInicio().addKeyListener(this);
        winPrincipal.getTxtTiempoFin().addCaretListener(this);
        winPrincipal.getTxtTiempoFin().addKeyListener(this);
        winPrincipal.getTxtDuracion().addCaretListener(this);
              
        winPrincipal.getBtnCambiarLinea().addActionListener(this);
        
        winPrincipal.getBtnAgregarBitacora().addActionListener(this);
        winPrincipal.getBtnRevisarHoras().addActionListener(this);
        winPrincipal.getBtnGuardar().addActionListener(this);
        winPrincipal.getBtnGuardar().setVisible(false);
        winPrincipal.getBtnCancelar().addActionListener(this);
        winPrincipal.getBtnCancelar().setVisible(false);
        
        //Panel Piezas Producias
        winPrincipal.getTxtTcP().addCaretListener(this);
        winPrincipal.getTxtTcP().addKeyListener(this);       
       
        //Menu EditarBitacora
        winPrincipal.getMniListarDia().addActionListener(this);
        winPrincipal.getMniEditarPorDia().addActionListener(this);
        winPrincipal.getMniEditarPorTurno().addActionListener(this);
        winPrincipal.getMniERangoTurno().addActionListener(this);  
        
        //MENU ADMINISTRACION
        winPrincipal.getMniBTS().addActionListener(this);
        winPrincipal.getMniCargaMasiva().addActionListener(this);
        winPrincipal.getMniOperaciones().addActionListener(this);
        winPrincipal.getMniLineas().addActionListener(this);
        winPrincipal.getMniNewNoParte().addActionListener(this);
        //winPrincipal.getMniTarget().addActionListener(this);
        winPrincipal.getMniCrearUsuario().addActionListener(this);
        winPrincipal.getMniManuales().addActionListener(this);
        winPrincipal.getMniAbout().addActionListener(this);
        
        //MenuEliminarBitacora
        winPrincipal.getMniEliminar().addActionListener(this);  
        
        //Menu Graficas
        winPrincipal.getMniHourlyCount().addActionListener(this);
        winPrincipal.getMniIndicadores().addActionListener(this);
        
        //Tabla Bitacora
        winPrincipal.getTblBitacora().getModel().addTableModelListener(this);
        winPrincipal.getTblBitacora().setRowHeight(35);   
        
        //Boton Paro por Rangos 
        winPrincipal.getBtnParoPeriodo().addActionListener(this);
        winPrincipal.getBtnParoPeriodo().setVisible(false);        
        winPrincipal.setVisible(true);     
        
        //ACTIONS PARA EL PANEL GENERAL
        winPrincipal.getTxtCantPzas().addCaretListener(this);
        winPrincipal.getTxtCantPzas().addKeyListener(this);
        
        winPrincipal.getCmbOperacion1().addActionListener(this);
        winPrincipal.getCmbOperacion1().addItemListener(this);
        
        winPrincipal.getCmbArea1().addActionListener(this);
        winPrincipal.getCmbArea1().addItemListener(this);
        
        winPrincipal.getCmbProblema1().addActionListener(this);
        winPrincipal.getCmbProblema1().addItemListener(this);
        
        winPrincipal.getCmbCliente1().addActionListener(this);
        winPrincipal.getCmbCliente1().addItemListener(this);      
        
        winPrincipal.getCmbNoParte1().addActionListener(this);
        winPrincipal.getCmbNoParte1().addItemListener(this);
        
        winPrincipal.getTxtMatFaltante1().addCaretListener(this);
        winPrincipal.getTxtMatFaltante1().addKeyListener(this);
        
        winPrincipal.getTxtScrap1().addCaretListener(this);
        winPrincipal.getTxtScrap1().addKeyListener(this);
        
        winPrincipal.getBtnAdd().addActionListener(this);
        winPrincipal.getBtnRemove().addActionListener(this);
        
        //PANEL 2
        winPrincipal.getCmbTema2().addActionListener(this);
        winPrincipal.getCmbTema2().addItemListener(this);
        
        winPrincipal.getCmbOperacion2().addActionListener(this);
        winPrincipal.getCmbOperacion2().addItemListener(this);
        
        winPrincipal.getCmbArea2().addActionListener(this);
        winPrincipal.getCmbArea2().addItemListener(this);
        
        winPrincipal.getCmbProblema2().addActionListener(this);
        winPrincipal.getCmbProblema2().addItemListener(this);
        
        winPrincipal.getCmbCliente2().addActionListener(this);
        winPrincipal.getCmbCliente2().addItemListener(this);      
        
        winPrincipal.getCmbNoParte2().addActionListener(this);
        winPrincipal.getCmbNoParte2().addItemListener(this);
        
        winPrincipal.getTxtMatFaltante2().addCaretListener(this);
        winPrincipal.getTxtMatFaltante2().addKeyListener(this);
        
        winPrincipal.getTxtScrap2().addCaretListener(this);
        winPrincipal.getTxtScrap2().addKeyListener(this);
        
        //PANEL 3
        winPrincipal.getCmbTema3().addActionListener(this);
        winPrincipal.getCmbTema3().addItemListener(this);
        
        winPrincipal.getCmbOperacion3().addActionListener(this);
        winPrincipal.getCmbOperacion3().addItemListener(this);
        
        winPrincipal.getCmbArea3().addActionListener(this);
        winPrincipal.getCmbArea3().addItemListener(this);
        
        winPrincipal.getCmbProblema3().addActionListener(this);
        winPrincipal.getCmbProblema3().addItemListener(this);
        
        winPrincipal.getCmbCliente3().addActionListener(this);
        winPrincipal.getCmbCliente3().addItemListener(this);      
        
        winPrincipal.getCmbNoParte3().addActionListener(this);
        winPrincipal.getCmbNoParte3().addItemListener(this);
        
        winPrincipal.getTxtMatFaltante3().addCaretListener(this);
        winPrincipal.getTxtMatFaltante3().addKeyListener(this);
        
        winPrincipal.getTxtScrap3().addCaretListener(this);
        winPrincipal.getTxtScrap3().addKeyListener(this);
                
        //PANEL 4
        winPrincipal.getCmbTema4().addActionListener(this);
        winPrincipal.getCmbTema4().addItemListener(this);
        
        winPrincipal.getCmbOperacion4().addActionListener(this);
        winPrincipal.getCmbOperacion4().addItemListener(this);
        
        winPrincipal.getCmbArea4().addActionListener(this);
        winPrincipal.getCmbArea4().addItemListener(this);
        
        winPrincipal.getCmbProblema4().addActionListener(this);
        winPrincipal.getCmbProblema4().addItemListener(this);
        
        winPrincipal.getCmbCliente4().addActionListener(this);
        winPrincipal.getCmbCliente4().addItemListener(this);      
        
        winPrincipal.getCmbNoParte4().addActionListener(this);
        winPrincipal.getCmbNoParte4().addItemListener(this);
        
        winPrincipal.getTxtMatFaltante4().addCaretListener(this);
        winPrincipal.getTxtMatFaltante4().addKeyListener(this);
        
        winPrincipal.getTxtScrap4().addCaretListener(this);
        winPrincipal.getTxtScrap4().addKeyListener(this);
        
        //SESION
        winPrincipal.getMniSesion().addActionListener(this);  
        winPrincipal.getMnuSesion().addActionListener(this);         
        
        //DURACION
        winPrincipal.getTxtDur2().addCaretListener(this);
        winPrincipal.getTxtDur2().addKeyListener(this);  
        winPrincipal.getTxtDur3().addCaretListener(this);
        winPrincipal.getTxtDur3().addKeyListener(this);  
        winPrincipal.getTxtDur4().addCaretListener(this);
        winPrincipal.getTxtDur4().addKeyListener(this);         
    }   
    
    @Override
    public void actionPerformed(ActionEvent evt) {
        LoginControl.auxiliarLogin = 0;        
        //System.out.println("control.PrincipalControl.initFrame().PRIVILEGIO: "+privilegio);
        if ( privilegio > 0){                    
            winPrincipal.getMniSesion().setText("CerrarSesion"); 
            //winPrincipal.getMniAdminU().setVisible(false);
        } else {
            winPrincipal.getMniSesion().setText("IniciarSesion");
            //winPrincipal.getMniAdminU().setVisible(true);
        } 
        
        switch (evt.getActionCommand()) {
            //INICIO DE SESION
            case "_mniSesion": 
                //System.out.println("control.PrincipalControl.actionPerformed() PRIVILEGIO: "+privilegio);
                if (privilegio > 0){
                    String[] options2 = {"Si", "No"};            
                    int seleccion = JOptionPane.showOptionDialog(null,"¿Desea cerrar su sesion en la bitacora? ", "Peticion", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options2, options2[1]);
            
                    if (seleccion == 0) {
                        usuario = "";
                        privilegio = 0;
                        winPrincipal.getLblUsuario().setText(usuario);
                        winPrincipal.getMniSesion().setText("IniciarSesion");
                    }
                } else {                    
                    PrincipalControl.auxiliarPrincipal = 6;
                    new LoginControl(new Login(winPrincipal, true)); 
                    if (privilegio != 0) {
                        winPrincipal.getMniSesion().setText("CerrarSesion");
                    } else {
                        winPrincipal.getMniSesion().setText("IniciarSesion");
                    }                
                }                
                break;
                
            case "_btnAceptarNoParte":
                winPrincipal.getCmbCliente().setEnabled(false);
                winPrincipal.getCmbNoParte().setEnabled(false);
                break;
            
            //***** Barra de Menu *****
            case "_mniCargaMasiva":
                try {
                    if (privilegio != 0){
                        if(privilegio > 2 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                            new Cargas(winPrincipal, true).setVisible(true); 
                        } else {
                            JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                        }
                    } else {
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    }
                } catch (IOException ex) {
                        Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                break;
                
            case "_mniLineas":
                //QUITAR LOGIN
                if(privilegio > 2 ) { 
                        new Lineas(winPrincipal, true).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                }
                    //break;
                //}
                break;
                
            case "_mniNewNoParte":
                if (privilegio != 0){
                    if(privilegio > 1 ) { //SI ES MAYOR A AJUSTADOR ENTONCES LA ABRE
                        new ListaNumerosParte(winPrincipal, true).setVisible(true);      
                    } else {
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    }
                } else {
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                }
                break; 
                
            case "_mniListarDia":
                bnEdicion = 0;
                if (winPrincipal.getTblBitacora().getRowCount() != 0) {
                    JOptionPane.showMessageDialog(winPrincipal, "Se tienen registros en la bitacora sin guardar. \n ¿Deseas continuar?");
                } else {
                    winPrincipal.getDteFecha().setEnabled(true);
                    winPrincipal.getBtnCancelar().setVisible(true);
                    /*
                        winPrincipal.getTblBitacora().setEnabled(false);
                    */
                    winPrincipal.getMniEliminar().setVisible(false);
                }
                break;
                
            case "_mniEditarPorDia":
                if (privilegio != 0){
                    if(privilegio >= 2 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                        bnEdicion = 2;
                        winPrincipal.getBtnCambiarLinea().setEnabled(true);
                        winPrincipal.getDteFecha().setEnabled(true);
                        winPrincipal.getBtnCancelar().setVisible(true);      
                    } else {
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    }
                } else {
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");            
                }
                winPrincipal.getMniEliminar().setVisible(true);
                break;
                
            case "_mniEditarPorTurno":                 
                if (privilegio != 0) { 
                    if(privilegio >= 2 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                        bnEdicion = 2;
                        winPrincipal.getDteFecha().setEnabled(true);
                        winPrincipal.getBtnCancelar().setVisible(true);
                        winPrincipal.getDteFecha().setEditable(true);
                    } else { 
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    } 
                } else { 
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                } 
                break; 
                
            case "_mniERangoTurno":
                if (privilegio != 0) { 
                    if(privilegio >= 2 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                        new EdicionRangoTurno(winPrincipal,true).setVisible(true); 
                        winPrincipal.getDteFecha().setEditable(true); 
                    } else { 
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    } 
                } else { 
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                } 
                break; 
                
            case "_mniEliminar": 
                linea = winPrincipal.getCmbLinea().getSelectedItem().toString(); 
                fecha = winPrincipal.getDteFecha().getText(); 
                principalMetodos.eliminarRegistroBitacora(winPrincipal); 
                new PrincipalMetodos().actualizaOEEBitacoraDia(fecha, linea); 
                break; 
                
            case "_mniCrearUsuario": 
                if (privilegio != 0){ 
                    if(privilegio > 2 ) { 
                        try { 
                            //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                            new AdministracionUsuarios(winPrincipal, true).setVisible(true);
                        } catch (Exception ex) { 
                            Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                    } else { 
                        JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                    } 
                } else { 
                    JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                } 
                break; 
                
            case "_mniHourlyCount": 
                linea = winPrincipal.getCmbLinea().getSelectedItem().toString(); 
                fecha = winPrincipal.getDteFecha().getText(); 
                URL url = null; 
                try { 
                    //url = new URL("http://localhost/SEG_SMC_MX/vLive/db/sesionHourly.php?line="+linea+"&f="+fecha+"&u="+usuario+"&p="+privilegio);
                    url = new URL("http://sglersm01:8080/SMC/db/sesionHourly.php?line="+linea+"&f="+fecha+"&u="+usuario+"&p="+privilegio);
                    try { 
                        Desktop.getDesktop().browse(url.toURI()); 
                    } catch (IOException e) { 
                        e.printStackTrace(); 
                    } catch (URISyntaxException e) { 
                        e.printStackTrace(); 
                    } 
                } catch (MalformedURLException e1) { 
                    e1.printStackTrace(); 
                } 
                break; 
            case "_mniBTS": 
                //System.out.println("control.PrincipalControl.actionPerformed().BTS");
                //new BTS1(winPrincipal, true).setVisible(true);
                break; 
            case "_mniAbout": 
                //System.out.println("control.PrincipalControl.actionPerformed().ABOUT"); 
                new AboutKPI(winPrincipal, true).setVisible(true); 
                break; 
            case "_mniIndicadores": 
                linea = winPrincipal.getCmbLinea().getSelectedItem().toString(); 
                fecha = winPrincipal.getDteFecha().getText(); 
                try { 
                    //generarReporteProduccionActualOEE 
                    //url = new URL("http://localhost/SEG_SMC_MX/vLive/db/sesionReportes.php?linea="+linea+"&f="+fecha+"&u="+usuario+"&p="+privilegio);
                    url = new URL("http://sglersm01:8080/SMC/db/sesionReportes.php?linea="+linea+"&f="+fecha+"&u="+usuario+"&p="+privilegio);
                    try { 
                        Desktop.getDesktop().browse(url.toURI()); 
                    } catch (IOException e) { 
                        e.printStackTrace(); 
                    } catch (URISyntaxException e) { 
                        e.printStackTrace(); 
                    } 
                } catch (MalformedURLException e1) { 
                    e1.printStackTrace(); 
                } 
                break; 
                
            //***** Panel Superior *****
            case "_btnCambiarLinea":                
                winPrincipal.getLblTurno().setForeground(Color.black);
                
                if (privilegio != 0){
                    principalMetodos.cancelarEdicion(winPrincipal);
                    new TiempoTurno(winPrincipal, true).setVisible(true);
                } else {
                     JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                }
                
            break;
            
            case "_cmbTema":                                 
                contAdd = 0; 
                PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);                
                if ( winPrincipal.getPnlProduccionCollapsible().getSize().height <= 26){
                    winPrincipal.getPnlProduccionCollapsible().repaint();  
                    winPrincipal.getPnlProduccionCollapsible().setContent(winPrincipal.getPnlGeneral());
                } 
                switch (winPrincipal.getCmbTema().getSelectedIndex()) { 
                    case 0: 
                        winPrincipal.getCmbCliente().setEnabled(false); 
                        winPrincipal.getCmbNoParte().setEnabled(false); 
                        winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel());
                        winPrincipal.getTxtTiempoInicio().setEnabled(false); 
                        break; 
                    case 1: 
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                            //winPrincipal.getTxtCantPzas().requestFocus();//transferFocus(); 
                            winPrincipal.getCmbTema().setNextFocusableComponent(winPrincipal.getTxtCantPzas());
                            winPrincipal.getCmbCliente().setEnabled(false); 
                            winPrincipal.getCmbNoParte().setEnabled(false); 
                            principalMetodos.panelPiezasProducidas(winPrincipal);                                                          
                        } else { 
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    case 2:
                    case 3:
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                            //winPrincipal.getCmbOperacion1().requestFocus(); 
                            winPrincipal.getCmbTema().setNextFocusableComponent(winPrincipal.getCmbOperacion1());
                            winPrincipal.getCmbCliente().setEnabled(false);
                            winPrincipal.getCmbNoParte().setEnabled(false);
                            principalMetodos.panelCalidad1(winPrincipal);
                        } else { 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    case 4:
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                            //winPrincipal.getCmbArea1().requestFocus();
                            winPrincipal.getCmbTema().setNextFocusableComponent(winPrincipal.getCmbArea1());
                            winPrincipal.getCmbCliente().setEnabled(false);
                            winPrincipal.getCmbNoParte().setEnabled(false);
                            principalMetodos.panelOrganizacionales(winPrincipal);
                        } else { 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    case 5:
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                            //winPrincipal.getCmbArea1().requestFocus(); 
                            winPrincipal.getCmbTema().setNextFocusableComponent(winPrincipal.getCmbArea1());
                            principalMetodos.panelCambios(winPrincipal);
                        } else { 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    case 6:
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty() ) {
                            //winPrincipal.getCmbArea1().requestFocus(); 
                            winPrincipal.getCmbTema().setNextFocusableComponent(winPrincipal.getCmbArea1());
                            winPrincipal.getCmbCliente().setEnabled(false);
                            winPrincipal.getCmbNoParte().setEnabled(false);   
                            principalMetodos.panelPlaneados(winPrincipal);
                        } else { 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    case 7:
                        if (winPrincipal.getCmbNoParte().getSelectedIndex() > 0 
                        && winPrincipal.getCmbCliente().getSelectedIndex() > 0
                        && winPrincipal.getTxtTcP().getText().length() > 0 && !winPrincipal.getTxtTcP().getText().isEmpty()) {
                            //winPrincipal.getCmbArea1().requestFocus(); 
                            winPrincipal.getCmbHora().setEnabled(true);
                            
                            winPrincipal.getCmbCliente().setEnabled(false);
                            winPrincipal.getCmbNoParte().setEnabled(false);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel());
                        } else { 
                            winPrincipal.getCmbCliente().setEnabled(true);
                            winPrincipal.getCmbNoParte().setEnabled(true);
                            winPrincipal.getPnlProduccionCollapsible().setContent(new javax.swing.JLabel()); 
                            JOptionPane.showMessageDialog(null, "PARA INICIAR DEBES SELECCIONAR CLIENTE Y NUMERO DE PARTE");
                        }
                        break;
                    default:                        
                        PrincipalValidaciones.limpiarTiemposIncidencia(winPrincipal);
                        break;
                }
                break;  
            //SEGUNDO PANEL    
            case "_cmbTema2":               
                switch (winPrincipal.getCmbTema2().getSelectedIndex()) {                    
                    case 1:   //CALIDAD
                    case 2:   //TECNICAS
                        principalMetodos.panelCalidad2(winPrincipal);                        
                        break;
                    case 3: //ORGANIZACIONALES                       
                        principalMetodos.panelOrganizacionales2(winPrincipal);
                        break;
                    case 4: //CAMBIOS DE MODELO
                        principalMetodos.panelCambios2(winPrincipal);
                        break;                   
                    default:
                        principalMetodos.panel2(winPrincipal);
                        break;
                }
                break;
                
            case "_cmbTema3":               
                switch (winPrincipal.getCmbTema3().getSelectedIndex()) {                    
                    case 1:   //CALIDAD
                    case 2:   //TECNICAS
                        principalMetodos.panelCalidad3(winPrincipal);                        
                        break;
                    case 3: //ORGANIZACIONALES                       
                        principalMetodos.panelOrganizacionales3(winPrincipal);
                        break;
                    case 4: //CAMBIOS DE MODELO
                        principalMetodos.panelCambios3(winPrincipal);
                        break;                   
                    default:
                        principalMetodos.panel3(winPrincipal);
                        break;
                }
                break;
                
            case "_cmbTema4": 
                switch (winPrincipal.getCmbTema4().getSelectedIndex()) { 
                    case 1:   //CALIDAD 
                    case 2:   //TECNICAS 
                        principalMetodos.panelCalidad4(winPrincipal); 
                        break; 
                    case 3: //ORGANIZACIONALES 
                        principalMetodos.panelOrganizacionales4(winPrincipal); 
                        break; 
                    case 4: //CAMBIOS DE MODELO 
                        principalMetodos.panelCambios4(winPrincipal); 
                        break; 
                    default: 
                        principalMetodos.panel4(winPrincipal); 
                        break; 
                }
                break;
                
            case "_cmbNoPartePlaneados":
                winPrincipal.getCmbHora().setSelectedIndex(0);
                winPrincipal.getCmbHora().setEnabled(true);
                break;
                
            //***** Panel Tiempo de Incidencia *****
            case "_cmbHora":
                winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                if (winPrincipal.getCmbHora().getSelectedIndex() != 0) {    
                    //System.out.println("control.PrincipalControl.actionPerformed() *xD: "+winPrincipal.getCmbTema().getSelectedIndex());
                    if(winPrincipal.getCmbTema().getSelectedIndex() != 2 ){
                        winPrincipal.getBtnParoPeriodo().setVisible(false);
                        winPrincipal.getTxtTiempoInicio().setEnabled(true);
                        winPrincipal.getBtnAgregarBitacora().setEnabled(false);
                    } else {                        
                        winPrincipal.getBtnParoPeriodo().setVisible(false);
                        winPrincipal.getTxtTiempoInicio().setEnabled(false);
                        winPrincipal.getBtnAgregarBitacora().setEnabled(true);                        
                    }                     
                }else if (winPrincipal.getCmbTema().getSelectedIndex() == 3 ||
                        winPrincipal.getCmbTema().getSelectedIndex() == 4 || 
                        winPrincipal.getCmbTema().getSelectedIndex() == 6 ){
                        winPrincipal.getBtnParoPeriodo().setVisible(true);
                }
                break; 
            case "_btnAgregarBitacora":
                bnEntrada = 1;
                principalMetodos.agregarRegistroBitacora(winPrincipal);                         
                break;
                
            case "_btnRevisarHoras":
                //System.out.println("control.PrincipalControl.actionPerformed().btnEdicion: "+bnEdicion);
                if (bnEdicion != 0) { 
                    principalMetodos.revisarTiemposFaltantes(winPrincipal, 1); 
                } else { 
                    principalMetodos.revisarTiemposFaltantes(winPrincipal, 3); 
                } 
                break; 
                
            case "_btnAdd": 
                contAdd ++; 
                //System.out.println("control.PrincipalControl.actionPerformed(): contAdd: "+contAdd);
                switch (contAdd){ 
                    case 1: 
                        //winPrincipal.getCmbTema().setEnabled(false); 
                        principalMetodos.panel2(winPrincipal); 
                        break; 
                    case 2: 
                        principalMetodos.panel3(winPrincipal); 
                        break; 
                    case 3: 
                        principalMetodos.panel4(winPrincipal); 
                        break; 
                } 
                break; 
                
            case "_btnRemove": 
                System.out.println("control.PrincipalControl.actionPerformed(): contRemove: "+contAdd); 
                switch (contAdd){ 
                    case 1: 
                        principalMetodos.panel2R(winPrincipal); 
                        break; 
                    case 2: 
                        principalMetodos.panel3R(winPrincipal); 
                        break; 
                    case 3: 
                        principalMetodos.panel4R(winPrincipal); 
                        break; 
                    default: 
                        contAdd = 0; 
                        break; 
                } 
                break; 
                
            case "_btnGuardar": 
                Object[] regActividad = new Object[6];
                
                //CORTE DE FECHA DE COMPONENTE                
                String a = fecha.substring(6,10);
                String d = fecha.substring(0,2);
                String m = fecha.substring(3,5);
                
                String fechaRegAct =a+"-"+m+"-"+d;
                
                switch (winPrincipal.getBtnGuardar().getText()) {
                    case "Guardar Bitacora":
                        if (privilegio != 0){
                            if(privilegio >= 2 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE
                                principalMetodos.guardarRegistroAccess(winPrincipal);
                                //HACEMOS EL REGISTRO DE LA ACTIVIDAD 
                                regActividad[0] = PrincipalControl.usuario;
                                regActividad[1] = fechaRegAct;
                                regActividad[2] = linea;
                                regActividad[3] = "Turno";
                                regActividad[4] = "Registro";
                                regActividad[5] = winPrincipal.getLblTurno().getText();

                                try {
                                    new ActivityLog().resgistroActividad(regActividad);
                                } catch (Exception ex) {
                                    Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else {
                                JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                            }
                        } else {
                            JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                        }
                    break;
                    
                    case "Cerrar Turno":
                        if (privilegio != 0){ 
                            if(privilegio > 1 ) { //SI ES MAYOR A TEAM LIDER ENTONCES LA ABRE                        
                                principalMetodos.actualizarRegistroFechaAccess(winPrincipal);
                                
                                //HACEMOS EL REGISTRO DE LA ACTIVIDAD 
                                regActividad[0] = PrincipalControl.usuario;
                                regActividad[1] = fechaRegAct;
                                regActividad[2] = linea;
                                regActividad[3] = "Turno";
                                regActividad[4] = "Actualizacion";
                                regActividad[5] = winPrincipal.getLblTurno().getText();

                                try {
                                    new ActivityLog().resgistroActividad(regActividad);
                                } catch (Exception ex) {
                                    Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                                }                                
                            } else {
                                JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                            }
                        } else {
                            JOptionPane.showMessageDialog(winPrincipal, "Ops! \n Probablemente no se tiene los permisos de usuario necesarios para esta acción");
                        }
                    break;
                }
                break;
                
            case "_btnCancelar":
                String[] options = {"SI", "NO"};
                winPrincipal.getMniEliminar().setVisible(true);                
                if ( bnEdicion == 0 ) {                    
                    if (!winPrincipal.getLblTurno().getText().isEmpty()) {
                        if (!winPrincipal.getLblTurno().getText().contains("Visualización")) {
                            System.out.println("neta en !winPrincipal.getLblTurno().getText().isEmpty()");
                            try {
                                //new PrincipalMetodos().eliminarTurnoVacio(winPrincipal);
                                //new PrincipalMetodos().eliminarRegistroTiempoVacio(winPrincipal);
                                //new BitacoraDAOImpl().borrarRegistroHourly(winPrincipal.getCmbLinea().getSelectedItem().toString());
                            } catch (Exception ex) {
                                Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                    
                    principalMetodos.cancelarEdicion(winPrincipal);
                    bnEdicion = 0;
                    winPrincipal.getTblBitacora().setEnabled(true);  
                }else if (bnEdicion == 1 ){
                    int cancelar = JOptionPane.showOptionDialog(null,"¿Realmente desea cancelar el registro de informacion?."+ "\n Se eliminaran todos los registros ", "Peticion", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                    if (cancelar == 0){
                        bnEdicion = 0;
                        winPrincipal.getLblTurno().setForeground(Color.black);
                        principalMetodos.contadorFila = 0;
                        principalMetodos.cancelarEdicion(winPrincipal);
                    }  
                    winPrincipal.getTblBitacora().setEnabled(true);                   
                } else {
                    int cancelar = JOptionPane.showOptionDialog(null,"¿Realmente desea cancelar la edicion de informacion?."+ "\n ", "Peticion", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                    if (cancelar == 0){
                        bnEdicion = 0;
                        winPrincipal.getLblTurno().setForeground(Color.black);
                        principalMetodos.contadorFila = 0;
                        principalMetodos.cancelarEdicion(winPrincipal);
                    }  
                    winPrincipal.getTblBitacora().setEnabled(true);      
                }
                break;
                
            case "_mniOperaciones":
                linea = winPrincipal.getCmbLinea().getSelectedItem().toString();
                usuario = winPrincipal.getLblUsuario().getText();
                    new Operaciones(winOperaciones, true).setVisible(true);  
                break;
                
            case "cmbParoPeriodo":
                new ParoRango(winPrincipal, true).setVisible(true);
                break;
        }
        if (evt.getSource().equals(winPrincipal.getDteFecha())) {
            linea = winPrincipal.getCmbLinea().getSelectedItem().toString();
            fecha = winPrincipal.getDteFecha().getText();
            switch (bnEdicion) {
                case 0:
                    principalMetodos.consultarBitacoraPorDia(winPrincipal);
                    break;
                case 2:
                    new SelecTurno(winPrincipal, true).setVisible(true);
                    break;
                default:
                    try {
                        int registros = new BitacoraDAOImpl().edicionPorDia(fecha, linea);
                        if(registros == 0 ){
                            winPrincipal.getLblTurno().setText("  ");
                            new TiempoTurno(winPrincipal, true).setVisible(true);
                        } else {
                            winPrincipal.getLblTurno().setText("  ");
                            new SelecTurno(winPrincipal, true).setVisible(true);
                            winPrincipal.getDteFecha().setEditable(false);
                            winPrincipal.getDteFecha().setEnabled(false);
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(PrincipalControl.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                    break;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        PrincipalValidaciones.validarKeyTyped(winPrincipal, ke);
    }

    @Override
    public void keyPressed(KeyEvent ke) {}

    @Override
    public void keyReleased(KeyEvent ke) {
        PrincipalValidaciones.validarKeyReleased(winPrincipal, ke);
    }

    @Override
    public void caretUpdate(CaretEvent e) {
        PrincipalValidaciones.validarCaretUpdate(winPrincipal, e);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        PrincipalValidaciones.validarItemStateChanged(winPrincipal, e);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        PrincipalValidaciones.validarTableModelListener(winPrincipal, e);
    }
}
