package modelo;

/**
 * Hecho con <3 por:
 * @author GarciHard
 * @boschUsr GJA5TL
 */
public class UsuarioDTO {
    
    private String usuario;
    private String password;
    private String puesto;
    private int privilegio;
    private String linea;
    private String nombre;

    public UsuarioDTO(String usuario, String password, String puesto, int privilegio, String linea, String nombre) {
        this.usuario = usuario;
        this.password = password;
        this.puesto = puesto;
        this.privilegio = privilegio;
        this.linea = linea;
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public String getPuesto() {
        return puesto;
    }

    public int getPrivilegio() {
        return privilegio;
    }   

    public String getLinea() {
        return linea;
    }

    public String getNombre() {
        return nombre;
    }
    
    
    
}