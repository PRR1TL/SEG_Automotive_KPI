package modelo;

import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author PRR1TL
 */
public class ModelosListasOperaciones {
    
    private static final String[] temas = {"Seleccionar", "Calidad",
        "Tecnicas", "Organizacionales", "Cambio", "Planeados"};

    public static DefaultComboBoxModel listaTema() {
        return new DefaultComboBoxModel(temas);
    }
    
    private static final String[] areaCalidad = {
        "Seleccionar", "Scrap Operador", "Scrap Maquina",
        "Scrap Planeado", "Scrap Proceso", "Scrap Proveedor ", "Rework"
    };
    
    public static DefaultComboBoxModel listaAreaCalidad() {
        return new DefaultComboBoxModel(areaCalidad);
    }
    
//    private static final String[] areaTecnicas = {
//        "Seleccionar","Mecanicos","Neumaticos/hidraulicos","Electricos","Software Computadora",
//        "Reajuste Maquina","Reset Maquina","Ajuste herramental nuevo", "Cambio de consumible/herramental no planeado",
//        "Falta de herramental","Herramental no verificado","Validacion de proceso", "Parametros erroneos",
//        "Proceso especial TEF"            
//    };
    
//    public static DefaultComboBoxModel listaAreTecnicas(){
//        return new DefaultComboBoxModel(areaTecnicas);
//    }
    
//    private static final String[] areaOrganizacionales = {
//        "Seleccionar","Logistica","Produccion"
//    };
    
//    public static DefaultComboBoxModel listaAreaOrganizacionales(){ 
//        return new DefaultComboBoxModel(areaOrganizacionales);
//    }
    
    private static final String[] areaCambio = {
        "Seleccionar","Cambio de modelo planeado","Cambio de modelo NO Planeado","Perdidas despues del cambio"  
    };
    
    public static DefaultComboBoxModel listaAreaCambio(){
        return new DefaultComboBoxModel(areaCambio);
    }
    
//    private static final String[] areaPlaneados = {
//        "Seleccionar","Planeados","Mantenimiento Planeado/TPM","Otros Paros Planeados",
//        "Worshop Planeado","Entrenamiento Planeado","Produccion de Prototipos/Muestras",
//        "Juntas de trabajo Planeado","Comedor","Activacion Fisica","Descanso","5 Ss"
//    };
//    
//    public static DefaultComboBoxModel listaAreaPlaneados(){
//        return new DefaultComboBoxModel(areaPlaneados);
//    }
    
}
